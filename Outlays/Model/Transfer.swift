//
//  Transfer.swift
//  Created by Cools on 11/6/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Transfer: Object
{
    @objc dynamic var id = 0
    @objc dynamic var nameFrom = ""
    @objc dynamic var nameTo = ""
    @objc dynamic var amountFrom = 0.0
    @objc dynamic var amountTo = 0.0
    @objc dynamic var date = Date()
    @objc dynamic var note = ""
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}
