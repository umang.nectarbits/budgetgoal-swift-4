//
//  Transaction.swift
//  Finance
//
//  Created by Cools on 6/20/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Transaction: Object {
    
    @objc dynamic var id = 0
    // Category
    @objc dynamic var categoryName = ""
    @objc dynamic var categoryImage = ""
    @objc dynamic var categoryID = ""
    // Sub Category
    @objc dynamic var subCategoryID = ""
    @objc dynamic var subCatName = ""
    @objc dynamic var subCatImage = ""
    
    @objc dynamic var amount = ""
    @objc dynamic var date = Date()
    @objc dynamic var note = ""
    
    @objc dynamic var accountId = 0
    
    // 0-expence 1-income
    @objc dynamic var setType = ""
    
    //0-nothing, 1-daily , 2-weeakly , 3-monthly, 4 =2-months, 5= 3-Months, 6= 6 Month, 7=12 Months
    @objc dynamic var repeatType = ""
    @objc dynamic var isUpdateByService = ""
    @objc dynamic var updatedDate = Date()
    
    @objc dynamic var milliseconds = 0.0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}
