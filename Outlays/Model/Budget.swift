//
//  Budget.swift
//  Created by Nectarbits on 21/07/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Budget: Object
{
    @objc dynamic var id = 0
    @objc dynamic var mainCatID = 0
    var category = List<Category>()
    @objc dynamic var budgetAmount = 0.0
    @objc dynamic var currentAmount = 0.0
    @objc dynamic var date = Date()
    @objc dynamic var cycleType = 0
    @objc dynamic var originalDate = Date()
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
}

extension Results
{
    var toArray: [Element]? {
        return self.count > 0 ? self.map { $0 } : nil
    }
}

extension RealmSwift.List
{
    var toArray: [Element]? {
        return self.count > 0 ? self.map { $0 } : nil
    }
}

