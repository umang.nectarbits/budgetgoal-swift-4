//
//  Task.swift
//  RealmTasks
//
//  Created by Hossam Ghareeb on 10/13/15.
//  Copyright © 2015 Hossam Ghareeb. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var image = ""
    @objc dynamic var categoryID = ""
    @objc dynamic var subCategoryID = ""
    @objc dynamic var categoryType = ""
    @objc dynamic var isCategory = ""
    
    @objc dynamic var displayDashboard = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
