//
//  AllExpences.swift
//  Budget Goals
//
//  Created by nectarbits on 6/20/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit

class AllExpences {

    var CategoryName : String?
    var CategoryImage : String?
    var CategoryNote: String?
    var CategoryAmount : Double?
    var accountID : Int?
    var isExtra = false
    var ExtraID : Int?
    var isLoan = false
    var LoanID : Int?
    var isBill = false
    var BillID : Int?
    var ReminderType : Int?
    var isReminder = false
    var CreatedDate : Date?
    var Updateddate : Date?
    
    init(CategoryName: String, CategoryImage:String, CategoryAmount: Double,CategoryNote: String , accountID: Int, isExtra:Bool, ExtraID:Int, isLoan:Bool, LoanID:Int, isBill:Bool, BillID:Int, ReminderType:Int, isReminder:Bool, CreatedDate:Date, Updateddate:Date ) {
        
        self.CategoryName = CategoryName
        self.CategoryImage = CategoryImage
        self.CategoryNote = CategoryNote
        self.CategoryAmount = CategoryAmount
        self.accountID = accountID
        self.isExtra = isExtra
        self.ExtraID = ExtraID
        self.isLoan = isLoan
        self.LoanID = LoanID
        self.isBill = isBill
        self.BillID = BillID
        self.ReminderType = ReminderType
        self.isReminder = isReminder
        self.CreatedDate = CreatedDate
        self.Updateddate = Updateddate
        
    }
    
}
