//
//  Bills.swift
//  Created by Cools on 10/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Goals: Object {
    
    @objc dynamic var goalid = 0
    // Category
    @objc dynamic var name = ""
    @objc dynamic var amount = 0
    @objc dynamic var createDate = Date()
    @objc dynamic var acountName = ""
    //1-daily , 2-weeakly , 3-monthly , 0-nothing
    @objc dynamic var reminderType = ""
    @objc dynamic var gAccountid = 0

    
    override static func primaryKey() -> String? {
        return "goalid"
    }
    
}
