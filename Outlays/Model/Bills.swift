//
//  Bills.swift
//  Created by Cools on 10/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Bills: Object {
    /*
    @PrimaryKey
    private int id;
    private int categoryId;
    private String categoryName;
    private int subcategoryId;
    private String subcategoryName;
    private Date dueDate;
    private long amount;
    private String billId;
    //0-monthly 1-Bi monthly 2-Quarterly 4-Half year 5-year
    private int repeatType;
    private String note;
    */
    
    
    @objc dynamic var id = 0
    // Category
    
    @objc dynamic var categoryId = ""
    @objc dynamic var categoryName = ""
    // Sub Category
    @objc dynamic var subcategoryId = ""
    @objc dynamic var subcategoryName = ""
    
    @objc dynamic var dueDate = Date()
    @objc dynamic var currentDate = Date()
    @objc dynamic var amount = ""
    
    @objc dynamic var billId = ""
    
    //0-monthly 1-Bi monthly 2-Quarterly 3-Half year 4-year
    @objc dynamic var repeatType = ""
    @objc dynamic var note = ""
    
    @objc dynamic var url = ""
    @objc dynamic var isPaid = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
