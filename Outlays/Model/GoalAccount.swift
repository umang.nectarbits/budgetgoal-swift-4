//
//  Bills.swift
//  Created by Cools on 10/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class GoalAccount: Object {
    
    @objc dynamic var gAccountid = 0
    @objc dynamic var gName = ""
    
    override static func primaryKey() -> String? {
        return "gAccountid"
    }
}

//extension Results
//{
//
//    func toArrayGoal() -> [T]
//    {
//        return self.map{$0}
//    }
//}
//
//extension RealmSwift.List
//{
//
//    func toArrayGoal() -> [T]
//    {
//        return self.map{$0}
//    }
//}

