//
//  NewBills.swift
//  Budget Goals
//
//  Created by nectarbits on 6/13/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class NewBills: Object {
    
    @objc dynamic var BillID = 0
    
    @objc dynamic var BillMainCategoryName = ""
    @objc dynamic var BillMainCategoryImage = ""
    @objc dynamic var BillMainCategoryID = ""
    
    @objc dynamic var BillSubCategoryName = ""
    @objc dynamic var BillSubCategoryImage = ""
    @objc dynamic var BillSubCategoryID = ""
    
    @objc dynamic var BillAmount = ""
    @objc dynamic var BillDate = Date()
    @objc dynamic var BillName = ""
    
    @objc dynamic var accountID = 0
    @objc dynamic var isMain = 0 ///1 = Main , 0 = subBill
    @objc dynamic var MainBillID = 0
    
    //3-monthly, 4 =2-months, 5= 3-Months, 6= 6 Month, 7=12 Months
    @objc dynamic var BillCycle = ""
    @objc dynamic var BillUpdatedDate = Date()
    
}

class BillModel {
    var BillID : Int?
    var BillMainCategoryName : String?
    var BillMainCategoryImage : String?
    var BillMainCategoryID : String?
    var BillSubCategoryName : String?
    var BillSubCategoryImage : String?
    var BillSubCategoryID : String?
    var BillAmount : String?
    var BillDate : Date?
    var BillName : String?
    var accountID : Int?
    var isMain: Int?
    var MainBillID: Int?
    var BillCycle : String?
    var BillUpdatedDate : Date?
    
    init(BillID : Int?,BillMainCategoryName : String?, BillMainCategoryImage : String?, BillMainCategoryID : String?, BillSubCategoryName : String?, BillSubCategoryImage : String?, BillSubCategoryID : String?, BillAmount : String?, BillDate : Date?, BillName : String?, accountID : Int?, isMain: Int?, MainBillID: Int?, BillCycle : String?, BillUpdatedDate : Date?) {
        self.BillID = BillID
        self.BillMainCategoryName = BillMainCategoryName
        self.BillMainCategoryImage = BillMainCategoryImage
        self.BillMainCategoryID = BillMainCategoryID
        self.BillSubCategoryName = BillSubCategoryName
        self.BillSubCategoryImage = BillSubCategoryImage
        self.BillSubCategoryID = BillSubCategoryID
        self.BillAmount = BillAmount
        self.BillDate = BillDate
        self.BillName = BillName
        self.accountID = accountID
        self.isMain = isMain
        self.MainBillID = MainBillID
        self.BillCycle = BillCycle
        self.BillUpdatedDate = BillUpdatedDate
    }
}
