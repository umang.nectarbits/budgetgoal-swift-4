//
//  Loans.swift
//  Budget Goals
//
//  Created by nectarbits on 6/11/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Loans: Object {

    @objc dynamic var loanID = 0
    
    @objc dynamic var loanMainCategoryID = ""
    @objc dynamic var loanMainCategoryName = ""
    @objc dynamic var loanMainCategoryImage = ""
    
    @objc dynamic var loanSubCategoryID = ""
    @objc dynamic var loanSubCategoryName = ""
    @objc dynamic var loanSubCategoryImage = ""
    
    @objc dynamic var accountID = 0
    
    @objc dynamic var loanAmount = 0
    @objc dynamic var loanIntrest = 0.0
    @objc dynamic var loanYear = 0
    @objc dynamic var loanMonth = 0
    @objc dynamic var loanStartDate = Date()
    @objc dynamic var loanName = ""
    @objc dynamic var loanPayOn = "" ////1-Monthly , 2-Yearly
    @objc dynamic var loanIsReminder = false
    
    @objc dynamic var TotalAmount = 0.0
    @objc dynamic var loanComplete = false
    @objc dynamic var PaidEmiCount = 0
    @objc dynamic var EmiValue = 0.0
    @objc dynamic var EmiDate = Date()
    @objc dynamic var EmiUpdatedDate = Date()
    @objc dynamic var NextEmiDate = Date()
    
    @objc dynamic var loanCreateDate = Date()
    
    override static func primaryKey() -> String? {
        return "loanID"
    }
    
}
