//
//  Bills.swift
//  Created by Cools on 10/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Contribution: Object {
    
    @objc dynamic var contributid = 0
    @objc dynamic var goalId = 0
    // Category
    @objc dynamic var amount = 0
    @objc dynamic var note = ""
    @objc dynamic var createDate = Date()
    
    override static func primaryKey() -> String? {
        return "contributid"
    }
}

//extension Results
//{
//
//    func toArrayGoal() -> [T]
//    {
//        return self.map{$0}
//    }
//}

extension Results
{
    var toArrayGoal: [Element]? {
        return self.count > 0 ? self.map { $0 } : nil
    }
}

//extension RealmSwift.List
//{
//
//    func toArrayGoal() -> [T]
//    {
//        return self.map{$0}
//    }
//}

extension RealmSwift.List
{
    var toArrayGoal: [Element]? {
        return self.count > 0 ? self.map { $0 } : nil
    }
}

