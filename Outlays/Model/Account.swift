//
//  Account.swift
//  Created by Cools on 11/6/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import Foundation
import RealmSwift

class Account: Object
{
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var remainigAmount = 0.0
    @objc dynamic var date = Date()
    @objc dynamic var isIncludeOnDashboard = true
    @objc dynamic var isDefaultAccount = false
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}

