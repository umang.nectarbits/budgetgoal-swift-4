//
//  KDCalendarHeaderView.swift
//  KDCalendar
//
//  Created by Michael Michailidis on 07/04/2015.
//  Copyright (c) 2015 Karmadust. All rights reserved.
//

import UIKit

class CalendarHeaderView: UIView {
    
    lazy var monthLabel : UILabel = {
        
        let lbl = UILabel()
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Montserrat-Regular", size: 15.0)
        lbl.frame = CGRect(x: 56, y: 0, width: 188, height: 30)
//        lbl.textColor = subHeaderColor
        lbl.textColor =  UIColor(red: 0/255.0, green: 177/255.0, blue: 221/255.0, alpha: 1.0)
        self.addSubview(lbl)
        return lbl
     }()
    
    
    lazy var dayLabelContainerView : UIView = {
        
        let v = UIView()
        
        let formatter : DateFormatter = DateFormatter()
        
        for index in 1...7 {
            
            let day : NSString = formatter.weekdaySymbols[index % 7] as NSString
            
            let weekdayLabel = UILabel()
            
            weekdayLabel.font = UIFont(name: "Montserrat-Bold", size: 13.0)
            
            weekdayLabel.text = day.substring(to: 3).uppercased()
//            weekdayLabel.textColor = subHeaderColor//UIColor(colorLiteralRed: 16.0/255.0, green: 152.0/255.0, blue: 192.0/255.0, alpha: 1)
            weekdayLabel.textColor = UIColor(red: 0/255.0, green: 177/255.0, blue: 221/255.0, alpha: 1.0)
            weekdayLabel.textAlignment = NSTextAlignment.center
            
            v.addSubview(weekdayLabel)
        }
        
        v.backgroundColor = UIColor(red: 246.0/255.0, green: 248.0/255.0, blue: 250.0/255.0, alpha: 1)
        
        self.addSubview(v)
        
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        var frm = self.bounds
        frm.origin.y += 0.0
        frm.size.height = 30.0
        
//        var bfrm = self.bounds
//        bfrm.origin.x += 30.0
//        bfrm.origin.y += 10.0
//        bfrm.size.height = 32.0
//        bfrm.size.width = 32.0
//        
//        var bfrm2 = self.bounds
//        bfrm2.origin.x += 280.0
//        bfrm2.origin.y += 10.0
//        bfrm2.size.height = 32.0
//        bfrm2.size.width = 32.0
        
        
        
        self.monthLabel.frame = frm
//        self.previousButton.frame = bfrm
//        self.nextButton.frame = bfrm2
        
        var labelFrame = CGRect(x: 0.0, y: self.bounds.size.height / 2.0, width: self.bounds.size.width / 7.0, height: self.bounds.size.height / 2.0)
        
        for lbl in self.dayLabelContainerView.subviews {
            
            lbl.frame = labelFrame
            labelFrame.origin.x += labelFrame.size.width
        }
        
        
        
    }
    
}
