//
//  Reminder.swift
//  Reminder App
//
//  Created by Mac on 25/05/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UserNotifications
import UserNotificationsUI
import UIKit

class Reminder: NSObject, NSCoding {
    
    
    //Properties
    var notification : String
    var name : String
    var time : Date
    
    //Archive paths for Persistent Data
    static let DocumentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveUrl = DocumentDirectory.appendingPathComponent("reminders")
    
    //Enum for property keys
    struct PropertyKey {
        static let nameKey = "name"
        static let timeKey = "time"
        static let notificationKey = "notification"
    }
    
    //Intializer
    init(name:String,time:Date,notification:String) {
        self.name = name
        self.time = time
        self.notification = notification
        
        super.init()
    }
    
    //Destructor
    deinit {
        //cancel notification
//        UIApplication.shared.cance
//        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    //NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.nameKey)
        aCoder.encode(time, forKey: PropertyKey.timeKey)
        aCoder.encode(notification, forKey: PropertyKey.notificationKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: PropertyKey.nameKey) as! String
        let time = aDecoder.decodeObject(forKey: PropertyKey.timeKey) as! Date
        let notification = aDecoder.decodeObject(forKey: PropertyKey.notificationKey) as! String
        
        self.init(name: name, time: time, notification: notification)
    }
}
