//
//  SLSheetCell.swift
//  SLSheetLayout
//
//  Created by Macbook on 6/29/17.
//  Copyright © 2017 Macbook. All rights reserved.
//

import UIKit

class SLSheetCell: UICollectionViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        valueLabel.text = nil
        backgroundColor = UIColor.white
        valueLabel.textColor = UIColor.black
    }
    
    func updateContent(value: String?) {
        valueLabel.text = value
    }
    
    func updateTopDockState() {
        backgroundColor = SecondColor
        valueLabel.textColor = UIColor.white
    }
    
    func updateLeftDockState() {
        backgroundColor = SecondColor
        valueLabel.textColor = UIColor.white
    }
    
    func updateCellState(indexPath: IndexPath,currentMonth: Bool) {
        
        if currentMonth == true
        {
            backgroundColor = SecondColor
            valueLabel.textColor = UIColor.white
        }
        else
        {
            if indexPath.section % 2 == 0 {
                backgroundColor = UIColor.groupTableViewBackground
            } else {
                backgroundColor = UIColor.white
            }
        }
        
        
    }

}
