//
//  DBManager.swift
//  Finance
//
//  Created by Cools on 6/13/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import RealmSwift


class DBManager: NSObject {
    
    func categoryDB()
    {
        let uiRealm = try! Realm()
        
        if let path = Bundle.main.path(forResource: "CategoryList", ofType: "plist") {
            let dictRoot = NSArray(contentsOfFile: path)
            if let dict = dictRoot {
                
                debugPrint(dict.count)
                
                for i in 0..<dict.count
                {
                    var taskListB = Category()
                    let autoCatId = uiRealm.objects(Category.self).max(ofProperty: "id") as Int?
                    
                    let dictCat = dict[i] as! NSArray
                    print(dictCat[0])
                    if(autoCatId == nil)
                    {
                        taskListB = Category(value: [1,(dictCat[0] as! NSDictionary).value(forKey: "name"),(dictCat[0] as! NSDictionary).value(forKey: "image"),(dictCat[0] as! NSDictionary).value(forKey: "categoryID"),(dictCat[0] as! NSDictionary).value(forKey: "subCategoryID"),(dictCat[0] as! NSDictionary).value(forKey: "categoryType"),(dictCat[0] as! NSDictionary).value(forKey: "isCategory"),Int((dictCat[0] as! NSDictionary).value(forKey: "displayDashboard") as! String)])
                    }
                    else{
                        
                        let catId = (uiRealm.objects(Category.self).max(ofProperty: "id") as Int?)! + 1
                        taskListB = Category(value: [catId,(dictCat[0] as! NSDictionary).value(forKey: "name"),(dictCat[0] as! NSDictionary).value(forKey: "image"),(dictCat[0] as! NSDictionary).value(forKey: "categoryID"),(dictCat[0] as! NSDictionary).value(forKey: "subCategoryID"),(dictCat[0] as! NSDictionary).value(forKey: "categoryType"),(dictCat[0] as! NSDictionary).value(forKey: "isCategory"),Int((dictCat[0] as! NSDictionary).value(forKey: "displayDashboard") as! String)])
                    }
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                }
            }
            
            var taskListB = Account()
            taskListB = Account(value: [1,"My Account",0.0,0.0,Date(),true,true])
            try! uiRealm.write { () -> Void in
                uiRealm.add(taskListB)
            }
            taskListB = Account(value: [2,"Saving",0.0,0.0,Date(),false,false])
            try! uiRealm.write { () -> Void in
                uiRealm.add(taskListB)
            }
            taskListB = Account(value: [3,"Salary",0.0,0.0,Date(),false,false])

            try! uiRealm.write { () -> Void in
                uiRealm.add(taskListB)
            }
        }
    }
    /*
    func subCategoryDB()
    {
        let uiRealm = try! Realm()
        
        if let path = Bundle.main.path(forResource: "SubCategoryList", ofType: "plist") {
            let dictRoot = NSArray(contentsOfFile: path)
            if let dict = dictRoot {
                debugPrint(dict.count)
                
                for i in 0..<dict.count
                {
                    var taskListB = SubCategory()
                    let autoCatId = uiRealm.objects(SubCategory.self).max(ofProperty: "id") as Int?
                    
                    let dictCat = dict[i] as! NSArray
                    print(dictCat[0])
                    if(autoCatId == nil)
                    {
                        taskListB = SubCategory(value: [1,(dictCat[0] as! NSDictionary).value(forKey: "name"),(dictCat[0] as! NSDictionary).value(forKey: "Image"),(dictCat[0] as! NSDictionary).value(forKey: "categoryId"),(dictCat[0] as! NSDictionary).value(forKey: "subCategoryType")])
                    }
                    else{
                        
                        let catId = (uiRealm.objects(SubCategory.self).max(ofProperty: "id") as Int?)! + 1
                        taskListB = SubCategory(value: [catId,(dictCat[0] as! NSDictionary).value(forKey: "name"),(dictCat[0] as! NSDictionary).value(forKey: "Image"),(dictCat[0] as! NSDictionary).value(forKey: "categoryId"),(dictCat[0] as! NSDictionary).value(forKey: "subCategoryType")])
                    }
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                }
            }
        }
    } */

}
