//
//  Constant.swift
//  Finance
//
//  Created by Cools on 6/19/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Foundation

let ONE_MONTH_SUBSCRIPTION = "com.budgetgoals.1month"
let THREE_MONTH_SUBSCRIPTION = "com.budgetgoals.3month"
let ONE_YEAR_SUBSCRIPTION = "com.budgetgoals.1year"

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let headerFont = UIFont(name: ".SFUIDisplay-Bold", size: 27)
let headerTitleFont = UIFont(name: ".SFUIDisplay-Bold", size: 23)
let tabBarFont = UIFont(name: ".SFUIDisplay", size: 11)
let tableHeaderFont = UIFont(name: ".SFUIDisplay-Medium", size: 15)
let HeaderFont_Bold_15 = UIFont(name: ".SFUIDisplay-Bold", size: 15)
let TopHeaderFont = UIFont(name: ".SFUIDisplay-Medium", size: 17)
let tableMainFont = UIFont(name: ".SFUIDisplay-Semibold", size: 15)
let tableSubFont = UIFont(name: ".SFUIDisplay", size: 13)
let sheetSubFont = UIFont(name: ".SFUIDisplay", size: 12)

let DashboardTableLabelTitle = UIFont(name: ".SFUIDisplay-Bold", size: 20)
let DashboardAmount = UIFont(name: ".SFUIDisplay-Medium", size: 24)
let DashboardSubLabel = UIFont(name: ".SFUIDisplay-Medium", size: 15)
let DashboardSubLabel_17 = UIFont(name: ".SFUIDisplay-Medium", size: 17)
let DashboardSubAmont = UIFont(name: ".SFUIDisplay-Regular", size: 18)

let verySmallFont = UIFont(name: ".SFUIDisplay-Medium", size: 11)
let smallFont = UIFont(name: ".SFUIDisplay-Medium", size: 13)
let mediumFont = UIFont(name: ".SFUIDisplay-Medium", size: 15)
let mediumBiggerFont = UIFont(name: ".SFUIDisplay-Medium", size: 17)

let tableHeaderAmountFont = UIFont(name: ".SFUIDisplay", size: 18)
let tblMainTitleFont = UIFont(name: ".SFUIDisplay-Medium", size: 20)

let timelineImage = UIImage(named: "ic_timeline.png")
let overviewImage = UIImage(named: "ic_overview.png")
let addTransactionImage = UIImage(named: "ic_add.png")
let settingImage = UIImage(named: "ic_settings.png")

let timelineHightlitedImage = UIImage(named: "ic_timeline_highlighted.png")
let overviewHightlitedImage = UIImage(named: "ic_overview_highlighted.png")
let addTransactionHightlitedImage = UIImage(named: "ic_add_highlighted.png")
let settingHightlitedImage = UIImage(named: "ic_settings_highlighted.png")

let FirstColor = UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1)
let SecondColor = UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1)

let BillCategoryID = "18"
let LoanCategoryID = "19"
let dailySavingCategoryID = "20"

let HeaderTitleColor = UIColor.white
let MenuHeadersFont = UIFont(name: "HelveticaNeue-Bold", size: 25)
let InnerHeadersFont = UIFont(name: "HelveticaNeue-Bold", size: 20)

struct AlertString
{
    static let CurrencyConverter = "No Internet Connection."
    static let ExportDataView = "There Is No Data To Export."
    static let ExportDataSelectmonth = "Please Select Proper Month Order."
    static let AddAmount = "Please Enter Amount."
    static let AddGoal = "Please Add Goal."
    static let SucessFullyUpdate = "Sucessfully Update Data."
    static let SucessAdd = "Sucessfully Add Goal."
    static let SucessContribute = "Sucessfully Add Contribution."
    static let AccountName = "Please Enter Account Name."
    static let NotChangeCategory = "You Can Not Change Category."
    static let SelectCategory = "Please Select Category."
    static let SelectIcon = "Please Select Icon Image."
    static let CategoryName = "Please Enter Category Name."
    static let ProperAmount = "Please Enter Proper Amount."
    static let BudgetAmount = "Please Enter Budget Amount."
    static let GoalPrice = "Please Enter Goal Price."
    static let GoalName = "Please Enter Goal."
    static let GoalReminder = "Please Select Reminders."
    static let SelectCycle = "Please Select Budget Cycle."
    static let ProperMont = "Please Select Proper Month Order."
    static let EditingMode = "You Can Not Split Amount In Editing Mode."
    static let PastDate = "You Could Not Select Past Date For Transaction..!"
    static let DataExport = "There Is No Data To Export."
    static let Purchase = "You Have Already Purchase..!"
    static let PrincipalAmount = "Please Enter  Principal Amount."
    static let EnterInterest = "Please Enter Interest."
    static let EnterDownPayment = "Please Enter Down Payment."
    static let EnterYear = "Please Enter Year."
    static let SelectDate = "Please Select Date"
    static let PrincipalHigh = "Please Enter Principal Amount Higher Than Down Payment Amount."
    static let LessAmount = "Please Enter Amount Less Then Current Amount"
    static let SplitAmount = "All Amount Splited So Cant Add Another Amount."
    static let AmountCategory = "Please Enter Amount And Category."
    static let SplitValue = "Please Split Your Amount."
    static let GreaterZero = "Please Enter Value Greater Then Zero."
    static let EnterAccount = "Please Enter Second Account"
}

struct Constant
{
//    App ID: ca-app-pub-1183954182004239~3836926671
//    Ad unit ID: ca-app-pub-1183954182004239/8988470569
    static let banner_admob_id = "ca-app-pub-3940256099942544/4411468910"
    static let admob_app_id = "ca-app-pub-3258200689610307~1325662679"
    static let interstitial_admob_id = "ca-app-pub-3940256099942544/4411468910"
    
    static let categoryImage = [["name":"ic_cat_restorent.png","isCell":false],["name":"ic_cat_car.png","isCell":false],["name":"ic_investment.png","isCell":false],["name":"ic_rental_income.png","isCell":false],["name":"ic_food.png","isCell":false],["name":"ic_charities.png","isCell":false],["name":"ic_property_taxts.png","isCell":false],["name":"ic_internet.png","isCell":false],["name":"ic_clothing.png","isCell":false],["name":"ic_repair.png","isCell":false],["name":"ic_fuel.png","isCell":false],["name":"ic_insurance.png","isCell":false],["name":"ic_laundry.png","isCell":false],["name":"ic_tools.png","isCell":false],["name":"ic_rent.png","isCell":false],["name":"ic_maintance.png","isCell":false],["name":"ic_cat_electricity.png","isCell":false],["name":"ic_transport.png","isCell":false]]
    
    //Header Color & Header Font Size
    static let headerColorCode = UIColor.white
    static let headerFontColor = UIColor.black
    static let tempheaderFontColor = UIColor.white
    
    //Tab bar Color & Tab Bar Font Size
    static let tabBarColorCode = UIColor.white
    static let normalColorCode = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1)
    static let highlightedColorCode = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1)

    //Color Code :  Green : (Income)00B300(0,179,0)  Red : (Expense)FF0000(255,0,0)
    static let incomeColorCode = UIColor(red: 90.0/255.0, green: 204.0/255.0, blue: 140.0/255.0, alpha: 1)
    static let expenseColorCode = UIColor(red: 234.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1)
    
    //Tableview Text Color
//    static let tableTextColorCode = UIColor(colorLiteralRed: 122.0/255.0, green: 137.0/255.0, blue: 142.0/255.0, alpha: 1)
    static let tableTextColorCode = UIColor.black
    static let tableSubTextColorCode = UIColor(red: 160.0/255.0, green: 169.0/255.0, blue: 178.0/255.0, alpha: 1)
    
    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    static func changeDate(_ mydate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let convertedDate = dateFormatter.date(from: mydate)
        return convertedDate!
    }
    
    static func getMonthYear(_ mydate:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM-yyyy"
        let convertedDate = dateFormatter.string(from: mydate)
        return convertedDate
    }
    
    static func getStringToDate(_ mydate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM-yyyy"
        let convertedDate = dateFormatter.date(from: mydate)
        return convertedDate!
    }
    
    static func convertDate(_ mydate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM d, yyyy"
        let convertedDate = dateFormatter.date(from: mydate)
        return convertedDate!
    }
    
    static func convertEMIDate(_ mydate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
//        dateFormatter.dateStyle = DateFormatter.Style.long
//        dateFormatter.dateFormat = "dd-MM-yyyy"
        let convertedDate = dateFormatter.date(from: mydate)
        return convertedDate!
    }
        
    static func getNextMonthDate(mydate:Date) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM d, yyyy"
        let date = Calendar.current.date(byAdding: .month, value: 1, to: mydate)
        return date!
    }
    
    static func getPriviousMonthDate(mydate:Date) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM d, yyyy"
        let date = Calendar.current.date(byAdding: .month, value: -1, to: mydate)
        return date!
    }
    
    static func dayDifferenceBetweenTwoDate(date1 : Date, date2 : Date) -> Int
    {
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        return components.day!
    }
    
    static func getListOfMonth(_ date:Date) -> NSMutableArray{
        let year = Calendar.current.component(.year, from:date)
        let monthList = NSMutableArray()
        let cal = Calendar.current
        var comps = DateComponents()
        comps.month = 1
        comps.year = year
        comps.day = 0
        var date1: Date = cal.date(from: comps)!
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM-yyyy"
        
        var comps2 = DateComponents()
        comps2.month = 1
        for i in 0..<12
        {
            date1 = cal.date(byAdding: comps2, to: date1)!
            print(date1)
            let dict = NSMutableDictionary()
            dict.setValue(date1, forKey: "Date")
            dict.setValue(formatter.string(from: date1), forKey: "Month")
            monthList.add(dict)
        }
        
        return monthList
    }
    static func getMonthListBetween2Date(_ startDate2 : Date, endDate2 : Date)-> NSMutableArray {
        
        let dateFormtter = DateFormatter()
        dateFormtter.dateFormat = "MM/dd/yyyy"
        
        let startDate1 = dateFormtter.string(from: startDate2)
        let endDate1 = dateFormtter.string(from: endDate2)
        
        let monthList = NSMutableArray()
        

        let startDate = dateFormtter.date(from: startDate1)
        let endDate = dateFormtter.date(from: endDate1)
        
        var monthsStringArray = [String]()
        var monthsIntArray = [Int]()
        var monthsWithyear = [String]()
        var dateArray = Date()
        dateFormtter.dateFormat = "MM"
        
        if let startYear: Int = startDate?.year(), let endYear = endDate?.year() {
            
            if let startMonth: Int = startDate?.month(), let endMonth: Int = endDate?.month() {
                for i in startYear...endYear {
                    for j in (i == startYear ? startMonth : 1)...(i < endYear ? 12 : endMonth) {
                        let monthTitle = dateFormtter.monthSymbols[j - 1]
                        let tempMonth = dateFormtter.shortMonthSymbols[j-1]
                        monthsStringArray.append(monthTitle)
                        monthsIntArray.append(j)
                        let cal = Calendar.current
                        var comps = DateComponents()
                        comps.month = j+1
                        comps.year = i
                        comps.day = 0
                        var date1: Date = cal.date(from: comps)!
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MMM-yyyy"
                        let dict = NSMutableDictionary()
                        dict.setValue(date1, forKey: "Date")
                        dict.setValue(formatter.string(from: date1), forKey: "Month")
                        monthList.add(dict)
                        let monthWithYear = "\(tempMonth)-\(i)"
                        monthsWithyear.append(monthWithYear)
                    }
                }
            }
            
        }
        print(monthsStringArray)
        print(monthsIntArray)
        print(monthsWithyear)
        print(monthList)
        return monthList
    }
    
    static func TO_STRING(_ obj : Any?) -> String {
        if obj == nil {
            return ""
        }
        else if obj is String {
            return (obj as! String)
        }
        else if obj is NSString {
            return obj as! String
        }
        return "\(obj!)"
    }
    
    static func TO_INT(_ obj : Any?) -> Int {
        if obj == nil {
            return 0
        }
        else if let answer = obj as? Int {
            return answer
        }
        else if let answer = obj as? NSNumber {
            return answer.intValue
        }
        else if let answer = obj as? String {
            return (answer as NSString).integerValue
        }
        return 0
    }
    
    static func TO_INTMax(_ obj : Any?) -> Int64 {
        if obj == nil {
            return 0
        }
        else if let answer = obj as? Int {
            return Int64(answer)
        }
        else if let answer = obj as? NSNumber {
            return Int64(answer.int64Value)
        }
        else if let answer = obj as? String {
            return Int64((answer as NSString).integerValue)
        }
        return 0
    }
    
    
    static func TO_DOUBLE(_ obj : Any?) -> Double {
        if obj == nil {
            return 0.0
        }
        else if let answer = obj as? Double {
            return answer
        }
        else if let answer = obj as? NSNumber {
            return Double(answer.doubleValue)
        }
        else if let answer = obj as? String {
            return (answer as NSString).doubleValue
        }
        return 0.0
    }
    
    static func TO_FLOAT(_ obj : Any?) -> Float {
        if obj == nil {
            return 0.0
        }
        else if let answer = obj as? Float {
            return answer
        }
        else if let answer = obj as? NSNumber {
            return answer.floatValue
        }
        else if let answer = obj as? String {
            return (answer as NSString).floatValue
        }
        return 0.0
    }
    
    static func TO_BOOL(_ obj : Any?) -> Bool {
        if obj == nil {
            return false
        }
        else if let answer = obj as? Int, answer == 1 {
            return true
        }
        else if let answer = obj as? NSNumber, answer == 1 {
            return true
        }
        
        return false
    }
    
}

struct SectionTransaction
{
    //    setType
    //    repeatType
    var amount: String!
    var date: String!
    var items: NSMutableArray!
    var collapsed: Bool!
    
    init(amount: String,date: String, items: NSMutableArray, collapsed: Bool = false)
    {
        self.amount = amount
        self.date = date
        self.items = items
        self.collapsed = collapsed
    }
}

struct SectionCategory
{
    var amount: String!
    var categoryID: String!
    var categoryName: String!
    var categoryImage: String!
    var items: NSMutableArray!
    var collapsed: Bool!
    
    init(amount: String,categoryID: String!,categoryName: String!,categoryImage: String!, items: NSMutableArray, collapsed: Bool = false)
    {
        self.amount = amount
        self.categoryID = categoryID
        self.categoryName = categoryName
        self.categoryImage = categoryImage
        self.items = items
        self.collapsed = collapsed
    }
}

extension Date {
    
    func GetCurrentMonth(date: Date) -> String{
        let now = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func month() -> Int {
        let month = Calendar.current.component(.month, from: self)
        return month
    }
    
    func year() -> Int {
        let year = Calendar.current.component(.year, from: self)
        return year
    }
    
    static func convertStringToDateWithUTC(strDate:String, dateFormate strFormate:String) -> Date{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = strFormate
        dateFormate.timeZone = TimeZone.init(abbreviation: "UTC")
        let dateResult:Date = dateFormate.date(from: strDate)!
        
        return dateResult
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIImage {
    
    func maskWithColor( color:UIColor) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        color.setFill()
        
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        let rect = CGRect(x: 0.0, y: 0.0, width: self.size.width, height: self.size.height)
        context.draw(self.cgImage!, in: rect)
        
        context.setBlendMode(CGBlendMode.sourceIn)
        context.addRect(rect)
        context.drawPath(using: CGPathDrawingMode.fill)
        
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return coloredImage!
    }
}

//MARK: - View shadow
extension UIView {
    
    func dropShadow(scale: Bool = true) {
        
//        self.layer.cornerRadius = 8
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowOpacity = 3
//        self.layer.shadowOffset = CGSize.zero
//        self.layer.shadowRadius = 5
//        self.layer.masksToBounds = false
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 8
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 2
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 5
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func viewShadow(scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 4
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1.5
    }    
    
    func setGradientColor(color1:UIColor,color2:UIColor)
    {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.frame.size.height)
        gradient.colors = [color1.cgColor,color2.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)

        self.layer.insertSublayer(gradient, at: 1)
    }
}

extension UICollectionView {
    func collectionShadow(scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 4
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1.5
    }
}

//MARK: - Rounds the double to decimal places value
extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


//MARK: - Keyboard with Done button when click on textfield
extension UITextField{
    
        func addDoneButtonToKeyboard(myAction:Selector?){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: myAction)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
            
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
}

//MARK: - Dateformmated To Retrurn into String
extension String {
    
    static func convertFormatOfDate(date: Date) -> String! {
        
        
        // Destination format :
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.timeStyle = .none
        dateDestinationFormat.dateStyle = .medium

        // Convert new NSDate created above to String with the good format
        let dateFormated = dateDestinationFormat.string(from: date)
        
        return dateFormated
    }
    
    static func convertFormatOfDateToStringSpecificFormat(date: Date, format:String) -> String! {
        
        // Destination format :
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.timeStyle = .none
        dateDestinationFormat.dateStyle = .medium
        dateDestinationFormat.dateFormat = format
        
        // Convert new NSDate created above to String with the good format
        let dateFormated = dateDestinationFormat.string(from: date)
        
        return dateFormated
        
    }
}

extension String {
    
    static func convertFormatOfStringToDate(date: String) -> Date! {
        
        
        // Destination format :
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.timeStyle = .none
        dateDestinationFormat.dateStyle = .medium
        
        // Convert new NSDate created above to String with the good format
        let dateFormated = dateDestinationFormat.date(from: date)
        
        return dateFormated
        
    }
    
    static func getMonthDiffrenceBetweenTwoDate(startDate: Date, endDate: Date) -> Int
    {
        
        let dateDestinationFormat = DateComponentsFormatter()
        dateDestinationFormat.unitsStyle = .full
        dateDestinationFormat.allowedUnits = [.month]
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.month]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: startDate, to: endDate);
        
        
        let string = dateDestinationFormat.string(from: startDate, to: endDate)!
        
        print(string)
        return difference.month!
    }
    
    static func getMonthWiseDate(mydate:Date, Days:Int) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM d, yyyy"
        let date = Calendar.current.date(byAdding: .month, value: Days, to: mydate)
        return date!
    }
    
    static func getDayWiseDate(mydate:Date, Days:Int) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM d, yyyy"
        let date = Calendar.current.date(byAdding: .day, value: Days, to: mydate)
        return date!
    }
}

extension String {
    
    static func sum(array : NSArray)->String! {
        //Sum of Array
        var sum : Double = 0.0
        for num in array{
            sum += Double(num as! String)!
        }
        return String(sum)
    }
}

extension UIViewController {
    func alertView(alertMessage : String)->Void{
        //UIAlertView using Swift
        let alert = UIAlertController(title: "", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

//extension Date {
//    func startOfMonth() -> Date? {
////        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: self)
//        var comp: DateComponents = Calendar.current.dateComponents([.day, .month, .year], from: Calendar.current.startOfDay(for: self))
////        comp.month = 1
//        comp.day = 1
//        
//        return Calendar.current.date(from: comp)
//    }
//    
//    func endOfMonth() -> Date? {
//        var comp: DateComponents = Calendar.current.dateComponents([.month, .day, .hour], from: Calendar.current.startOfDay(for: self))
//        comp.month = 1
//        comp.day = -1
//        return Calendar.current.date(byAdding: comp, to: self.startOfMonth()!)
//    }
//}

extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
}

extension Date {
    
    func startOfMonth() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        //        var unitFlags = [.year, .month]
        var comps: DateComponents? = calendar.dateComponents([.year, .month], from: self)

        let firstDateOfMonth = returnDate(forMonth: (comps?.month)!, year: (comps?.year)!, day: 1)
        return firstDateOfMonth
    }
    
    func returnDate(forMonth month: Int, year: Int, day: Int) -> Date {
        var components = DateComponents()
        components.day = day
        components.month = month
        components.year = year
        let gregorian = Calendar(identifier: .gregorian)
        return gregorian.date(from: components)!
    }
    
    func endOfMonth() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        //var unitFlags = [.year, .month]
        var comps: DateComponents? = calendar.dateComponents([.year, .month], from: self)

        let lastDateOfMonth = returnDate(forMonth: (comps?.month)! + 1, year: (comps?.year)!, day: 1)
        
        return lastDateOfMonth
    }
    
    func convertFormatOfDateToStringNoFormat(date: String) -> Date! {
        
        
        // Destination format :
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.timeStyle = .none
        dateDestinationFormat.dateStyle = .medium
        
        // Convert new NSDate created above to String with the good format
        let dateFormated = dateDestinationFormat.date(from: date)
        
        return dateFormated
        
    }
    
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
}

extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }}

