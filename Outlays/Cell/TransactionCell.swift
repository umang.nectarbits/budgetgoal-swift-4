//
//  TransactionCell.swift
//  Finance
//
//  Created by Cools on 6/26/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet weak var img_subCategory : UIImageView!
    @IBOutlet weak var lblSubCategoryName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblAmount : UICountingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
