//
//  CategoryCell.swift
//  Finance
//
//  Created by Cools on 6/13/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet var lblCategoryTitle: UILabel!
    @IBOutlet var btnAddSubCategory: UIButton!
    @IBOutlet var img_Category: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnAddSubCategory.setImage(UIImage(named: "ic_add.png"), for: .normal)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
