//
//  CategoryCollectionViewCell.swift
//  Created by harshesh on 28/10/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgvBudget: UIImageView!
    @IBOutlet var lblCatName: UILabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var categoryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        bgView.layer.cornerRadius = 35
        //        bgView.layer.masksToBounds = true
    }

    
}
