//
//  BillSubCategoryCell.swift
//  Created by harshesh on 26/10/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class BillSubCategoryCell: UITableViewCell {
    
    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var imgTitle: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
