//
//  SettingCell.swift
//  Finance
//
//  Created by Nectarbits on 12/06/17.
//  Copyright © 2017 nectarbits. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var switchController : UISwitch!
    @IBOutlet weak var titlelbl : UILabel?
    @IBOutlet weak var lblCurrency : UILabel!
    @IBOutlet weak var img_setting: UIImageView!
    @IBOutlet weak var lblReminderTime : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
