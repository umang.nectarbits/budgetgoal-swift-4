//
//  BudgetHistoryCell.swift
//  Created by Nectarbits on 24/07/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class BudgetHistoryCell: UITableViewCell
{
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgv : UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
 
    }
}
