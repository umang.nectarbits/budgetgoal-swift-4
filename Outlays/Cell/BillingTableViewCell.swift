//
//  BillingTableViewCell.swift
//  BillingApp
//
//  Created by harshesh  on 12/10/17.
//  Copyright © 2017 harshesh . All rights reserved.
//

import UIKit

class BillingTableViewCell: UITableViewCell {
    
    @IBOutlet var img_type: UIImageView!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblItems: UILabel!
    @IBOutlet var lblDescribtion: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblReminder: UILabel!
    @IBOutlet  var HeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnPayNow: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var lblpaidTitle: UILabel!
    
    
    var isExpanded:Bool = false
    {
        didSet
        {
            if !isExpanded {
                self.HeightConstraint.constant = 0.0
//                btnPayNow.isHidden = true
                btnPayNow.setTitle("", for: .normal)
                
            } else {
                self.HeightConstraint.constant = 50.0
//                btnPayNow.isHidden = false
                btnPayNow.setTitle("Pay Now", for: .normal)
            }
        }
    }
    
    var isPaid : Bool = false
    {
        didSet
        {
            if !isPaid {
                btnPayNow.isHidden = false
                
            } else {
                btnPayNow.isHidden = true
            }
        }
    }
    
}
