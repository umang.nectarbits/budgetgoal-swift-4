//
//  BudgetViewCell.swift
//  Created by Nectarbits on 21/07/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class BudgetViewCell: UITableViewCell
{
    
    @IBOutlet weak var imgv : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblTotalPrice : UILabel!
    @IBOutlet weak var lblRemainPrice : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblRemainingAmount : UILabel!
    
    @IBOutlet var viewTotal: UIView!
    
    @IBOutlet var viewRemain: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
