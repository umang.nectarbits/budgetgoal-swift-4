//
//  SubCategoryCell.swift
//  Finance
//
//  Created by Cools on 6/13/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class SubCategoryCell: UITableViewCell {

    @IBOutlet var lblSubCategoryTitle: UILabel!
    @IBOutlet var btnDeleteSubCategory: UIButton!
    @IBOutlet var img_SubCategory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnDeleteSubCategory.setImage(UIImage(named: "ic_category_delete.png"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
