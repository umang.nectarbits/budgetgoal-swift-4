//
//  SplitAddToListCell.swift
//  Finance
//
//  Created by Cools on 6/23/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class SplitAddToListCell: UITableViewCell {
    
    @IBOutlet weak var cardView : UIView!
    @IBOutlet weak var lblamount : UICountingLabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblCategoryName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var img_category : UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
