//
//  cardCell.swift
//  Finance
//
//  Created by Cools on 6/27/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class cardCell: UICollectionViewCell {
    
    @IBOutlet weak var lblMonth : UILabel!
    @IBOutlet weak var lblAccountValue : UICountingLabel!
    @IBOutlet weak var lblIncomeValue : UICountingLabel!
    @IBOutlet weak var lblExpenceValue : UICountingLabel!
    @IBOutlet weak var viewCard : UIView!
    
}
