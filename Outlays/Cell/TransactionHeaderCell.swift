//
//  TransactionHeaderCell.swift
//  Finance
//
//  Created by Cools on 6/26/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class TransactionHeaderCell: UITableViewCell {
    
    @IBOutlet weak var viewByDate : UIView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTotalAmount : UICountingLabel!
    @IBOutlet weak var lblCategoryTotalAmount : UICountingLabel!
    @IBOutlet weak var lblCategoryName : UILabel!
    @IBOutlet weak var img_Category : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
