//
//  AccountCell.swift
//  Created by Cools on 11/14/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {
    
    @IBOutlet weak var lblAccountCount : UILabel!
    @IBOutlet weak var lblAccountCountTitle : UILabel!
    @IBOutlet weak var lblCreditsAmount : UILabel!
    @IBOutlet weak var lblDebitsAmount : UILabel!
    @IBOutlet weak var lblCreditsTitle : UILabel!
    @IBOutlet weak var lblDebitsTitle : UILabel!
    @IBOutlet weak var lblAccountTitle : UILabel!
    @IBOutlet var bgimageBudget: UIImageView!
    @IBOutlet weak var viewBG : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblAccountTitle.font = tblMainTitleFont
        lblCreditsTitle.font = TopHeaderFont
        lblDebitsTitle.font = TopHeaderFont
        lblAccountCountTitle.font = TopHeaderFont
        lblCreditsAmount.font = tblMainTitleFont
        lblDebitsAmount.font = tblMainTitleFont
        lblAccountCount.font = tblMainTitleFont
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
