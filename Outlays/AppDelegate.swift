//
//  AppDelegate.swift
//  Outlays
//
//  Created by Cools on 10/4/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import RealmSwift
import UserNotifications
import GoogleMobileAds
import IQKeyboardManagerSwift
import StoreKit
import SVProgressHUD

let kConstantObj = kConstant()

let uiRealm = try! Realm()
var AiReport: Bool = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SKPaymentTransactionObserver {

    var window: UIWindow?
    var bannerView: GADBannerView!
    
    lazy var passcodeLockPresenter: PasscodeLockPresenter =
        {
            
            let configuration = PasscodeLockConfiguration()
            let presenter = PasscodeLockPresenter(mainWindow: self.window, configuration: configuration)
            
            return presenter
    }()

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        
        IQKeyboardManager.sharedManager().enable = true

        Appirater.setAppId("1257754660");
        Appirater.setTimeBeforeReminding(7);
        
        Appirater.setDaysUntilPrompt(1);
        Appirater.setUsesUntilPrompt(3);
//        Appirater.setSignificantEventsUntilPrompt(3);
//        Appirater.setDebug(true);
        Appirater.appLaunched(true);
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainAccountListVC")
        
        let mainVcIntial = kConstantObj.SetIntialMainViewController("DashboardViewController")
        self.window?.rootViewController = mainVcIntial
        passcodeLockPresenter.presentPasscodeLock()
        
//        GADMobileAds.configure(withApplicationID: "ca-app-pub-3258200689610307~1325662679")
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.configure(withApplicationID: Constant.admob_app_id)
        
        //In App Purchase 
        SKPaymentQueue.default().add(self)
        
        self.receiptValidation { (status) in
            if status {
                print("Yes")
            }
        }
        
        if !Constant.isKeyPresentInUserDefaults(key: "ReminderSwitch") {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            } else {
                // Fallback on earlier versions
            }
        }
        
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 5,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    print("oldSchemaVersion",oldSchemaVersion)
                }
                
                if oldSchemaVersion < 2 {
                    migration.enumerateObjects(ofType: Transaction.className()) { (_, newPerson) in
                        
                    }
                }
                
                if oldSchemaVersion < 3 {
                    migration.enumerateObjects(ofType: Transaction.className()) { (_, newPerson) in
                    
                    }
                }
                
                if oldSchemaVersion < 4 {
                    migration.enumerateObjects(ofType: Bills.className()) { (_, newPerson) in
                        newPerson?["url"] = ""
                        newPerson?["isPaid"] = false
                    }
                }
                
                if oldSchemaVersion < 5
                {
                    //Account
                    migration.enumerateObjects(ofType: Account.className()) { (_, newPerson) in
                        newPerson?["id"] = 0
                        newPerson?["name"] = ""
                        newPerson?["amount"] = 0.0
                        newPerson?["remainigAmount"] = 0.0
                        newPerson?["date"] = Date()
                        newPerson?["isIncludeOnDashboard"] = true
                        newPerson?["isDefaultAccount"] = false
                    }
                    
                    //Transfer
                    migration.enumerateObjects(ofType: Transfer.className()) { (_, newPerson) in
                        newPerson?["id"] = 0
                        newPerson?["nameFrom"] = ""
                        newPerson?["nameTo"] = ""
                        newPerson?["amountFrom"] = 0.0
                        newPerson?["amountTo"] = 0.0
                        newPerson?["date"] = Date()
                        newPerson?["note"] = ""
                    }
                    migration.enumerateObjects(ofType: Transaction.className()) { (_, newPerson) in
                        newPerson?["accountId"] = 0
                    }
                }
         })
        Realm.Configuration.defaultConfiguration = config
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
            (granted, error) in
            if granted {
                print("yes")
            } else {
                print("No")
            }
        }
        
        
      /*  if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
                if !accepted
                {
                    print("Notification access denied.")
                }
                
                let action = UNNotificationAction(identifier: "remindLater", title: "Remind me later", options: [])
                let category = UNNotificationCategory(identifier: "myCategory", actions: [action], intentIdentifiers: [], options: [])
                UNUserNotificationCenter.current().setNotificationCategories([category])
            }
        } else {
            // Fallback on earlier versions
        }*/
        
        let categoryList = uiRealm.objects(Category.self)
        if (categoryList.count == 0)
        {
            DBManager().categoryDB()
        }
    
        print(categoryList[0]["id"] as! Int)
        
        self.getScheduleDataFromDB()
        
        return true
    }
    
    //In App Purchase
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .failed:
                queue.finishTransaction(transaction)
                print("Transaction Failed \(transaction)")
            case .purchased, .restored:
                queue.finishTransaction(transaction)
                print("Transaction purchased or restored: \(transaction)")
            case .deferred, .purchasing:
                print("Transaction in progress: \(transaction)")
            }
        }
    }
    func getScheduleDataFromDB()
    {
        let types = uiRealm.objects(Transaction.self).filter("isUpdateByService = %@ AND repeatType != %@", "0","0")
        
        for i in 0..<types.count
        {
            var score : Transaction!
            score = types[i]
            let calendar = NSCalendar.current
            let date1 = calendar.startOfDay(for:Date())
            let date2 = calendar.startOfDay(for: score.updatedDate)
            
            let units: Set<Calendar.Component> = [.hour, .day, .month, .year]
            let components = calendar.dateComponents(units, from: date2, to: date1)
            var dates = Date()
            if(components.day! > 0)
            {
                var count = 0
                
                if(score.repeatType == "1")
                {
                    count = components.day!
                }
                else if(score.repeatType == "2")
                {
                    count = components.day!/7
                }
                else if(score.repeatType == "3")
                {
                    count = components.month!
                }
                
                for i in 0..<count
                {
                    var taskListB = Transaction()
                    
                    let catId = (uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?)! + 1
                    
                    var dayComponent = DateComponents()
                    
                    if(score.repeatType == "1")
                    {
                        dayComponent.day = i + 1
                    }
                    else if(score.repeatType == "2")
                    {
                        dayComponent.day = i + 7
                    }
                    else if(score.repeatType == "3")
                    {
                        dayComponent.month = i + 1
                    }
                    
                    let theCalendar = Calendar.current
                    dates = theCalendar.date(byAdding: dayComponent, to: date2)!
                    let tempDate = theCalendar.date(byAdding: dayComponent, to: date2)!
                    // let dates = Calendar.current.date(byAdding: .month, value: -(i), to: date2)
                    
                    print(i)
                    taskListB = Transaction(value: [ catId,score.categoryName,score.categoryImage,score.categoryID,score.subCategoryID,
                                                     score.subCatName,
                                                     score.subCatImage,
                                                     score.amount,
                                                     tempDate,
                                                     score.note,
                                                     UserDefaults.standard.integer(forKey: "AccountID"),
                                                     score.setType,
                                                     score.repeatType,
                                                     "1",
                                                     tempDate])
                    
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                }
            }
            
            let workouts = uiRealm.objects(Transaction.self).filter("id = %@", score.id)
            
            if let workout = workouts.first
            {
                try! uiRealm.write
                {
                    workout.categoryID =  score.categoryID
                    workout.categoryImage = score.categoryImage
                    workout.categoryName =  score.categoryName
                    
                    workout.subCategoryID = score.subCategoryID
                    workout.subCatImage = score.subCatImage
                    workout.subCatName =  score.subCatName
                    
                    workout.amount = score.amount
                    workout.date = score.date
                    workout.note = score.note
                    
                    workout.accountId = score.accountId
                    workout.setType = score.setType
                    workout.repeatType = score.repeatType
                    workout.isUpdateByService =  "0"
                    workout.updatedDate =  dates
                }
            }
        }
        
        let budgetCycle = uiRealm.objects(Budget.self)
        
        for j in 0..<budgetCycle.count
        {
            var scoreBudget : Budget!
            scoreBudget = budgetCycle[j]
            let calendarBudget = NSCalendar.current
            let dateBudget1 = calendarBudget.startOfDay(for:Date())
            let dateBudget2 = calendarBudget.startOfDay(for: scoreBudget.date)
            
            let unitsBudget: Set<Calendar.Component> = [.hour, .day, .month, .year]
            let componentsBudget = calendarBudget.dateComponents(unitsBudget, from: dateBudget2, to: dateBudget1)
            var datesBudget = Date()
            if(componentsBudget.day! > 0)
            {
                var countBudget = 0
                
                if(scoreBudget.cycleType == 1)
                {
                    countBudget = componentsBudget.day!
                }
                else if(scoreBudget.cycleType == 2)
                {
                    countBudget = componentsBudget.day!/7
                }
                else if(scoreBudget.cycleType == 3)
                {
                    countBudget = componentsBudget.month!
                }
                
                for i in 0..<countBudget
                {
                    var dayComponent = DateComponents()
                    
                    if(scoreBudget.cycleType == 1)
                    {
                        dayComponent.day = i + 1
                        let workouts = uiRealm.objects(Budget.self).filter("id = %@", scoreBudget.id)
                        
                        if let workout = workouts.first
                        {
                            try! uiRealm.write
                            {
                                workout.currentAmount = 0.0
                                workout.date = String.getDayWiseDate(mydate: scoreBudget.date, Days: 1)
                            }
                        }
                    }
                    else if(scoreBudget.cycleType == 2)
                    {
                        dayComponent.day = i + 7
                        let workouts = uiRealm.objects(Budget.self).filter("id = %@", scoreBudget.id)
                        
                        if let workout = workouts.first
                        {
                            try! uiRealm.write
                            {
                                workout.currentAmount = 0.0
                                workout.date = String.getDayWiseDate(mydate: scoreBudget.date, Days: 7)
                            }
                        }
                    }
                    else if(scoreBudget.cycleType == 3)
                    {
                        dayComponent.month = i + 1
                        let workouts = uiRealm.objects(Budget.self).filter("id = %@", scoreBudget.id)
                        
                        if let workout = workouts.first
                        {
                            try! uiRealm.write
                            {
                                workout.currentAmount = 0.0
                                workout.date = String.getMonthWiseDate(mydate: scoreBudget.date, Days: 1)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func scheduleNotification(at date: Date)
    {
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.hour, .minute], from: date)
        
        
        
        
        if #available(iOS 10.0, *) {
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: comp, repeats: true)
            
            let content = UNMutableNotificationContent()
            content.title = "Outlays"
            content.body = "Add today income/expense before you forget."
            content.sound = UNNotificationSound.default()
            content.categoryIdentifier = "myCategory"
            
            if let path = Bundle.main.path(forResource: "logo", ofType: "png") {
                let url = URL(fileURLWithPath: path)
                
                do {
                    let attachment = try UNNotificationAttachment(identifier: "logo", url: url, options: nil)
                    content.attachments = [attachment]
                } catch {
                    print("The attachment was not loaded.")
                }
            }
            
            let request = UNNotificationRequest(identifier: "myCategory", content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().add(request) {(error) in
                if let error = error {
                    print("Uh oh! We had an error: \(error)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        passcodeLockPresenter.presentPasscodeLock()
        
        Appirater.appEnteredForeground(true)
        self.getScheduleDataFromDB()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "comeBackForground"), object: nil)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.removeObject(forKey: "firstTimeEnter")
        UserDefaults.standard.removeObject(forKey: "AccountID")
    }

    //MARK:- Subscribtion Recipt
    func receiptValidation(completion: @escaping (Bool) -> ()) {
        let SUBSCRIPTION_SECRET = "3145e9e73aba4ec7a18459890565dbb0"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        var vc : UIViewController?
        
        /*if let visibleViewCtrl = UIApplication.shared.keyWindow?.currentViewController() {
            print(visibleViewCtrl)
            if let currentController = visibleViewCtrl.childViewControllers[1].childViewControllers[0].childViewControllers[1] as? PurchaseSubscribtionVC {
                vc = currentController
            }
        }*/

        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            
            print(base64encodedReceipt!)
            
            
            let requestDictionary = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]
            
            guard JSONSerialization.isValidJSONObject(requestDictionary) else {  print("requestDictionary is not valid JSON");  return }
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
                let validationURLString = "https://buy.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
                // live:- https://buy.itunes.apple.com/verifyReceipt
                // SandBox Tester:- https://sandbox.itunes.apple.com/verifyReceipt
                guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
                let session = URLSession(configuration: URLSessionConfiguration.default)
                var request = URLRequest(url: validationURL)
                request.httpMethod = "POST"
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
                let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
                    if let data = data , error == nil {
                        do {
                            let appReceiptJSON = try? JSONSerialization.jsonObject(with: data)
                            print("success. here is the json representation of the app receipt: \(appReceiptJSON)")
                            // if you are using your server this will be a json representation of whatever your server provided
                            
                            if appReceiptJSON != nil {
                                let temp = (appReceiptJSON as! [String:AnyObject])["status"]!
                                
                                if(temp as! NSNumber == 0){
                                    if ((appReceiptJSON as! [String:AnyObject])["latest_receipt_info"] as? [[String:AnyObject]]) != nil {
                                        
                                        if ((appReceiptJSON as! [String:AnyObject])["latest_receipt_info"] as! [[String:AnyObject]]).last != nil {
                                            let lastReceiptInfo = ((appReceiptJSON as! [String:AnyObject])["latest_receipt_info"] as! [[String:AnyObject]]).last!
                                            
                                            if lastReceiptInfo["product_id"] as! String == ONE_MONTH_SUBSCRIPTION ||
                                                lastReceiptInfo["product_id"] as! String == THREE_MONTH_SUBSCRIPTION ||
                                                lastReceiptInfo["product_id"] as! String == ONE_YEAR_SUBSCRIPTION {
                                                let isoDate = Date()
                                                
                                                let dateFormatter = DateFormatter()
                                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
                                                let strDate = dateFormatter.string(from: isoDate)
                                                let currentDate = dateFormatter.date(from: strDate)!
                                                
                                                let expireDate = dateFormatter.date(from: lastReceiptInfo["expires_date"] as! String)!
                                                
                                                if expireDate > currentDate {
                                                    UserDefaults.standard.set(true, forKey: "Purchase")
                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckPurchase"), object: nil)
                                                    completion(true)
                                                }else{
                                                    UserDefaults.standard.set(false, forKey: "Purchase")
                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckPurchase"), object: nil)
                                                    completion(false)
                                                }
                                            }
                                            else{
//                                                UIApplication.shared.endIgnoringInteractionEvents()
//                                                SVProgressHUD.dismiss()
//                                                if vc != nil {
//                                                    MBProgressHUD.hide(for: vc!.view, animated: true)
//                                                }
                                                
                                                completion(false)
                                            }
                                        }else{
//                                            UIApplication.shared.endIgnoringInteractionEvents()
//                                            SVProgressHUD.dismiss()
//                                            if vc != nil {
//                                                MBProgressHUD.hide(for: vc!.view, animated: true)
//                                            }
                                            completion(false)
                                        }
                                    }else{
//                                        UIApplication.shared.endIgnoringInteractionEvents()
//                                        SVProgressHUD.dismiss()
//                                        if vc != nil {
//                                            MBProgressHUD.hide(for: vc!.view, animated: true)
//                                        }
                                        completion(false)
                                    }
                                }else{
                                    DispatchQueue.main.async { // Correct
//                                        UIApplication.shared.endIgnoringInteractionEvents()
//                                        SVProgressHUD.dismiss()
//                                        if vc != nil {
//                                            MBProgressHUD.hide(for: vc!.view, animated: true)
//                                        }
                                        print("Subscription is not Success")
                                        completion(false)
                                    }
                                    
                                }
                            }else{
//                                UIApplication.shared.endIgnoringInteractionEvents()
//                                SVProgressHUD.dismiss()
//                                if vc != nil {
//                                    MBProgressHUD.hide(for: vc!.view, animated: true)
//                                }
                                completion(false)
                            }
                            
                            
                        } catch let error as NSError {
                            completion(false)
//                            UIApplication.shared.endIgnoringInteractionEvents()
//                            SVProgressHUD.dismiss()
//                            if vc != nil {
//                                MBProgressHUD.hide(for: vc!.view, animated: true)
//                            }
                            print("json serialization failed with error: \(error)")
                        }
                    } else {
//                        UIApplication.shared.endIgnoringInteractionEvents()
//                        SVProgressHUD.dismiss()
//                        if vc != nil {
//                            MBProgressHUD.hide(for: vc!.view, animated: true)
//                        }
                        completion(false)
                        print("the upload task returned an error: \(error)")
                    }
                }
                task.resume()
            } catch let error as NSError {
                completion(false)
                print("json serialization failed with error: \(error)")
            }
        }else{
            completion(false)
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    private func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("daily")
    }
}

