//
//  ContributionListVC.swift
//  Created by harshesh on 20/06/1940 Saka.
//  Copyright © 1940 Cools. All rights reserved.
//

import UIKit
import RealmSwift

class contributionCell: UITableViewCell {
    @IBOutlet var lblContributNote: UILabel!
    @IBOutlet var lblContributPrice: UILabel!
    @IBOutlet var lblContributDate: UILabel!
}

class ContributionListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tblContribution: UITableView!

    var contributData = [[String:AnyObject]]()
    var getgoalId = ""
    var getAmount : Double = 0.0
    var totalAmount : Double = 0.0
    var contributionInfo = Contribution()
    var getContributeId = 0
    var setGoalDate : Date!

    override func viewDidLoad() {
        super.viewDidLoad()

         UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        self.getcontributInfo()
        tblContribution.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    //Get Goals Data
    func getcontributInfo()
    {
        let puppies = uiRealm.objects(Contribution.self).filter("goalId = %d", Int("\(getgoalId)")!)
        contributData = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["contributid"] = puppies[j]["contributid"]! as AnyObject
            dict1["goalId"] = puppies[j]["goalId"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["note"] = puppies[j]["note"]! as AnyObject
            dict1["createDate"] = String.convertFormatOfDate(date: puppies[j]["createDate"]! as! Date) as AnyObject
            contributData.append(dict1)
            tblContribution.reloadData()
        }
    }
    
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contributData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contributionCell", for: indexPath) as! contributionCell
        cell.selectionStyle = .none
        getContributeId = contributData[indexPath.row]["contributid"] as! Int
        cell.lblContributNote.text = contributData[indexPath.row]["note"] as? String
        cell.lblContributPrice.text = "\(contributData[indexPath.row]["amount"]!)"
        cell.lblContributDate.text = contributData[indexPath.row]["createDate"] as? String
//        contributId = contributData[indexPath.row]["contributid"] as! Int
            for i in 0..<contributData.count
            {
                getAmount = Double("\(contributData[indexPath.row]["amount"]!)")!
                totalAmount = getAmount + Double((contributData[i] as NSDictionary).value(forKey: "amount") as! Int)
                print(totalAmount)
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContributionVC") as! ContributionVC
        vc.passingView = "Edit"
        vc.contributEditInfo = contributData[indexPath.row]
        vc.setGoalDate = setGoalDate
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete{
            
            self.deleteContributeItem(index: contributData[indexPath.row]["contributid"] as! Int)
            contributData.remove(at: indexPath.row)
            tblContribution.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
        tblContribution.reloadData()
    }
    
    func deleteContributeItem(index: Int){

        let workouts = uiRealm.objects(Contribution.self).filter("contributid = %d", index)
            
            uiRealm.beginWrite()
            uiRealm.delete(workouts)
            do
            {
                try uiRealm.commitWrite()
            }
            catch
            {
        
            }
    }

    //MARK:- Action Method
    @IBAction func btnBackTAP(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAddcontributionTAP(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContributionVC") as! ContributionVC
        vc.getgoalId = getgoalId
        
//        let getDate = String.convertFormatOfStringToDate(date: "\(editData["createDate"]!)")
//        vc.setGoalDate = getDate //(editData["createDate"]! as! Date)

        vc.setGoalDate = setGoalDate
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
