//
//  SplitViewController.swift
//  Outlays
//
//  Created by Cools on 10/10/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds


class splitCell: UITableViewCell {
    @IBOutlet var lblCategory : UILabel!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnSelectCategory: UIButton!
    @IBOutlet var txtAmount : UITextField!
}
class SplitViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,JBDatePickerViewDelegate,UITextFieldDelegate,GADBannerViewDelegate {
    @IBOutlet var lblHeaderLabel : UILabel!
    @IBOutlet var headerView: UIView!
    @IBOutlet var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var txtTotalAmount: UITextField!
    @IBOutlet var txtDate: UITextField!
    @IBOutlet var btnDateIcon: UIButton!
    @IBOutlet var btnDisplayDate: UIButton!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var tblSplitTable: UITableView!
    @IBOutlet var lblReminaingAmount: UILabel!
    @IBOutlet var btnTransactrionAdd: UIButton!
    @IBOutlet var dateView: UIView!
    @IBOutlet var AmountView: UIView!
    @IBOutlet var remainingView: UIView!
    @IBOutlet var splitTableContainerView: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var tableShadowView: UIView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var splitTransactionArray = NSMutableArray()
    var dateToSelect: Date!
    var strSelectedCategory : String = ""
    var selectedDate1 = Date()
    var dict = NSMutableDictionary()
    var index : Int = 0
    var amount : String = ""
    var editSplitTransaction : SectionTransaction!
    var passingView : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        IQKeyboardManager.sharedManager().enable = true
        splitTableContainerView.isHidden = true
//        splitTableContainerView.layer.cornerRadius = 4
        
        btnBack.setImage(UIImage(named : "ic_ic_transactionCancel")?.maskWithColor(color: UIColor.white), for: .normal)
        
        //            dateView.layer.masksToBounds = false
        //            dateView.layer.cornerRadius = 0
        //            dateView.layer.shadowColor = UIColor.lightGray.cgColor
        //            dateView.layer.shadowOpacity = 0.6
        //            dateView.layer.shadowOffset = CGSize(width: -1, height: 1)
        //            dateView.layer.shadowRadius = 1.5
        
        //            AmountView.layer.masksToBounds = false
        //            AmountView.layer.cornerRadius = 0
        //            AmountView.layer.shadowColor = UIColor.lightGray.cgColor
        //            AmountView.layer.shadowOpacity = 0.6
        //            AmountView.layer.shadowOffset = CGSize(width: -1, height: 1)
        //            AmountView.layer.shadowRadius = 1.5
        
//        remainingView.layer.masksToBounds = false
//        remainingView.layer.cornerRadius = 0
//        remainingView.layer.shadowColor = UIColor.lightGray.cgColor
//        remainingView.layer.shadowOpacity = 0.6
//        remainingView.layer.shadowOffset = CGSize(width: -1, height: 1)
//        remainingView.layer.shadowRadius = 1.5
        
        
        //            tableShadowView.layer.masksToBounds = false
        //            tableShadowView.layer.cornerRadius = 0
        //            tableShadowView.layer.shadowColor = UIColor.lightGray.cgColor
        //            tableShadowView.layer.shadowOpacity = 0.6
        //            tableShadowView.layer.shadowOffset = CGSize(width: -1, height: 1)
        //            tableShadowView.layer.shadowRadius = 1.5
        
        self.calendarView.isHidden = true
        self.bgView.isHidden = true
        datePickerView.delegate = self
        
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription
        
        tblHeightConstraint.constant = 0.0
        
        //       btnBack.setBackgroundImage(UIImage(named : "ic_backArrow")?.maskWithColor(color: UIColor.white), for: .normal)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SplitViewController.showSpinningWheel(notification:)), name: NSNotification.Name(rawValue: "fetchSplitCategoryData"), object: nil)
        
        
        if passingView == "EditSplitTransaction"
        {
            lblReminaingAmount.text = String(0.0)
            txtTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(editSplitTransaction!.amount!)"
            let temp = editSplitTransaction.items[0] as! NSMutableDictionary
            txtDate.text = temp["date"] as? String
            selectedDate1 = String.convertFormatOfStringToDate(date: (temp["date"] as! String))
        }
        else
        {
            lblReminaingAmount.text = String(0.0)//String(Double(amount)!)
            txtTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(amount)"
            txtDate.text = String.convertFormatOfDate(date: Date())
            //            btnDisplayDate.setTitle(String.convertFormatOfDate(date: Date()), for: .normal)
            selectedDate1 = Date()
        }
        
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        tblSplitTable.estimatedRowHeight = 44.0
        tblSplitTable.rowHeight = UITableViewAutomaticDimension
        tblSplitTable.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        btnTransactrionAdd.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = false
    }
    
    /*
     func handleTap(sender: UITapGestureRecognizer) {
     self.view.endEditing(true)
     
     if splitTransactionArray.count == 0
     {
     
     }
     else
     {
     let indexPath = IndexPath(item: index, section: 0)
     
     let cell = tblSplitTable.cellForRow(at: indexPath) as! splitCell
     
     print(cell.txtAmount.text!)
     
     var totalUsedAmount : Double = 0.0
     
     for i in 0..<splitTransactionArray.count
     {
     if (splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String == ""
     {
     totalUsedAmount = totalUsedAmount + 0.0
     }
     else
     {
     totalUsedAmount = totalUsedAmount + Double((splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String)!
     }
     }
     
     if Double(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!)) >= 0
     {
     lblReminaingAmount.text = String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
     (splitTransactionArray[index] as! NSDictionary).setValue(String(Double(cell.txtAmount.text!)!), forKey: "amount")
     }
     else
     {
     self.alertView(alertMessage: "Please enter amount less then current amount")
     }
     }
     
     }
     */
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
    }
    
    @objc func showSpinningWheel(notification: NSNotification) {
        dict = notification.object as! NSMutableDictionary
        print(dict)
        if(dict["subCategoryID"] as! String == "0")
        {
            //            btnSelectCategory.setTitle("\(dict["categoryName"] as! String)", for: .normal)
            strSelectedCategory = "\(dict["categoryName"] as! String)"
            
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryName"] as! String)", forKey: "name")//value(forKey: "name") as? String
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["name"] as! String)", forKey: "subcategoryName")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryName"] as! String)", forKey: "categoryName")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["image"] as! String)", forKey: "subcategoryImage")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryimage"] as! String)", forKey: "categoryimage")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryType"] as! String)", forKey: "categoryType")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["subCategoryID"] as! String)", forKey: "subCategoryID")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["isCategory"] as! String)", forKey: "isCategory")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryID"] as! String)", forKey: "categoryID")
            
            //            btnCategoryImage.setBackgroundImage(UIImage(named: dict["categoryimage"] as! String), for: .normal)
        }
        else
        {
            //            btnSelectCategory.setTitle("\(dict["categoryName"] as! String)/\(dict["name"] as! String)", for: .normal)
            strSelectedCategory = "\(dict["categoryName"] as! String)/\(dict["name"] as! String)"
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryName"] as! String)/\(dict["name"] as! String)", forKey: "name")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["name"] as! String)", forKey: "subcategoryName")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryName"] as! String)", forKey: "categoryName")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["image"] as! String)", forKey: "subcategoryImage")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryimage"] as! String)", forKey: "categoryimage")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryType"] as! String)", forKey: "categoryType")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["subCategoryID"] as! String)", forKey: "subCategoryID")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["isCategory"] as! String)", forKey: "isCategory")
            (splitTransactionArray[index] as! NSDictionary).setValue("\(dict["categoryID"] as! String)", forKey: "categoryID")
            //            btnCategoryImage.setBackgroundImage(UIImage(named: dict["image"] as! String), for: .normal)
        }
        print(dict)
        tblSplitTable.reloadData()
    }
    
    //MARK:- Tableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return splitTransactionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "splitCell", for: indexPath) as! splitCell
        
        cell.btnSelectCategory.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        cell.btnSelectCategory.addTarget(self, action: #selector(SplitViewController.btnSelectCategoryAction(_:)), for: .touchUpInside)
        cell.txtAmount.tag = indexPath.row
        
        cell.txtAmount.inputAccessoryView = accessoryView()
        cell.txtAmount.inputAccessoryView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
        cell.txtAmount.text = (splitTransactionArray[indexPath.row] as! NSDictionary).value(forKey: "amount") as? String
        
        //        cell.txtAmount.addDoneButtonToKeyboard(myAction: #selector(SplitViewController.doneAction(_:)))
        if (splitTransactionArray[indexPath.row] as! NSDictionary).value(forKey: "name") as! String == ""
        {
            cell.lblCategory.text = "Select Category >"
        }
        else
        {
            cell.lblCategory.text = (splitTransactionArray[indexPath.row] as! NSDictionary).value(forKey: "name") as? String
        }
        
        
        return cell
    }
    //MARK:- Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        index = textField.tag
        print(index)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        print(string)
        index = textField.tag
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.characters.count
        
        if numberOfChars <= 10
        {
            if((textField.text?.characters.count)! == 1)
            {
                if(textField.text == "0")
                {
                    if(string == "0")
                    {
                        return false
                    }
                    else if(string == "")
                    {
                        return false
                    }
                    else
                    {
                        textField.text = ""
                        //                        txtAmount.text = string
                        return true
                    }
                }
                else
                {
                    if(string == "")
                    {
                        textField.text = "0"
                        return false
                    }
                    return true
                }
            }
            else
            {
                let newCharacters = CharacterSet(charactersIn: string)
                let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacters)
                if boolIsNumber == true {
                    return true
                } else {
                    if string == "." {
                        let countdots = textField.text!.components(separatedBy: ".").count - 1
                        let array = textField.text?.characters.map { String($0) }
                        var decimalCount = 0
                        for character in array! {
                            if character == "." {
                                decimalCount += 1
                            }
                        }
                        
                        if countdots == 0 {
                            return true
                        } else {
                            if countdots > 0 && string == "." {
                                return false
                            } else {
                                if decimalCount == 2 {
                                    return true
                                } else {
                                    return false
                                }
                                //                                return true
                            }
                        }
                    } else {
                        return false
                    }
                }
            }
        }
        else
        {
            return false
        }
        
        
        
    }
    
    
    //MARK:- Helper Method
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    func accessoryView() -> UIView {
        
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        
        let doneButton = UIButton()
        doneButton.frame = CGRect(x: self.view.frame.width - 80, y: 7, width: 60, height: 30)
        //        doneButton.backgroundColor = UIColor.green
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(SplitViewController.doneAction(_:)), for: .touchUpInside)
        view.addSubview(doneButton)
        
        return view
        
    }
    
    @objc func doneAction(_ textfield:UITextField) {
        
        self.view.endEditing(true)
        
        let indexPath = IndexPath(item: index, section: 0)
        
        let cell = tblSplitTable.cellForRow(at: indexPath) as! splitCell
        
        //        let cell = tblSplitTable.dequeueReusableCell(withIdentifier: "splitCell", for: indexPath) as! splitCell
        print(cell.txtAmount.text!)
        
        if cell.txtAmount.text != ""
        {
            if Double(amount)! < Double(cell.txtAmount.text!)!
            {
                self.alertView(alertMessage: AlertString.LessAmount)
                return
            }
            
            
            
            if (splitTransactionArray[index] as! NSDictionary).value(forKey: "amount") as! String == cell.txtAmount.text!
            {
                var totalUsedAmount : Double = 0.0
                
                for i in 0..<splitTransactionArray.count
                {
                    if (splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String == ""
                    {
                        totalUsedAmount = totalUsedAmount + 0.0
                    }
                    else
                    {
                        totalUsedAmount = totalUsedAmount + Double((splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String)!
                    }
                }
                
                
                if (splitTransactionArray[index] as! NSDictionary).value(forKey: "amount") as! String != ""
                {
                    
                }
                else
                {
                    if (splitTransactionArray[index] as! NSDictionary).value(forKey: "amount") as! String == ""
                    {
                        
                    }
                    else
                    {
                        if Double(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!)) >= 0
                        {
                            lblReminaingAmount.text = String(totalUsedAmount + Double(cell.txtAmount.text!)!)//String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
                            txtTotalAmount.text = String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
                            (splitTransactionArray[index] as! NSDictionary).setValue(String(Double(cell.txtAmount.text!)!), forKey: "amount")
                        }
                        else
                        {
                            self.alertView(alertMessage: AlertString.LessAmount)
                        }
                    }
                    
                    
                }
            }
            else
            {
                var totalUsedAmount : Double = 0.0
                
                for i in 0..<splitTransactionArray.count
                {
                    if (splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String == ""
                    {
                        totalUsedAmount = totalUsedAmount + 0.0
                    }
                    else
                    {
                        totalUsedAmount = totalUsedAmount + Double((splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String)!
                    }
                }
                
                if (splitTransactionArray[index] as! NSDictionary).value(forKey: "amount") as! String == ""
                {
                    if Double(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!)) >= 0
                    {
                        (splitTransactionArray[index] as! NSDictionary).setValue(String(Double(cell.txtAmount.text!)!), forKey: "amount")
                        lblReminaingAmount.text = String(totalUsedAmount + Double(cell.txtAmount.text!)!)//String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
                        
                        txtTotalAmount.text = String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
                    }
                    else
                    {
                        self.alertView(alertMessage: AlertString.LessAmount)
                    }
                    
                    
                }
                else
                {
                    totalUsedAmount = totalUsedAmount - Double((splitTransactionArray[index] as! NSDictionary).value(forKey: "amount") as! String)!
                    
                    if Double(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!)) >= 0
                    {
                        lblReminaingAmount.text = String(totalUsedAmount + Double(cell.txtAmount.text!)!)//String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
                        
                        txtTotalAmount.text = String(Double(amount)!-(totalUsedAmount + Double(cell.txtAmount.text!)!))
                        (splitTransactionArray[index] as! NSDictionary).setValue(String(Double(cell.txtAmount.text!)!), forKey: "amount")
                    }
                    else
                    {
                        self.alertView(alertMessage: AlertString.LessAmount)
                    }
                }
                
                
            }
        }
        
        
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        //        BottomHeightConstraint.constant = 0
        
    }
    
    
    //MARK:- Action Method
    
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        //        txtAmount.resignFirstResponder()
        //        txtDescription.resignFirstResponder()
        showAnimate()
    }
    @IBAction func btnDeleteSplitTransaction(_ sender: UIButton) {
        splitTransactionArray.removeObject(at: sender.tag)
        
        var totalUsedAmount : Double = 0.0
        
        for i in 0..<splitTransactionArray.count
        {
            if (splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String == ""
            {
                totalUsedAmount = totalUsedAmount + 0.0
            }
            else
            {
                totalUsedAmount = totalUsedAmount + Double((splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String)!
            }
        }
        
        //        lblReminaingAmount.text = String(totalUsedAmount)
        
        if splitTransactionArray.count == 0
        {
            lblReminaingAmount.text = String(0.0)
            tblHeightConstraint.constant = 0.0
        }
        else
        {
            lblReminaingAmount.text = String(totalUsedAmount)
            tblHeightConstraint.constant = CGFloat(Double(splitTransactionArray.count) * 44.0)
        }
        tblSplitTable.reloadData()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddSplitAction(_ sender: UIButton) {
        
        splitTableContainerView.isHidden = false
        var totalUsedAmount : Double = 0.0
        
        for i in 0..<splitTransactionArray.count
        {
            if (splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String == ""
            {
                totalUsedAmount = totalUsedAmount + 0.0
            }
            else
            {
                totalUsedAmount = totalUsedAmount + Double((splitTransactionArray[i] as! NSDictionary).value(forKey: "amount") as! String)!
            }
        }
        
        if String(Double(amount)!-totalUsedAmount) == "0.0"
        {
            self.alertView(alertMessage: AlertString.SplitAmount)
        }
        else
        {
            if splitTransactionArray.count > 0
            {
                
                if (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "name") as! String == "" && (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "amount") as! String == ""
                {
                    self.alertView(alertMessage: AlertString.AmountCategory)
                }
                else if (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "name") as! String == ""
                {
                    self.alertView(alertMessage: AlertString.SelectCategory)
                }
                else if (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "amount") as! String == ""
                {
                    self.alertView(alertMessage: AlertString.AddAmount)
                }
                else
                {
                    let dict1 = NSMutableDictionary()
                    dict1.setValue("", forKey: "amount")
                    dict1.setValue("", forKey: "name")
                    dict1.setValue("", forKey: "subcategoryName")
                    dict1.setValue("", forKey: "categoryName")
                    dict1.setValue("", forKey: "subcategoryImage")
                    dict1.setValue("", forKey: "categoryimage")
                    dict1.setValue("", forKey: "description")
                    dict1.setValue(selectedDate1, forKey: "date")
                    dict1.setValue("", forKey: "categoryType")
                    dict1.setValue("", forKey: "subCategoryID")
                    dict1.setValue("", forKey: "isCategory")
                    dict1.setValue("", forKey: "categoryID")
                    splitTransactionArray.add(dict1)
                    
                    if tblHeightConstraint.constant > self.view.frame.size.height - (remainingView.frame.maxY + 150)
                    {
                        
                    }
                    else
                    {
                        tblHeightConstraint.constant = CGFloat(Double(splitTransactionArray.count) * 50.0)
                    }
                    
                    
                    
                    tblSplitTable.reloadData()
                    if splitTransactionArray.count > 1
                    {
                        tblSplitTable.scrollToRow(at: IndexPath(item:splitTransactionArray.count-1, section: 0), at: .bottom, animated: true)
                    }
                }
            }
            else
            {
                let dict1 = NSMutableDictionary()
                dict1.setValue("", forKey: "amount")
                dict1.setValue("", forKey: "name")
                dict1.setValue("", forKey: "subcategoryName")
                dict1.setValue("", forKey: "categoryName")
                dict1.setValue("", forKey: "subcategoryImage")
                dict1.setValue("", forKey: "categoryimage")
                dict1.setValue("", forKey: "description")
                dict1.setValue(selectedDate1, forKey: "date")
                dict1.setValue("", forKey: "categoryType")
                dict1.setValue("", forKey: "subCategoryID")
                dict1.setValue("", forKey: "isCategory")
                dict1.setValue("", forKey: "categoryID")
                splitTransactionArray.add(dict1)
                
                tblHeightConstraint.constant = CGFloat(Double(splitTransactionArray.count) * 44.0)
                tblSplitTable.reloadData()
            }
        }
        
        
    }
    
    @IBAction func btnSelectCategoryAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController//CategoryViewController
        vc.passingView = "split"
        vc.strCategoryID = "0"
        index = sender.tag
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func doneSplitTransactionEntry(_ sender: UIButton) {
        
        
        if splitTransactionArray.count > 0
        {
            if (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "name") as! String == "" && (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "amount") as! String == ""
            {
                self.alertView(alertMessage: AlertString.AmountCategory)
            }
            else if (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "name") as! String == ""
            {
                self.alertView(alertMessage: AlertString.SelectCategory)
            }
            else if (splitTransactionArray[splitTransactionArray.count-1] as! NSDictionary).value(forKey: "amount") as! String == ""
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
            else
            {
                let currentTimeInMiliseconds = Date().timeIntervalSince1970
                let milliseconds = currentTimeInMiliseconds * 1000.0
                
                for i in 0..<splitTransactionArray.count
                {
                    var taskListB = Transaction()
                    let autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
                    
                    let dictCat : NSMutableDictionary = splitTransactionArray[i] as! NSMutableDictionary
                    print(dictCat)
                    if(autoCatId == nil)
                    {
                        taskListB = Transaction(value: [1,dictCat.value(forKey: "categoryName"),dictCat.value(forKey: "categoryimage"),dictCat.value(forKey: "categoryID"),dictCat.value(forKey: "subCategoryID"),dictCat.value(forKey: "subcategoryName"),dictCat.value(forKey: "subcategoryImage"),dictCat.value(forKey: "amount"),dictCat.value(forKey: "date"),dictCat.value(forKey: "description"),UserDefaults.standard.integer(forKey: "AccountID"),dictCat.value(forKey: "categoryType"),"0","",selectedDate1,milliseconds])
                    }
                    else{
                        
                        let catId = (uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?)! + 1
                        taskListB = Transaction(value: [catId,dictCat.value(forKey: "categoryName"),dictCat.value(forKey: "categoryimage"),dictCat.value(forKey: "categoryID"),dictCat.value(forKey: "subCategoryID"),dictCat.value(forKey: "subcategoryName"),dictCat.value(forKey: "subcategoryImage"),dictCat.value(forKey: "amount"),dictCat.value(forKey: "date"),dictCat.value(forKey: "description"),UserDefaults.standard.integer(forKey: "AccountID"),dictCat.value(forKey: "categoryType"),"0","",selectedDate1,milliseconds])
                    }
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                    
                    let vc = self.presentingViewController//self.storyboard?.instantiateViewController(withIdentifier: "AddTransactionVC") as! AddTransactionVC
                    self.dismiss(animated: true, completion: {
                        //                self.parent?.dismiss(animated: false, completion: nil)
                        vc?.dismiss(animated: false, completion: nil)
                    })
                    
                }
            }
        }
        else
        {
            self.alertView(alertMessage: AlertString.SplitValue)
        }
        
        
        
    }
    
    //MARK: - JBDatePicker Delegate
    
    func didSelectDay(_ dayView: JBDatePickerDayView) {
        
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            txtDate.text = stringDate
            //            btnDisplayDate.setTitle(stringDate, for: .normal)
            selectedDate1 = selectedDate
            removeAnimate()
        }
        
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        //        self.navigationItem.title = datePickerView.presentedMonthView.monthDescription
        
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
}
