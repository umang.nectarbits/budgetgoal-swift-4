//
//  ViewController.swift
//  Finance
//
//  Created by Cools on 6/12/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import RealmSwift
import Realm
import LocalAuthentication
import NotificationCenter
import GoogleMobileAds

class AllTransactionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIViewControllerPreviewingDelegate,NCWidgetProviding{

    //MARK: - Define IBoutlet
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var visualBlur : UIVisualEffectView!
    @IBOutlet var cardCollection : UICollectionView!
    @IBOutlet var lblAvaliableAmount : UICountingLabel!
    @IBOutlet var lblTotalExpense : UICountingLabel!
    @IBOutlet var lblTotalIncome : UICountingLabel!
    @IBOutlet var summaryView: UIView!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnCard: UIButton!
    @IBOutlet var tblTransactionHistory: UITableView!
    @IBOutlet var tblTransactionReminder: UITableView!
    @IBOutlet var lblTimeline : UILabel!
    @IBOutlet var lblOverview : UILabel!
    @IBOutlet var lblBudget : UILabel!
    @IBOutlet var lblSetting : UILabel!
    @IBOutlet var lblheader : UILabel!
    @IBOutlet var headerView : UIView!
    @IBOutlet var tabBarView : UIView!
    @IBOutlet var lblUnderline : UILabel!
    @IBOutlet var scrvSwipeView: UIScrollView!
    @IBOutlet var currentMonthView : UIView!
    @IBOutlet var schedualView : UIView!
    @IBOutlet var btnCurrentMonth : UIButton!
    @IBOutlet var btnSchedual : UIButton!
    @IBOutlet var tableshedowView : UIView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var currencyView: UIView!
    @IBOutlet weak var tblcurrency: UITableView!
    @IBOutlet weak var currencySearchBar: UISearchBar!
    @IBOutlet weak var imgTimeline: UIImageView!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var imgOverview: UIImageView!
       @IBOutlet var TopheaderView : UIView!

    //MARK: - Define Variable
    var currentCreateAction:UIAlertAction!
    var dateArray : [NSDate] = []
    var uniqueDateArray : NSMutableArray = []
    var sections = [SectionTransaction]()
    var sectionsReminder = [SectionTransaction]()
    var sectionsReminderCat = [SectionCategory]()
    var sectionsCat = [SectionCategory]()
    var transactionArray = NSMutableArray()
    var transactionReminderArray = NSMutableArray()
    var strFilter : String = "date"
    var filterBool : Bool = false
    var popTip: AMPopTip!
    var listMonth: NSMutableArray = []
    var monthWiseDate : Date!
    var searchActive: Bool = false
    var filtered: [String] = []
    var countryArray = NSMutableArray()
    var countryNewArray : [String]! = []
    var strCurrency : String = ""
    
    let SIZE_EXPANDED = 275
    let SIZE_COMPACT = 110
    //MARK: - View Method
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        //For Show more/less button
        if #available(iOS 10.0, *) {
            self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        } else {
            // Fallback on earlier versions
        }
        
        //TableView with 2 records at starting
        self.preferredContentSize = CGSize(width: 0, height: SIZE_EXPANDED)
        
        currencyView.isHidden = true
        bgView.isHidden = true
        currencyView.layer.cornerRadius = 8
        currencyView.layer.masksToBounds = true
//        TopheaderView.setGradientColor(color1:UIColor(colorLiteralRed: 74.0/255.0, green: 147.0/255.0, blue: 207.0/255.0, alpha: 1).cgColor , (color2: UIColor(colorLiteralRed: 148.0/255.0, green: 110.0/255.0, blue: 226.0/255.0, alpha: 1).cgColor))
        
//        [UIColor(colorLiteralRed: 74.0/255.0, green: 147.0/255.0, blue: 207.0/255.0, alpha: 1)
//            .cgColor,UIColor(colorLiteralRed: 148.0/255.0, green: 110.0/255.0, blue: 226.0/255.0, alpha: 1).cgColor]
        
        
//        if !Constant.isKeyPresentInUserDefaults(key: "currencySymbol") {
//            UserDefaults.standard.set("", forKey: "currencySymbol")
//            showAnimate1()
//        }
        
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: view)
        }
//        tableshedowView.dropShadow()
//        tblTransactionHistory.layer.cornerRadius = 8
//        tblTransactionHistory.layer.masksToBounds = true
        
//      headerView.backgroundColor = Constant.headerColorCode
//        headerView.backgroundColor = self.setGradient()
        
        
//        tabBarView.backgroundColor = Constant.tabBarColorCode
        lblheader.font = headerFont
        lblheader.textColor = Constant.tempheaderFontColor
        
        lblTotalIncome.textColor = Constant.incomeColorCode
        lblTotalIncome.font = tableHeaderAmountFont
        lblTotalExpense.font = tableHeaderAmountFont
        lblAvaliableAmount.font = tableHeaderAmountFont
        lblTotalExpense.textColor = Constant.expenseColorCode
        
        self.scrvSwipeView!.addSubview(currentMonthView);
        self.scrvSwipeView!.addSubview(schedualView);
        
        let scrollWidth: CGFloat  = 2 * self.view.frame.width
        
        currentMonthView.frame.origin.x = 0
        schedualView.frame.origin.x  = 1 * self.view.frame.width + 1
        
        self.automaticallyAdjustsScrollViewInsets = false;
        
        countryArray = (IsoCountries.allCountries as! NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<countryArray.count {
            let temp = countryArray[i] as! IsoCountryInfo
            countryNewArray.append(temp.name)
        }
        print(countryNewArray)
        
        currentMonthView.frame.origin.y  = 0
        currentMonthView.frame.size.width = self.scrvSwipeView.frame.size.width
        schedualView.frame.origin.y  = 0
        schedualView.frame.size.width = self.scrvSwipeView.frame.size.width
        
        scrvSwipeView.contentSize = CGSize(width: scrollWidth, height: self.view.frame.height - (headerView.frame.size.height));
        currentMonthView.frame.size.height  =  self.scrvSwipeView.frame.size.height
        schedualView.frame.size.height  =  self.scrvSwipeView.frame.size.height
        
        
        btnCurrentMonth.setTitleColor(UIColor(red: 122.0/255.0, green: 137.0/255.0, blue: 142.0/255.0, alpha: 1), for: .normal)
        btnSchedual.setTitleColor(UIColor(red: 122.0/255.0, green: 137.0/255.0, blue: 142.0/255.0, alpha: 1), for: .normal)
//        lblUnderline.backgroundColor = UIColor(red: 122.0/255.0, green: 137.0/255.0, blue: 142.0/255.0, alpha: 1)
        lblUnderline.backgroundColor = SecondColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(AllTransactionViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AllTransactionViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AllTransactionViewController.showSpinningWheel(notification:)), name: NSNotification.Name(rawValue: "detectAfterDeleteEvent"), object: nil)
        
        let app = UIApplication.shared
        
        NotificationCenter.default.addObserver(self, selector: #selector(AllTransactionViewController.applicationWillResignActive(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: app)
    
        let origImage = UIImage(named: "ic_filter.png")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(tintedImage, for: .normal)
        btnFilter.tintColor = UIColor.white
        
        let menuImage = UIImage(named: "ic_AddWhite.png")
        let tintedImage1 = menuImage?.withRenderingMode(.alwaysTemplate)
        btnCard.setImage(tintedImage1, for: .normal)
        btnCard.tintColor = UIColor.white
        
        let Image1 = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        btnMenu.setImage(Image1, for: .normal)
        btnMenu.tintColor = UIColor.white
        
        let path = Realm.Configuration.defaultConfiguration.fileURL
        print(path as! URL)
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    @objc func showSpinningWheel(notification: NSNotification) {
        viewWillAppear(true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.setGradient()
        updateCellsLayout()
        summaryView.dropShadow()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        
        if #available(iOS 10.0, *) {
            self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        } else {
            // Fallback on earlier versions
        }
 
        
        
//        lblTimeline.textColor = Constant.highlightedColorCode
//        lblTimeline.font = tabBarFont
//        lblOverview.textColor = Constant.normalColorCode
//        lblOverview.font = tabBarFont
//        lblBudget.textColor = Constant.normalColorCode
//        lblBudget.font = tabBarFont
//        lblSetting.textColor = Constant.normalColorCode
//        lblSetting.font = tabBarFont
//        
//        imgTimeline.image = timelineHightlitedImage
//        imgOverview.image = overviewImage
//        imgSettings.image = settingImage
// 
//        imgTimeline.isHighlighted = true

        self.visualBlur.isHidden = true
        
        cardCollection.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "cardCell")
        cardCollection.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
        cardCollection.decelerationRate = UIScrollViewDecelerationRateFast
        
        for i in 0..<12 {
            let previousMonth = Calendar.current.date(byAdding: .month, value: -i, to: Date())
            
            let sumOfIncome = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"1").value(forKey: "amount") as! NSArray
            
            
            
            let sumOfExpense = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0").value(forKey: "amount") as! NSArray
            
            let dict = NSMutableDictionary()
            
            dict.setValue(previousMonth, forKey: "previousMonthDate")
            if(sumOfIncome.count>0)
            {
                dict.setValue(String.sum(array: sumOfIncome), forKey: "sumOfIncome")
            }
            else
            {
                dict.setValue("0", forKey: "sumOfIncome")
            }
            
            if(sumOfExpense.count>0)
            {
                dict.setValue(String.sum(array: sumOfExpense), forKey: "sumOfExpense")
            }
            else
            {
                dict.setValue("0", forKey: "sumOfExpense")
            }
            dict.setValue(String(Double(Double(String.sum(array: sumOfIncome))! - Double(String.sum(array: sumOfExpense))!)), forKey: "avaliableBal")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM-yyyy"
            dict.setValue(dateFormatter.string(from: previousMonth!), forKey: "PreviousMonth")
            listMonth.add(dict)
        }
        print(listMonth)
        print(uiRealm)
        tblTransactionHistory.estimatedRowHeight = 60.0
        
        tblTransactionHistory.rowHeight = UITableViewAutomaticDimension
        
        tblTransactionHistory.tableFooterView = UIView.init(frame: CGRect.zero)
        
        let puppies = uiRealm.objects(Transaction.self).filter("accountId = %@",UserDefaults.standard.integer(forKey: "AccountID"))
        
        setUpValue(selectDate: Date())
        transactionArray = []
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(puppies[j]["accountId"]!, forKey: "accountId")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            dict1.setValue(puppies[j]["repeatType"]!, forKey: "repeatType")
            dict1.setValue(puppies[j]["isUpdateByService"]!, forKey: "isUpdateByService")
            dict1.setValue(puppies[j]["updatedDate"]!, forKey: "updatedDate")
            dict1.setValue(String(describing: puppies[j]["milliseconds"]!), forKey: "milliseconds")
            transactionArray.add(dict1)
        }
        popTip = AMPopTip()
        popTip.frame.size.height = 100
        popTip?.shouldDismissOnTap = true
        popTip?.edgeMargin = 5
        popTip?.offset = 2
        popTip.borderWidth = 1
        popTip.borderColor = Constant.tableSubTextColorCode
        popTip?.edgeInsets = UIEdgeInsets(top: 0,left: 10,bottom: 0,right: 10)
        popTip?.popoverColor = UIColor.white
        
        
        let button = UIButton(frame: CGRect(x: 1, y: 10, width: 133, height: 30))
        button.setTitle("By Date", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = tableHeaderFont
        button.addTarget(self, action: #selector(btnFilterByDate(_:)), for: .touchUpInside)
        popTip.addSubview(button)
        
        let seperator = UILabel(frame: CGRect(x: 1, y: button.frame.maxY, width: 133, height: 1))
        seperator.backgroundColor = Constant.tableSubTextColorCode
        popTip.addSubview(seperator)
        
        let button1 = UIButton(frame: CGRect(x: 1, y: seperator.frame.maxY, width: 133, height: 30))
        button1.setTitle("By Category", for: .normal)
        button1.setTitleColor(UIColor.black, for: .normal)
        button1.titleLabel?.font = tableHeaderFont
        button1.addTarget(self, action: #selector(btnFilterByCategory(_:)), for: .touchUpInside)
        popTip.addSubview(button1)
        monthWiseDate = Date()
        
        if(strFilter == "category")
        {
            filterByCategory(selectDate: Date())
            filterByCategoryReminder(selectDate: Date())
        }
        else
        {
            
            filterByDate(selectDate: Date())
            filterByDateReminder(selectDate: Date())
        }
    }
    
    @objc func applicationWillResignActive(notification: NSNotification)
    {
        self.viewWillAppear(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        popTip.hide()
        filterBool = false
    }
    
    
    func setUpValue(selectDate : Date)
    {
        
        
        let sumOfIncome = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"1").value(forKey: "amount") as! NSArray
        
        let sumOfExpense = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0").value(forKey: "amount") as! NSArray
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-yyyy"
        btnCurrentMonth.setTitle(dateFormatter.string(from: selectDate), for: .normal)
        
        var formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        lblTotalExpense.formatBlock = {(_ value: CGFloat) -> String in
            var formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        
        lblTotalIncome.formatBlock = {(_ value: CGFloat) -> String in
            var formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        
        lblAvaliableAmount.formatBlock = {(_ value: CGFloat) -> String in
            var formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        
        
        
        if(sumOfIncome.count>0)
        {
            lblTotalIncome.method = .easeOut
            lblTotalIncome.count(from: 0, to: CGFloat(Double(String.sum(array: sumOfIncome))!), withDuration: 0.3)
            
        }
        else
        {
            lblTotalIncome.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) 0.00"
        }
        
        if(sumOfExpense.count>0)
        {
            lblTotalExpense.method = .easeOut
            
            lblTotalExpense.count(from: 0, to: CGFloat(Double(String.sum(array: sumOfExpense))!), withDuration: 0.3)
        }
        else
        {
            lblTotalExpense.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) 0.00"
        }
        
        
        lblAvaliableAmount.method = .easeOut
        
        lblAvaliableAmount.count(from: 0, to: CGFloat(Double(Double(String.sum(array: sumOfIncome))! - Double(String.sum(array: sumOfExpense))!)), withDuration: 0.3)
      
        let income =  String(format:"%.2f",Double(String.sum(array: sumOfIncome))!)
        let expnese = String(format:"%.2f",Double(String.sum(array: sumOfExpense))!)
        
        let info : [String: Any] = ["lblTotalExpense": expnese, "lblTotalIncome" : income]
        
        let sharedDefaults = UserDefaults(suiteName: "group.com.nectarbits.outlays")
        sharedDefaults?.set(info, forKey: "dataForKey")
        sharedDefaults?.synchronize()
    }
  
    //MARK: - Filter Function
    func filterByDate(selectDate : Date)
    {
        sections = [SectionTransaction]()
        uniqueDateArray = []
        dateArray = []
        let dateList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@", selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueDateArray.contains(temp!))
            {
                uniqueDateArray.add(temp!)
            }
        }
        
        let milisecondArray = NSMutableArray()
        
        for j in 0..<uniqueDateArray.count
        {
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String
                    )
                {
                    if !milisecondArray.contains((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                    {
                        milisecondArray.add((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                    }
                }
            }
        }
        
//        for k in 0..<milisecondArray.count
//        {
            for j in 0..<uniqueDateArray.count
            {
                var totalAmount : Double = 0.0
                let temp1 : NSMutableArray = []
                var totalSplitedAmount : Double = 0.0
                let temp2 : NSMutableArray = []
                for i in 0..<transactionArray.count
                {
                    if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String == "0")
                    {
                        
                        totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                        print(totalAmount)
                        var dict = NSMutableDictionary()
                        dict = transactionArray[i] as! NSMutableDictionary
                        print(dict)
                        temp1.add(dict)
                    }
                }
                let temp = SectionTransaction(amount: String(totalAmount), date: uniqueDateArray[j] as! String, items: temp1)
                print(temp)
                sections.append(temp)
                
                for i in 0..<transactionArray.count
                {
                    if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String != "0")
                    {
                        
//                        if milisecondArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String
//                        {
                            totalSplitedAmount = totalSplitedAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalSplitedAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            print(dict)
                            temp2.add(dict)
//                        }
                    }
                }
                
                if temp2.count > 0
                {
                    let tempSplit = SectionTransaction(amount: String(totalSplitedAmount), date: "Split Amount", items: temp2)
                    print(tempSplit)
                    sections.append(tempSplit)
                }
                
            }
//        }
        
        
        tblTransactionHistory.reloadData()
    }
    
    func filterByDateReminder(selectDate: Date)
    {
        let reminderArray = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND repeatType != %@",selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0")
        
        transactionReminderArray = []
        for j in 0..<reminderArray.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(reminderArray[j]["id"] as! Int, forKey: "id")
            dict1.setValue(reminderArray[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(reminderArray[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(reminderArray[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(reminderArray[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(reminderArray[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(reminderArray[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(reminderArray[j]["amount"]!, forKey: "amount")
            dict1.setValue(reminderArray[j]["setType"]!, forKey: "setType")
            
            if reminderArray[j]["repeatType"] as! String == "1"
            {
                dict1.setValue(String.convertFormatOfDate(date: String.getDayWiseDate(mydate: reminderArray[j]["date"]! as! Date, Days: 1)), forKey: "date")
            }
            else if reminderArray[j]["repeatType"] as! String == "2"
            {
                dict1.setValue(String.convertFormatOfDate(date: String.getDayWiseDate(mydate: reminderArray[j]["date"]! as! Date, Days: 7)), forKey: "date")
            }
            else if reminderArray[j]["repeatType"] as! String == "3"
            {
                dict1.setValue(String.convertFormatOfDate(date: String.getMonthWiseDate(mydate: reminderArray[j]["date"]! as! Date, Days: 1)), forKey: "date")
            }
            
            dict1.setValue(reminderArray[j]["note"]!, forKey: "note")
            dict1.setValue(reminderArray[j]["repeatType"]!, forKey: "repeatType")
            dict1.setValue(reminderArray[j]["isUpdateByService"]!, forKey: "isUpdateByService")
            dict1.setValue(reminderArray[j]["updatedDate"]!, forKey: "updatedDate")
            dict1.setValue(String(describing: reminderArray[j]["milliseconds"]!), forKey: "milliseconds")
            transactionReminderArray.add(dict1)
        }
        
        print("Reminder Array : \(transactionReminderArray)")
        
        
        sectionsReminder = [SectionTransaction]()
        uniqueDateArray = []
        dateArray = []
        
        for i in 0..<transactionReminderArray.count
        {
            
            if(!uniqueDateArray.contains((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "date") as! String))
            {
                uniqueDateArray.add((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
            }
        }
        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0.0
            let temp1 : NSMutableArray = []
            for i in 0..<transactionReminderArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
                {
                    
                    totalAmount = totalAmount + Double((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                    print(totalAmount)
                    var dict = NSMutableDictionary()
                    dict = transactionReminderArray[i] as! NSMutableDictionary
                    print(dict)
                    temp1.add(dict)
                    
                }
            }
            let temp = SectionTransaction(amount: String(totalAmount), date: uniqueDateArray[j] as! String, items: temp1)
            print(temp)
            sectionsReminder.append(temp)
        }
        print(sectionsReminder)
        tblTransactionReminder.reloadData()
    }
    
    func filterByCategory(selectDate : Date)
    {
        
        sectionsCat = [SectionCategory]()
        uniqueDateArray = []
        dateArray = []
        let dateList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@", selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        let categoryIDList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@", selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "categoryID") as! NSArray
        
        for i in 0..<categoryIDList.count
        {
            if(!uniqueDateArray.contains(categoryIDList[i]))
            {
                uniqueDateArray.add(categoryIDList[i])
            }
        }
        let uniqueArray : NSMutableArray = []
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueArray.contains(temp!))
            {
                uniqueArray.add(temp!)
            }
        }

        let milisecondArray = NSMutableArray()
        
        for j in 0..<uniqueArray.count//uniqueArray
        {
            for i in 0..<transactionArray.count
            {
                if(uniqueArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String )
                {
                    for k in 0..<uniqueDateArray.count
                    {
                        if !milisecondArray.contains((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                        {
                            milisecondArray.add((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                        }
                    }
                }
            }
        }
        
        
        
        for l in 0..<milisecondArray.count
        {
            for j in 0..<uniqueArray.count//uniqueArray
            {
                var totalSplitedAmount : Double = 0.0
                let temp2 : NSMutableArray = []
                
                for i in 0..<transactionArray.count
                {
                    if(uniqueArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String )
                    {
                        for k in 0..<uniqueDateArray.count
                        {
                            if(uniqueArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && uniqueDateArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String != "0")
                            {
                                if milisecondArray[l] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String
                                {
                                    totalSplitedAmount = totalSplitedAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                                    print(totalSplitedAmount)
                                    var dict = NSMutableDictionary()
                                    dict = transactionArray[i] as! NSMutableDictionary
                                    
                                    print(dict)
                                    temp2.add(dict)

                                }
                            }
                        }
                    }
                }
                
                if temp2.count > 0
                {
                    let tempSplit = SectionCategory(amount: String(totalSplitedAmount) , categoryID: "", categoryName: "Split Amount", categoryImage: "ic_split", items: temp2)
                    print(tempSplit)
                    sectionsCat.append(tempSplit)
                }
            }
        }
        
        
        
        
        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0
            let temp1 : NSMutableArray = []
            var category_name : String = ""
            var category_image : String = ""
            
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String == "0")
                {
                    for k in 0..<uniqueArray.count
                    {
                        if(uniqueArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String == "0")
                        {
                            totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            print(dict)
                            category_name = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String
                            category_image = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryImage") as! String
                            temp1.add(dict)
                        }
                    }
                }
            }
            
            if temp1.count > 0
            {
                let temp = SectionCategory(amount: String(totalAmount) , categoryID: uniqueDateArray[j] as! String, categoryName: category_name, categoryImage: category_image, items: temp1)
                print(temp)
                sectionsCat.append(temp)
            }
            
        }
        tblTransactionHistory.reloadData()
    }
    
    func filterByCategoryReminder(selectDate: Date)
    {
        
        let reminderArray = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND repeatType != %@",selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0")
        
        transactionReminderArray = []
        for j in 0..<reminderArray.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(reminderArray[j]["id"] as! Int, forKey: "id")
            dict1.setValue(reminderArray[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(reminderArray[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(reminderArray[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(reminderArray[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(reminderArray[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(reminderArray[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(reminderArray[j]["amount"]!, forKey: "amount")
            dict1.setValue(reminderArray[j]["setType"]!, forKey: "setType")
            
            if reminderArray[j]["repeatType"] as! String == "1"
            {
                dict1.setValue(String.convertFormatOfDate(date: String.getDayWiseDate(mydate: reminderArray[j]["date"]! as! Date, Days: 1)), forKey: "date")
            }
            else if reminderArray[j]["repeatType"] as! String == "2"
            {
                dict1.setValue(String.convertFormatOfDate(date: String.getDayWiseDate(mydate: reminderArray[j]["date"]! as! Date, Days: 7)), forKey: "date")
            }
            else if reminderArray[j]["repeatType"] as! String == "3"
            {
                dict1.setValue(String.convertFormatOfDate(date: String.getMonthWiseDate(mydate: reminderArray[j]["date"]! as! Date, Days: 7)), forKey: "date")
            }
            
            dict1.setValue(reminderArray[j]["note"]!, forKey: "note")
            dict1.setValue(reminderArray[j]["repeatType"]!, forKey: "repeatType")
            dict1.setValue(reminderArray[j]["isUpdateByService"]!, forKey: "isUpdateByService")
            dict1.setValue(reminderArray[j]["updatedDate"]!, forKey: "updatedDate")
            dict1.setValue(String(describing: reminderArray[j]["milliseconds"]!), forKey: "milliseconds")
            transactionReminderArray.add(dict1)
        }
        
        print("Reminder Array : \(transactionReminderArray)")
        
        sectionsReminderCat = [SectionCategory]()
        uniqueDateArray = []
        dateArray = []
        
        for i in 0..<transactionReminderArray.count
        {
            
            if(!uniqueDateArray.contains((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String))
            {
                uniqueDateArray.add((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String)
            }
        }
        
        let uniqueArray : NSMutableArray = []
        for i in 0..<transactionReminderArray.count
        {
            if(!uniqueArray.contains((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "date") as! String))
            {
                uniqueArray.add((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
            }
        }
        
        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0
            let temp1 : NSMutableArray = []
            var category_name : String = ""
            var category_image : String = ""
            for i in 0..<transactionReminderArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String )
                {
                    for k in 0..<uniqueArray.count
                    {
                        if(uniqueArray[k] as! String == (transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
                        {
                            totalAmount = totalAmount + Double((transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionReminderArray[i] as! NSMutableDictionary
                            print(dict)
                            category_name = (transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String
                            category_image = (transactionReminderArray[i] as! NSMutableDictionary).value(forKey: "categoryImage") as! String
                            temp1.add(dict)
                        }
                    }
                }
            }
            let temp = SectionCategory(amount: String(totalAmount) , categoryID: uniqueDateArray[j] as! String, categoryName: category_name, categoryImage: category_image, items: temp1)
            print(temp)
            sectionsReminderCat.append(temp)
        }
        print(sectionsReminderCat)
        tblTransactionReminder.reloadData()
    }
    
    
    //MARK: - Helper Method
    
    func AddIncome()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddTransactionVC") as! AddTransactionVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func budgetAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetViewController") as! BudgetViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    func setGradient()
    {
        let gradient = CAGradientLayer()
        gradient.frame = headerView.bounds
        gradient.colors = [FirstColor.cgColor,SecondColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        headerView.layer.addSublayer(gradient)
        
        let gradient2 = CAGradientLayer()
        gradient2.frame = headerView.bounds
        gradient2.colors = [FirstColor.cgColor,SecondColor.cgColor]
        gradient2.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient2.endPoint = CGPoint(x: 1.0, y: 0.5)
        TopheaderView.layer.addSublayer(gradient2)
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            
            let window = self.currencyView.window?.frame {
            let viewHeight = self.currencyView.frame.height
            self.currencyView.frame = CGRect(x: self.currencyView.frame.origin.x,
                                             y: self.currencyView.frame.origin.y,
                                             width: self.currencyView.frame.width,
                                             height: viewHeight - keyboardSize.height)
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.currencyView.frame.height
            self.currencyView.frame = CGRect(x: self.currencyView.frame.origin.x,
                                             y: self.currencyView.frame.origin.y,
                                             width: self.currencyView.frame.width,
                                             height: viewHeight + keyboardSize.height)
        }
        
    }
    
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
            print("Currency Symbol : \(currencySymbol)")
        }
        
        return currencySymbol
    }
    
    
    // MARK: - UIScrollView Delegate Methods
    /*
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if(scrollView == scrvSwipeView)
        {
            if(scrollView.contentOffset.x == 0)
            {
//                btnCurrentMonth.titleLabel?.textColor = UIColor.black
//                btnClosed.titleLabel?.textColor  = UIColor.lightGray
//                btnSold.titleLabel?.textColor = UIColor.lightGray
//                btnDraft.titleLabel?.textColor = UIColor.lightGray
                
                lblUnderline.frame = CGRect(origin: CGPoint(x:btnCurrentMonth.frame.minX, y:self.lblUnderline.frame.origin.y),  size: CGSize(width: self.lblUnderline.frame.size.width, height: self.lblUnderline.frame.size.height))
                self.scrvSwipeView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            }
            else if(scrollView.contentOffset.x ==  UIScreen.main.bounds.size.width * 1)
            {
                
//                btnCurrent.titleLabel?.textColor = UIColor.lightGray
//                btnClosed.titleLabel?.textColor  = UIColor.black
//                btnSold.titleLabel?.textColor = UIColor.lightGray
//                btnDraft.titleLabel?.textColor = UIColor.lightGray
                
                lblUnderline.frame = CGRect(origin: CGPoint(x:btnSchedual.frame.minX, y:self.lblUnderline.frame.origin.y), size: CGSize(width: self.lblUnderline.frame.size.width, height: self.lblUnderline.frame.size.height))
                self.scrvSwipeView.setContentOffset(CGPoint(x: (UIScreen.main.bounds.size.width * 1), y: 0), animated: true)
            }
        }
    }
 */
    
    //MARK: - Tableview Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(tableView == tblcurrency)
        {
            return 1
        }
        else if tableView == tblTransactionReminder
        {
            if(strFilter == "category")
            {
                return sectionsReminderCat.count
            }
            else
            {
                return sectionsReminder.count
            }
        }
        else
        {
            if(strFilter == "category")
            {
                return sectionsCat.count
            }
            else
            {
                return sections.count
            }
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == tblTransactionReminder
        {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! TransactionHeaderCell
            
            headerCell.viewByDate.backgroundColor = UIColor.clear
            
            if(strFilter == "category")
            {
                headerCell.backgroundColor = UIColor.white
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                headerCell.lblCategoryTotalAmount.formatBlock = {(_ value: CGFloat) -> String in
                    let formatted: String? = formatter.string(for: Double(value))
                    return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
                }
                
                
                headerCell.img_Category.isHidden = false
                headerCell.lblCategoryName.isHidden = false
                headerCell.lblCategoryName.font = tableMainFont
                headerCell.lblCategoryTotalAmount.isHidden = false
                headerCell.viewByDate.isHidden = true
                headerCell.lblCategoryName.text = sectionsReminderCat[section].categoryName
                headerCell.lblCategoryTotalAmount.font = tableMainFont
                
                headerCell.lblCategoryTotalAmount.method = .easeOut
                
                headerCell.lblCategoryTotalAmount.count(from: 0, to: CGFloat(Double(sectionsReminderCat[section].amount!)!), withDuration: 0.0)
                
                //            headerCell.lblCategoryName.textColor = Constant.tableSubTextColorCode
                headerCell.lblCategoryName.textColor = Constant.tableTextColorCode
                headerCell.lblCategoryTotalAmount.textColor = Constant.tableTextColorCode
                //            headerCell.lblCategoryTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(sectionsCat[section].amount!)"//sectionsCat[section].amount
                headerCell.img_Category.image = UIImage(named: sectionsReminderCat[section].categoryImage)
            }
            else
            {
                headerCell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                headerCell.lblTotalAmount.formatBlock = {(_ value: CGFloat) -> String in
                    let formatted: String? = formatter.string(for: Double(value))
                    return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
                }
                
                headerCell.img_Category.isHidden = true
                headerCell.lblCategoryName.isHidden = true
                headerCell.lblCategoryTotalAmount.isHidden = true
                headerCell.viewByDate.isHidden = false
                headerCell.lblDate.text = sectionsReminder[section].date
                headerCell.lblDate.font = tableHeaderFont
                headerCell.lblTotalAmount.font = tableHeaderFont
                headerCell.lblDate.textColor = Constant.tableSubTextColorCode
                headerCell.lblTotalAmount.textColor = Constant.tableTextColorCode
                //            headerCell.lblTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(sections[section].amount!)"//sections[section].amount
                
                headerCell.lblTotalAmount.method = .easeOut
                
                headerCell.lblTotalAmount.count(from: 0, to: CGFloat(Double(sectionsReminder[section].amount!)!), withDuration: 0.0)
            }
            return headerCell
        }
        else
        {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! TransactionHeaderCell
            
            headerCell.viewByDate.backgroundColor = UIColor.clear
            
            if(strFilter == "category")
            {
                headerCell.backgroundColor = UIColor.white
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                headerCell.lblCategoryTotalAmount.formatBlock = {(_ value: CGFloat) -> String in
                    let formatted: String? = formatter.string(for: Double(value))
                    return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
                }
                
                
                headerCell.img_Category.isHidden = false
                headerCell.lblCategoryName.isHidden = false
                headerCell.lblCategoryName.font = tableMainFont
                headerCell.lblCategoryTotalAmount.isHidden = false
                headerCell.viewByDate.isHidden = true
                headerCell.lblCategoryName.text = sectionsCat[section].categoryName
                headerCell.lblCategoryTotalAmount.font = tableMainFont
                
                headerCell.lblCategoryTotalAmount.method = .easeOut
                
                headerCell.lblCategoryTotalAmount.count(from: 0, to: CGFloat(Double(sectionsCat[section].amount!)!), withDuration: 0.0)
                
                //            headerCell.lblCategoryName.textColor = Constant.tableSubTextColorCode
                headerCell.lblCategoryName.textColor = Constant.tableTextColorCode
                headerCell.lblCategoryTotalAmount.textColor = Constant.tableTextColorCode
                //            headerCell.lblCategoryTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(sectionsCat[section].amount!)"//sectionsCat[section].amount
                headerCell.img_Category.image = UIImage(named: sectionsCat[section].categoryImage)
            }
            else
            {
                headerCell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                headerCell.lblTotalAmount.formatBlock = {(_ value: CGFloat) -> String in
                    let formatted: String? = formatter.string(for: Double(value))
                    return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
                }
                
                headerCell.img_Category.isHidden = true
                headerCell.lblCategoryName.isHidden = true
                headerCell.lblCategoryTotalAmount.isHidden = true
                headerCell.viewByDate.isHidden = false
                headerCell.lblDate.text = sections[section].date
                headerCell.lblDate.font = tableHeaderFont
                headerCell.lblTotalAmount.font = tableHeaderFont
                headerCell.lblDate.textColor = Constant.tableSubTextColorCode
                headerCell.lblTotalAmount.textColor = Constant.tableTextColorCode
                //            headerCell.lblTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(sections[section].amount!)"//sections[section].amount
                
                headerCell.lblTotalAmount.method = .easeOut
                
                headerCell.lblTotalAmount.count(from: 0, to: CGFloat(Double(sections[section].amount!)!), withDuration: 0.0)
            }
            return headerCell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(tableView == tblcurrency)
        {
            return 0
        }
        else{
            return 30.0
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tblcurrency)
        {
            if(searchActive) {
                return filtered.count
            }
            return countryArray.count
        }
        else if tableView == tblTransactionReminder
        {
            if(strFilter == "category")
            {
                return sectionsReminderCat[section].items.count
            }
            else
            {
                return sectionsReminder[section].items.count
            }
        }
        else
        {
            if(strFilter == "category")
            {
                return sectionsCat[section].items.count
            }
            else
            {
                return sections[section].items.count
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(tableView == tblcurrency)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath as IndexPath)
            
            cell.imageView?.layer.cornerRadius = 5
            cell.imageView?.layer.masksToBounds = true
            
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                        
                    {
                        cell.imageView?.image = UIImage(named: "\(temp.alpha2).png")
                        cell.textLabel?.text = temp.name
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = countryname.currency
                
                cell.imageView?.image = UIImage(named: "\(countryname.alpha2).png")
                cell.textLabel?.text = countryname.name
                
            }
            
            
            //getCurrencySymbol(currencyCode: countryname.name)
            
            return cell
        }
        else if tableView == tblTransactionReminder
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subCell") as! TransactionCell
            
            var temp = NSMutableDictionary()
            
            if(strFilter == "category")
            {
                cell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
                temp = sectionsReminderCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                cell.lblSubCategoryName.textColor = Constant.tableSubTextColorCode
                cell.lblSubCategoryName.font = tableHeaderFont
                cell.lblAmount.font = tableHeaderFont
            }
            else
            {
                cell.backgroundColor = UIColor.white
                temp = sectionsReminder[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                cell.lblSubCategoryName.textColor = Constant.tableTextColorCode
                cell.lblSubCategoryName.font = tableMainFont
                cell.lblAmount.font = tableMainFont
            }
            
            cell.lblSubCategoryName.text = temp["subCatName"] as? String
            
            cell.lblDescription.textColor = Constant.tableSubTextColorCode
            
            cell.lblDescription.text = temp["note"] as? String
            cell.lblDescription.font = tableSubFont
            cell.img_subCategory.image = UIImage(named: temp["subCatImage"] as! String)
            cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(temp["amount"] as! String)"
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            cell.lblAmount.formatBlock = {(_ value: CGFloat) -> String in
                let formatted: String? = formatter.string(for: Double(value))
                return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
            }
            cell.lblAmount.method = .easeOut
            
            cell.lblAmount.count(from: 0, to: CGFloat(Double(temp["amount"] as! String)!), withDuration: 0.0)
            
            if(temp["setType"] as! String == "0")
            {
                cell.lblAmount.textColor = Constant.expenseColorCode
            }
            else
            {
                cell.lblAmount.textColor = Constant.incomeColorCode
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subCell") as! TransactionCell
            
            var temp = NSMutableDictionary()
            
            if(strFilter == "category")
            {
                cell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
                temp = sectionsCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                cell.lblSubCategoryName.textColor = Constant.tableSubTextColorCode
                cell.lblSubCategoryName.font = tableHeaderFont
                cell.lblAmount.font = tableHeaderFont
            }
            else
            {
                cell.backgroundColor = UIColor.white
                temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                cell.lblSubCategoryName.textColor = Constant.tableTextColorCode
                cell.lblSubCategoryName.font = tableMainFont
                cell.lblAmount.font = tableMainFont
            }
            
            cell.lblSubCategoryName.text = temp["subCatName"] as? String
            
            cell.lblDescription.textColor = Constant.tableSubTextColorCode
            
            cell.lblDescription.text = temp["note"] as? String
            cell.lblDescription.font = tableSubFont
            cell.img_subCategory.image = UIImage(named: temp["subCatImage"] as! String)
            cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(temp["amount"] as! String)"
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            cell.lblAmount.formatBlock = {(_ value: CGFloat) -> String in
                let formatted: String? = formatter.string(for: Double(value))
                return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
            }
            cell.lblAmount.method = .easeOut
            
            cell.lblAmount.count(from: 0, to: CGFloat(Double(temp["amount"] as! String)!), withDuration: 0.0)
            
            if(temp["setType"] as! String == "0")
            {
                cell.lblAmount.textColor = Constant.expenseColorCode
            }
            else
            {
                cell.lblAmount.textColor = Constant.incomeColorCode
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(tableView == tblcurrency)
        {
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                    {
                        let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                        strCurrency = currencyCode
                        UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = getCurrencySymbol(currencyCode: countryname.currency)
                strCurrency = currencyCode
                UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                print(currencyCode)
            }
            removeAnimate1()
            viewWillAppear(true)
        }
        else
        {
            if(strFilter == "category")
            {
                if sectionsCat[indexPath.section].categoryName == "Split Amount"
                {
                    
                }
                else
                {
                    let detailVC = storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController
                    var temp = NSMutableDictionary()
                    
                    temp = sectionsCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                    detailVC.transactionDict = temp
                    detailVC.passingView = "edit"
                    
                    if(temp["setType"] as! String == "0")
                    {
                        detailVC.strCategoryType = "Expense"
                    }
                    else
                    {
                        detailVC.strCategoryType = "Income"
                    }
                    present(detailVC, animated: true, completion: nil)
                }
            }
            else
            {
                if sections[indexPath.section].date == "Split Amount"
                {
                    
                }
                else
                {
                    var temp = NSMutableDictionary()
                    temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                    
                    if("\(temp["categoryID"]!)" == BillCategoryID){
                        let BillVC = storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
                        BillVC.isEditBill = true
                        BillVC.billDetail = temp
                        self.navigationController?.pushViewController(BillVC, animated: true)
                        
                    }else if("\(temp["categoryID"]!)" == LoanCategoryID){
                        print("Loan Transaction Tap")
                    }else{
                        let detailVC = storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController
                        detailVC.transactionDict = temp
                        detailVC.passingView = "edit"
                        
                        if(temp["setType"] as! String == "0")
                        {
                            detailVC.strCategoryType = "Expense"
                        }
                        else
                        {
                            detailVC.strCategoryType = "Income"
                        }
                        present(detailVC, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    }
    
    
    //MARK: - CollectionView Delegate And Datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listMonth.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cardCell", for: indexPath) as! cardCell
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        cell.lblAccountValue.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        cell.lblIncomeValue.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        cell.lblExpenceValue.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        
        cell.lblMonth.text = (listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "PreviousMonth") as? String
        
        cell.lblAccountValue.method = .easeOut
        
        cell.lblAccountValue.count(from: 0, to: CGFloat(Double((listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "avaliableBal") as! String)!), withDuration: 0.0)
        
        cell.lblIncomeValue.method = .easeOut
        
        cell.lblIncomeValue.count(from: 0, to: CGFloat(Double((listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "sumOfIncome") as! String)!), withDuration: 0.0)
        
        cell.lblExpenceValue.method = .easeOut
        
        cell.lblExpenceValue.count(from: 0, to: CGFloat(Double((listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "sumOfExpense") as! String)!), withDuration: 0.0)
        
        
//        cell.lblAccountValue.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \((listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "avaliableBal") as! String)"
//        cell.lblIncomeValue.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \((listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "sumOfIncome") as! String)"
//        cell.lblExpenceValue.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \((listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "sumOfExpense") as! String)"
        cell.viewCard.layer.cornerRadius = 8.0
        cell.viewCard.layer.masksToBounds = true
        cell.layer.cornerRadius = 8.0
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        removeAnimate()
        monthWiseDate = (listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "previousMonthDate") as! Date
        if(strFilter == "category")
        {
            filterByCategory(selectDate: (listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "previousMonthDate") as! Date)
            filterByCategoryReminder(selectDate: (listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "previousMonthDate") as! Date)
        }
        else
        {
            filterByDate(selectDate: (listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "previousMonthDate") as! Date)
            filterByDateReminder(selectDate: (listMonth[indexPath.row] as! NSMutableDictionary).value(forKey: "previousMonthDate") as! Date)
        }
        
        setUpValue(selectDate: monthWiseDate)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize: CGSize = collectionView.bounds.size
        
        cellSize.width -= collectionView.contentInset.left * 2
        cellSize.width -= collectionView.contentInset.right * 2
        cellSize.height = cellSize.width
        
        return cellSize
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCellsLayout()
    }
    
    func updateCellsLayout()  {
        
        let centerX = cardCollection.contentOffset.x + (cardCollection.frame.size.width)/2
        for cell in cardCollection.visibleCells {
            
            var offsetX = centerX - cell.center.x
            if offsetX < 0 {
                offsetX *= -1
            }
            cell.transform = CGAffineTransform.identity
            let offsetPercentage = offsetX / (view.bounds.width * 2.7)
            let scaleX = 1-offsetPercentage
            cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
            
        }
    }
    
    // MARK: - UIViewControllerPreviewingDelegate methods
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        let point : CGPoint = self.tblTransactionHistory.convert(location, from: self.view)
        
        guard let indexPath = self.tblTransactionHistory.indexPathForRow(at: point) else { return nil }
        
        guard let cell = tblTransactionHistory.cellForRow(at: indexPath) else { return nil }
        
        guard let detailVC = storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as? NewCategoryViewController else { return nil }
        
        var temp = NSMutableDictionary()
        
        if(strFilter == "category")
        {
            temp = sectionsCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        }
        else
        {
            temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        }
        
        
        detailVC.preferredContentSize = CGSize(width: 0.0, height: 500)
        detailVC.transactionDict = temp
        detailVC.passingView = "edit"
        previewingContext.sourceRect = cell.frame
        
        return detailVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
//        show(viewControllerToCommit, sender: self)
        present(viewControllerToCommit, animated: true, completion: nil)
        
    }

    
    // MARK: - SearchBar Delegate Method
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = countryNewArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tblcurrency.reloadData()
        
    }
    
    //MARK: - Action Method
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func btnCurrentMonthAction(_ sender: UIButton)
    {
        self.lblUnderline.frame = CGRect(origin: CGPoint(x:btnCurrentMonth.frame.minX, y:self.lblUnderline.frame.origin.y),  size: CGSize(width: self.lblUnderline.frame.size.width, height: self.lblUnderline.frame.size.height))
        self.scrvSwipeView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func btnSchedualAction(_ sender: UIButton)
    {
        self.lblUnderline.frame = CGRect(origin: CGPoint(x:btnSchedual.frame.minX, y:self.lblUnderline.frame.origin.y), size: CGSize(width: self.lblUnderline.frame.size.width, height: self.lblUnderline.frame.size.height))
        self.scrvSwipeView.setContentOffset(CGPoint(x: (UIScreen.main.bounds.size.width * 1), y: 0), animated: true)
    }
    
    
    @IBAction func btnCardAction(_ sender: UIButton) {
        popTip.hide()
        filterBool = false
        cardCollection.contentOffset.x = 0
        showAnimate()
    }
    @IBAction func btnFilterAction(_ sender: UIButton)
    {
        if(filterBool == false)
        {
            
            popTip.showText("This is a poptip!", direction: AMPopTipDirection.down, maxWidth: 200, in: self.view, fromFrame: btnFilter.frame)
            filterBool = true
        }
        else
        {
            popTip.hide()
            filterBool = false
        }
    }
    
    @IBAction func btnFilterByDate(_ sender: UIButton)
    {
        strFilter = "date"
        filterByDate(selectDate: monthWiseDate)
        filterByDateReminder(selectDate: monthWiseDate)
        popTip.hide()
        filterBool = false
    }
    
    @IBAction func btnFilterByCategory(_ sender: UIButton)
    {
        strFilter = "category"
        filterByCategory(selectDate: monthWiseDate)
        filterByCategoryReminder(selectDate: monthWiseDate)
        popTip.hide()
        filterBool = false
    }
    
    //MARK: - Helper Method
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        popTip.hide()
        filterBool = false
        
        let touch = touches.first!
        if touch.view != cardCollection {
            removeAnimate()
        }
        
    }
    
    func showAnimate()
    {
        visualBlur.isHidden = false
        visualBlur.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//        bgView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
//            self.bgView.alpha = 0.7
            self.visualBlur.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.visualBlur.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.visualBlur.isHidden = true
            }
        });
    }
    
    func showAnimate1()
    {
        bgView.isHidden = false
        currencyView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        currencyView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        currencyView.alpha = 0.0;
        tblcurrency.delegate = self
        tblcurrency.dataSource = self
        tblcurrency.reloadData()
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.currencyView.alpha = 1.0
            self.currencyView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate1()
    {
        filtered = []
        searchActive = false;
        currencySearchBar.text = ""
        self.currencySearchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.25, animations: {
            self.currencyView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.currencyView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.currencyView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }

}

