//
//  BudgetDetailViewController.swift
//  Created by Nectarbits on 21/07/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds


class BudgetDetailViewController: UIViewController, JBDatePickerViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate,GADBannerViewDelegate, UITextFieldDelegate
{
    
//    @IBOutlet var lblCategory: UILabel!
    
    @IBOutlet var HeaderView : UIView!
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var btnBudgetCycle: UIButton!
    @IBOutlet var btnStartDate: UIButton!
    @IBOutlet var btnBudgetHistory: UIButton!
    @IBOutlet var btnRemoveBudget: UIButton!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet var budgetscrollView : UIScrollView!
    @IBOutlet var btnDaily: UIButton!
    @IBOutlet var btnWeekly: UIButton!
    @IBOutlet var btnMonthly: UIButton!
    @IBOutlet var clevBudget: UICollectionView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var budgetAmountView : UIView!
    @IBOutlet var budgetDateView : UIView!
    @IBOutlet var budgetRepeatTypeView : UIView!
    @IBOutlet var budgetCollectionView : UIView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnDoneHeader: UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet var lblNoCategory: UILabel!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    
    var collectionView: UICollectionView!
    var dateToSelect: Date!
    var categoryArray : NSMutableArray = []
    var dateObject = Date()
    var categoryInfo = Category()
    var budgetInfo = Budget()
    var isBudget = 0
    var isHide = 0
    var info = NSMutableDictionary()
    var isComeFromHistory = 0
    var strBudgetCycle = ""
    var strCategoryID = ""
    var viewMode = ""
    var passingDate : Date!
 
//MARK:- Views Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        
        IQKeyboardManager.sharedManager().enable = true
        
        HeaderView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        lblHeaderTitle.font = headerTitleFont
        lblHeaderTitle.textColor = UIColor.white
        
        btnDoneHeader.setTitleColor(UIColor.white, for: .normal)
        
        doInitializationSetting()
        getCategoryData()
        
        lblNoCategory.isHidden = true
        if(categoryArray.count == 0)
        {
            lblNoCategory.isHidden = false
            clevBudget.isHidden = true
        }else
        {
            lblNoCategory.isHidden = true
            clevBudget.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        budgetAmountView.viewShadow()
        budgetDateView.viewShadow()
        budgetRepeatTypeView.viewShadow()
        budgetCollectionView.viewShadow()
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    func getCategoryData()
    {
        categoryArray = []
        
        let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@","1","0")
        
        if viewMode == "edit"
        {
            for i in 0..<categoryList.count
            {
                let dict = NSMutableDictionary()
                dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
                dict.setValue(categoryList[i]["name"]!, forKey: "name")
                dict.setValue(categoryList[i]["image"]!, forKey: "image")
                dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
                dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
                dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
                dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
                if (info["categoryID"] as? String)! == categoryList[i]["categoryID"] as! String
                {
                    dict.setValue(true, forKey: "isCell")
                    strCategoryID = categoryList[i]["categoryID"] as! String
                }
                else
                {
                    dict.setValue(false, forKey: "isCell")
                }
                categoryArray.add(dict)
            }
        }
        else
        {
            for i in 0..<categoryList.count
            {
                let dict = NSMutableDictionary()
                dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
                dict.setValue(categoryList[i]["name"]!, forKey: "name")
                dict.setValue(categoryList[i]["image"]!, forKey: "image")
                dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
                dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
                dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
                dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
                dict.setValue(false, forKey: "isCell")
                
                let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(categoryList[i]["categoryID"] as! String)!)
                if budgetUpdate.count == 0
                {
                    categoryArray.add(dict)
                }
                else
                {
                    
                }
            }
        }
        print(categoryArray)
        clevBudget.reloadData()
    }
    
// MARK:- CollectionView Method
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier", for: indexPath) as! BudgetCollectionCell
//        cell.imgvBudget?.image = UIImage(named: self.aImgArr[indexPath.row]["Image"] as! String)
        cell.categoryImage.image = UIImage(named: (self.categoryArray[indexPath.row] as! NSDictionary).value(forKey: "image") as! String)
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
//        cell.bgView.layer.cornerRadius = 30
//        cell.bgView.layer.masksToBounds = true

 
        cell.lblCatName.text =  (self.categoryArray[indexPath.row] as! NSDictionary).value(forKey: "name") as? String
        
        let isCellSelected : Bool = (self.categoryArray[indexPath.row] as! NSDictionary).value(forKey: "isCell") as! Bool
        print(cell.lblCatName.frame)
        if isCellSelected {
           // cell.bgView.backgroundColor = UIColor.blue
            print(cell.imgvBudget.frame)
            cell.imgvBudget.layer.masksToBounds = true
            cell.imgvBudget.layer.cornerRadius = 25
            cell.imgvBudget.layer.borderWidth = 2
            cell.imgvBudget.layer.borderColor = UIColor.blue.cgColor
        
        }
        else {
             //cell.bgView.backgroundColor = UIColor.clear
            cell.imgvBudget.layer.masksToBounds = true
            cell.imgvBudget.layer.cornerRadius = 0
            cell.imgvBudget.layer.borderWidth = 0
            cell.imgvBudget.layer.borderColor = UIColor.clear.cgColor
        }
            return cell
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
    }

    
//MARK:- Action Methods
    @IBAction func btnHistoryTap(_ sender: Any)
    {
        
    }
    @IBAction func btnDailyTap(_ sender: Any)
    {
        btnDaily.isSelected = true
        btnWeekly.isSelected = false
        btnMonthly.isSelected = false
        
        self.btnDaily.setTitle("Daily", for: .normal)
        strBudgetCycle = "Daily"
    }
    
    @IBAction func btnWeeklyTap(_ sender: Any)
    {
        btnDaily.isSelected = false
        btnWeekly.isSelected = true
        btnMonthly.isSelected = false

        self.btnWeekly.setTitle("Weekly", for: .normal)
        strBudgetCycle = "Weekly"
    }
    
    @IBAction func btnMonthlyTap(_ sender: Any)
    {
        btnDaily.isSelected = false
        btnWeekly.isSelected = false
        btnMonthly.isSelected = true

        self.btnMonthly.setTitle("Monthly", for: .normal)
        strBudgetCycle = "Monthly"
    }
    
    @IBAction func btnStartDateTap(_ sender: Any)
    {
        self.view.endEditing(true)
        showAnimate()
    }
    
    @IBAction func btnRemoveBudgetTap(_ sender: Any)
    {
        let workouts = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(categoryInfo.categoryID)!)
        
        for j in 0..<workouts.count
        {
            if workouts.first != nil
            {
                uiRealm.beginWrite()
                uiRealm.delete(workouts)
                do
                {
                    try uiRealm.commitWrite()
                }
                catch
                {
                    
                }
                
                
            }
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func btnDoneTap(_ sender: Any)
    {
        if viewMode == "edit"
        {
            if(txtAmount.text == "")
            {
                self.alertView(alertMessage: AlertString.BudgetAmount)
                return
            }
            
            let workouts = uiRealm.objects(Budget.self).filter("mainCatID = %@", Int(strCategoryID)!)
            
            if let workout = workouts.first
            {
                var type = 1
                
                if(strBudgetCycle == "Daily")
                {
                    type =  1
                }
                else if(strBudgetCycle == "Weekly")
                {
                    type =  2
                }
                else if(strBudgetCycle == "Monthly")
                {
                    type =  3
                }
                
                try! uiRealm.write
                {
                    workout.date = passingDate//dateObject
                    workout.originalDate = passingDate//dateObject
                    workout.cycleType = type
                    workout.budgetAmount = Double(txtAmount.text!)!
                    if(workout.budgetAmount == 0)
                    {
                        self.alertView(alertMessage: "Do Not Add Zero Value.")
                        return
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else if(txtAmount.text == "0")
        {
            self.alertView(alertMessage: "Do Not Set Zero Value")
            return
        }
        else
        {
            if strCategoryID == ""
            {
                self.alertView(alertMessage: AlertString.SelectCategory)
                return
            }
            
            if(txtAmount.text == "")
            {
                self.alertView(alertMessage: AlertString.BudgetAmount)
                return
            }
            if (txtAmount.text == "0")
            {
                self.alertView(alertMessage: "Do Not Set Zero Value")
                return
            }
            
            if strBudgetCycle == ""
            {
                self.alertView(alertMessage: AlertString.SelectCycle)
                return
            }
            
            var budgetInfo = Budget()
            var autoCatId = uiRealm.objects(Budget.self).max(ofProperty: "id") as Int?
            
            if(autoCatId == nil)
            {
                autoCatId = 1
            }
            else
            {
                autoCatId = autoCatId! + 1
            }
            
            var type = 1
            
            if(strBudgetCycle == "Daily")
            {
                type =  1
            }
            else if(strBudgetCycle == "Weekly")
            {
                type =  2
            }
            else if(strBudgetCycle == "Monthly")
            {
                type =  3
            }
                        
            let tempCategory = uiRealm.objects(Transaction.self).filter("categoryID = %@ AND date = %@ AND setType = %@",strCategoryID,passingDate,"0")
            
            for i in 0..<tempCategory.count
            {
                let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(tempCategory[i].categoryID)!)
                
                if let budgetWorkout = budgetUpdate.first
                {
                    try! uiRealm.write
                    {
                        budgetWorkout.currentAmount = budgetWorkout.currentAmount + Double(tempCategory[i].amount)!
                    }
                }
            }
            
            let temp = uiRealm.objects(Category.self).filter("categoryID = %@ AND isCategory = %@",strCategoryID,"1")
            
            budgetInfo = Budget(value: [autoCatId!,
                                        NSInteger(strCategoryID)!,
                                        [],//temp
                                        Double(txtAmount.text!)!,
                                        0,
                                        passingDate!,
                                        type,
                                        passingDate!])
            
            try! uiRealm.write { () -> Void in
                uiRealm.add(budgetInfo)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func btnBackTap(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Helper Method
    
    func getAction(sender:UITableViewCell)->Void {
        if(sender.tag == 0) {
            bgView.backgroundColor = UIColor.blue
        }
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        
       /* var aDict : [String: Any] = aImgArr[sender.tag]
      //  let isCellSelected : Bool = (self.aImgArr[sender.tag]["isCell"] != nil)
        
        let isCellSelected : Bool = self.aImgArr[sender.tag]["isCell"]! as! Bool
        if isCellSelected {
            aDict["isCell"] = false
        }
        else {
            aDict["isCell"] = true
        }
        
        aImgArr.remove(at: sender.tag)
        aImgArr.insert(aDict, at: sender.tag)
       
        let aIndexPath = IndexPath(row: sender.tag, section: 0)
        clevBudget.reloadItems(at: [aIndexPath])*/
        
        if viewMode == "edit"
        {
            self.alertView(alertMessage: AlertString.NotChangeCategory)
        }
        else
        {
            for i in 0...categoryArray.count-1 {
                
                if sender.tag == i {
                    
                    
                    (categoryArray[i] as! NSDictionary).setValue(true, forKey: "isCell")
                    strCategoryID = (categoryArray[i] as! NSDictionary).value(forKey:"categoryID") as! String
                    print("Category Id:- \(strCategoryID)")
                }
                else {
                    
                    (categoryArray[i] as! NSDictionary).setValue(false, forKey: "isCell")
                }
                print(i)
            }
            
            clevBudget.reloadData()
        }
    }
    
    func doInitializationSetting()
    {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
        
        txtAmount.addDoneButtonToKeyboard(myAction:  #selector(txtAmount.resignFirstResponder))

        if (isHide == 0)
        {
            btnDone.isHidden = false
        }
        else
        {
            btnDone.isHidden = true
        }
        
        if(isComeFromHistory == 1)
        {
            btnDoneHeader.isHidden = false
        }
        else
        {
            btnDoneHeader.isHidden = true
        }
        
//        budgetscrollView.dropShadow()
        datePickerView.delegate = self
        
//        btnBudgetHistory.isHidden = true
        
        //        imgv.image = UIImage(named: categoryInfo.image)
        //        imgv.layer.borderWidth = 0.3
        //        imgv.layer.borderColor = Constant.tableTextColorCode.cgColor
        //        imgv.layer.cornerRadius = imgv.frame.size.width/2
        //        imgv.layer.masksToBounds = true
        
//        budgetscrollView.layer.cornerRadius = 10
//        budgetscrollView.layer.masksToBounds = true
        
        if(isBudget > 0)
        {
            btnRemoveBudget.isHidden = false
            
            //            lblCategory.text = info["name"] as? String
            
            self.btnStartDate.setTitle(String.convertFormatOfDate(date: budgetInfo.date), for: .normal)
            passingDate = budgetInfo.date
            if(budgetInfo.cycleType == 1)
            {
                self.btnDaily.setTitle("Daily", for: .normal)
                strBudgetCycle = "Daily"
                btnDaily.isSelected = true
            }
            if(budgetInfo.cycleType == 2)
            {
                self.btnWeekly.setTitle("Weekly", for: .normal)
                strBudgetCycle = "Weekly"
                btnWeekly.isSelected = true
            }
            if(budgetInfo.cycleType == 3)
            {
                self.btnMonthly.setTitle("Monthly", for: .normal)
                strBudgetCycle = "Monthly"
                btnMonthly.isSelected = true
            }
            txtAmount.text = String(format: "%.2f",budgetInfo.budgetAmount)
            
        }
        else
        {
            btnRemoveBudget.isHidden = true
            //            lblCategory.text = info["name"] as? String
            self.btnStartDate.setTitle(String.convertFormatOfDate(date: passingDate), for: .normal)
//            passingDate = String.convertFormatOfStringToDate(date: budgetInfo.date as! String)


            //            self.btnBudgetCycle.setTitle("Daily", for: .normal)
        }
        
    }
    
// MARK: - Touch method
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            let contentInset:UIEdgeInsets = .zero
//            self.budgetscrollView.contentInset = contentInset
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
        
    }
    //MARK: - JBDatePicker Delegate
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date
        {
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            dateObject = selectedDate
            self.btnStartDate.setTitle(stringDate, for: .normal)
            
            removeAnimate()
        }
    }
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView)
    {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return passingDate//Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fulltext = textField.text! + (string)
        if fulltext.count == 2 {
            if (((fulltext as? NSString)?.substring(to: 1)) == "0") && !(fulltext == "0.") {
                textField.text = (fulltext as NSString).substring(with: NSRange(location: 1, length: 1))
                return false
            }
        }
        return true
    }
}

//extension String
//{
//    static func convertFormatOfDate(date: Date) -> String!
//    {
//        // Destination format :
//        let dateDestinationFormat = DateFormatter()
//        dateDestinationFormat.timeStyle = .none
//        dateDestinationFormat.dateStyle = .medium
//
//        // Convert new NSDate created above to String with the good format
//        let dateFormated = dateDestinationFormat.string(from: date)
//        
//        return dateFormated
//    }
//}

