//
//  CurrencyConverterViewController.swift
//  Outlays
//
//  Created by Cools on 11/2/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class CurrencyConverterViewController: UIViewController,CalculatorDelegate,UITextFieldDelegate {
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var txtFirstCurrency: UITextField!
    @IBOutlet var txtSecondCurrency: UITextField!
    @IBOutlet var btnFirstCurrencyFlag: UIButton!
    @IBOutlet var btnSecondCurrencyFlag: UIButton!
    @IBOutlet var btnFirstCurrencySymbol: UIButton!
    @IBOutlet var btnSecondCurrencySymbol: UIButton!
    @IBOutlet var secondCurrencyView: UIView!
    @IBOutlet var firstCurrencyView: UIView!
    @IBOutlet var lblFirstCurrencyCode: UILabel!
    @IBOutlet var lblSecondCurrencyCode: UILabel!
    
    var firstCurrency : String = ""
    var secondCurrency : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        setKeyboard()
        lblHeaderTitle.font = headerTitleFont
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            let countryArray = (IsoCountries.allCountries as! NSArray).mutableCopy() as! NSMutableArray
            for i in 0..<countryArray.count
            {
                let temp = countryArray[i] as! IsoCountryInfo
                if(countryCode == temp.alpha2)
                    
                {
                    
                    btnFirstCurrencyFlag.setBackgroundImage(UIImage(named: "\(temp.alpha2).png"), for: .normal)
                    btnFirstCurrencySymbol.setTitle(temp.currency, for: .normal)
                    lblFirstCurrencyCode.text = getCurrencySymbol(currencyCode: temp.currency)
                    firstCurrency = temp.currency
                    
                    btnSecondCurrencyFlag.setBackgroundImage(UIImage(named: "\(temp.alpha2).png"), for: .normal)
                    btnSecondCurrencySymbol.setTitle(temp.currency, for: .normal)
                    lblSecondCurrencyCode.text = getCurrencySymbol(currencyCode: temp.currency)
                    secondCurrency = temp.currency
                    //                    getCurrencySymbol(currencyCode: temp.currency)
                }
            }
            
            getCurrentConverter(strFirstCurrency: firstCurrency, strsecondCurrency: secondCurrency)
            
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(CurrencyConverterViewController.showSpinningWheel(notification:)), name: NSNotification.Name(rawValue: "getCountryCurrency"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstCurrencyView.viewShadow()
        secondCurrencyView.viewShadow()
    }
    
    func getCurrentConverter(strFirstCurrency: String, strsecondCurrency: String)
    {
        let rechability = Reachability()!
        
        if rechability.isReachableViaWiFi || rechability.isReachableViaWWAN    {
            let config = URLSessionConfiguration.default // Session Configuration
            let session = URLSession(configuration: config) // Load configuration into Session
            let url = URL(string: "https://free.currencyconverterapi.com/api/v4/convert?q=\(strFirstCurrency)_\(strsecondCurrency)&compact=y")!
            
            let task = session.dataTask(with: url, completionHandler: {
                (data, response, error) in
                
                if error != nil {
                    
                    print(error!.localizedDescription)
                    
                } else {
                    
                    do {
                        
                        if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : AnyObject]
                        {
                            
                            //Implement your logic
                            print(json)
                            
                            DispatchQueue.main.async {
                                
                                self.txtFirstCurrency.text = "1"
                                
                                if json.count > 0
                                {
                                    self.txtSecondCurrency.text = String(describing: (json["\(strFirstCurrency)_\(strsecondCurrency)"] as! NSDictionary).value(forKey: "val")!)
                                }
                                else
                                {
                                    self.txtSecondCurrency.text = "1"
                                }
                                
                            }
                            
                        }
                    }
                    catch {
                        
                        print("error in JSONSerialization")
                        
                    }
                    
                }
                
            })
            task.resume()
        } else {
            alertView(alertMessage: AlertString.CurrencyConverter)
            self.txtFirstCurrency.text = "1"
            self.txtSecondCurrency.text = "1"
        }
        
    }
    
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
            print("Currency Symbol : \(currencySymbol)")
        }
        
        return currencySymbol
    }
    
    @objc func showSpinningWheel(notification: NSNotification) {
        let dict = notification.object as! NSMutableDictionary
        print(dict)
        
        if dict["currencyNumber"] as! String == "First"
        {
            btnFirstCurrencyFlag.setBackgroundImage(UIImage(named: dict["currencyFlag"] as! String), for: .normal)
            //            btnSecondCurrencyFlag
            firstCurrency = dict["currency"] as! String
            lblFirstCurrencyCode.text = dict["currencyCode"] as? String
            btnFirstCurrencySymbol.setTitle(dict["currency"]! as? String, for: .normal)
            //            btnSecondCurrencySymbol
        }
        else
        {
            btnSecondCurrencyFlag.setBackgroundImage(UIImage(named: dict["currencyFlag"] as! String), for: .normal)
            secondCurrency = dict["currency"] as! String
            lblSecondCurrencyCode.text = dict["currencyCode"] as? String
            btnSecondCurrencySymbol.setTitle(dict["currency"]! as? String, for: .normal)
        }
        
        if btnFirstCurrencySymbol.titleLabel?.text == "" || btnSecondCurrencySymbol.titleLabel?.text == "" {
            
        }
        else if btnFirstCurrencySymbol.titleLabel?.text != "" && btnSecondCurrencySymbol.titleLabel?.text != ""
        {
            print("Call Webservice")
            getCurrentConverter(strFirstCurrency: firstCurrency, strsecondCurrency: secondCurrency)
        }
        
        
    }
    
    func setKeyboard()
    {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 300)
        let keyboard = CalculatorKeyboard(frame: frame)
        keyboard.delegate = self
        keyboard.showDecimal = true
        txtFirstCurrency.inputView = keyboard
        txtFirstCurrency.delegate = self
        
        txtSecondCurrency.inputView = keyboard
        txtSecondCurrency.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtFirstCurrency.becomeFirstResponder()
    }
    
    // MARK: - RFCalculatorKeyboard delegate
    func calculator(_ calculator: CalculatorKeyboard, didChangeValue value: String) {
        
        if txtFirstCurrency.isFirstResponder
        {
            txtFirstCurrency.text = value
        }
        if (txtSecondCurrency.isFirstResponder)
        {
            txtSecondCurrency.text = value
        }
        
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func btnFirstCurrency(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CurrencyViewController") as! CurrencyViewController
        //        vc.currencyNumber = "First"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSecondCurrency(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CurrencyViewController") as! CurrencyViewController
        //        vc.currencyNumber = "Second"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
