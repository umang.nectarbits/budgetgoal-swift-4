//
//  AccountsViewController.swift
//  Outlays
//
//  Created by Cools on 11/14/17.
//  Copyright © 2017 Cools. All rights reserved.
//


import UIKit
import GoogleMobileAds

class AccountListCell: UITableViewCell {
    @IBOutlet var lblAccountName : UILabel!
    @IBOutlet var lblAccountTotalAmount : UILabel!
    @IBOutlet var lblAccountRemainAmount : UILabel!
    @IBOutlet var img_Account : UIImageView!
    @IBOutlet var BgView : UIView!

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblAccountName.font = tblMainTitleFont
        lblAccountTotalAmount.font = smallFont
        lblAccountRemainAmount.font = tblMainTitleFont
        
    }
}


class AccountsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,GADBannerViewDelegate {

    @IBOutlet var HeaderView : UIView!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet weak var tblAccountList : UITableView!
    @IBOutlet var btnTransfer : UIButton!
    @IBOutlet var btnAddAccount : UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var accountSelection = ""
    var accountArray = [[String:AnyObject]]()
    var passingDate : Date!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        HeaderView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        
        btnTransfer.setImage(UIImage(named: "ic_transfer")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnTransfer.tintColor = UIColor.white

        lblHeaderTitle.font = InnerHeadersFont
        lblHeaderTitle.textColor = HeaderTitleColor
        tblAccountList.estimatedRowHeight = 56.0
        
        tblAccountList.rowHeight = UITableViewAutomaticDimension
        
        tblAccountList.tableFooterView = UIView.init(frame: CGRect.zero)
        
        btnAddAccount.layer.masksToBounds = false
        btnAddAccount.layer.cornerRadius = btnAddAccount.frame.size.width/2
        btnAddAccount.layer.shadowColor = UIColor.lightGray.cgColor
        btnAddAccount.layer.shadowOpacity = 0.7
        btnAddAccount.layer.shadowOffset = CGSize(width: -1, height: 1)
        btnAddAccount.layer.shadowRadius = 1
        tblAccountList.estimatedRowHeight = 56.0
        tblAccountList.rowHeight = UITableViewAutomaticDimension
        
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
        getAccountData()
    }

    func getAccountData()
    {
        let puppies = uiRealm.objects(Account.self)
        accountArray = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["id"] = puppies[j]["id"]! as AnyObject
            dict1["name"] = puppies[j]["name"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["remainigAmount"] = puppies[j]["remainigAmount"]! as AnyObject
            dict1["date"] = String.convertFormatOfDate(date: puppies[j]["date"]! as! Date) as AnyObject
            dict1["isIncludeOnDashboard"] = puppies[j]["isIncludeOnDashboard"]! as AnyObject
            dict1["isDefaultAccount"] = puppies[j]["isDefaultAccount"]! as AnyObject
            
            accountArray.append(dict1)
        }
        print(accountArray)
        tblAccountList.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AccountListCell
        
        cell.lblAccountName.text = accountArray[indexPath.row]["name"] as? String
        cell.lblAccountTotalAmount.text = "Recounciled: \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(accountArray[indexPath.row]["amount"] as! Double))"
        cell.lblAccountRemainAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(accountArray[indexPath.row]["remainigAmount"] as! Double))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if accountSelection == ""
        {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAccountViewController") as! AddAccountViewController
            vc.passingView = "Edit"
            vc.passingDate = passingDate
            vc.editData = accountArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if accountSelection == "From"
        {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAccountData"), object: ["AccountDetail":accountArray[indexPath.row],"transferAccount":"From"])
            self.navigationController?.popViewController(animated: true)
        }
        else if accountSelection == "To"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAccountData"), object: ["AccountDetail":accountArray[indexPath.row],"transferAccount":"To"])
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
        
    }
    
    //MARK:- Action Method
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTransferAction(_ sender: UIButton)
    {
        if (accountArray.count >= 2)
        {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransferViewController") as! TransferViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
         self.alertView(alertMessage: AlertString.EnterAccount)
        }
    }
    
    @IBAction func btnAddAccountAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAccountViewController") as! AddAccountViewController
        vc.passingDate = passingDate
        vc.transferView = "Accounts"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
