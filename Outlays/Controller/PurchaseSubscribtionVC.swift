//
//  PurchaseSubscribtionVC.swift
//  Budget Goals
//
//  Created by harshesh on 04/06/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit
import StoreKit
import SVProgressHUD

class PurchaseSubscribtionVC: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    //Define InApp Purchase View
    @IBOutlet weak var btn1Month: UIButton!
    @IBOutlet weak var btn3Month: UIButton!
    @IBOutlet weak var btn1Year: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lbl1MonthPrice: UILabel!
    @IBOutlet weak var lbl3MonthPrice: UILabel!
    @IBOutlet weak var lbl1YearPrice: UILabel!
    
    @IBOutlet weak var view1month: UIView!
    @IBOutlet weak var view3month: UIView!
    @IBOutlet weak var view1Year: UIView!
    @IBOutlet var lblTrialDes: UILabel!
    
    let productIdentifiers =  Set(["com.budgetgoals.1month","com.budgetgoals.3month","com.budgetgoals.1year"])// DDIT
    var product: SKProduct?
    var productsArray = Array<SKProduct>()
    var strScreenTag = ""
    var progress = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestProductData()
        
        lbl1MonthPrice.textColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        
        view1month.layer.borderWidth = 2
        view1month.layer.borderColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1).cgColor
        btn1Month.isSelected = true
        
        lbl3MonthPrice.textColor = UIColor.lightGray
        view3month.layer.borderWidth = 2
        view3month.layer.borderColor = UIColor.lightGray.cgColor

        lbl1YearPrice.textColor = UIColor.lightGray
        view1Year.layer.borderWidth = 2
        view1Year.layer.borderColor = UIColor.lightGray.cgColor
        
        let image = UIImage(named: "ic_BackLeft")?.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(image, for: .normal)
        btnBack.tintColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        
        if (strScreenTag == "SetGoal"){
            let image = UIImage(named: "ic_cancel")?.withRenderingMode(.alwaysTemplate)
            btnBack.setImage(image, for: .normal)
            btnBack.tintColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        }else{
            let image = UIImage(named: "ic_BackLeft")?.withRenderingMode(.alwaysTemplate)
            btnBack.setImage(image, for: .normal)
            btnBack.tintColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        }

        // Do any additional setup after loading the view.
    }
   

    //MARK:- Action Method
    @IBAction func btnContinueScriptionTap(_ sender:UIButton) {
        
        if(btn1Month.isSelected)
        {
            progress = MBProgressHUD.showAdded(to: view, animated: true)
            self.buyProduct(product: self.productsArray[0])
        }
        else if(btn3Month.isSelected)
        {
            progress = MBProgressHUD.showAdded(to: view, animated: true)
            self.buyProduct(product: self.productsArray[2])
        }
        else
        {
            progress = MBProgressHUD.showAdded(to: view, animated: true)
            self.buyProduct(product: self.productsArray[1])
        }
    }
    
     @IBAction func btn7DayFreeTrialTap(_ sender:UIButton) {
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        self.buyProduct(product: self.productsArray[0])
    }
    
    @IBAction func btn1YearSubScriptionAction(_ sender:UIButton) {
        
        btn1Year.isSelected = true
        btn1Month.isSelected = false
        btn3Month.isSelected = false
        
        lbl1YearPrice.textColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        lbl1MonthPrice.textColor = UIColor.lightGray
        lbl3MonthPrice.textColor = UIColor.lightGray
        
        view1Year.layer.borderWidth = 2
        view1Year.layer.borderColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1).cgColor
        
        view3month.layer.borderWidth = 2
        view3month.layer.borderColor = UIColor.lightGray.cgColor
        
        view1month.layer.borderWidth = 2
        view1month.layer.borderColor = UIColor.lightGray.cgColor
        
        //        showProgressHUD()
        //        self.buyProduct(product: self.productsArray[1])
    }
    
    @IBAction func btn1MonthSubScriptionAction(_ sender:UIButton) {
        
        btn1Month.isSelected = true
        btn1Year.isSelected = false
        btn3Month.isSelected = false
        
        lbl1MonthPrice.textColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        lbl1YearPrice.textColor = UIColor.lightGray
        lbl3MonthPrice.textColor = UIColor.lightGray
        
        view1month.layer.borderWidth = 2
        view1month.layer.borderColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1).cgColor
        
        view3month.layer.borderWidth = 2
        view3month.layer.borderColor = UIColor.lightGray.cgColor
        
        view1Year.layer.borderWidth = 2
        view1Year.layer.borderColor = UIColor.lightGray.cgColor
        
        //        showProgressHUD()
        //        self.buyProduct(product: self.productsArray[0])
    }
    
    @IBAction func btn3MonthSubScriptionAction(_ sender:UIButton) {
        
        btn3Month.isSelected = true
        btn1Year.isSelected = false
        btn1Month.isSelected = false
        
        lbl3MonthPrice.textColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1)
        lbl1MonthPrice.textColor = UIColor.lightGray
        lbl1YearPrice.textColor = UIColor.lightGray
        
        view3month.layer.borderWidth = 2
        view3month.layer.borderColor = UIColor(red: 52.0/255.0, green: 66.0/255.0, blue: 116.0/255.0, alpha: 1).cgColor
        
        view1Year.layer.borderWidth = 2
        view1Year.layer.borderColor = UIColor.lightGray.cgColor
        
        view1month.layer.borderWidth = 2
        view1month.layer.borderColor = UIColor.lightGray.cgColor
        
        //        showProgressHUD()
        //        self.buyProduct(product: self.productsArray[2])
    }
    
    @IBAction func btnRestoreAction(_ sender:UIButton) {
        self.restorePurchases()
    }
    
    @IBAction func btnTerms_ConditionTap(_ sender:UIButton) {
        print("Terms & Condition Press")
    }
    
    @IBAction func btnPrivacyPolicyTap(_ sender:UIButton) {
        print("Privacy Policy")
    }

    func popView() {
        if (strScreenTag == "SetGoal"){
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnBackAction(_ sender:UIButton) {
        
        if (strScreenTag == "SetGoal"){
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK:- In App Purchase Delegate
    func requestProductData()
    {
        if SKPaymentQueue.canMakePayments()
        {
            let request = SKProductsRequest(productIdentifiers:self.productIdentifiers)
            request.delegate = self
            request.start()
            progress = MBProgressHUD.showAdded(to: view, animated: true)
            progress.mode = MBProgressHUDMode.indeterminate
        }
        else
        {
            let alert = UIAlertController(title: "In-App Purchases Not Enabled", message: "Please enable In App Purchase in Settings", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
                
                let url: URL? = URL(string: UIApplicationOpenSettingsURLString)
                if url != nil
                {
                    UIApplication.shared.openURL(url!)
                }
                
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        var products = response.products
        
        if (products.count != 0)
        {
            for i in 0 ..< products.count
            {
                self.product = products[i] as SKProduct
                
                let d : Double = (self.product?.price.doubleValue)!
                
                let price = String(format: "%.2f", d)
                
                print("price %@",price)
                
                DispatchQueue.main.async {
                    if(i==0)
                    {
                        self.lbl1MonthPrice.text = String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price)
                        self.lblTrialDes.text = "than \(String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price)) per month"
                    }
                    else if(i==1)
                    {
                        self.lbl1YearPrice.text = String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price)
                    }
                    else if(i==2)
                    {
                        self.lbl3MonthPrice.text = String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price)
                    }
                    self.productsArray.append(self.product!)
                    print("end ")
                    
                }
                
                
            }
            
            if(UserDefaults.standard.string(forKey: "InAppPurchase") == "0")
            {
                UserDefaults.standard.set("1", forKey: "InAppPurchase")
                
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().restoreCompletedTransactions()
            }
            
             DispatchQueue.main.async {
                self.progress.hide(animated: true)
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                self.progress.hide(animated: true)
                print("No products found")
            }
        }
        
    }
    
    @available(iOS 3.0, *)
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
    {
        for transaction in transactions
        {
            switch transaction.transactionState
            {
                
            case SKPaymentTransactionState.purchased:
                print("Transaction Approved")
                print("Product Identifier: \(transaction.payment.productIdentifier)")
                
                SKPaymentQueue.default().finishTransaction(transaction)
                UIApplication.shared.endIgnoringInteractionEvents()
//                progress = MBProgressHUD.showAdded(to: view, animated: true)
                APPDELEGATE.receiptValidation { (status) in
                    self.progress.hide(animated: true)
                    if status {
                        self.popView()
                    }
                }
                
                
            case SKPaymentTransactionState.failed:
                
                progress.hide(animated: true)
                
                print("Transaction Failed")
                
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case SKPaymentTransactionState.restored:
                
                progress.hide(animated: true)
                
                SKPaymentQueue.default().finishTransaction(transaction)
                
                print("Transaction restore")
                
            default:
                break
            }
        }
    }
    
    func restorePurchases()
    {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func buyProduct(product: SKProduct)
    {
//        progress = MBProgressHUD.showAdded(to: view, animated: true)
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().add(self)
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        progress.hide(animated: true)
        print("Error Fetching product information");
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue)
    {
        progress.hide(animated: true)
        
        for transaction:SKPaymentTransaction in queue.transactions
        {
            if transaction.payment.productIdentifier == "com.budgetgoals.1month"
            {
                UIApplication.shared.endIgnoringInteractionEvents()
//                progress = MBProgressHUD.showAdded(to: view, animated: true)
                APPDELEGATE.receiptValidation { (status) in
                    self.progress.hide(animated: true)
                    if status {
                        self.popView()
                    }
                }
            }
            else if transaction.payment.productIdentifier == "com.budgetgoals.1year"
            {
                UIApplication.shared.endIgnoringInteractionEvents()
//                progress = MBProgressHUD.showAdded(to: view, animated: true)
                APPDELEGATE.receiptValidation { (status) in
                    self.progress.hide(animated: true)
                    if status {
                        self.popView()
                    }
                }
            }
            else if transaction.payment.productIdentifier == "com.budgetgoals.3month" //"6Month"  Nikhil
            {
                UIApplication.shared.endIgnoringInteractionEvents()
//                progress = MBProgressHUD.showAdded(to: view, animated: true)
                APPDELEGATE.receiptValidation { (status) in
                    self.progress.hide(animated: true)
                    if status {
                        self.popView()
                    }
                }
            }else{
                UserDefaults.standard.set(false, forKey: "Purchase")
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error)
    {
        progress.hide(animated: true)
        print(error)
        //        self.showAlertFromController(controller: self, withMessage: "There are no items available to restore at this time.")
    }
}
