//
//  SettingViewController.swift
//  Finance
//
//  Created by Cools on 6/27/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import UserNotifications
import MessageUI
//import GoogleMobileAds
import Realm
import RealmSwift
import Foundation
import CoreGraphics
import StoreKit
import GoogleMobileAds
import MobileCoreServices

class SettingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UNUserNotificationCenterDelegate,MFMailComposeViewControllerDelegate,UIWebViewDelegate,SambagDatePickerViewControllerDelegate,GADBannerViewDelegate,UIDocumentPickerDelegate,UIDocumentMenuDelegate
{
    
    //MARK: - Define IBOutlet
    //    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet var GradientView : UIView!
    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var lblTimeline : UILabel!
    @IBOutlet var lblOverview : UILabel!
    @IBOutlet var lblBudget : UILabel!
    @IBOutlet var lblSetting : UILabel!
    @IBOutlet weak var viewCurrency: UIView!
    @IBOutlet weak var tblSetting: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var tblcurrency: UITableView!
    @IBOutlet weak var currencySearchBar: UISearchBar!
    @IBOutlet var headerView : UIView!
    @IBOutlet var tabBarView : UIView!
    @IBOutlet weak var imgTimeline: UIImageView!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var imgOverview: UIImageView!
    @IBOutlet var viewCategoryFilter: UIView!
    @IBOutlet var btnLeftMonth: UIButton!
    @IBOutlet var btnRightMonth: UIButton!
    @IBOutlet var btnApply: UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var productsArray = Array<SKProduct>()
    let productIdentifiers =  Set(["OneTimeSubscribe"])  // DDIT
    var product: SKProduct?
    
    //MARK: - Define Variable
    var searchActive: Bool = false
    var filtered: [String] = []
    let datePicker: UIDatePicker = UIDatePicker()
    var countryArray = NSMutableArray()
    var viewDatePicker : UIView!
    let donePicker = UIButton()
    let cancelPicker = UIButton()
    var reminderDate = Date()
    var countryNewArray : [String]! = []
    let section = ["General", "About"]
    var items = [["Currency","Passcode", "Reminder","Export Data","Manage Category"],["Rate This App on Appstore","Share With Friends","Purchase"],["About Us","Contact Us", "Privacy Policy", "Terms & Conditions", "Version"]]
    var image = [["ic_currency","ic_passcode","ic_reminder","ic_export","ic_category"],["ic_rate","ic_share","ic_purchase"],["ic_about","ic_contact","ic_version","ic_purchase","ic_version"]]
    var strCurrency : String = ""
    var strReminderTime = ""
    let done = UIButton()
    var dateBtnSelection = ""
    var taskArr = [Task]()
    var task: Task!
    var startDateofMonth = Date()
    var endDateofMonth = Date()
    var animatedViewSelection = ""
    var viewSelection = ""
    
    class Task: NSObject {
        var date: String = ""
        var categoryType: String = ""
        var categoryName: String = ""
        var subCategoryName: String = ""
        var Amount: String = ""
        var Note: String = ""
    }
    
    fileprivate let configuration: PasscodeLockConfigurationType
    
    init(configuration: PasscodeLockConfigurationType) {
        
        self.configuration = configuration
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        let repository = UserDefaultsPasscodeRepository()
        configuration = PasscodeLockConfiguration(repository: repository)
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - View Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        headerView.backgroundColor = Constant.headerColorCode
        //        tabBarView.backgroundColor = Constant.tabBarColorCode
        lblHeaderTitle.font = headerFont
        lblHeaderTitle.textColor = UIColor.white
        strCurrency = UserDefaults.standard.value(forKey: "currencySymbol") as! String
        btnLeftMonth.setTitle(Constant.getMonthYear(Date()), for: .normal)
        btnRightMonth.setTitle(Constant.getMonthYear(Date()), for: .normal)
        startDateofMonth = Date().startOfMonth()
        endDateofMonth = Date().startOfMonth()
        //        bannerView.adUnitID = Constant.banner_admob_id
        //        bannerView.rootViewController = self
        //        bannerView.load(GADRequest())
        
        if !Constant.isKeyPresentInUserDefaults(key: "ReminderSwitch") {
            strReminderTime = ""
        }
        else{
            strReminderTime = UserDefaults.standard.value(forKey: "ReminderSwitch") as! String
        }
        
        viewCurrency.isHidden = true
        viewCategoryFilter.isHidden = true
        bgView.isHidden = true
        viewCurrency.layer.cornerRadius = 8
        viewCurrency.layer.masksToBounds = true
        viewCategoryFilter.layer.cornerRadius = 8
        viewCategoryFilter.layer.masksToBounds = true
        btnApply.layer.cornerRadius = 5
        btnApply.layer.borderWidth = 2
        btnApply.layer.borderColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1).cgColor
        btnClose.layer.cornerRadius = 5
        btnClose.layer.borderWidth = 2
        btnClose.layer.borderColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1).cgColor
        btnClose.layer.masksToBounds = true
        tblSetting.tableFooterView = UIView.init(frame: CGRect.zero)
        //        tblcurrency.tableFooterView = UIView.init(frame: CGRect.zero)
        
        countryArray = (IsoCountries.allCountries as! NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<countryArray.count {
            let temp = countryArray[i] as! IsoCountryInfo
            countryNewArray.append(temp.name)
        }
        print(countryNewArray)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SettingViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        setupDatePicker()
        
        GradientView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        let Image = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        btnMenu.setImage(Image, for: .normal)
        btnMenu.tintColor = UIColor.white
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }

        //        lblTimeline.textColor = Constant.normalColorCode
        //        lblOverview.textColor = Constant.normalColorCode
        //        lblBudget.textColor = Constant.normalColorCode
        //        lblSetting.textColor = Constant.highlightedColorCode
        //        lblTimeline.font = tabBarFont
        //        lblOverview.font = tabBarFont
        //        lblBudget.font = tabBarFont
        //        lblSetting.font = tabBarFont
        //        imgTimeline.image = timelineImage
        //        imgOverview.image = overviewImage
        //        imgSettings.image = settingHightlitedImage
        tblSetting.reloadData()
        updatePasscodeView()
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    func setupDatePicker() {
        viewDatePicker = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 250))
        viewDatePicker.backgroundColor = UIColor.white
        self.view.addSubview(viewDatePicker)
        //        self.view.bringSubview(toFront: viewDatePicker)
        
        datePicker.frame = CGRect(x: 0, y:50, width: self.view.frame.width, height: 200)
        
        // Set some of UIDatePicker properties
        datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = UIDatePickerMode.time
        // Add an event to call onDidChangeDate function when value is changed.
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: .valueChanged)
        viewDatePicker.addSubview(datePicker)
        
        //done.buttonType = custom
        donePicker.frame = CGRect(x: self.view.frame.width - 70, y: 0, width: 60, height: 50)
        donePicker.setTitle("Done", for: .normal)
        donePicker.setTitleColor(UIColor.black, for: .normal)
        donePicker.addTarget(self, action: #selector(self.btndone(_:)), for: .touchUpInside)
        viewDatePicker.addSubview(donePicker)
        
        cancelPicker.frame = CGRect(x: 10, y: 0, width: 60, height: 50)
        cancelPicker.setTitle("Cancel", for: .normal)
        cancelPicker.setTitleColor(UIColor.black, for: .normal)
        cancelPicker.addTarget(self, action: #selector(self.btnCancel(_:)), for: .touchUpInside)
        viewDatePicker.addSubview(cancelPicker)
        
        viewDatePicker.isHidden = true
    }
    
    func hidePickerView(){
        viewDatePicker.isHidden = true
    }
    
    func showPickerView() {
        viewDatePicker.isHidden = false
    }
    
    func updatePasscodeView()
    {
        let hasPasscode = configuration.repository.hasPasscode
        
        if hasPasscode
        {
            UserDefaults.standard.set("On", forKey: "PasscodeSwitch")
            tblSetting.reloadData()
        }
        else
        {
            tblSetting.reloadData()
        }
    }
    
    //MARK: - Helper Method
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            
            let window = self.viewCurrency.window?.frame {
            let viewHeight = self.viewCurrency.frame.height
            self.viewCurrency.frame = CGRect(x: self.viewCurrency.frame.origin.x,
                                             y: self.viewCurrency.frame.origin.y,
                                             width: self.viewCurrency.frame.width,
                                             height: viewHeight - keyboardSize.height)
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.viewCurrency.frame.height
            self.viewCurrency.frame = CGRect(x: self.viewCurrency.frame.origin.x,
                                             y: self.viewCurrency.frame.origin.y,
                                             width: self.viewCurrency.frame.width,
                                             height: viewHeight + keyboardSize.height)
        }
    }
    
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
            print("Currency Symbol : \(currencySymbol)")
        }
        
        return currencySymbol
    }
    
    func PopUpView()  {
        
        animatedViewSelection = "currencyView"
        
        showAnimate(strViewSelection: animatedViewSelection)
        //        let PopUpVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        //
        //        self.addChildViewController(PopUpVC)
        //        PopUpVC.view.frame = self.view.frame
        //        self.view.addSubview(PopUpVC.view)
        //        PopUpVC.didMove(toParentViewController: self)
        
        
    }
    
    func showAnimate(strViewSelection : String)
    {
        if strViewSelection == "currencyView"
        {
            bgView.isHidden = false
            viewCurrency.isHidden = false
            viewCategoryFilter.isHidden = true
            bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            bgView.alpha = 0.0;
            viewCurrency.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            viewCurrency.alpha = 0.0;
            tblcurrency.delegate = self
            tblcurrency.dataSource = self
            tblcurrency.reloadData()
            UIView.animate(withDuration: 0.25, animations: {
                self.bgView.alpha = 0.7
                self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.viewCurrency.alpha = 1.0
                self.viewCurrency.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
            });
        }
        else
        {
            bgView.isHidden = false
            viewCurrency.isHidden = true
            viewCategoryFilter.isHidden = false
            bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            bgView.alpha = 0.0;
            viewCategoryFilter.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            viewCategoryFilter.alpha = 0.0;
            UIView.animate(withDuration: 0.25, animations: {
                self.bgView.alpha = 0.7
                self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.viewCategoryFilter.alpha = 1.0
                self.viewCategoryFilter.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
            });
        }
    }
    
    func removeAnimate(strViewSelection : String)
    {
        if strViewSelection == "currencyView"
        {
            filtered = []
            searchActive = false;
            currencySearchBar.text = ""
            
            viewCategoryFilter.isHidden = true
            
            self.currencySearchBar.resignFirstResponder()
            UIView.animate(withDuration: 0.25, animations: {
                self.viewCurrency.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.viewCurrency.alpha = 0.0;
                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.viewCurrency.isHidden = true
                    self.bgView.isHidden = true
                }
            });
        }
        else
        {
            filtered = []
            searchActive = false;
            currencySearchBar.text = ""
            viewCurrency.isHidden = true
            self.currencySearchBar.resignFirstResponder()
            UIView.animate(withDuration: 0.25, animations: {
                self.viewCategoryFilter.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.viewCategoryFilter.alpha = 0.0;
                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.viewCategoryFilter.isHidden = true
                    self.bgView.isHidden = true
                }
            });
        }
    }
    
    func creatCSV() -> Void {
        
        let puppies = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@}", startDateofMonth, endDateofMonth)//uiRealm.objects(Transaction.self)
        
        let transactionArray = NSMutableArray()
        
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            transactionArray.add(dict1)
        }
        
        if transactionArray.count > 0
        {
            let fileName = "Tasks.csv"
            let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
            var csvText = "Date,Category Type,Category Name,Sub Category Name,Amount,Note\n"
            
            for i in 0..<transactionArray.count {
                var typeOfCategory = ""
                if (transactionArray[i] as! NSMutableDictionary).value(forKey: "setType") as! String == "1"
                {
                    typeOfCategory = "Income"
                }
                else
                {
                    typeOfCategory = "Expense"
                }
                
                let newLine = "\((transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String),\(typeOfCategory),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "subCatName") as! String),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "note") as! String)\n"
                csvText.append(newLine)
            }
            
            do {
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                
                if MFMailComposeViewController.canSendMail() {
                    let emailController = MFMailComposeViewController()
                    emailController.mailComposeDelegate = self
                    emailController.setToRecipients([])
                    emailController.setSubject("data export")
                    //                    emailController.setMessageBody("Hi,\n\nThe .csv data export is attached\n\n\nSent from the MPG app: http://www.justindoan.com/mpg-fuel-tracker", isHTML: false)
                    let tempData = try NSData(contentsOf: path!) as Data
                    
                    emailController.addAttachmentData(tempData, mimeType: "text/csv", fileName: "\(fileName)")
                    present(emailController, animated: true, completion: nil)
                }
                
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        }
        else {
            alertView(alertMessage: AlertString.DataExport)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        
        if animatedViewSelection == "currencyView"
        {
            if touch.view != viewCurrency {
                removeAnimate(strViewSelection: "currencyView")
            }
        }
        else
        {
            if touch.view != viewCategoryFilter {
                removeAnimate(strViewSelection: "categoryFilter")
            }
        }
    }
    
    //MARK: - Tableview Delegate And Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(tableView == tblcurrency)
        {
            return 1
        }
        else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(tableView == tblcurrency)
        {
            
            return 0
        }
        else{
            
            if section == 0
            {
                return 0
            }
            else if section == 1
            {
                return 40
            }
            else{
                return 40
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == tblcurrency)
        {
            if(searchActive) {
                return filtered.count
            }
            return countryArray.count
        }
        else{
            if(section == 0)
            {
                return 5
            }
            else if(section == 1)
            {
                return 3
            }else{
                return 5
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(tableView == tblcurrency)
        {
            return nil
        }
        else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 240.0/255.0, green: 239.0/255.0, blue: 245.0/255.0, alpha: 1)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == tblcurrency)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath as IndexPath)
            
            cell.imageView?.layer.cornerRadius = 5
            cell.imageView?.layer.masksToBounds = true
            
            if(searchActive == true)
            {
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                        
                    {
                        cell.imageView?.image = UIImage(named: "\(temp.alpha2).png")
                        cell.textLabel?.text = temp.name
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = countryname.currency
                
                cell.imageView?.image = UIImage(named: "\(countryname.alpha2).png")
                cell.textLabel?.text = countryname.name
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! SettingCell
            cell.img_setting.layer.cornerRadius = 8
            cell.img_setting.layer.masksToBounds = true
            cell.img_setting.image = UIImage(named: self.image[indexPath.section][indexPath.row])
            cell.titlelbl?.text = self.items[indexPath.section][indexPath.row]
            if(indexPath.section == 0)
            {
                if(indexPath.row == 0)
                {
                    cell.accessoryType = .disclosureIndicator
                    cell.switchController.isHidden = true
                    cell.lblCurrency.isHidden = false
                    cell.lblCurrency!.text = UserDefaults.standard.value(forKey: "currencySymbol") as? String//value(forKey: "currencySymbol")
                    cell.lblReminderTime.isHidden = true
                    
                }
                else if(indexPath.row == 1 || indexPath.row == 2)
                {
                    cell.switchController.isHidden = false
                    cell.lblCurrency.isHidden = true
                    if(indexPath.row == 1)
                    {
                        cell.lblReminderTime.isHidden = true
                        if(Constant.isKeyPresentInUserDefaults(key: "PasscodeSwitch"))
                        {
                            cell.switchController.isOn = true
                            cell.switchController.frame = CGRect(x: 232, y: 6, width: 68, height: 31)
                        }
                        else
                        {
                            cell.switchController.isOn = false
                            cell.switchController.frame = CGRect(x: 232, y: 6, width: 68, height: 31)
                            
                        }
                     }
                    else
                    {
                        cell.lblReminderTime.isHidden = false
                        cell.lblReminderTime.text = strReminderTime
                        if(Constant.isKeyPresentInUserDefaults(key: "ReminderSwitch"))
                        {
                            cell.switchController.isOn = true
                        }
                    }
                    cell.accessoryType = .none
                    cell.switchController.tag = indexPath.row
                }
                else
                {
                    cell.lblReminderTime.isHidden = true
                    cell.lblCurrency.isHidden = true
                    cell.accessoryType = .disclosureIndicator
                    cell.switchController.isHidden = true
                }
            }
            else if(indexPath.section == 2){
                cell.switchController.isHidden = true
                cell.lblCurrency.isHidden = true
                cell.accessoryType = .disclosureIndicator
                if(indexPath.row == 4){
                    cell.selectionStyle = .none
                    let info:NSDictionary = Bundle.main.infoDictionary! as NSDictionary
                    let version:String = info.object(forKey: "CFBundleShortVersionString") as! String
                    print(version)
                    cell.titlelbl?.text = "Version \(version)"
                    cell.accessoryType = .none
                }
            }
            else
            {
                cell.lblReminderTime.isHidden = true
                cell.accessoryType = .disclosureIndicator
                cell.switchController.isHidden = true
                cell.lblCurrency.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(tableView == tblcurrency)
        {
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                    {
                        let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                        strCurrency = currencyCode
                        UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                        tblSetting.reloadData()
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = getCurrencySymbol(currencyCode: countryname.currency)
                strCurrency = currencyCode
                UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                tblSetting.reloadData()
                print(currencyCode)
            }
            removeAnimate(strViewSelection: "currencyView")
        }
        else
        {
            if(indexPath.section == 0)
            {
                if(indexPath.row == 0)
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingCurrencyViewController") as! SettingCurrencyViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if indexPath.row == 3
                {
                    // Upload File
                    //Create the file path of the document to upload
//                    var filePathToUpload = URL(fileURLWithPath: Bundle.main.path(forResource: "testing", ofType: "doc") ?? "")
                    let path =  Realm.Configuration.defaultConfiguration.fileURL
                        //Create a object of document picker view and set the mode to Export
                    let docPicker = UIDocumentPickerViewController(url: path!, in: .exportToService)
                    //Set the delegate
                    docPicker.delegate = self
                    //present the document picker
                    present(docPicker, animated: true)
                    
                    
                    ///* Download File
                    //  The converted code is limited to 2 KB.
                    //  Upgrade your plan to remove this limitation.
                    //
                    let types = [kUTTypeImage as String, kUTTypeSpreadsheet as String, kUTTypePresentation as String, kUTTypeDatabase as String, kUTTypeFolder as String, kUTTypeZipArchive as String, kUTTypeVideo as String, kUTTypeItem as String, kUTTypeContent as String]
                }
                else if indexPath.row == 4
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController//CategoryViewController
                    vc.passingView = "setting"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if (indexPath.section == 1){
                if (indexPath.row == 0){
                    let APPSTORE_ID = "1169353587"
                    if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(APPSTORE_ID)"),
                        UIApplication.shared.canOpenURL(url){
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                            UIApplication.shared.openURL(url)
                        }
                    }else{
                        //Just check it on phone not simulator!
                        print("Can not open")
                    }
                }
                else if(indexPath.row == 1){
                    // set up activity view controller
                    let text = "Check this app Budget Goal - Save money, manage bills,track expenses and achieve your financial goals now."
                    let url = URL(string: "https://itunes.apple.com/us/app/Budget%20Goal/id1169353587?ls=1&mt=8")
                    let textToShare = [text,url!] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    
                    if let popoverController = activityViewController.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = (self.view as AnyObject).bounds
                    }
                    //activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                    
                    // exclude some activity types from the list (optional)
                    activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                    
                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                }
                else{
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseSubscribtionVC") as! PurchaseSubscribtionVC
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
            else{
                if (indexPath.row == 0)
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                    
                    nextViewController.viewSelection = "SideMenuVC"
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                else if (indexPath.row == 1)
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                else if (indexPath.row == 2)
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicy") as! PrivacyPolicy
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                else if (indexPath.row == 3)
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionVC") as! TermsAndConditionVC
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }

            }
        }
    }
    
    //MARK:- Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        //  The converted code is limited to 2 KB.
        //  Upgrade your plan to remove this limitation.
        //
        if controller.documentPickerMode == .import {
                // Condition called when user download the file
//            let file: FileHandle? = FileHandle(forWritingAtPath: "default.realm")
//            let data = try! Data(contentsOf: url)
//            file?.write(data)
//            // Close the file
//            file?.closeFile()
            
            //  The converted code is limited to 2 KB.
            //  Upgrade your plan to remove this limitation.
            //
            
            var urlData: Data? = nil
            urlData = try! Data(contentsOf: url)
            
            if urlData != nil {
                var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                var documentsDirectory = paths[0]
                var filePath = URL(fileURLWithPath: "\(documentsDirectory)/default.realm")
                try! urlData?.write(to: filePath)
            }
            
            /*
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url: url)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    
                    // then lets create your document folder url
                    let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    
                    let destinationUrl = documentsDirectoryURL.appendingPathComponent(tempLocalUrl.lastPathComponent)
                    print(destinationUrl)
                    
                    // to check if it exists before downloading it
                    if FileManager.default.fileExists(atPath: destinationUrl.path) {
                        print("The file already exists at path")
                        try! FileManager.default.removeItem(atPath: destinationUrl.path)
                        
                        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                            print("Success: \(statusCode)")
                        }
                        let localUrl = URL(string: "/var/mobile/Containers/Data/Application/3EFCD02C-FF2D-4BFE-9893-0C5533CEB56A/Documents/")
                        do {
                            try FileManager.default.moveItem(at: tempLocalUrl, to: localUrl!)
                        } catch (let writeError) {
                            print("error writing file \(localUrl) : \(writeError)")
                        }
                        
                    } else {
                    
                        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                            print("Success: \(statusCode)")
                        }
                        let localUrl = URL(string: "/var/mobile/Containers/Data/Application/3EFCD02C-FF2D-4BFE-9893-0C5533CEB56A/Documents/")
                        do {
                            try FileManager.default.moveItem(at: tempLocalUrl, to: localUrl!)
                        } catch (let writeError) {
                            print("error writing file \(localUrl) : \(writeError)")
                        }
                    }
                    
                    
                } else {
                    print("Failure: %@", error?.localizedDescription);
                }
            }
            task.resume()
             */
        }
        
    }
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true)
    }
    
    // MARK: - SearchBar Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = countryNewArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tblcurrency.reloadData()
        
    }
    
    //MARK: - SambagDate Picker Method
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        
        let resultDate = Constant.convertDate(String(describing: result))
        if dateBtnSelection == "left"
        {
            startDateofMonth = resultDate.startOfMonth()
            btnLeftMonth.setTitle(Constant.getMonthYear(resultDate), for: .normal)
        }
        else if dateBtnSelection == "right"
        {
            endDateofMonth = resultDate.startOfMonth()
            btnRightMonth.setTitle(Constant.getMonthYear(resultDate), for: .normal)
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        BottomHeightConstraint.constant = 0
    }
    
    
    
    //MARK: - Action Method
    
    @IBAction func btnApplyDateSelection(_ sender: UIButton) {
        
        if startDateofMonth < endDateofMonth
        {
            removeAnimate(strViewSelection: "categoryFilter")
            creatCSV()
        }
        else
        {
            self.alertView(alertMessage: AlertString.ProperMont)
        }
        
        
    }
    
    @IBAction func btnLeftDateSelection(_ sender: UIButton) {
        dateBtnSelection = "left"
        let vc = SambagDatePickerViewController()
        vc.delegate = self
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnRightDateSelection(_ sender: UIButton) {
        dateBtnSelection = "right"
        let vc = SambagDatePickerViewController()
        vc.delegate = self
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseAction (_ sender : UIButton)
    {
        removeAnimate(strViewSelection: "currencyView")
    }
    
    @IBAction func switchOnOff(_ sender: UISwitch) {
        
        if(sender.tag == 2)
        {
            if (sender.isOn)
            {
//                // Posiiton date picket within a view
//                datePicker.frame = CGRect(x: 0, y:self.view.frame.height - 200, width: self.view.frame.width, height: 200)
//
//                // Set some of UIDatePicker properties
//                datePicker.timeZone = NSTimeZone.local
//                datePicker.backgroundColor = UIColor.white
//                datePicker.datePickerMode = UIDatePickerMode.time
//                // Add an event to call onDidChangeDate function when value is changed.
//                datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: .valueChanged)
//
//                // Add DataPicker to the view
//                self.view.addSubview(datePicker)
//
//                done.frame = CGRect(x: 0, y: -30, width: 60, height: 30)
//                done.setTitle("Done", for: .normal)
//                done.setTitleColor(UIColor.black, for: .normal)
//                done.addTarget(self, action: #selector(self.btndone(_:)), for: .touchUpInside)
//                //done.bringSubview(toFront: datePicker)
//                datePicker.addSubview(done)
                
                 self.showPickerView()
            }
            else
            {
                strReminderTime = ""
                self.hidePickerView()
//                datePicker.removeFromSuperview()
                UserDefaults.standard.removeObject(forKey: "ReminderSwitch")
                //let delegate = UIApplication.shared.delegate as? AppDelegate
                //delegate?.
                if #available(iOS 10.0, *) {
                    UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                } else {
                    // Fallback on earlier versions
                }
                tblSetting.reloadData()
            }
        }
        else
        {
            let passcodeVC: PasscodeLockViewController
            
            if sender.isOn {
                
                passcodeVC = PasscodeLockViewController(state: .setPasscode, configuration: configuration)
                //                UserDefaults.standard.set("On", forKey: "PasscodeSwitch")
                
            } else {
                
                passcodeVC = PasscodeLockViewController(state: .removePasscode, configuration: configuration)
                
                passcodeVC.successCallback = { lock in
                    
                    lock.repository.deletePasscode()
                    UserDefaults.standard.removeObject(forKey: "PasscodeSwitch")
                    //                    UserDefaults.standard.set("On", forKey: "PasscodeSwitch")
                    sender.isOn = false
                }
            }
            present(passcodeVC, animated: true, completion: nil)
        }
    }
    
    @objc func btndone(_ sender: UIButton)
    {
        print("done btn called...")
        self.hidePickerView()
        //Set Current Date
        let selectedDate = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        print("Selected date: \(timeFormatter.string(from: selectedDate))")
        strReminderTime = timeFormatter.string(from: selectedDate)
        
        UserDefaults.standard.set(strReminderTime, forKey: "ReminderSwitch")
        tblSetting.reloadData()
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.scheduleNotification(at: reminderDate)
    }
    
    @objc func btnCancel(_ sender: UIButton)
    {
        self.hidePickerView()
    }
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        let selectedDate = sender.date
        reminderDate = selectedDate
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        print("Selected date: \(timeFormatter.string(from: selectedDate))")
        strReminderTime = timeFormatter.string(from: selectedDate)
        // datePicker.removeFromSuperview()
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
}

