//
//  CompareLoansVC.swift
//  Budget Goals
//
//  Created by nectarbits on 7/2/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit

class CompareLoansVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var headerView: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnReload: UIButton!
    @IBOutlet var btnCalculate: UIButton!
    
    @IBOutlet var txtAmount1: UITextField!
    @IBOutlet var txtAmount2: UITextField!
    @IBOutlet var txtIntrest1: UITextField!
    @IBOutlet var txtIntrest2: UITextField!
    @IBOutlet var txtTenure1: UITextField!
    @IBOutlet var txtTenure2: UITextField!
    @IBOutlet var txtEMI1: UITextField!
    @IBOutlet var txtEMI2: UITextField!
    @IBOutlet var txtTotalIntrest1: UITextField!
    @IBOutlet var txtTotalIntrest2: UITextField!
    @IBOutlet var txtTotalPayment1: UITextField!
    @IBOutlet var txtTotalPayment2: UITextField!
    @IBOutlet var ViewResultHeightConstraint: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.ViewResultHeightConstraint.constant = 0
        
        let Image = UIImage(named: "circular-arrow-in-rounded-button 2")?.withRenderingMode(.alwaysTemplate)
        btnReload.setImage(Image, for: .normal)
        btnReload.tintColor = UIColor.white
        
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- TextFiled Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtEMI1){
            txtEMI1.resignFirstResponder()
        }else if(textField == txtEMI2){
            txtEMI2.resignFirstResponder()
        }else if(textField == txtTotalIntrest1){
            txtTotalIntrest1.resignFirstResponder()
        }else if(textField == txtTotalIntrest2){
            txtTotalIntrest2.resignFirstResponder()
        }else if(textField == txtTotalPayment1){
            txtTotalPayment1.resignFirstResponder()
        }else if(textField == txtTotalPayment2){
            txtTotalPayment2.resignFirstResponder()
        }
        
    }
    
    //MARK:- Helper Methods
    func CalculateLoan1(LoanAmount: String, LoanIntrest: String, LoanTime: String , Emi : UITextField , TotalIntrest: UITextField, TotalPayment: UITextField){
        
        let loantime = (Constant.TO_DOUBLE(LoanTime))/12
        
        let r =  Constant.TO_DOUBLE(LoanIntrest) / 1200//(Double(LoanTime)*100)
        let r1 = pow(r+1, Double(loantime) * 12)
        let monthlyPayment = (r + (r/(r1 - 1))) * Constant.TO_DOUBLE(LoanAmount)
        
        let totalPayment = monthlyPayment * Double(loantime) * 12
        let totalInterest = totalPayment - Constant.TO_DOUBLE(LoanAmount)
        
//        let r =  Constant.TO_DOUBLE(LoanIntrest) / (Constant.TO_DOUBLE(LoanTime) * 100)
//        let r1 = pow(r+1, Double(LoanTime)!)
//        let monthlyPayment = (r + (r/(r1 - 1))) * Constant.TO_DOUBLE(LoanAmount)
//        let TotalAmount = Constant.TO_DOUBLE(LoanTime) * Constant.TO_DOUBLE(monthlyPayment)
//        let totalInterest = TotalAmount - Constant.TO_DOUBLE(LoanAmount)
        
        Emi.text = String(format: "%.2f", monthlyPayment)
        TotalIntrest.text = String(format: "%.2f", totalInterest)
        TotalPayment.text = String(format: "%.2f", totalPayment)
        
    }
    
    func Validate() -> Bool{
        if(txtAmount1.text == ""){
            alertView(alertMessage: "Please Enter Amount")
            return false
        }else if(txtAmount2.text == ""){
            alertView(alertMessage: "Please Enter Amount")
            return false
        }else if(txtIntrest1.text == ""){
            alertView(alertMessage: "Please Enter Intrest")
            return false
        }else if(txtIntrest2.text == ""){
            alertView(alertMessage: "Please Enter Intrest")
            return false
        }else if(txtTenure1.text == ""){
            alertView(alertMessage: "Please Enter Months")
            return false
        }else if(txtTenure2.text == ""){
            alertView(alertMessage: "Please Enter Months")
            return false
        }
        
        return true
    }
    
    func ReloadFields(){
        txtAmount1.text = ""
        txtAmount2.text = ""
        txtIntrest1.text = ""
        txtIntrest2.text = ""
        txtTenure1.text = ""
        txtTenure2.text = ""
        txtEMI1.text = ""
        txtEMI2.text = ""
        txtTotalIntrest1.text = ""
        txtTotalIntrest2.text = ""
        txtTotalPayment1.text = ""
        txtTotalPayment2.text = ""
        
        self.ViewResultHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.btnCalculate.backgroundColor = UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1)
        }
    }
    
    
    //MARK:- Action Methods
    @IBAction func btnBackTap(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCalculateTap(_ sender: UIButton){
        
        if(ViewResultHeightConstraint.constant == 0){
            if(Validate()){
                
                //For set All Two Filed That's Y this Function Call Two Time with Different TextField
                self.CalculateLoan1(LoanAmount: txtAmount1.text!, LoanIntrest: txtIntrest1.text!, LoanTime: txtTenure1.text!, Emi: txtEMI1, TotalIntrest: txtTotalIntrest1, TotalPayment: txtTotalPayment1)
                self.CalculateLoan1(LoanAmount: txtAmount2.text!, LoanIntrest: txtIntrest2.text!, LoanTime: txtTenure2.text!, Emi: txtEMI2, TotalIntrest: txtTotalIntrest2, TotalPayment: txtTotalPayment2)
                
                self.ViewResultHeightConstraint.constant = 230
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                    self.btnCalculate.backgroundColor = UIColor.darkGray
                }
            }
        }
    }
    
    @IBAction func btnReloadDataTap(_ sender: UIButton){
        self.ReloadFields()
    }
}
