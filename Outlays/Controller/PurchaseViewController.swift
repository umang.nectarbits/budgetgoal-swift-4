//
//  PurchaseViewController.swift
//  Budget Goals
//
//  Created by harshesh on 24/05/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit
import StoreKit
import MBCircularProgressBar

class PurchaseViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    //MARK:- Outlet Method
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnPurchaseAdd: UIButton!
    @IBOutlet var btnPurchaseGoal: UIButton!
    @IBOutlet var btnPurchaseAll: UIButton!
    @IBOutlet var lblPurchaseAdd: UILabel!
    @IBOutlet var lblPurchaseGoal: UILabel!
    @IBOutlet var lblPurchaseAll: UILabel!
    
    //MARK:- Variable Decalration
    var productsArray = Array<SKProduct>()
    var product: SKProduct?
    var strScreenTag = ""
    var progress = MBProgressHUD()
    var strPurchaseTag = ""
    let productIdentifiers =  Set(["com.nectarbits.remove_ads","com.nectarbits.unlimited_goals","com.nectarbits.unlock_all"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPurchaseAdd.layer.cornerRadius = 10
        btnPurchaseGoal.layer.cornerRadius = 10
        btnPurchaseAll.layer.cornerRadius = 10
        let image = UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(image, for: .normal)
        btnBack.tintColor = UIColor.white
        
        if (strScreenTag == "SetGoal"){
            btnCancel.isHidden = false
            btnBack.isHidden = true
        }else{
            btnCancel.isHidden = true
            btnBack.isHidden = false
        }
        
        requestProductData()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Method
    func restorePurchases()
    {
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        progress.mode = MBProgressHUDMode.indeterminate
        progress.label.text = "Restore"
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

    //MARK:- InAppPurchase
    func requestProductData()
    {
        if SKPaymentQueue.canMakePayments()
        {
            let request = SKProductsRequest(productIdentifiers:self.productIdentifiers)
            request.delegate = self
            request.start()
            progress = MBProgressHUD.showAdded(to: view, animated: true)
        }
        else
        {
            let alert = UIAlertController(title: "In-App Purchases Not Enabled", message: "Please enable In App Purchase in Settings", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
                
                let url: URL? = URL(string: UIApplicationOpenSettingsURLString)
                if url != nil
                {
                    UIApplication.shared.openURL(url!)
                }
                
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        var products = response.products
        
        if (products.count != 0)
        {
            for i in 0 ..< products.count
            {
                self.product = products[i] as SKProduct
                let d : Double = (self.product?.price.doubleValue)!
                let price = String(format: "%.2f", d)
                
                print("price %@",price)
                
                if(i==0)
                {
                    lblPurchaseAdd.text = "To prevent all advertisement \(String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price))"
                }
                else if(i==1){
                    
                    lblPurchaseGoal.text = "Get Unlimited access for Goal \(String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price))"
                }
                else if(i==2){
                    
                    lblPurchaseAll.text = "Unlock All features \(String(format: "%@ %@",(self.product?.priceLocale.currencySymbol)!, price))"
                }
                self.productsArray.append(product!)
                
//                self.buyProduct(product: productsArray[0])
            }
            
            if(self.isKeyExistInUserDefault(kUsernameKey: "Restore") == false)
            {
                UserDefaults.standard.set("1", forKey: "Restore")
                
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().restoreCompletedTransactions()
            }
            progress.hide(animated: true)
        }
        else
        {
            print("No products found")
        }
        
        self.view.isUserInteractionEnabled = true
        
        progress.hide(animated: true)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
    {
        progress.hide(animated: true)
        
        for transaction in transactions
        {
            switch transaction.transactionState
            {
                
            case SKPaymentTransactionState.purchased:
                print("Transaction Approved")
                print("Product Identifier: \(transaction.payment.productIdentifier)")
                
                if (("\(transaction.payment.productIdentifier)") == "com.nectarbits.remove_ads"){
                    UserDefaults.standard.set(true, forKey: "GetAdvertiseFeature")
                    
                }else if(("\(transaction.payment.productIdentifier)") == "com.nectarbits.unlimited_goals"){
                    UserDefaults.standard.set(true, forKey: "GetGoalFeature")
                    
                }else{
                    UserDefaults.standard.set(true, forKey: "GetAllFeature")
                }
                
                SKPaymentQueue.default().finishTransaction(transaction)
                progress.hide(animated: true)
                
                if (strScreenTag == "SetGoal"){
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
                
            case SKPaymentTransactionState.failed:
                
                print("Transaction Failed")
                progress.hide(animated: true)
                SKPaymentQueue.default().finishTransaction(transaction)
                
                progress.hide(animated: true)
                
            case SKPaymentTransactionState.restored:
                
                SKPaymentQueue.default().finishTransaction(transaction)
                
                print("Transaction restore")
                
                progress.hide(animated: true)
                
            default:
                break
            }
        }
    }
    
    func buyProduct(product: SKProduct)
    {
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        progress.mode = MBProgressHUDMode.indeterminate
        progress.label.text = "Wait"
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().add(self)
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        progress.hide(animated: true)
        
        print("Error Fetching product information");
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue)
    {
        progress.hide(animated: true)

        for transaction:SKPaymentTransaction in queue.transactions
        {
            if transaction.payment.productIdentifier == "com.nectarbits.remove_ads"
            {
//                UserDefaults.standard.set("1",forKey:"Purchase")
                UserDefaults.standard.set(true, forKey: "GetAdvertiseFeature")
                self.restoreSuccessfully()
                progress.hide(animated: true)
            }
            else if transaction.payment.productIdentifier == "com.nectarbits.unlimited_goals"
            {
//                UserDefaults.standard.set("1",forKey:"Purchase")
                UserDefaults.standard.set(true, forKey: "GetGoalFeature")
                self.restoreSuccessfully()
                progress.hide(animated: true)
            }
            else if transaction.payment.productIdentifier == "com.nectarbits.unlimited_goals"
            {
                UserDefaults.standard.set(true, forKey: "GetAllFeature")
                self.restoreSuccessfully()
                progress.hide(animated: true)
            }else{
                UserDefaults.standard.set(true, forKey: "purchase")
            }
        }
    }
    
    func isKeyExistInUserDefault(kUsernameKey: String) -> Bool
    {
        return UserDefaults.standard.object(forKey: kUsernameKey) != nil
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error)
    {
        progress.hide(animated: true)
        print(error)
    }
    
    func restoreSuccessfully() {
        let alert = UIAlertController(title: "Restore", message: "Your purchase restore successfully.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            if Constant.isKeyPresentInUserDefaults(key: "GetAdvertiseFeature")
            {
                if (self.strScreenTag == "SetGoal"){
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if Constant.isKeyPresentInUserDefaults(key: "GetGoalFeature")
            {
                if (self.strScreenTag == "SetGoal"){
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if Constant.isKeyPresentInUserDefaults(key: "GetAllFeature")
            {
                if (self.strScreenTag == "SetGoal"){
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    //MARK: - Action Method
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPuchaseAdd(_ sender: UIButton){
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        self.buyProduct(product: productsArray[0])
        
//        if SKPaymentQueue.canMakePayments()
//        {
//            strPurchaseTag = "PurchaseAdd"
//            let request = SKProductsRequest(productIdentifiers: ["com.nectarbits.remove_ads"])
//            request.delegate = self
//            request.start()
//            progress = MBProgressHUD.showAdded(to: view, animated: true)
//            progress.mode = MBProgressHUDMode.indeterminate
//            progress.label.text = "Wait"
//            self.view.isUserInteractionEnabled = false
//        }
//        else
//        {
//            let alert = UIAlertController(title: "In-App Purchases Not Enabled", message: "Please enable In App Purchase in Settings", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { alertAction in
//                alert.dismiss(animated: true, completion: nil)
//
//                let url: URL? = URL(string: UIApplicationOpenSettingsURLString)
//                if url != nil
//                {
//                    UIApplication.shared.openURL(url!)
//                }
//
//            }))
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
//                alert.dismiss(animated: true, completion: nil)
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
    }
    
    @IBAction func btnPuchaseGoal(_ sender: UIButton){
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        self.buyProduct(product: productsArray[1])
    }
    
    @IBAction func btnPuchaseAll(_ sender: UIButton){
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        self.buyProduct(product: productsArray[2])
    }
    
    @IBAction func btnRestoreAction(_ sender: UIButton){
        restorePurchases()
    }
}
