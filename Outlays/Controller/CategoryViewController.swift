//
//  CategoryViewController.swift
//  Finance
//
//  Created by Cools on 6/13/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

struct Section
{
    var name: String!
    var image: String!
    var id: String!
    var categoryID: String!
    var subCategoryID: String!
    var categoryType: String!
    var isCategory: String!
    var items: NSMutableArray!
    var collapsed: Bool!
    
    init(name: String,image: String,id: String,categoryID: String,subCategoryID: String,categoryType: String,isCategory: String, items: NSMutableArray, collapsed: Bool)
    {
        self.name = name
        self.image = image
        self.id = id
        self.categoryID = categoryID
        self.subCategoryID = subCategoryID
        self.categoryType = categoryType
        self.isCategory = isCategory
        self.items = items
        self.collapsed = collapsed
    }
}

class CategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate {
    //MARK: - Define Outlet
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet weak var tblCategory : UITableView!
    @IBOutlet var txtCategoryName: UITextField!
    @IBOutlet var img_selectedCategory: UIImageView!
    @IBOutlet var btncancel: UIButton!
    @IBOutlet var btnPopUpcancel: UIButton!
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var categoryView: UIView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var categorySegment: UISegmentedControl!
    @IBOutlet var lblHeaderAddCategory: UILabel!
    @IBOutlet var img_pushBack: UIImageView!
    @IBOutlet var btnPushBack: UIButton!
    @IBOutlet weak var currencySearchBar: UISearchBar!
    @IBOutlet var segmentTopConstraint: NSLayoutConstraint!
    @IBOutlet var segmentHeightConstraint: NSLayoutConstraint!
    @IBOutlet var searchTopConstraint: NSLayoutConstraint!
    @IBOutlet var categorySearchBar: UISearchBar!
    
    @IBOutlet var lblNoDataFoundSearch: UILabel!

    //MARK: - Define Variable
    var isEdited : Bool = false
    var categoryType : String = ""
    var strCategoryID : String = ""
    var passingView : String = ""
    var isMainCategory : String = "Main"
    var sections = [Section]()
    var items = NSMutableArray()
    var img_icon : String = ""
    var editDictionary = NSMutableDictionary()
    var cellDescriptors = NSMutableArray()
    var visibleRowsPerSection = [[Int]]()
    var subCategoryArray = NSMutableArray()
    var searchActive: Bool = false
    var filtered : NSMutableArray = []

//    var filtered: [String] = []
    var filteredArray:[Section] = []


    var filteredArrays = NSMutableArray()

    var filterSubCat = NSMutableArray()

    //MARK: - View Method
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        let toolbarDone = UIToolbar.init()
        toolbarDone.sizeToFit()
        let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.cancel,
                                              target: self, action: #selector(handleTaps))
        
        let space1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        let space2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        toolbarDone.items = [space1, space2, barBtnDone] // You can even add cancel button too
        categorySearchBar.inputAccessoryView = toolbarDone
        
        
        categorySearchBar.delegate = self
        bgView.isHidden = true
        categoryView.isHidden = true
        categoryView.layer.cornerRadius = 8
        categoryView.layer.masksToBounds = true
        categoryView.layer.shadowColor = UIColor.black.cgColor
        categoryView.layer.shadowOpacity = 1
        categoryView.layer.shadowOffset = CGSize.zero
        categoryView.layer.shadowRadius = 8
        btnPopUpcancel.layer.cornerRadius = btnPopUpcancel.frame.size.height / 2
        btnPopUpcancel.layer.masksToBounds = true
        btnOk.layer.cornerRadius = btnOk.frame.size.height / 2
        btnOk.layer.masksToBounds = true
        img_selectedCategory.layer.cornerRadius = img_selectedCategory.frame.size.width / 2
        img_selectedCategory.layer.masksToBounds = true
        img_selectedCategory.layer.borderWidth = 1
        img_selectedCategory.layer.borderColor = UIColor.black.cgColor
//        tblCategory.backgroundColor = UIColor(colorLiteralRed: 230.0/255.0, green: 229.0/255.0, blue: 235.0/255.0, alpha: 1)
        
        if passingView == "setting"
        {
            //            btncancel.setTitle("", for: .normal)
            //            btncancel.setBackgroundImage(UIImage(named: "ic_back_category.png"), for: .normal)
            btnPushBack.isHidden = false
            img_pushBack.isHidden = false
            btncancel.isHidden = true
            
            categoryType = "1"
            self.getCategoryData(categoryType: categoryType)
        }
        else if passingView == "split"
        {
            btncancel.isHidden = false
            btnPushBack.isHidden = true
            img_pushBack.isHidden = true
            categorySegment.isHidden = true
            segmentTopConstraint.constant = 0.0
            segmentHeightConstraint.constant = 0.0
            
            categoryType = "0"
            self.getCategoryData(categoryType: categoryType)
        }
        else
        {
            btncancel.isHidden = false
            btnPushBack.isHidden = true
            img_pushBack.isHidden = true
            categorySegment.isHidden = true
            segmentTopConstraint.constant = 0.0
            segmentHeightConstraint.constant = 0.0
            
//            categoryType = "1"
            
            if strCategoryID == "0"
            {
                self.getCategoryData(categoryType: categoryType)
            }
            else
            {
                self.getCategoryDataWithID(categoryType: categoryType)
            }
            
            
            
            //            btncancel.setTitle("Cancel", for: .normal)
            //            btncancel.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
        
        
        tblCategory.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "categorycell")
        tblCategory.tableFooterView = UIView.init(frame: CGRect.zero)
        
        categorySearchBar.layer.borderWidth = 1;
        categorySearchBar.layer.borderColor =  UIColor.clear.cgColor
        categorySearchBar.backgroundImage = UIImage()
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    @objc func handleTaps()
    {
        categorySearchBar.resignFirstResponder()
    }

    //MARK: - Helper Method
    func getCategoryData(categoryType : String)
    {
        subCategoryArray = []
        let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@","1",categoryType)
        let mutableArray = NSMutableArray()
        sections = [Section]()
        
        for i in 0..<categoryList.count
        {
            let dict = NSMutableDictionary()
            dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
            dict.setValue(categoryList[i]["name"]!, forKey: "name")
            dict.setValue(categoryList[i]["image"]!, forKey: "image")
            dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
            dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
            dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
            dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
            items = []
            
            let puppies = uiRealm.objects(Category.self).filter("categoryID = %@ AND isCategory = %@" ,(categoryList[i]["categoryID"] as! String),"0")
            
            for j in 0..<puppies.count
            {
                let dict1 = NSMutableDictionary()
                dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
                dict1.setValue(puppies[j]["name"]!, forKey: "name")
                dict1.setValue(puppies[j]["image"]!, forKey: "image")
                dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
                dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
                dict1.setValue(puppies[j]["categoryType"]!, forKey: "categoryType")
                dict1.setValue(puppies[j]["isCategory"]!, forKey: "isCategory")
                items.add(dict1)
                subCategoryArray.add(dict1)
            }
            
//            let temp = Section(name: dict["name"]! as! String, image: dict["image"]! as! String, id: String(dict["id"]! as! Int) , categoryID: dict["categoryID"]! as! String, subCategoryID: dict["subCategoryID"]! as! String, categoryType: dict["categoryType"]! as! String, isCategory: dict["isCategory"]! as! String, items: items)
            
            let temp = Section(name: dict["name"]! as! String, image: dict["image"]! as! String, id: String(dict["id"]! as! Int) , categoryID: dict["categoryID"]! as! String, subCategoryID: dict["subCategoryID"]! as! String, categoryType: dict["categoryType"]! as! String, isCategory: dict["isCategory"]! as! String, items: items, collapsed: false)
            
            sections.append(temp)
            mutableArray.add(dict)
        }
        print(subCategoryArray)
        tblCategory.reloadData()
    }
    func getCategoryDataWithID(categoryType : String)
    {
        subCategoryArray = []
        let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND categoryID = %@","1",categoryType,strCategoryID)
        let mutableArray = NSMutableArray()
        sections = [Section]()
        
        for i in 0..<categoryList.count
        {
            let dict = NSMutableDictionary()
            dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
            dict.setValue(categoryList[i]["name"]!, forKey: "name")
            dict.setValue(categoryList[i]["image"]!, forKey: "image")
            dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
            dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
            dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
            dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
            items = []
            
            let puppies = uiRealm.objects(Category.self).filter("categoryID = %@ AND isCategory = %@" ,(categoryList[i]["categoryID"] as! String),"0")
            
            for j in 0..<puppies.count
            {
                let dict1 = NSMutableDictionary()
                dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
                dict1.setValue(puppies[j]["name"]!, forKey: "name")
                dict1.setValue(puppies[j]["image"]!, forKey: "image")
                dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
                dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
                dict1.setValue(puppies[j]["categoryType"]!, forKey: "categoryType")
                dict1.setValue(puppies[j]["isCategory"]!, forKey: "isCategory")
                items.add(dict1)
                subCategoryArray.add(dict1)
            }
            
//            let temp = Section(name: dict["name"]! as! String, image: dict["image"]! as! String, id: String(dict["id"]! as! Int) , categoryID: dict["categoryID"]! as! String, subCategoryID: dict["subCategoryID"]! as! String, categoryType: dict["categoryType"]! as! String, isCategory: dict["isCategory"]! as! String, items: items)
            
            let temp = Section(name: dict["name"]! as! String, image: dict["image"]! as! String, id: String(dict["id"]! as! Int) , categoryID: dict["categoryID"]! as! String, subCategoryID: dict["subCategoryID"]! as! String, categoryType: dict["categoryType"]! as! String, isCategory: dict["isCategory"]! as! String, items: items, collapsed: false)
            
            sections.append(temp)
            mutableArray.add(dict)
        }
        print(subCategoryArray)
        tblCategory.reloadData()
    }
    
    func showAnimate()
    {
        txtCategoryName.text = ""
        img_selectedCategory.image = UIImage(named: "ic_Add_blue.png")
        bgView.isHidden = false
        categoryView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        categoryView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        categoryView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.categoryView.alpha = 1.0
            self.categoryView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.categoryView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.categoryView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.categoryView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        if(passingView == "split" || passingView == "addTransaction")
        {
            
            sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed! = !sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed!
            tblCategory.reloadData()
         }
        else
        {
            if(isEdited == true)
            {
                let temp = sections[Int(gestureRecognizer.accessibilityHint!)!]
                print(temp)
                
                
                editDictionary = NSMutableDictionary()
                editDictionary.setValue(temp.id, forKey: "id")
                editDictionary.setValue(temp.categoryID, forKey: "categoryID")
                editDictionary.setValue(temp.categoryType, forKey: "categoryType")
                editDictionary.setValue(temp.image, forKey: "image")
                editDictionary.setValue(temp.isCategory, forKey: "isCategory")
                editDictionary.setValue(temp.name, forKey: "name")
                editDictionary.setValue(temp.subCategoryID, forKey: "subCategoryID")
                print(editDictionary)
                isMainCategory = "editMain"
                lblHeaderAddCategory.text = "Edit Main Category"
                showAnimate()
                txtCategoryName.text = temp.name
                img_selectedCategory.image = UIImage(named: temp.image!)
                img_icon = temp.image!
            }
            else
            {
                sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed! = !sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed!
                tblCategory.reloadData()
//                tblCategory.reloadSections(NSIndexSet(index: Int(gestureRecognizer.accessibilityHint!)!) as IndexSet, with: .automatic)
//                tblCategory.beginUpdates()
//                for i in 0..<sections[Int(gestureRecognizer.accessibilityHint!)!].items.count
//                {
//                    tblCategory.reloadRows(at: [IndexPath(row: i, section: Int(gestureRecognizer.accessibilityHint!)!)], with: .automatic)
//                }
//                tblCategory.endUpdates()
            }
        }
    }
    
    //MARK: - Tableview Delegate And Datasource
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if(searchActive) {
            return 1
        }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(searchActive) {
            return filterSubCat.count
        }
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if(searchActive) {
            return nil
        }
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "categorycell") as! CategoryCell
        
        headerCell.lblCategoryTitle.text = sections[section].name
        headerCell.lblCategoryTitle.font = tableMainFont
        headerCell.img_Category.image = UIImage(named: sections[section].image)
        headerCell.img_Category.layer.borderWidth = 0.3
        headerCell.img_Category.layer.cornerRadius = headerCell.img_Category.frame.size.width / 2
        headerCell.img_Category.layer.borderColor = Constant.tableTextColorCode.cgColor
        headerCell.img_Category.layer.masksToBounds = true
        headerCell.btnAddSubCategory.tag = section
//        headerCell.backgroundColor = UIColor(colorLiteralRed: 230.0/255.0, green: 229.0/255.0, blue: 235.0/255.0, alpha: 1)
//        headerCell.backgroundColor = UIColor.init(colorLiteralRed: 247/255, green: 250/255, blue: 253/255, alpha: 1)
        headerCell.backgroundColor = UIColor(red: 247/255, green: 250/255, blue: 253/255, alpha: 1)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.delegate = self
        tapRecognizer.accessibilityHint = String(section)
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        headerCell.addGestureRecognizer(tapRecognizer)
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subcategorycell") as! SubCategoryCell
        
//        if(searchActive) {
//            let temp = filtered[indexPath.row] as! NSMutableDictionary
        
        if(searchActive == true)
        {
          let temp = filterSubCat[indexPath.row] as! NSMutableDictionary
            cell.lblSubCategoryTitle.text = temp["name"] as? String
            cell.lblSubCategoryTitle.font = tableMainFont
            cell.img_SubCategory.image = UIImage(named: temp["image"] as! String)
            cell.img_SubCategory.layer.borderWidth = 0.3
            cell.img_SubCategory.layer.borderColor = Constant.tableTextColorCode.cgColor
            cell.img_SubCategory.layer.cornerRadius = cell.img_SubCategory.frame.size.width / 2
            cell.img_SubCategory.layer.masksToBounds = true
            if(isEdited == false)
            {
                cell.btnDeleteSubCategory.isHidden = true
            }
            else
            {
                cell.btnDeleteSubCategory.isHidden = false
            }
            cell.btnDeleteSubCategory.tag = temp["id"] as! Int
//            return cell
        }
        else
        {
            let temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
            cell.lblSubCategoryTitle.text = temp["name"] as? String
            cell.lblSubCategoryTitle.font = tableMainFont
            cell.img_SubCategory.image = UIImage(named: temp["image"] as! String)
            cell.img_SubCategory.layer.borderWidth = 0.3
            cell.img_SubCategory.layer.borderColor = Constant.tableTextColorCode.cgColor
            cell.img_SubCategory.layer.cornerRadius = cell.img_SubCategory.frame.size.width / 2
            cell.img_SubCategory.layer.masksToBounds = true
            if(isEdited == false)
            {
                cell.btnDeleteSubCategory.isHidden = true
            }
            else
            {
                cell.btnDeleteSubCategory.isHidden = false
            }
            cell.btnDeleteSubCategory.tag = temp["id"] as! Int
        }
        
        return cell
            
//            return cell
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if passingView == "split" || passingView == "addTransaction"
        {
            if(searchActive)
            {
                let  temp1 = filterSubCat[indexPath.row] as! NSMutableDictionary
                
                let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND categoryID = %@","1",temp1["categoryType"]!,temp1["categoryID"]!)
                temp1.setValue(categoryList[0].name, forKey: "categoryName")
                temp1.setValue(categoryList[0].image, forKey: "categoryimage")
                if(passingView == "split")
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetchSplitCategoryData"), object: temp1)
                }
                else if(passingView == "addTransaction")
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetchCategoryData"), object: temp1)
                }
                
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                let temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                print(sections[indexPath.section])
                temp.setValue(sections[indexPath.section].name!, forKey: "categoryName")
                temp.setValue(sections[indexPath.section].image!, forKey: "categoryimage")
                if(categoryType == "1")
                {
                    temp.setValue("1", forKey: "categoryType")
                }
                else{
                    temp.setValue("0", forKey: "categoryType")
                }
                
                if(passingView == "split")
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetchSplitCategoryData"), object: temp)
                }
                else if(passingView == "addTransaction")
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetchCategoryData"), object: temp)
                }
                
                self.dismiss(animated: true, completion: nil)
            }
        }
        else
        {
            
            if(isEdited == false)
            {
            }
            else
            {
                editDictionary = NSMutableDictionary()
                editDictionary = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
                editDictionary.setValue(sections[indexPath.section].name!, forKey: "categoryName")
                editDictionary.setValue(sections[indexPath.section].image!, forKey: "categoryimage")
                print(editDictionary)
                isMainCategory = "editSub"
                lblHeaderAddCategory.text = "Edit Sub Category"
                showAnimate()
                txtCategoryName.text = editDictionary["name"] as? String
                img_selectedCategory.image = UIImage(named: editDictionary["image"] as! String)
                img_icon = editDictionary["image"] as! String
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(searchActive)
        {
            return 0
        }
        else
        {
            return 44.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if sections[indexPath.section].collapsed
        {
            return 44
        }
        else
        {
            return 0
        }
    }
    
    
    
    //MARK: - Collectionview Delegate And Datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constant.categoryImage.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryimagecell", for: indexPath) as! categoryImageCell
        
        cell.categoryImg.image = UIImage(named: ((Constant.categoryImage[indexPath.row] as! NSDictionary).value(forKey: "name") as! String))
        cell.layer.cornerRadius = cell.frame.size.width / 2
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.3
        cell.layer.borderColor = UIColor.black.cgColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        img_selectedCategory.image = UIImage(named: ((Constant.categoryImage[indexPath.row] as! NSDictionary).value(forKey: "name") as! String))
        img_icon = ((Constant.categoryImage[indexPath.row] as! NSDictionary).value(forKey: "name") as! String)
    }
    
     // MARK: - Touch method
    
//    func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            
//            let contentInset:UIEdgeInsets = .zero
//            self.budgetscrollView.contentInset = contentInset
//            
//        }
//    }
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch = touches.first!
//        if touch.view != datePickerView && touch.view != dateHeaderView {
//            removeAnimate()
//        }
//        
//    }

    // MARK: - SearchBar Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
         //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
//        filteredArray = sections.filter {
//                let section : Section = $0
//            return (section.items["name"].range(of: searchText) != nil)
//          }
        
        /*
        filtered = subCategoryArray.filtered(using:
            {
            let s = $0 as? NSMutableDictionary
            
            let string = s.value(forKey: "name") as! NSString
            
            return $0["name"]?.rangeOfString(searchText, options: NSString.CompareOptions.caseInsensitive) != nil
            return  string.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location != NSNotFound
        }
        )*/
        
        let filteredVisitors = subCategoryArray.filter()
        {
            let s = $0 as? NSDictionary
            
            if (s?.value(forKey:"name") as! String).lowercased().contains(searchText.lowercased())
            {
                return true
            }
            else
            {
                return false
            }
        }
        
        if filteredVisitors.count == 0
        {
            filtered = []
        }
        else
        {
            filtered = filteredVisitors as! NSMutableArray
            print("Search Array : ",filtered)
        }
        /*
        filtered = subCategoryArray.filter()
        {
            let s = $0 as? NSMutableDictionary
            let string = s.value(forKey: "name") as! NSString
            return $0["name"]?.rangeOfString(searchText, options: NSString.CompareOptions.caseInsensitive) != nil
            return  string.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location != NSNotFound
        }*/
        
//        filteredArray = sections.filter
//            {
//            let section : Section = $0
//            let itemsArray = section.items.value(forKey: "name") as! NSMutableArray
//            
//            let filteredStrings = itemsArray.filter(using: {(item: String) -> Bool in
//                
//                var stringMatch = item.lowercaseString.rangeOfString(searchToSearch.lowercaseString)
//                return stringMatch != nil ? true : false
//            })
//            
//            return ((section.items.value(forKey:"name") as! String).range(of: searchText) != nil)
//        }
        
        let filteredArrays = subCategoryArray.filter
        {
            let section = $0

            let temp = section as! NSMutableDictionary

            return ((temp["name"] as! String).lowercased().range(of: searchText.lowercased()) != nil)
        }
        
        filterSubCat = filteredArrays as! NSMutableArray
       
        if(searchText == "")
        {
            searchActive = false;
            lblNoDataFoundSearch.isHidden = true
            tblCategory.isHidden = false
        }
        else
        {
            if(filterSubCat.count == 0)
            {
                lblNoDataFoundSearch.isHidden = false
                tblCategory.isHidden = true
            }
            else
            {
                lblNoDataFoundSearch.isHidden = true
                tblCategory.isHidden = false
            }
            searchActive = true;
        }
        
//        if(filterSubCat.count == 0)
//        {
//            self.filteredArrays = filteredArrays as! NSMutableArray
//            searchActive = false;
//        }
//        else
//        {
//            searchActive = true;
//        }
        tblCategory.reloadData()
        
    }

    
    //MARK: - Textfield Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Action Method
    
    @IBAction func indexChanged(sender: UISegmentedControl) {
        switch categorySegment.selectedSegmentIndex {
        case 0:
            categoryType = "1"
            self.getCategoryData(categoryType: categoryType)
            
        case 1:
            categoryType = "0"
            self.getCategoryData(categoryType: categoryType)
        default:
            break;
        }
    }
    
    @IBAction func addCategoryAction(_ sender: UIButton) {
        isMainCategory = "Main"
        lblHeaderAddCategory.text = "Add New Main Category"
        showAnimate()
    }
    
    @IBAction func addSubCategoryAction(_ sender: UIButton) {
        btnOk.tag = sender.tag
        isMainCategory = "Sub"
        lblHeaderAddCategory.text = "Add New Sub Category"
        showAnimate()
    }
    
    @IBAction func editAction(_ sender: UIButton)
    {
        if(isEdited == false)
        {
            isEdited = true
            tblCategory.reloadData()
        }
        else
        {
            isEdited = false
            tblCategory.reloadData()
        }
    }
    
    @IBAction func deleteSubCategoryAction(_ sender: UIButton) {
//        
        let categoryList = uiRealm.objects(Category.self).filter("id = %@",sender.tag)
        try! uiRealm.write {
            uiRealm.delete(categoryList)
        }
        
        getCategoryData(categoryType: categoryType)
        
    }
    
    @IBAction func closePopUp(_ sender: UIButton) {
        btnOk.tag = 0
        removeAnimate()
    }
    
    @IBAction func addNewSubCategory(_ sender: UIButton) {
        
        if isMainCategory == "Main"
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let catId = (uiRealm.objects(Category.self).max(ofProperty: "id") as Int?)! + 1
                    let count = sections.count - 1
                    let categoryId = uiRealm.objects(Category.self).value(forKey: "categoryID") as! NSArray
                    var uniqueDateArray = [Int]()
                    for i in 0..<categoryId.count
                    {
//                        if(!uniqueDateArray.contains(categoryId[i] as! Int))
//                        {
//                            uniqueDateArray.append(Int(categoryId[i] as! String)!)
//                        }
                        
                        if !uniqueDateArray.contains(Int(categoryId[i] as! String)!) {
                            uniqueDateArray.append(Int(categoryId[i] as! String)!)
                        }
                    }
                    
                    let max = uniqueDateArray.max()
                    
                    let categoryID : Int!
                    if(count == -1)
                    {
                        categoryID = 1
                    }
                    else
                    {
                        categoryID = max! + 1
                    }
                    
                    var taskListB = Category()
                    taskListB = Category(value: [catId,txtCategoryName.text!,img_icon,String(categoryID),String(0),categoryType,"1"])
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                    getCategoryData(categoryType: categoryType)
                    removeAnimate()
                }
                else{
                    self.alertView(alertMessage: AlertString.SelectIcon)
                }
            }
            else{
                self.alertView(alertMessage: AlertString.CategoryName)
            }
        }
        else if isMainCategory == "editSub"
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let workouts = uiRealm.objects(Category.self).filter("id = %@", (editDictionary["id"] as! Int))
//
                    if let workout = workouts.first {
                        try! uiRealm.write {
                            workout.categoryID = editDictionary["categoryID"] as! String
                            workout.categoryType = editDictionary["categoryType"] as! String
                            workout.image = img_icon
                            workout.isCategory = editDictionary["isCategory"] as! String
                            workout.name = txtCategoryName.text!
                            workout.subCategoryID = editDictionary["subCategoryID"] as! String
                        }
                        getCategoryData(categoryType: categoryType)
                        removeAnimate()
                    }
                }
                else{
                    self.alertView(alertMessage: "please select icon image")
                }
            }
            else{
                self.alertView(alertMessage: "please enter Category Name")
            }
        }
        else if isMainCategory == "editMain"
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let workouts = uiRealm.objects(Category.self).filter("id = %@", (Int(editDictionary["id"] as! String)!))
                    //
                    if let workout = workouts.first {
                        try! uiRealm.write {
                            workout.categoryID = editDictionary["categoryID"] as! String
                            workout.categoryType = editDictionary["categoryType"] as! String
                            workout.image = img_icon
                            workout.isCategory = editDictionary["isCategory"] as! String
                            workout.name = txtCategoryName.text!
                            workout.subCategoryID = editDictionary["subCategoryID"] as! String
                        }
                        getCategoryData(categoryType: categoryType)
                        removeAnimate()
                    }
                }
                else{
                    self.alertView(alertMessage: "please select icon image")
                }
            }
            else{
                self.alertView(alertMessage: "please enter Category Name")
            }
        }
        else
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let catId = (uiRealm.objects(Category.self).max(ofProperty: "id") as Int?)! + 1
                    let count = sections[sender.tag].items.count - 1
                    let subCatId : Int!
                    if(count == -1)
                    {
                        subCatId = 1
                    }
                    else
                    {
                        subCatId = Int((sections[sender.tag].items[count] as! NSMutableDictionary).value(forKey: "subCategoryID") as! String)! + 1
                    }
                    
                    var taskListB = Category()
                    taskListB = Category(value: [catId,txtCategoryName.text!,img_icon,sections[sender.tag].categoryID,String(subCatId),sections[sender.tag].categoryType,"0"])
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                    getCategoryData(categoryType: categoryType)
                    removeAnimate()
                }
                else{
                    self.alertView(alertMessage: "please select icon image")
                }
            }
            else{
                self.alertView(alertMessage: "please enter Category Name")
            }
            
        }
        

        
    }
    @IBAction func btnPushBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnback(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }

}
