//
//  BillsViewController.swift
//  Created by Cools on 10/13/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import EventKit

class BillsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,CalendarViewDataSource, CalendarViewDelegate {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var tblBilling: UITableView!
    @IBOutlet var btnAddBill: UIButton!
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var dateHeader: UIView!
    @IBOutlet var lblNoBills: UILabel!
    
    var expandedRows = Set<Int>()
    var isLoadingScreen: Bool = true
    var firstLogin : Bool = true
    var arrBillList = [[String: Any]]()
    var arrMonthlyBillList = [[String: Any]]()
    var arrBiMonthlyBillList = [[String: Any]]()
    var arrQuarterlyBillList = [[String: Any]]()
    var arrHalfYearlyBillList = [[String: Any]]()
    var arrYearlyBillList = [[String: Any]]()
    var selectedDateList = [[String: Any]]()
    var passingDate : Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        calendarView.dataSource = self
        calendarView.delegate = self
        
        calendarView.direction = .horizontal
        lblHeaderTitle.font = headerTitleFont
        lblNoBills.isHidden = true
        tblBilling.estimatedRowHeight = 74.0
        tblBilling.rowHeight = UITableViewAutomaticDimension
        tblBilling.tableFooterView = UIView.init(frame: CGRect.zero)
        
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstLogin {
            firstLogin = false
            let today = Date()
            self.calendarView.setDisplayDate(today, animated: false)
        }
        arrMonthlyBillList = getMonthlyBills(date: Date())
        arrBiMonthlyBillList = getBiMonthlyBills(date: Date())
        arrQuarterlyBillList = getQuarterlyBills(date: Date())
        arrHalfYearlyBillList = getHalfYearlyBills(date: Date())
        arrYearlyBillList = getYearlyBills(date: Date())
        
        getBillsTransaction()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnAddBill.layer.masksToBounds = false
        btnAddBill.layer.cornerRadius = btnAddBill.frame.size.width/2
        btnAddBill.layer.shadowColor = UIColor.lightGray.cgColor
        btnAddBill.layer.shadowOpacity = 0.7
        btnAddBill.layer.shadowOffset = CGSize(width: -1, height: 1)
        btnAddBill.layer.shadowRadius = 1
        
        
    }
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        self.calendarView.frame.size.width = UIScreen.main.bounds.size.width
        
        
    }
    
    // MARK:- KDCalendarDataSource
    
    func startDate() -> Date? {
        
        var dateComponents = DateComponents()
        dateComponents.month = -120
        
        let today = Date()
        
        let threeMonthsAgo = (self.calendarView.calendar as NSCalendar).date(byAdding: dateComponents, to: today, options: NSCalendar.Options())
        
        
        return threeMonthsAgo
    }
    
    func endDate() -> Date? {
        
        var dateComponents = DateComponents()
        
        dateComponents.year = 10;
        let today = Date()
        
        let twoYearsFromNow = (self.calendarView.calendar as NSCalendar).date(byAdding: dateComponents, to: today, options: NSCalendar.Options())
        
        return twoYearsFromNow
        
    }
    
    // MARK:- KDCalendarDelegate
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        print("Did Select: \(date) with Events: \(events)")
        
        if arrBillList.count > 0
        {
            selectedDateList = []
            for i in 0..<arrBillList.count
            {
                if String.convertFormatOfDateToStringSpecificFormat(date: date, format: "dd-MM-yyyy") == String.convertFormatOfDateToStringSpecificFormat(date: String.convertFormatOfStringToDate(date: arrBillList[i]["dueDate"] as! String), format: "dd-MM-yyyy")
                {
                    selectedDateList.append(arrBillList[i])
                }
            }
            
            if selectedDateList.count > 0
            {
                tblBilling.isHidden = false
                lblNoBills.isHidden = true
                tblBilling.reloadData()
            }
            else
            {
                tblBilling.isHidden = true
                lblNoBills.isHidden = false
            }
            
            
        }
        else
        {
            tblBilling.isHidden = true
            lblNoBills.isHidden = false
        }
        
    }
    
    func getMonthlyBills(date: Date) -> [[String:Any]]
    {
        var Temp = [[String: Any]]()
        let puppies = uiRealm.objects(Bills.self).filter("repeatType = %@","0")
        for j in 0..<puppies.count
        {
            var dict1 = [String: Any]()
            dict1 = ["id" : puppies[j]["id"] as! Int,
                     "categoryId" : puppies[j]["categoryId"]!,
                     "categoryName" : puppies[j]["categoryName"]!,
                     "subcategoryId" : puppies[j]["subcategoryId"]!,
                     "subcategoryName" : puppies[j]["subcategoryName"]!,
                     "dueDate" : String.convertFormatOfDate(date: puppies[j]["dueDate"]! as! Date),
                     "currentDate" : String.convertFormatOfDate(date: puppies[j]["currentDate"]! as! Date),
                     "amount" : puppies[j]["amount"]!,
                     "billId" : puppies[j]["billId"]!,
                     "repeatType" : puppies[j]["repeatType"]!,
                     "note" : puppies[j]["note"]!,
                     "url" : puppies[j]["url"]!,
                     "isPaid" : puppies[j]["isPaid"]!
            ]
            
            Temp.append(dict1)
        }
        print(Temp)
        return Temp
    }
    
    func getBiMonthlyBills(date: Date) -> [[String:Any]]
    {
        var Temp = [[String: Any]]()
        let puppies = uiRealm.objects(Bills.self).filter("repeatType = %@","1")
        for j in 0..<puppies.count
        {
            var dict1 = [String: Any]()
            dict1 = ["id" : puppies[j]["id"] as! Int,
                     "categoryId" : puppies[j]["categoryId"]!,
                     "categoryName" : puppies[j]["categoryName"]!,
                     "subcategoryId" : puppies[j]["subcategoryId"]!,
                     "subcategoryName" : puppies[j]["subcategoryName"]!,
                     "dueDate" : String.convertFormatOfDate(date: puppies[j]["dueDate"]! as! Date),
                     "currentDate" : String.convertFormatOfDate(date: puppies[j]["currentDate"]! as! Date),
                     "amount" : puppies[j]["amount"]!,
                     "billId" : puppies[j]["billId"]!,
                     "repeatType" : puppies[j]["repeatType"]!,
                     "note" : puppies[j]["note"]!,
                     "url" : puppies[j]["url"]!,
                     "isPaid" : puppies[j]["isPaid"]!
            ]
            
            Temp.append(dict1)
        }
        print(Temp)
        return Temp
    }
    
    func getQuarterlyBills(date: Date) -> [[String:Any]]
    {
        var Temp = [[String: Any]]()
        let puppies = uiRealm.objects(Bills.self).filter("repeatType = %@","2")
        for j in 0..<puppies.count
        {
            var dict1 = [String: Any]()
            dict1 = ["id" : puppies[j]["id"] as! Int,
                     "categoryId" : puppies[j]["categoryId"]!,
                     "categoryName" : puppies[j]["categoryName"]!,
                     "subcategoryId" : puppies[j]["subcategoryId"]!,
                     "subcategoryName" : puppies[j]["subcategoryName"]!,
                     "dueDate" : String.convertFormatOfDate(date: puppies[j]["dueDate"]! as! Date),
                     "currentDate" : String.convertFormatOfDate(date: puppies[j]["currentDate"]! as! Date),
                     "amount" : puppies[j]["amount"]!,
                     "billId" : puppies[j]["billId"]!,
                     "repeatType" : puppies[j]["repeatType"]!,
                     "note" : puppies[j]["note"]!,
                     "url" : puppies[j]["url"]!,
                     "isPaid" : puppies[j]["isPaid"]!
            ]
            
            Temp.append(dict1)
        }
        print(Temp)
        return Temp
    }
    
    func getHalfYearlyBills(date: Date) -> [[String:Any]]
    {
        var Temp = [[String: Any]]()
        let puppies = uiRealm.objects(Bills.self).filter("repeatType = %@","3")
        for j in 0..<puppies.count
        {
            var dict1 = [String: Any]()
            dict1 = ["id" : puppies[j]["id"] as! Int,
                     "categoryId" : puppies[j]["categoryId"]!,
                     "categoryName" : puppies[j]["categoryName"]!,
                     "subcategoryId" : puppies[j]["subcategoryId"]!,
                     "subcategoryName" : puppies[j]["subcategoryName"]!,
                     "dueDate" : String.convertFormatOfDate(date: puppies[j]["dueDate"]! as! Date),
                     "currentDate" : String.convertFormatOfDate(date: puppies[j]["currentDate"]! as! Date),
                     "amount" : puppies[j]["amount"]!,
                     "billId" : puppies[j]["billId"]!,
                     "repeatType" : puppies[j]["repeatType"]!,
                     "note" : puppies[j]["note"]!,
                     "url" : puppies[j]["url"]!,
                     "isPaid" : puppies[j]["isPaid"]!
            ]
            
            Temp.append(dict1)
        }
        print(Temp)
        return Temp
    }
    
    func getYearlyBills(date: Date) -> [[String:Any]]
    {
        var Temp = [[String: Any]]()
        let puppies = uiRealm.objects(Bills.self).filter("repeatType = %@","4")
        for j in 0..<puppies.count
        {
            var dict1 = [String: Any]()
            dict1 = ["id" : puppies[j]["id"] as! Int,
                     "categoryId" : puppies[j]["categoryId"]!,
                     "categoryName" : puppies[j]["categoryName"]!,
                     "subcategoryId" : puppies[j]["subcategoryId"]!,
                     "subcategoryName" : puppies[j]["subcategoryName"]!,
                     "dueDate" : String.convertFormatOfDate(date: puppies[j]["dueDate"]! as! Date),
                     "currentDate" : String.convertFormatOfDate(date: puppies[j]["currentDate"]! as! Date),
                     "amount" : puppies[j]["amount"]!,
                     "billId" : puppies[j]["billId"]!,
                     "repeatType" : puppies[j]["repeatType"]!,
                     "note" : puppies[j]["note"]!,
                     "url" : puppies[j]["url"]!,
                     "isPaid" : puppies[j]["isPaid"]!
            ]
            
            Temp.append(dict1)
        }
        print(Temp)
        return Temp
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
        print(date)
        calendar.events?.removeAll()
        
        if firstLogin {
            
        }
        else
        {
            arrMonthlyBillList = getMonthlyBills(date: Date())
            arrBiMonthlyBillList = getBiMonthlyBills(date: Date())
            arrQuarterlyBillList = getQuarterlyBills(date: Date())
            arrHalfYearlyBillList = getHalfYearlyBills(date: Date())
            arrYearlyBillList = getYearlyBills(date: Date())
        }
        
        
        arrBillList = []
        selectedDateList = []
        if arrMonthlyBillList.count > 0
        {
            for i in 0..<arrMonthlyBillList.count
            {
                let tempDate = String.convertFormatOfStringToDate(date: arrMonthlyBillList[i]["dueDate"] as! String)
                let noOfMonth = String.getMonthDiffrenceBetweenTwoDate(startDate: tempDate!.startOfMonth(), endDate: date)
                
                if noOfMonth > 0
                {
                    let nextMonthDate = String.getMonthWiseDate(mydate: tempDate!, Days: noOfMonth)
                    arrMonthlyBillList[i]["dueDate"] = String.convertFormatOfDate(date: nextMonthDate)
                    arrBillList.append(arrMonthlyBillList[i])
                    selectedDateList.append(arrMonthlyBillList[i])
                }
                else if noOfMonth < 0
                {
                    
                }
                else
                {
                    if Constant.getMonthYear(tempDate!) == Constant.getMonthYear(date)
                    {
                        arrBillList.append(arrMonthlyBillList[i])
                        selectedDateList.append(arrMonthlyBillList[i])
                    }
                }
            }
        }
        
        if arrBiMonthlyBillList.count > 0
        {
            for i in 0..<arrBiMonthlyBillList.count
            {
                let tempDate = String.convertFormatOfStringToDate(date: arrBiMonthlyBillList[i]["dueDate"] as! String)
                let noOfMonth = String.getMonthDiffrenceBetweenTwoDate(startDate: tempDate!.startOfMonth(), endDate: date)
                
                if noOfMonth > 0
                {
                    if noOfMonth%2 == 0
                    {
                        let nextMonthDate = String.getMonthWiseDate(mydate: tempDate!, Days: noOfMonth)
                        arrBiMonthlyBillList[i]["dueDate"] = String.convertFormatOfDate(date: nextMonthDate)
                        arrBillList.append(arrBiMonthlyBillList[i])
                        selectedDateList.append(arrBiMonthlyBillList[i])
                    }
                }
                else if noOfMonth < 0
                {
                    
                }
                else
                {
                    if Constant.getMonthYear(tempDate!) == Constant.getMonthYear(date)
                    {
                        arrBillList.append(arrBiMonthlyBillList[i])
                        selectedDateList.append(arrBiMonthlyBillList[i])
                    }
                    
                }
            }
        }
        
        if arrQuarterlyBillList.count > 0
        {
            for i in 0..<arrQuarterlyBillList.count
            {
                let tempDate = String.convertFormatOfStringToDate(date: arrQuarterlyBillList[i]["dueDate"] as! String)
                let noOfMonth = String.getMonthDiffrenceBetweenTwoDate(startDate: tempDate!.startOfMonth(), endDate: date)
                
                if noOfMonth > 0
                {
                    if noOfMonth%3 == 0
                    {
                        let nextMonthDate = String.getMonthWiseDate(mydate: tempDate!, Days: noOfMonth)
                        arrQuarterlyBillList[i]["dueDate"] = String.convertFormatOfDate(date: nextMonthDate)
                        arrBillList.append(arrQuarterlyBillList[i])
                        selectedDateList.append(arrQuarterlyBillList[i])
                    }
                }
                else if noOfMonth < 0
                {
                    
                }
                else
                {
                    if Constant.getMonthYear(tempDate!) == Constant.getMonthYear(date)
                    {
                        arrBillList.append(arrQuarterlyBillList[i])
                        selectedDateList.append(arrQuarterlyBillList[i])
                    }
                    
                }
            }
        }
        
        if arrHalfYearlyBillList.count > 0
        {
            for i in 0..<arrHalfYearlyBillList.count
            {
                let tempDate = String.convertFormatOfStringToDate(date: arrHalfYearlyBillList[i]["dueDate"] as! String)
                let noOfMonth = String.getMonthDiffrenceBetweenTwoDate(startDate: tempDate!.startOfMonth(), endDate: date)
                
                if noOfMonth > 0
                {
                    if noOfMonth%6 == 0
                    {
                        let nextMonthDate = String.getMonthWiseDate(mydate: tempDate!, Days: noOfMonth)
                        arrHalfYearlyBillList[i]["dueDate"] = String.convertFormatOfDate(date: nextMonthDate)
                        arrBillList.append(arrHalfYearlyBillList[i])
                        selectedDateList.append(arrHalfYearlyBillList[i])
                    }
                }
                else if noOfMonth < 0
                {
                    
                }
                else
                {
                    if Constant.getMonthYear(tempDate!) == Constant.getMonthYear(date)
                    {
                        arrBillList.append(arrHalfYearlyBillList[i])
                        selectedDateList.append(arrHalfYearlyBillList[i])
                    }
                    
                }
            }
        }
        
        if arrYearlyBillList.count > 0
        {
            for i in 0..<arrYearlyBillList.count
            {
                let tempDate = String.convertFormatOfStringToDate(date: arrYearlyBillList[i]["dueDate"] as! String)
                let noOfMonth = String.getMonthDiffrenceBetweenTwoDate(startDate: tempDate!.startOfMonth(), endDate: date)
                
                if noOfMonth > 0
                {
                    if noOfMonth%12 == 0
                    {
                        let nextMonthDate = String.getMonthWiseDate(mydate: tempDate!, Days: noOfMonth)
                        arrYearlyBillList[i]["dueDate"] = String.convertFormatOfDate(date: nextMonthDate)
                        arrBillList.append(arrYearlyBillList[i])
                        selectedDateList.append(arrYearlyBillList[i])
                    }
                }
                else if noOfMonth < 0
                {
                    
                }
                else
                {
                    if Constant.getMonthYear(tempDate!) == Constant.getMonthYear(date)
                    {
                        arrBillList.append(arrYearlyBillList[i])
                        selectedDateList.append(arrYearlyBillList[i])
                    }
                    
                }
            }
        }
        
        if arrBillList.count > 0
        {
            let store = EKEventStore()
            var eventEK = [EKEvent]()
            for i in 0 ..< arrBillList.count
            {
                //                var tomorrowComponents = DateComponents()
                //                tomorrowComponents.day = 1
                let event = EKEvent(eventStore: store)
                event.title = "Remain"
                event.startDate = String.convertFormatOfStringToDate(date: arrBillList[i]["dueDate"] as! String)
                event.endDate = String.convertFormatOfStringToDate(date: arrBillList[i]["dueDate"] as! String)
                event.notes = ""
                event.calendar = store.defaultCalendarForNewEvents
                eventEK.append(event)
            }
            self.calendarView.events = eventEK
            if selectedDateList.count > 0
            {
                tblBilling.isHidden = false
                lblNoBills.isHidden = true
                lblNoBills.text = "You have not added any Bills yet, To add Bills tap on + button"
                tblBilling.reloadData()
     //           tblBilling.contentSize.height = CGFloat(Float((selectedDateList.count * 70) + 70))
            }
            else
            {
                tblBilling.isHidden = true
                lblNoBills.isHidden = false
                lblNoBills.text = "You have not added any Bills yet, To add Bills tap on + button"
            }
        }
        else
        {
            if selectedDateList.count > 0
            {
                tblBilling.isHidden = false
                lblNoBills.isHidden = true
                lblNoBills.text = "You have not added any Bills yet, To add Bills tap on + button"
                tblBilling.reloadData()
//                tblBilling.contentSize.height = CGFloat(Float((selectedDateList.count * 70) + 70))
                
            }
            else
            {
                tblBilling.isHidden = true
                lblNoBills.isHidden = false
                lblNoBills.text = "You have not added any Bills yet, To add Bills tap on + button"
            }
        }
        
        
        
//        if isLoadingScreen {
//            isLoadingScreen = false
//            getBillsTransaction()
//        }
    }
    
    func getBillsTransaction()
    {
        let puppies = uiRealm.objects(Bills.self)
        arrBillList = []
        selectedDateList = []
        for j in 0..<puppies.count
        {
            var dict1 = [String: Any]()
            dict1 = ["id" : puppies[j]["id"] as! Int,
                     "categoryId" : puppies[j]["categoryId"]!,
                     "categoryName" : puppies[j]["categoryName"]!,
                     "subcategoryId" : puppies[j]["subcategoryId"]!,
                     "subcategoryName" : puppies[j]["subcategoryName"]!,
                     "dueDate" : String.convertFormatOfDate(date: puppies[j]["dueDate"]! as! Date),
                     "currentDate" : String.convertFormatOfDate(date: puppies[j]["currentDate"]! as! Date),
                     "amount" : puppies[j]["amount"]!,
                     "billId" : puppies[j]["billId"]!,
                     "repeatType" : puppies[j]["repeatType"]!,
                     "note" : puppies[j]["note"]!,
                "url" : puppies[j]["url"]!,
                "isPaid" : puppies[j]["isPaid"]!
            ]
            
            arrBillList.append(dict1)
            selectedDateList.append(dict1)
            
        }
        
        let store = EKEventStore()
        var eventEK = [EKEvent]()
        for i in 0 ..< arrBillList.count
        {
            //                var tomorrowComponents = DateComponents()
            //                tomorrowComponents.day = 1
            let event = EKEvent(eventStore: store)
            event.title = "Remain"
            event.startDate = String.convertFormatOfStringToDate(date: arrBillList[i]["dueDate"] as! String)
            event.endDate = String.convertFormatOfStringToDate(date: arrBillList[i]["dueDate"] as! String)
            event.notes = ""
            event.calendar = store.defaultCalendarForNewEvents
            eventEK.append(event)
        }
        print(eventEK)
        self.calendarView.events = eventEK
//        self.calendarView.reloadData()
        
        if selectedDateList.count > 0
        {
            tblBilling.isHidden = false
            lblNoBills.isHidden = true
            tblBilling.reloadData()
//            tblBilling.contentSize.height = CGFloat(Float((selectedDateList.count * 126) + 70))
        }
        else
        {
            tblBilling.isHidden = true
            lblNoBills.isHidden = false
        }

    }
    
    //MARK:- Action Method
    
    @IBAction func btnAddBillAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillCategoryViewController") as! BillCategoryViewController
        vc.passingDate = passingDate
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayNowAction(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblBilling)
        let indexPath = tblBilling.indexPathForRow(at: buttonPosition)
        
        let workouts = uiRealm.objects(Bills.self).filter("id = %@", (selectedDateList[sender.tag]["id"] as! Int))
        
        if let workout = workouts.first
        {
            try! uiRealm.write
            {
                workout.isPaid = true
            }
        }
        selectedDateList[sender.tag]["isPaid"] = true
        tblBilling.beginUpdates()
        tblBilling.reloadRows(at: [indexPath!], with: .automatic)
        tblBilling.endUpdates()
        
        
    }
    
    @IBAction func btnDeleteBillAction(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblBilling)
        let indexPath = tblBilling.indexPathForRow(at: buttonPosition)
        let categoryList = uiRealm.objects(Bills.self).filter("id = %@",selectedDateList[indexPath!.row]["id"] as! Int)
        try! uiRealm.write {
            uiRealm.delete(categoryList)
        }
        
        if selectedDateList.count == 1
        {
            for i in 0..<selectedDateList.count
            {
                for j in 0..<arrBillList.count
                {
                    if selectedDateList[indexPath!.row]["id"] as! Int == arrBillList[j]["id"] as! Int
                    {
                        
                        
                        self.selectedDateList.remove(at: indexPath!.row)
                        self.arrBillList.remove(at: j)
                        self.calendarView.events?.remove(at: j)
                        break
                    }
                }
                break
            }
        }
        else
        {
            for j in 0..<arrBillList.count
            {
                if arrBillList[j]["id"] as! Int == selectedDateList[indexPath!.row]["id"] as! Int
                {
                    self.selectedDateList.remove(at: indexPath!.row)
                    self.arrBillList.remove(at: j)
                    break
                }
            }
            
//            self.selectedDateList.remove(at: indexPath!.row)
        }
        
        
        
        tblBilling.beginUpdates()
        tblBilling.deleteRows(at: [indexPath!], with: .fade)
        tblBilling.endUpdates()
        
        if selectedDateList.count > 0
        {
            tblBilling.isHidden = false
            lblNoBills.isHidden = true

        }
        else
        {
            tblBilling.isHidden = true
            lblNoBills.isHidden = false
        }
        
    }
    @IBAction func btnEditBillAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddBillViewController") as! AddBillViewController
        vc.editBillData = selectedDateList[sender.tag] as [String : AnyObject]
        vc.passingView = "BillList"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return selectedDateList.count
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let aCell = tableView.dequeueReusableCell(withIdentifier: "BillingCell") as! BillingTableViewCell
        
        let strAount : String = (selectedDateList[indexPath.row]["amount"] as? String)!
        aCell.lblAmount.text = strAount
        aCell.lblDescribtion.text = (selectedDateList[indexPath.row]["categoryName"] as? String)!
        aCell.lblItems.text = (selectedDateList[indexPath.row]["subcategoryName"] as? String)!
        aCell.lblDate.text = (selectedDateList[indexPath.row]["currentDate"] as? String)!
        aCell.lblReminder.text = "\(String(Constant.dayDifferenceBetweenTwoDate(date1: Date(), date2: String.convertFormatOfStringToDate(date: selectedDateList[indexPath.row]["dueDate"] as! String)))) days left"
        aCell.btnPayNow.addTarget(self, action: #selector(BillsViewController.btnPayNowAction(_:)), for: .touchUpInside)
        aCell.btnDelete.addTarget(self, action: #selector(BillsViewController.btnDeleteBillAction(_:)), for: .touchUpInside)
        aCell.btnEdit.addTarget(self, action: #selector(BillsViewController.btnEditBillAction(_:)), for: .touchUpInside)
        aCell.btnEdit.tag = indexPath.row
        aCell.btnDelete.tag = indexPath.row
        aCell.btnPayNow.tag = indexPath.row
        if selectedDateList[indexPath.row]["isPaid"] as! Bool
        {
//            aCell.btnPayNow.isHidden = true
            aCell.lblpaidTitle.isHidden = false
            aCell.lblReminder.isHidden = true
        }
        else
        {
//            aCell.btnPayNow.isHidden = false
            aCell.lblpaidTitle.isHidden = true
            aCell.lblReminder.isHidden = false
        }
        
        aCell.isPaid = selectedDateList[indexPath.row]["isPaid"] as! Bool
        
        aCell.isExpanded = self.expandedRows.contains(indexPath.row)
        
        
        
        return aCell
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 150.0
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? BillingTableViewCell
            else { return }
        
        switch cell.isExpanded
        {
        case true:
            self.expandedRows.remove(indexPath.row)
//            tblBilling.contentSize.height = CGFloat(Float((selectedDateList.count * 70) + 70))
            
        case false:
            self.expandedRows.insert(indexPath.row)
        
//            tblBilling.contentSize.height = CGFloat(Float((selectedDateList.count * 126) + 50))
        }
        
        cell.isExpanded = !cell.isExpanded
        
        tblBilling.beginUpdates()
        tblBilling.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? BillingTableViewCell
            else { return }
        
        self.expandedRows.remove(indexPath.row)
//        tblBilling.contentSize.height = CGFloat(Float((selectedDateList.count * 70) + 70))
        cell.isExpanded = false
        
        tblBilling.beginUpdates()
        tblBilling.endUpdates()
    }
    
//    func scrollToLastRow() {
//        let indexPath = NSIndexPath(forRow: .count - 1, inSection: 0)
//        self.tblBilling.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
//    }
}

    
