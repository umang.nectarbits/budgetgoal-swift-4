//
//  BillSubCategoryViewController.swift
//  Outlays
//
//  Created by harshesh on 26/10/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BillSubCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,GADBannerViewDelegate {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var HeaderView: UIView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var tblBilSubCategory: UITableView!
    @IBOutlet var BillSerch: UISearchBar!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var subcategorylist  = ""
    var categoryData = [String:AnyObject]()
    var subcategoryData = [String:AnyObject]()
    var subCategoryArray = [[String:AnyObject]]()
    var searchActive: Bool = false
    var filterSubCat = [[String:AnyObject]]()
    var passingDate : Date!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        print(subcategoryData)

        lblHeaderTitle.font = headerTitleFont
        tblBilSubCategory.estimatedRowHeight = 44.0
        tblBilSubCategory.rowHeight = UITableViewAutomaticDimension
        tblBilSubCategory.tableFooterView = UIView.init(frame: CGRect.zero)
        
        subCategoryArray = subcategoryData["data"] as! [[String : AnyObject]]
        let textFieldInsideSearchBar = BillSerch.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.addDoneButtonToKeyboard(myAction:  #selector(textFieldInsideSearchBar?.resignFirstResponder))
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

//MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchActive) {
            return filterSubCat.count
        }
        return subCategoryArray.count
    
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let aCell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell") as! BillSubCategoryCell
        
        if searchActive{
            aCell.lblSubTitle.text = filterSubCat[indexPath.row]["vSubCatName"] as? String
            
            if filterSubCat[indexPath.row]["imagePath"] as! String != ""
            {
                aCell.imgTitle.imageFromServerURL(urlString: filterSubCat[indexPath.row]["imagePath"] as! String)
            }
        }
        else
        {
            aCell.lblSubTitle.text = subCategoryArray[indexPath.row]["vSubCatName"] as? String
            
            if subCategoryArray[indexPath.row]["imagePath"] as! String != ""
            {
                aCell.imgTitle.imageFromServerURL(urlString: subCategoryArray[indexPath.row]["imagePath"] as! String)
            }
        }
        
        
         return aCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddBillViewController") as! AddBillViewController
        vc.categoryData = categoryData
        vc.passingDate = passingDate
        if searchActive{
            vc.subCategoryData = filterSubCat[indexPath.row]
        }
        else{
            vc.subCategoryData = subCategoryArray[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK:- UISearchview Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        let filteredArrays = subCategoryArray.filter{($0["vSubCatName"] as! String).lowercased().range(of: searchText.lowercased(), options: .caseInsensitive) != nil}
//        {
//            let section = $0
//            
//            let temp = section
//            
//            return ((temp["vSubCatName"] as! String).range(of: searchText) != nil)
//        }
        
        filterSubCat = filteredArrays
        
        if(searchText == "")
        {
            searchActive = false;
//            lblNoDataFoundSearch.isHidden = true
            tblBilSubCategory.isHidden = false
        }
        else
        {
            if(filterSubCat.count == 0)
            {
//                lblNoDataFoundSearch.isHidden = false
                tblBilSubCategory.isHidden = true
            }
            else
            {
//                lblNoDataFoundSearch.isHidden = true
                tblBilSubCategory.isHidden = false
            }
            searchActive = true;
        }
        
        tblBilSubCategory.reloadData()
    }

    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
        
    }

    
//MARK: - Action View
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
