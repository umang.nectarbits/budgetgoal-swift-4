//
//  MainAccountListVC.swift
//  Outlays
//
//  Created by Cools on 12/8/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit

class accountTableCell : UITableViewCell{
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var BgView: UIView!
    @IBOutlet weak var lblCurrentBalanceTitle: UILabel!
    @IBOutlet weak var lblCurrentBalance: UILabel!
    @IBOutlet weak var lblAccountTitle: UILabel!
    
}

class MainAccountListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    //MARK:- IBOutlet Variable
    @IBOutlet weak var accountTableView: UITableView!
    @IBOutlet var lblHeaderName : UILabel!
    @IBOutlet var viewAddAccount : UIView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var currencyView: UIView!
    @IBOutlet weak var tblcurrency: UITableView!
    @IBOutlet weak var currencySearchBar: UISearchBar!
    @IBOutlet var btnCancel: UIButton!
    
    //MARK:- Local Variable
    var accountArray = [[String:AnyObject]]()
    var searchActive: Bool = false
    var countryArray = NSMutableArray()
    var countryNewArray : [String]! = []
    var strCurrency : String = ""
    var filtered: [String] = []
    var selectedIndex = Int ()
    var accountSelect = ""
    
    //MARK:- Views Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        lblHeaderName.font = headerFont
        viewAddAccount.viewShadow()
        
        let image = UIImage(named: "ic_cancel")?.withRenderingMode(.alwaysTemplate)
        btnCancel.setImage(image, for: .normal)
        btnCancel.tintColor = UIColor.black
        
        currencyView.isHidden = true
        bgView.isHidden = true
        currencyView.layer.cornerRadius = 8
        currencyView.layer.masksToBounds = true
        accountTableView.setEditing(false, animated: true)
        accountTableView.estimatedRowHeight = 99.0
        accountTableView.rowHeight = UITableViewAutomaticDimension
        accountTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        countryArray = (IsoCountries.allCountries as NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<countryArray.count {
            let temp = countryArray[i] as! IsoCountryInfo
            countryNewArray.append(temp.name)
        }
        if !Constant.isKeyPresentInUserDefaults(key: "currencySymbol") {
            UserDefaults.standard.set("", forKey: "currencySymbol")
            showAnimate1(selectOption: "Currency")
        }
        else
        {
            if UserDefaults.standard.value(forKey: "currencySymbol") as! String == ""
            {
                UserDefaults.standard.set("", forKey: "currencySymbol")
                showAnimate1(selectOption: "Currency")
            }
            else
            {
                getAccountData()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        tblcurrency.tableFooterView = UIView.init(frame: CGRect.zero)
        //Get Account Data
//        getAccountData()
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !Constant.isKeyPresentInUserDefaults(key: "currencySymbol") {
        }
        else
        {
            if UserDefaults.standard.value(forKey: "currencySymbol") as! String == ""
            {
            }
            else
            {
                getAccountData()
            }
        }
    }
    
    //MARK:- Keyboard Method
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            
            let window = self.currencyView.window?.frame {
            let viewHeight = self.currencyView.frame.height
            self.currencyView.frame = CGRect(x: self.currencyView.frame.origin.x,
                                             y: self.currencyView.frame.origin.y,
                                             width: self.currencyView.frame.width,
                                             height: viewHeight - keyboardSize.height)
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.currencyView.frame.height
            self.currencyView.frame = CGRect(x: self.currencyView.frame.origin.x,
                                             y: self.currencyView.frame.origin.y,
                                             width: self.currencyView.frame.width,
                                             height: viewHeight + keyboardSize.height)
        }
        
    }
    
    //MARK:- Helper Method
    func showAnimate1(selectOption : String)
    {
        if selectOption == "Currency"
        {
            bgView.isHidden = false
            currencyView.isHidden = false
            bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            bgView.alpha = 0.0;
            currencyView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            currencyView.alpha = 0.0;
            tblcurrency.dataSource = self
            tblcurrency.delegate = self
            tblcurrency.reloadData()
            UIView.animate(withDuration: 0.25, animations: {
                self.bgView.alpha = 0.7
                self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.currencyView.alpha = 1.0
                self.currencyView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
            });
        }
    }
    
    func removeAnimate1(selectOption : String)
    {
        if selectOption == "Currency"
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.currencyView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.currencyView.alpha = 0.0;
                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.currencyView.isHidden = true
                    self.bgView.isHidden = true
                    //                self.tblList.reloadData()
                }
            });
        }
    }
    
    func getAccountData()
    {
        let puppies = uiRealm.objects(Account.self)
        accountArray = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["id"] = puppies[j]["id"]! as AnyObject
            dict1["name"] = puppies[j]["name"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["remainigAmount"] = puppies[j]["remainigAmount"]! as AnyObject
            dict1["date"] = String.convertFormatOfDate(date: puppies[j]["date"]! as! Date) as AnyObject
            dict1["isIncludeOnDashboard"] = puppies[j]["isIncludeOnDashboard"]! as AnyObject
            dict1["isDefaultAccount"] = puppies[j]["isDefaultAccount"]! as AnyObject
            
            accountArray.append(dict1)
        }
        print(accountArray)
        accountTableView.reloadData()
    }
    
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
        }
        
        return currencySymbol
    }
    
    //MARK:- Tableview Method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == tblcurrency)
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == tblcurrency)
        {
            if(searchActive) {
                return filtered.count
            }
            return countryArray.count
        }
        else
        {
            return accountArray.count
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == tblcurrency)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCountry", for: indexPath as IndexPath)
            
            cell.imageView?.layer.cornerRadius = 5
            cell.imageView?.layer.masksToBounds = true
            
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                        
                    {
                        cell.imageView?.image = UIImage(named: "\(temp.alpha2).png")
                        cell.textLabel?.text = temp.name
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = countryname.currency
                
                cell.imageView?.image = UIImage(named: "\(countryname.alpha2).png")
                cell.textLabel?.text = countryname.name
            }
            return cell
        }
        else
        {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "accountCell", for: indexPath as IndexPath) as! accountTableCell
            cell2.BgView.layer.cornerRadius = 8
//            cell2.contentView.layer.shadowColor = UIColor(colorLiteralRed: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
            cell2.contentView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha:  0.32).cgColor
            cell2.contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
            cell2.contentView.layer.cornerRadius = 8
            cell2.contentView.layer.shadowOpacity = 1.5
            cell2.contentView.layer.shadowRadius = 3
            
            //Get Account Wise Expense Data
            var sumOfExpense = NSArray()
            var totalExpense = ""
            sumOfExpense = uiRealm.objects(Transaction.self).filter("accountId = %@ AND setType = %@ ", accountArray[indexPath.row]["id"] as! Int,"0").value(forKey: "amount") as! NSArray
            
            if sumOfExpense.count == 0
            {
                totalExpense = "0.0"
            }
            else
            {
                totalExpense = String.sum(array: sumOfExpense)
            }
            
//            let currentBalance = uiRealm.objects(Account.self).filter("isIncludeOnDashboard = %@ AND id = %@",true,accountArray[indexPath.row]["id"] as! Int).sum(ofProperty: "amount") - Double(totalExpense)!
            
            let newBalance = uiRealm.objects(Account.self).filter("id = %@",accountArray[indexPath.row]["id"] as! Int).sum(ofProperty: "amount") - Double(totalExpense)!
            
            cell2.lblAccountName.text = accountArray[indexPath.row]["name"] as? String
            cell2.lblCurrentBalance.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(newBalance))"
            if newBalance < 0
            {
                cell2.lblCurrentBalance.textColor = UIColor.red
            }
            else
            {
//                cell2.lblCurrentBalance.textColor = UIColor.green
                if #available(iOS 10.0, *) {
                    cell2.lblCurrentBalance.textColor = UIColor(displayP3Red: 50.0/255.0, green: 205.0/255.0, blue: 50.0/255.0, alpha: 1.0)
                } else {
                    // Fallback on earlier versions
                }
            }
            if ((accountArray[indexPath.row]["id"] as! Int) == (UserDefaults.standard.integer(forKey: "AccountID")))
            {
//                cell2.BgView.backgroundColor = UIColor.darkGray
                if #available(iOS 10.0, *) {
                    cell2.BgView.backgroundColor = UIColor(displayP3Red: 52.0/255.0, green: 63.0/255.0, blue: 116.0/255.0, alpha: 1.0)
                } else {
                    // Fallback on earlier versions
                }
                cell2.lblAccountName.textColor = UIColor.white
                cell2.lblAccountTitle.textColor = UIColor.white
                cell2.lblCurrentBalanceTitle.textColor = UIColor.white
            }
            else
            {
                if #available(iOS 10.0, *) {
                    cell2.BgView.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                } else {
                    // Fallback on earlier versions
                    cell2.BgView.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                }
            }
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {

//        let delete = UITableViewRowAction(style: .destructive, title: "Delete"){ (action, indexPath) -> Void in
//
//            self.accountArray.remove(at: indexPath.row)
//            self.accountTableView.reloadData()
//            }
        
        let action2 = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction: UITableViewRowAction, indexPAth: IndexPath) in
            //code for share
            print("Edit")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAccountViewController") as! AddAccountViewController
            vc.passingView = "Edit"
            vc.transferView = "MainAccounts"
            vc.editData = self.accountArray[indexPAth.row]
            vc.passingDate = Date()
            self.present(vc, animated: true, completion: nil)
        
        }
        return [action2]
//        return [action2, delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (tableView == tblcurrency) {
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                    {
                        let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                        strCurrency = currencyCode
                        UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = getCurrencySymbol(currencyCode: countryname.currency)
                strCurrency = currencyCode
                UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
            }
            removeAnimate1(selectOption: "Currency")
            currencySearchBar.resignFirstResponder()
            getAccountData()
        }
        else
        {
            UserDefaults.standard.set(accountArray[indexPath.row]["id"] as! Int, forKey: "AccountID")
            accountTableView.reloadData()
            UserDefaults.standard.set(accountArray[indexPath.row]["name"] as! String, forKey: "name")
            UserDefaults.standard.set(accountArray[indexPath.row]["remainigAmount"] as! Int, forKey: "remainigAmount")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - SearchBar Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = countryNewArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tblcurrency.reloadData()
        
    }
    
    //MARK:- Action Method
    @IBAction func btnMoveAction(_sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelAction(_sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddAccountAction(_sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAccountViewController") as! AddAccountViewController
        vc.transferView = "MainAccounts"
        vc.passingDate = Date()
        self.present(vc, animated: true, completion: nil)
    }

}
