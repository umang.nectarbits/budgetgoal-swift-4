//
//  DetailOverviewViewController.swift
//  Finance
//
//  Created by Cools on 7/6/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds

class DetailOverviewViewController: UIViewController,ChartViewDelegate,UITableViewDelegate,UITableViewDataSource,UIViewControllerPreviewingDelegate,GADBannerViewDelegate {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var detailChartView : PieChartView!
    @IBOutlet var lblCategoryTypeDetail : UICountingLabel!
    @IBOutlet var lblHeader : UILabel!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var chartArray = NSMutableArray()
    var sectionsCat = [SectionCategory]()
    var percentageCountArray = NSMutableArray()
    var transactionArray = NSMutableArray()
    var uniqueDateArray : NSMutableArray = []
    var steCategoryType = ""
    var selectMonth : Date!
    var dateArray : [NSDate] = []
    @IBOutlet var tblOverview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        if( traitCollection.forceTouchCapability == .available){
            
            registerForPreviewing(with: self, sourceView: view)
            
        }
        
        tblOverview.estimatedRowHeight = 60.0
        
        tblOverview.rowHeight = UITableViewAutomaticDimension
        
        tblOverview.tableFooterView = UIView.init(frame: CGRect.zero)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailOverviewViewController.showSpinningWheelOverview(notification:)), name: NSNotification.Name(rawValue: "detectAfterDeleteEventOverview"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    @objc func showSpinningWheelOverview(notification: NSNotification) {
        viewWillAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let puppies = uiRealm.objects(Transaction.self).filter("accountId = %@",UserDefaults.standard.integer(forKey: "AccountID"))
        
        lblHeader.text = Constant.getMonthYear(selectMonth)
        lblHeader.font = headerTitleFont//tableHeaderAmountFont
        
        transactionArray = []
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            dict1.setValue(puppies[j]["repeatType"]!, forKey: "repeatType")
            dict1.setValue(puppies[j]["isUpdateByService"]!, forKey: "isUpdateByService")
            dict1.setValue(puppies[j]["updatedDate"]!, forKey: "updatedDate")
            dict1.setValue(String(describing: puppies[j]["milliseconds"]!), forKey: "milliseconds")
            transactionArray.add(dict1)
        }
        chartFunc()
    }
    
    func filterByIncome()
    {
        print(selectMonth)
        
        uniqueDateArray = []
        sectionsCat = []
        
        dateArray = []
        let dateList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@}", selectMonth.startOfMonth(), selectMonth.endOfMonth()).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        let categoryIDList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND setType = %@", selectMonth.startOfMonth(), selectMonth.endOfMonth(),steCategoryType).value(forKey: "categoryID") as! NSArray
        
        for i in 0..<categoryIDList.count
        {
            if(!uniqueDateArray.contains(categoryIDList[i]))
            {
                uniqueDateArray.add(categoryIDList[i])
            }
        }
        
        let uniqueArray : NSMutableArray = []
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueArray.contains(temp!))
            {
                uniqueArray.add(temp!)
            }
        }
        
        for j in 0..<uniqueArray.count//uniqueArray
        {
            var totalSplitedAmount : Double = 0.0
            let temp2 : NSMutableArray = []
            
            for i in 0..<transactionArray.count
            {
                if(uniqueArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String )
                {
                    for k in 0..<uniqueDateArray.count
                    {
                        if(uniqueArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && uniqueDateArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String != "0")
                        {
                            totalSplitedAmount = totalSplitedAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalSplitedAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            
                            print(dict)
                            //                            category_name = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String
                            //                            category_image = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryImage") as! String
                            temp2.add(dict)
                        }
                    }
                }
            }
            
            if temp2.count > 0
            {
                let tempSplit = SectionCategory(amount: String(totalSplitedAmount) , categoryID: "", categoryName: "Split Amount", categoryImage: "ic_split", items: temp2)
                print(tempSplit)
                sectionsCat.append(tempSplit)
            }
        }
        
        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0
            let temp1 : NSMutableArray = []
            var category_name : String = ""
            var category_image : String = ""
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String == "0")
                {
                    for k in 0..<uniqueArray.count
                    {
                        if(uniqueArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String == "0")
                        {
                            totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            print(dict)
                            category_name = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String
                            category_image = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryImage") as! String
                            temp1.add(dict)
                        }
                    }
                    
                    
                }
            }
            
            if temp1.count > 0
            {
                let temp = SectionCategory(amount: String(totalAmount) , categoryID: uniqueDateArray[j] as! String, categoryName: category_name, categoryImage: category_image, items: temp1)
                print(temp)
                sectionsCat.append(temp)
            }
            
//            let temp = SectionCategory(amount: String(totalAmount) , categoryID: uniqueDateArray[j] as! String, categoryName: category_name, categoryImage: category_image, items: temp1)
//            print(temp)
//            sectionsCat.append(temp)
        }
        
        if sectionsCat.count > 0
        {
            tblOverview.reloadData()
        }
        else
        {
            tblOverview.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func chartFunc()
    {
        filterByIncome()
        tblOverview.reloadData()
        print(sectionsCat)
        setupPieChartView(detailChartView)
        detailChartView.delegate = self
        
        detailChartView.entryLabelColor = UIColor.white
        detailChartView.entryLabelFont = tableSubFont
        
        detailChartView.animate(xAxisDuration: 1.4)
        
        setDataCount(sectionsCat.count, range: 100, detailChartView)
    }
    
    func setDataCount(_ count: Int, range: Double, _ chartView: PieChartView) {
        var totalAmont : Double = 0.0
        percentageCountArray = []
        for i in 0..<sectionsCat.count
        {
            totalAmont = totalAmont + Double(sectionsCat[i].amount)!
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        if steCategoryType == "1"
        {
            lblCategoryTypeDetail.formatBlock = {(_ value: CGFloat) -> String in
                let formatted: String? = formatter.string(for: Double(value))
                return "Income : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
            }
            lblCategoryTypeDetail.count(from: 0, to: CGFloat(totalAmont), withDuration: 0.0)
            
//            lblCategoryTypeDetail.text = "Income : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(totalAmont)"
            lblCategoryTypeDetail.font = tableMainFont
            lblCategoryTypeDetail.textColor = Constant.incomeColorCode
        }
        else
        {
            lblCategoryTypeDetail.formatBlock = {(_ value: CGFloat) -> String in
                let formatted: String? = formatter.string(for: Double(value))
                return "Expense : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
            }
            lblCategoryTypeDetail.count(from: 0, to: CGFloat(totalAmont), withDuration: 0.0)
            
//            lblCategoryTypeDetail.text = "Expense : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(totalAmont)"
            lblCategoryTypeDetail.font = tableMainFont
            lblCategoryTypeDetail.textColor = Constant.expenseColorCode
        }
        
        for i in 0..<sectionsCat.count
        {
            let percentage : Double = (Double(sectionsCat[i].amount)! * 100)/totalAmont
            percentageCountArray.add(String(percentage))
        }
        
        let values : NSMutableArray = NSMutableArray()
        for i in 0..<count {
            values.add(PieChartDataEntry(value: Double(Float(percentageCountArray[i] as! String)!), label: "\(sectionsCat[i].categoryName!) (\(String(format:"%.1f", Float(percentageCountArray[i] as! String)!)))%", icon: UIImage(named: "icon")))
        }
        let dataSet = PieChartDataSet(values: values as? [ChartDataEntry], label: "")
        dataSet.drawIconsEnabled = false
        dataSet.sliceSpace = 0.0
        //        dataSet.iconsOffset = CGPoint(x: CGFloat(0), y: CGFloat(60))
        
        // add a lot of colors
        let colors : NSMutableArray = NSMutableArray()
        colors.addObjects(from: ChartColorTemplates.colorful())
        colors.addObjects(from: ChartColorTemplates.joyful())
        colors.addObjects(from: ChartColorTemplates.vordiplom())
        colors.addObjects(from: ChartColorTemplates.liberty())
        colors.addObjects(from: ChartColorTemplates.pastel())
        colors.addObjects(from: ChartColorTemplates.material())
        
        dataSet.colors = colors as! [NSUIColor]
        let data = PieChartData(dataSet: dataSet)
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 2
        pFormatter.multiplier = 1.0
        pFormatter.percentSymbol = "%"
        //        data.setValueFormatter(pFormatter as? IValueFormatter)
        
        data.setValueFont(UIFont(name: ".SFUIDisplay", size: 8.0))
        
        data.setValueTextColor(UIColor.white)
        
        chartView.data = data
        
        chartView.highlightValues(nil)
    }
    
    func setupPieChartView(_ chartView: PieChartView) {
        chartView.backgroundColor = UIColor.white
        chartView.usePercentValuesEnabled = false
        chartView.drawEntryLabelsEnabled = false
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.0
        chartView.transparentCircleRadiusPercent = 0.0
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 5.0, top: 10.0, right: 5.0, bottom: 5.0)
        chartView.drawCenterTextEnabled = true
        let paragraphStyle: NSMutableParagraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        let centerText = NSMutableAttributedString(string: "Income")
        centerText.setAttributes([NSAttributedStringKey.font: tableSubFont as Any, NSAttributedStringKey.paragraphStyle: paragraphStyle ], range: NSRange(location: 0, length: centerText.length))
        
        chartView.drawHoleEnabled = false
        chartView.rotationAngle = 0.0
        chartView.rotationEnabled = false
        chartView.highlightPerTapEnabled = false
        
//        let l: Legend? = chartView.legend
//        l?.horizontalAlignment = .right
//        l?.verticalAlignment = .top
//        l?.orientation = .vertical
//        l?.drawInside = false
//        l?.xEntrySpace = 7.0
//        l?.yEntrySpace = 0.0
//        l?.yOffset = 0.0

    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        let point : CGPoint = self.tblOverview.convert(location, from: self.view)
        
        guard let indexPath = self.tblOverview.indexPathForRow(at: point) else { return nil }
        
        guard let cell = tblOverview.cellForRow(at: indexPath) else { return nil }
        
        guard let detailVC = storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController else { return nil }
        
        var temp = NSMutableDictionary()
        
        temp = sectionsCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        
//        detailVC.preferredContentSize = CGSize(width: 0.0, height: 500)
//        detailVC.transactionDict = temp
//        detailVC.passingType = "edit"
        previewingContext.sourceRect = cell.frame
        
        return detailVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        //        show(viewControllerToCommit, sender: self)
        present(viewControllerToCommit, animated: true, completion: nil)
        
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
    }

    
    //MARK: - Action Method
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Tableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsCat.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! TransactionHeaderCell
        headerCell.backgroundColor = UIColor.white
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        headerCell.lblCategoryTotalAmount.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        
        
        headerCell.img_Category.isHidden = false
        headerCell.lblCategoryName.isHidden = false
        headerCell.lblCategoryName.font = tableMainFont
        headerCell.lblCategoryTotalAmount.isHidden = false
        headerCell.lblCategoryName.text = sectionsCat[section].categoryName
        headerCell.lblCategoryTotalAmount.font = tableMainFont
        
        headerCell.lblCategoryTotalAmount.method = .easeOut
        
        headerCell.lblCategoryTotalAmount.count(from: 0, to: CGFloat(Double(sectionsCat[section].amount!)!), withDuration: 0.0)
        
        headerCell.lblCategoryName.textColor = Constant.tableTextColorCode
        headerCell.lblCategoryTotalAmount.textColor = Constant.tableTextColorCode
        headerCell.img_Category.image = UIImage(named: sectionsCat[section].categoryImage)
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 30.0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sectionsCat[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "subCell") as! TransactionCell
        
        var temp = NSMutableDictionary()
        
        cell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
        temp = sectionsCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        cell.lblSubCategoryName.textColor = Constant.tableSubTextColorCode
        cell.lblSubCategoryName.font = tableHeaderFont
        cell.lblAmount.font = tableHeaderFont
        
        cell.lblSubCategoryName.text = temp["subCatName"] as? String
        cell.lblDescription.textColor = Constant.tableSubTextColorCode
        cell.lblDescription.text = temp["note"] as? String
        cell.lblDescription.font = tableSubFont
        cell.img_subCategory.image = UIImage(named: temp["subCatImage"] as! String)
        cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(temp["amount"] as! String)"
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        cell.lblAmount.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        cell.lblAmount.method = .easeOut
        
        cell.lblAmount.count(from: 0, to: CGFloat(Double(temp["amount"] as! String)!), withDuration: 0.0)
        
        if(temp["setType"] as! String == "0")
        {
            cell.lblAmount.textColor = Constant.expenseColorCode
        }
        else
        {
            cell.lblAmount.textColor = Constant.incomeColorCode
        }
        
        //        cell.lblAmount.textColor = Constant.tableTextColorCode
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "AddTransactionVC") as! AddTransactionVC
        var temp = NSMutableDictionary()
        temp = sectionsCat[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        detailVC.transactionDict = temp
        detailVC.passingType = "edit"
        present(detailVC, animated: true, completion: nil)
        */
    }



}
