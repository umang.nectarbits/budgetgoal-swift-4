//
//  ContactUsVC.swift
//  GoogleUtilities
//
//  Created by harshesh on 23/05/19.
//

import UIKit
import UserNotifications
import MessageUI

class ContactUsVC: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var HeaderView: UIView!
    @IBOutlet var HeaderTitle: UILabel!
    @IBOutlet var btnBack: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        HeaderTitle.font = InnerHeadersFont
        HeaderTitle.textColor = HeaderTitleColor
        
        let Image2 = UIImage(named: "ic_NewBack")
        btnBack.setImage(Image2, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    //MFMailComposeViewController Delegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    //MARK: - Action Method
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContactUsAction(_ sender: UIButton){
        
        if !MFMailComposeViewController.canSendMail()
        {
            print("Mail services are not available")
            return
        }
        else
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["info@nectarbits.com"])
            composeVC.setSubject("Feedback")
            composeVC.setMessageBody("Feature request or bug report?", isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }

}
