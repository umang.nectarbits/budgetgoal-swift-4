//
//  OverviewViewController.swift
//  Finance
//
//  Created by Cools on 6/27/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds

class OverviewViewController: UIViewController,ChartViewDelegate,SambagDatePickerViewControllerDelegate,GADBannerViewDelegate {

    //MARK: - Define IBoutlet
//    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet var GradientView : UIView!
    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var btnFilter : UIButton!
    @IBOutlet var btnBarChart : UIButton!
    
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblTimeline : UILabel!
    @IBOutlet var lblOverview : UILabel!
    @IBOutlet var lblBudget : UILabel!
    @IBOutlet var lblSetting : UILabel!
    @IBOutlet var incomePieChart : PieChartView!
    @IBOutlet var scrollChart : UIScrollView!
    @IBOutlet var expencePieChart : PieChartView!
    @IBOutlet var lblheader : UILabel!
    @IBOutlet var headerView : UIView!
    @IBOutlet var tabBarView : UIView!
    @IBOutlet weak var imgTimeline: UIImageView!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var imgOverview: UIImageView!
    @IBOutlet weak var lblIncomeTotal: UICountingLabel!
    @IBOutlet weak var lblExpenseTotal: UICountingLabel!
    @IBOutlet weak var AddHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var incomeArray = NSMutableArray()
    var transactionArray = NSMutableArray()
    var expenseArray = NSMutableArray()
    var uniqueDateArray : NSMutableArray = []
    var sectionsCatIncome = [SectionCategory]()
    var sectionsCatExpense = [SectionCategory]()
    var selectDateMonth = Date()
    var dateArray : [NSDate] = []
    var percentageCountArray = NSMutableArray()
    
    //MARK: - View Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        headerView.backgroundColor = Constant.headerColorCode
//        tabBarView.backgroundColor = Constant.tabBarColorCode
//        lblheader.font = headerFont
//        lblheader.textColor = Constant.headerFontColor
        
        lblheader.text = "Overview (\(Constant.getMonthYear(Date())))"
        
//        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerLandscape)
//        bannerView.adUnitID = "ca-app-pub-1183954182004239/8988470569"//Constant.banner_admob_id
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
//        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        scrollChart.contentSize = CGSize(width: self.view.frame.size.width, height: expencePieChart.frame.origin.y + expencePieChart.frame.size.height)
        
        GradientView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        let Image = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        btnMenu.setImage(Image, for: .normal)
        btnMenu.tintColor = UIColor.white
        
        let Image1 = UIImage(named: "ic_filter_overview")?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(Image1, for: .normal)
        btnFilter.tintColor = UIColor.white
        
        let Image2 = UIImage(named: "ic_barchart")?.withRenderingMode(.alwaysTemplate)
        btnBarChart.setImage(Image2, for: .normal)
        btnBarChart.tintColor = UIColor.white
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
//       topHeightConstraint.constant = 0
       
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.topHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.topHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.topHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
        
        let puppies = uiRealm.objects(Transaction.self).filter("accountId = %@",UserDefaults.standard.integer(forKey: "AccountID"))
        
        transactionArray = []
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            transactionArray.add(dict1)
        }
        
//        lblTimeline.textColor = Constant.normalColorCode
//        lblOverview.textColor = Constant.highlightedColorCode
//        lblSetting.textColor = Constant.normalColorCode
//        lblTimeline.font = tabBarFont
//        lblOverview.font = tabBarFont
//        lblSetting.font = tabBarFont
         
//        imgTimeline.image = timelineImage
//        imgOverview.image = overviewHightlitedImage
//        imgSettings.image = settingImage
        
//        imgOverview.isHighlighted = true
        
        incomeChart(selectMonth: selectDateMonth)
        expenseChart(selectMonth: selectDateMonth)
        
        let gestureIncome = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        self.incomePieChart.addGestureRecognizer(gestureIncome)
        
        let gestureExpense = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        self.expencePieChart.addGestureRecognizer(gestureExpense)
        
    }
    
    /*
    func AddIncome()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddTransactionVC") as! AddTransactionVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func budgetAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetViewController") as! BudgetViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    */
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        
        if sender.view == incomePieChart {
            
            if sectionsCatIncome.count > 0
            {
                
                let detailVC = storyboard?.instantiateViewController(withIdentifier: "DetailOverviewViewController") as! DetailOverviewViewController
                detailVC.steCategoryType = "1"
                detailVC.selectMonth = selectDateMonth
                self.navigationController?.pushViewController(detailVC, animated: true)
 
            }
        }
        if sender.view == expencePieChart {
            
            if sectionsCatExpense.count > 0
            {
                
                let detailVC = storyboard?.instantiateViewController(withIdentifier: "DetailOverviewViewController") as! DetailOverviewViewController
                detailVC.steCategoryType = "0"
                detailVC.selectMonth = selectDateMonth
                self.navigationController?.pushViewController(detailVC, animated: true)
                
            }
        }
    }
    
    
    func incomeChart(selectMonth : Date)
    {
        filterByIncome(selectMonthDate : selectMonth)
        
        setupPieChartView(incomePieChart)
        incomePieChart.delegate = self
        
        incomePieChart.entryLabelColor = UIColor.white
        incomePieChart.entryLabelFont = tableSubFont
        
        //        incomePieChart.animate(withXAxisDuration: 1.4, easingOption: .easeOutBack)
        incomePieChart.animate(xAxisDuration: 1.4)
        
        setDataCount(sectionsCatIncome.count, range: 100, incomePieChart)
    }
    
    func expenseChart(selectMonth : Date)
    {
        filterByExpense(selectMonthDate : selectMonth)
        setupPieChartView(expencePieChart)
        expencePieChart.delegate = self
        
        expencePieChart.entryLabelColor = UIColor.white
        expencePieChart.entryLabelFont = tableSubFont
        
        //        incomePieChart.animate(withXAxisDuration: 1.4, easingOption: .easeOutBack)
        expencePieChart.animate(xAxisDuration: 1.4)
        
        setDataCount(sectionsCatExpense.count, range: 100, expencePieChart)
    }
    
    func filterByIncome(selectMonthDate : Date)
    {
        uniqueDateArray = []
        sectionsCatIncome = []
        
        dateArray = []
        
        let dateList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@", selectMonthDate.startOfMonth(), selectMonthDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        let categoryIDList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@", selectMonthDate.startOfMonth(), selectMonthDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"1").value(forKey: "categoryID") as! NSArray
        
        for i in 0..<categoryIDList.count
        {
            if(!uniqueDateArray.contains(categoryIDList[i]))
            {
                uniqueDateArray.add(categoryIDList[i])
            }
        }
        
        let uniqueArray : NSMutableArray = []
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueArray.contains(temp!))
            {
                uniqueArray.add(temp!)
            }
        }

        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0
            let temp1 : NSMutableArray = []
            var category_name : String = ""
            var category_image : String = ""
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String )
                {
                    for k in 0..<uniqueArray.count
                    {
                        if(uniqueArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
                        {
                            totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            print(dict)
                            category_name = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String
                            category_image = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryImage") as! String
                            temp1.add(dict)
                        }
                    }
                    
                    
                }
            }
            let temp = SectionCategory(amount: String(totalAmount) , categoryID: uniqueDateArray[j] as! String, categoryName: category_name, categoryImage: category_image, items: temp1)
            print(temp)
            sectionsCatIncome.append(temp)
        }
    }
    
    func filterByExpense(selectMonthDate : Date)
    {
        uniqueDateArray = []
        sectionsCatExpense = []
        dateArray = []
        
        let dateList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@", selectMonthDate.startOfMonth(), selectMonthDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        let categoryIDList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@", selectMonthDate.startOfMonth(), selectMonthDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0").value(forKey: "categoryID") as! NSArray
        
        for i in 0..<categoryIDList.count
        {
            if(!uniqueDateArray.contains(categoryIDList[i]))
            {
                uniqueDateArray.add(categoryIDList[i])
            }
        }
        
        let uniqueArray : NSMutableArray = []
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueArray.contains(temp!))
            {
                uniqueArray.add(temp!)
            }
        }
        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0
            let temp1 : NSMutableArray = []
            var category_name : String = ""
            var category_image : String = ""
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String )
                {
                    for k in 0..<uniqueArray.count
                    {
                        if(uniqueArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
                        {
                            totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            print(dict)
                            category_name = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String
                            category_image = (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryImage") as! String
                            temp1.add(dict)
                        }
                    }
                    
                    
                }
            }
            let temp = SectionCategory(amount: String(totalAmount) , categoryID: uniqueDateArray[j] as! String, categoryName: category_name, categoryImage: category_image, items: temp1)
            print(temp)
            sectionsCatExpense.append(temp)
        }
    }
    
    
    func setDataCount(_ count: Int, range: Double, _ chartView: PieChartView) {
        var totalAmont : Double = 0.0
        percentageCountArray = []
        var values : NSMutableArray = NSMutableArray()
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        
        if chartView == incomePieChart
        {
            for i in 0..<sectionsCatIncome.count
            {
                totalAmont = totalAmont + Double(sectionsCatIncome[i].amount)!
            }
            
//            lblIncomeTotal.text = "Income : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(totalAmont)"
            
            lblIncomeTotal.formatBlock = {(_ value: CGFloat) -> String in
                let formatted: String? = formatter.string(for: Double(value))
                return "Income : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
            }
            lblIncomeTotal.count(from: 0, to: CGFloat(totalAmont), withDuration: 0.0)
            
            lblIncomeTotal.font = tableMainFont
            lblIncomeTotal.textColor = Constant.incomeColorCode
            
            for i in 0..<sectionsCatIncome.count
            {
                let percentage : Double = (Double(sectionsCatIncome[i].amount)! * 100)/totalAmont
                percentageCountArray.add(String(percentage))
            }
            
            values = NSMutableArray()
            for i in 0..<count {
                values.add(PieChartDataEntry(value: Double(Float(percentageCountArray[i] as! String)!), label: "\(sectionsCatIncome[i].categoryName!) (\(String(format:"%.1f", Float(percentageCountArray[i] as! String)!)))%", icon: UIImage(named: "icon")))
            }
        }
        else
        {
            for i in 0..<sectionsCatExpense.count
            {
                totalAmont = totalAmont + Double(sectionsCatExpense[i].amount)!
            }
            
            lblExpenseTotal.formatBlock = {(_ value: CGFloat) -> String in
                let formatted: String? = formatter.string(for: Double(value))
                return "Expense : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
            }
            lblExpenseTotal.count(from: 0, to: CGFloat(totalAmont), withDuration: 0.0)
            
//            lblExpenseTotal.text = "Expense : \(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(totalAmont)"
            lblExpenseTotal.font = tableMainFont
            lblExpenseTotal.textColor = Constant.expenseColorCode
            
            for i in 0..<sectionsCatExpense.count
            {
                let percentage : Double = (Double(sectionsCatExpense[i].amount)! * 100)/totalAmont
                percentageCountArray.add(String(percentage))
            }
            
            values = NSMutableArray()
            for i in 0..<count {
                values.add(PieChartDataEntry(value: Double(Float(percentageCountArray[i] as! String)!), label: "\(sectionsCatExpense[i].categoryName!) (\(String(format:"%.1f", Float(percentageCountArray[i] as! String)!)))%", icon: UIImage(named: "icon")))
            }
        }
        
        
        
        let dataSet = PieChartDataSet(values: values as? [ChartDataEntry], label: "")
        dataSet.drawIconsEnabled = false
        dataSet.sliceSpace = 0.0
        
        // add a lot of colors
        let colors : NSMutableArray = NSMutableArray()
        colors.addObjects(from: ChartColorTemplates.colorful())
        colors.addObjects(from: ChartColorTemplates.joyful())
        colors.addObjects(from: ChartColorTemplates.vordiplom())
        colors.addObjects(from: ChartColorTemplates.liberty())
        colors.addObjects(from: ChartColorTemplates.pastel())
        colors.addObjects(from: ChartColorTemplates.material())
        
        dataSet.colors = colors as! [NSUIColor]
        let data = PieChartData(dataSet: dataSet)
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 2
        pFormatter.multiplier = 1.0
        pFormatter.percentSymbol = "%"
//        data.setValueFormatter(pFormatter as? IValueFormatter)
        
        data.setValueFont(UIFont(name: ".SFUIDisplay", size: 8.0))
        
        data.setValueTextColor(UIColor.white)
        
        chartView.data = data
        
        chartView.highlightValues(nil)
    }
    
    func setupPieChartView(_ chartView: PieChartView) {
        chartView.backgroundColor = UIColor.white
        chartView.isUserInteractionEnabled = true
        chartView.usePercentValuesEnabled = false
        chartView.drawEntryLabelsEnabled = false
        chartView.drawSlicesUnderHoleEnabled = false
        
        chartView.holeRadiusPercent = 0.0
        chartView.transparentCircleRadiusPercent = 0.0
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 5.0, top: 10.0, right: 5.0, bottom: 5.0)
        chartView.drawCenterTextEnabled = false
        
        let paragraphStyle: NSMutableParagraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "Income")
        centerText.setAttributes([NSAttributedStringKey.font: tableSubFont as Any, NSAttributedStringKey.paragraphStyle: paragraphStyle ], range: NSRange(location: 0, length: centerText.length))
        
//        chartView.centerAttributedText = centerText
        chartView.drawHoleEnabled = false
        chartView.rotationAngle = 0.0
        chartView.rotationEnabled = false
        chartView.highlightPerTapEnabled = false
        
//        let l: Legend? = chartView.legend
//        l?.horizontalAlignment = .right
//        l?.verticalAlignment = .top
//        l?.orientation = .vertical
//        l?.drawInside = false
//        l?.xEntrySpace = 7.0
//        l?.yEntrySpace = 0.0
//        l?.yOffset = 0.0
    }
   
   
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        bannerView.isHidden = false
//        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
//            self.AddHeightConstraint.constant = 50
            bannerView.transform = CGAffineTransform.identity
//            self.topHeightConstraint.constant = 50
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.topHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.topHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.topHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
//        bannerView.isHidden = true
//        AddHeightConstraint.constant = 50
        topHeightConstraint.constant = 0
//        AddHeightConstraint.constant = 0
    }
  
    
    //MARK: - Action Method
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func showSambagDatePickerViewController(_ sender: UIButton) {
        let vc = SambagDatePickerViewController()
        vc.delegate = self
        vc.theme = .light
        present(vc, animated: true, completion: nil)

    }
    
    
    @IBAction func btnBarChartAction(_ sender: UIButton) {
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "BarChartOverviewViewController") as! BarChartOverviewViewController
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
 
    
    //MARK: - SambagDate Picker Method
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
//        resultLabel.text = "\(result)"
        
        
        let resultDate = Constant.convertDate(String(describing: result))
        lblheader.text = "Overview (\(Constant.getMonthYear(resultDate)))"
        print(resultDate)
        selectDateMonth = resultDate
        incomeChart(selectMonth: selectDateMonth)
        expenseChart(selectMonth: selectDateMonth)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }

}
