//
//  DashboardViewController.swift
//  Created by Nectarbits on 21/09/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import GoogleMobileAds

class countryCell: UITableViewCell {
    @IBOutlet var lblCountry : UILabel!
    @IBOutlet var img_country : UIImageView!
}

class dashboardCategoryCell: UITableViewCell {
    @IBOutlet var lblCategoryTitle : UILabel!
    @IBOutlet var btnCheck : UIButton!
}

class IncomeExpenseCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblAmount : UILabel!
    @IBOutlet var viewBG : UIView!
    @IBOutlet var viewGradient : UIView!
    @IBOutlet var btnAddTransaction : UIButton!
    @IBOutlet var bgimage: UIImageView!
    @IBOutlet var Cellimage: UIImageView!
    @IBOutlet var CellimageExpense: UIImageView!
//    override func awakeFromNib() {
//        viewBG.dropShadow()
//    }
}

class GoalsCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var lblGoalCountTitle : UILabel!
    @IBOutlet var lblGoalCount : UILabel!
    @IBOutlet var lblGoalTotlaTitle : UILabel!
    @IBOutlet var lblGoalTotla : UILabel!
    @IBOutlet var lblGoalSavingTitle : UILabel!
    @IBOutlet var lblGoalSaving : UILabel!
    @IBOutlet var lblGoalTitle : UILabel!
    @IBOutlet var viewBG : UIView!
    @IBOutlet var btnAddTransaction : UIButton!
    @IBOutlet var bgimage: UIImageView!
}

class AILIstCell: UITableViewCell {
    @IBOutlet var lblCategoryTitle : UILabel!
    @IBOutlet var lblPriviousMonthAmount : UILabel!
    @IBOutlet var lblCurrentMonthAmount : UILabel!
}

class BudgetCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var viewBGBudget : UIView!
    @IBOutlet var lblAmount : UILabel!
    @IBOutlet var lblDisplayBudget : UILabel!
    @IBOutlet var lblSave : UILabel!
    @IBOutlet var lblTotalBudget : UILabel!
    @IBOutlet var lblUsedBudget : UILabel!
    @IBOutlet var lblRemainBudget : UILabel!
    @IBOutlet var btnAddBudget : UIButton!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var bgimageBudget: UIImageView!
    @IBOutlet var CellimageBudget: UIImageView!
    @IBOutlet var viewGradient : UIView!
    @IBOutlet var budgetProgress : GradientProgressBar!
}
class BillsCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblTotalAmountTitle : UILabel!
    @IBOutlet var lblPaidBillsTitle : UILabel!
    @IBOutlet var lblRemainingBillsTitle : UILabel!
    @IBOutlet var lblTotalAmount : UILabel!
    @IBOutlet var lblPaidBills : UILabel!
    @IBOutlet var lblRemainingBills : UILabel!
    @IBOutlet var bgimageBills: UIImageView!
    @IBOutlet var viewBG : UIView!
    
}
//,GADBannerViewDelegate
class DashboardViewController: UIViewController,UITableViewDataSource,AppiraterDelegate,GADBannerViewDelegate {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var tblList : UITableView!
    @IBOutlet var headerHeightConstraint : NSLayoutConstraint!
    @IBOutlet var bgView : UIView!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet var currencyView: UIView!
    @IBOutlet weak var tblcurrency: UITableView!
    @IBOutlet weak var currencySearchBar: UISearchBar!
    @IBOutlet var lblCurrentBalance: UILabel!
    @IBOutlet var addCategoryView: UIView!
    @IBOutlet var titleView: UIView!
    @IBOutlet var tblCategoryList: UITableView!
    @IBOutlet var tblAIList: UITableView!
    @IBOutlet var btnAddCategory: UIButton!
    @IBOutlet var viewButtonBackground: UIView!
    @IBOutlet var aiView: UIView!
    @IBOutlet var lblAIViewTitle: UILabel!
    @IBOutlet var aiImageView: UIView!
    @IBOutlet var aiContainerView: UIView!
    @IBOutlet var tblAIListHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet weak var lblOutlays: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var Header: UIView!
    @IBOutlet weak var lblCurrentBalanceTitle: UILabel!
    @IBOutlet weak var lblCurrentMonthTitle: UILabel!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var TotalAmountView: UIView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    //For Monthly Saving card
    @IBOutlet var savingView: UIView!
    @IBOutlet var lblCurrencySymbol: UILabel!
    @IBOutlet var savingCardView: UIView!
    @IBOutlet var txtMonthlySaving: UITextField!
    @IBOutlet var btnAddMonthlySaving: UIButton!
    ///////////////////////////////////////
    
    let maxHeaderHeight: CGFloat = 160;
    let minHeaderHeight: CGFloat = 50;
    
    var previousScrollOffset: CGFloat = 0;
    
    var searchActive: Bool = false
    var countryArray = NSMutableArray()
    var countryNewArray : [String]! = []
    var strCurrency : String = ""
    var filtered: [String] = []
    
    let array = ["Expense","Income","Budget","Accounts","Goals"]
    let image = ["Cell1_Bg","Cell2_Bg","Cell3_Bg","Cell_Bg 5","Cell_Bg 6","Cell3_Bg"]
    let CategoryImage = ["Cell_Bg 4","Cell_Bg 7"]
    let img_Add = ["ic_addDashboard","ic_addDashboard","ic_addDashboard","ic_addDashboard","ic_addDashboard","ic_addDashboard"]
    var categoryArray = NSArray()
    var dashboardArray = [[String:AnyObject]]()
    var sumOfIncome = NSArray()
    var sumOfBills = NSArray()
    var sumOfExpense = NSArray()
    var totalBudget = NSArray()
    var totalIncome = ""
    var totalExpense = ""
    var totalBillsAmount = ""
    var selectCategoryArray = NSMutableArray()
    var finalAIArray = [[String:AnyObject]]()
    var accountCount : Int = 0
    var creditAmount : Double = 0.0
    var debitAmount : Double = 0.0
    var goalsCount : Int = 0
    var goalstotal : Double = 0.0
    var goalsSaving : Double = 0.0
    var selectedMonthDate : Date!
    var strCurrentMonth : String = ""
    var passingDate : Date!
    var TodayDate : Date!
    
    //MARK:- View Method
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !Constant.isKeyPresentInUserDefaults(key: "currencySymbol") {
        }
        else
        {
            if UserDefaults.standard.value(forKey: "currencySymbol") as! String == ""
            {
            }
            else
            {
                if !Constant.isKeyPresentInUserDefaults(key: "AccountID") {
                }
                else
                {
                    self.headerHeightConstraint.constant = self.maxHeaderHeight
                    updateHeader()
                    NotificationCenter.default.addObserver(self, selector: #selector(self.forgroundAction(notification:)), name: NSNotification.Name(rawValue: "comeBackForground"), object: nil)
                    
                    strCurrentMonth = Constant.getMonthYear(selectedMonthDate)
                    lblCurrentMonthTitle.text = strCurrentMonth
                    lblOutlays.text = (UserDefaults.standard.value(forKey: "name") as! String)
                    totalAllAmounts(date: selectedMonthDate)
                    
                    print("Selected Account ID:=> \(UserDefaults.standard.integer(forKey: "AccountID"))")
                    
                    let currentAmount1 : NSArray = uiRealm.objects(Account.self).filter("id = %@",UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "remainigAmount") as! NSArray
                    print("Current Selected Account:=> \((currentAmount1)[0])")
                    lblCurrentBalance.text = String(describing: "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \((currentAmount1)[0])")
                    if AiReport == true{
                        
                        AiReport = false
                        getAIReport()
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

//        if(UserDefaults.standard.value(forKey: "Purchase") != nil){
//            self.BottomHeightConstraint.constant = 0
//            self.BannerHeightConstraint.constant = 0
//        }else{
//            self.BottomHeightConstraint.constant = 50
//            self.BannerHeightConstraint.constant = 50
//        }
//
        lblOutlays.font = headerFont
        lblCurrentBalance.font = UIFont(name: ".SFUIDisplay-Bold", size: 30)
        lblCurrentMonthTitle.font = UIFont(name: ".SFUIDisplay-Medium", size: 18) //Current Balance
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        
        currencyView.isHidden = true
        aiView.isHidden = true
        bgView.isHidden = true
        addCategoryView.isHidden = true
        addCategoryView.layer.cornerRadius = 8
        addCategoryView.layer.masksToBounds = true
        currencyView.layer.cornerRadius = 8
        currencyView.layer.masksToBounds = true
        
        //for Monthly Saving PopUp Frame & load
        savingView.frame = self.view.bounds
        self.view.addSubview(savingView)
        savingView.isHidden = true
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        
        if !Constant.isKeyPresentInUserDefaults(key: "firstTimeEnter") {
            UserDefaults.standard.set("", forKey: "firstTimeEnter")
            UserDefaults.standard.set(TodayDate, forKey: "FirstOpenDate")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainAccountListVC")
            vc?.modalTransitionStyle = .crossDissolve
            self.present(vc!, animated: false, completion: nil)
        }else if (UserDefaults.standard.object(forKey: "AccountID") == nil){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainAccountListVC")
            vc?.modalTransitionStyle = .crossDissolve
            self.present(vc!, animated: false, completion: nil)
        }else{
            
            //for manage Daily saving PopUp
            if(UserDefaults.standard.value(forKey: "FirstOpenDate") != nil){
                let firstOpendate = UserDefaults.standard.value(forKey: "FirstOpenDate") as! Date
                let monthdiff = Calendar.current.dateComponents([.day], from: firstOpendate, to: TodayDate!).day
                print("diff:=  \(monthdiff!)")
                if(monthdiff! >= 15){
                    
                    UserDefaults.standard.removeObject(forKey: "FirstOpenDate")
                    lblCurrencySymbol.text = (UserDefaults.standard.value(forKey: "currencySymbol")! as! String)
                    savingView.isHidden = false
                }else{
                    print("days Different not 15")
                }
            }else{
                print(" First Launch Date not Found")
            }
            
        }
        
//        countryArray = (IsoCountries.allCountries as NSArray).mutableCopy() as! NSMutableArray
//        for i in 0..<countryArray.count {
//            let temp = countryArray[i] as! IsoCountryInfo
//            countryNewArray.append(temp.name)
//        }
//        if !Constant.isKeyPresentInUserDefaults(key: "currencySymbol") {
//                UserDefaults.standard.set("", forKey: "currencySymbol")
//                showAnimate1(selectOption: "Currency")
//        }
//        else
//        {
//            if UserDefaults.standard.value(forKey: "currencySymbol") as! String == ""
//            {
//                UserDefaults.standard.set("", forKey: "currencySymbol")
//                showAnimate1(selectOption: "Currency")
//            }
//            else
//            {
//                getAIReport()
//            }
//        }
 
        NotificationCenter.default.addObserver(self, selector: #selector(DashboardViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DashboardViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        tblcurrency.tableFooterView = UIView.init(frame: CGRect.zero)
        let path =  Realm.Configuration.defaultConfiguration.fileURL
        print("Path : \((path?.path)!)")
        selectedMonthDate = Date()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CheckPurchase), name: NSNotification.Name(rawValue: "CheckPurchase"), object: nil)
        
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    @objc func forgroundAction(notification: NSNotification) {
        selectedMonthDate = Date()
        strCurrentMonth = Constant.getMonthYear(selectedMonthDate)
        lblCurrentMonthTitle.text = strCurrentMonth
        totalAllAmounts(date: selectedMonthDate)
    }
    
    func getAccountData()
    {
        var sumOfExpenseAccount = NSArray()
        var totalExpenseAccount = ""
        sumOfExpenseAccount = uiRealm.objects(Transaction.self).filter("setType = %@ ", "0").value(forKey: "amount") as! NSArray
        
        if sumOfExpenseAccount.count == 0
        {
            totalExpenseAccount = "0.0"
        }
        else
        {
            totalExpenseAccount = String.sum(array: sumOfExpenseAccount)
        }
        
        accountCount = uiRealm.objects(Account.self).filter("isIncludeOnDashboard = %@",true).count
        creditAmount = uiRealm.objects(Account.self).filter("isIncludeOnDashboard = %@",true).sum(ofProperty: "amount")
        debitAmount = Double(totalExpenseAccount)!//creditAmount - uiRealm.objects(Account.self).filter("isIncludeOnDashboard = %@",true).sum(ofProperty: "remainigAmount")
        print(accountCount)
        print(creditAmount)
        print(debitAmount)
    }
    
    func getGoalsData()
    {
        goalsCount = uiRealm.objects(Goals.self).count
        goalstotal = uiRealm.objects(Goals.self).sum(ofProperty: "amount")
        goalsSaving = uiRealm.objects(Contribution.self).sum(ofProperty: "amount")
        print(goalsCount)
        print(goalstotal)
        print(goalsSaving)
    }
    
    func getDisplayCategoryList(date: Date)
    {
        categoryArray = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@","1","0").toArray! as NSArray
        
//        let temp = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND displayDashboard = %d","1","0",1).toArray! as NSArray
        
        var temp = NSMutableArray()
        
        do
        {
            let objs = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND displayDashboard = %d","1","0",1).toArray
            if objs != nil {
                temp = (objs! as NSArray).mutableCopy() as! NSMutableArray
            }
        }
        
        for i in 0..<temp.count
        {
            selectCategoryArray.add(temp[i] as! Category)
        }
        
        print(selectCategoryArray)
        
        for i in 0..<temp.count
        {
            var dict = [String:AnyObject]()
            dict["name"] = (temp[i] as! Category).name as AnyObject
            
            let totalAmount = uiRealm.objects(Transaction.self).filter("categoryID = %@ AND date BETWEEN {%@,%@} AND accountId = %@",(temp[i] as! Category).categoryID, date.startOfMonth(), date.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "amount") as! NSArray
            
            dict["totalAmount"] = String.sum(array: totalAmount)! as AnyObject
            dict["categoryId"] = ((temp[i] as! Category).categoryID) as AnyObject
            
            let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d AND date BETWEEN {%@,%@}",Int((temp[i] as! Category).categoryID)!, date.startOfMonth(), date.endOfMonth())
            
            if let budgetWorkout = budgetUpdate.last
            {
                dict["budgetAmount"] = budgetWorkout.budgetAmount as AnyObject
                dict["budgetCurrentAmount"] = budgetWorkout.currentAmount as AnyObject
                dict["isBudget"] = true as AnyObject
            }
            else
            {
                dict["isBudget"] = false as AnyObject
                dict["budgetAmount"] = 0.0 as AnyObject
                dict["budgetCurrentAmount"] = 0.0 as AnyObject
            }
            
            dict["backgroundImage"] = image[Int(arc4random_uniform(UInt32(image.count)))] as AnyObject
            dict["btnbackgroundImage"] = "ic_addDashboard" as AnyObject
            
            dashboardArray.append(dict)
        }
        print(dashboardArray)
        tblList.reloadData()
    }
    
    func totalAllAmounts(date: Date)
    {
        sumOfIncome = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", date.startOfMonth(), date.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"1").value(forKey: "amount") as! NSArray
        
        if sumOfIncome.count == 0
        {
            totalIncome = "0.0"
        }
        else
        {
            totalIncome = String.sum(array: sumOfIncome)
        }
        
        sumOfExpense = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", date.startOfMonth(), date.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0").value(forKey: "amount") as! NSArray
        
        if sumOfExpense.count == 0
        {
            totalExpense = "0.0"
        }
        else
        {
            totalExpense = String.sum(array: sumOfExpense)
        }
        
        sumOfBills = uiRealm.objects(Bills.self).filter("dueDate BETWEEN {%@,%@}", date.startOfMonth(), date.endOfMonth()).value(forKey: "amount") as! NSArray
        
        if sumOfBills.count == 0
        {
            totalBillsAmount = "0.0"
        }
        else
        {
            totalBillsAmount = String.sum(array: sumOfBills)
        }
        
//        totalBudget = uiRealm.objects(Budget.self).filter("date BETWEEN {%@,%@}",
//                                                          date.startOfMonth(), date.endOfMonth()).toArray! as NSArray
        do
        {
            let objs = uiRealm.objects(Budget.self).filter("date BETWEEN {%@,%@}",
                                                           date.startOfMonth(), date.endOfMonth()).toArray
            if objs != nil {
                totalBudget = (objs! as NSArray).mutableCopy() as! NSMutableArray
            }else{
                totalBudget = []
            }
        }
        
        dashboardArray = []
        getAccountData()
        getGoalsData()
        
//        lblCurrentBalance.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(Double(Double(totalIncome)! - Double(totalExpense)!)))"
        
        
        
        for i in 0..<array.count
        {
            var dict = [String:AnyObject]()
            dict["name"] = array[i] as AnyObject
            if i == 0
            {
                dict["totalAmount"] = totalExpense as AnyObject
                dict["budgetAmount"] = "0.0" as AnyObject
                dict["budgetCurrentAmount"] = "0.0" as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 1
            {
                dict["totalAmount"] = totalIncome as AnyObject
                dict["budgetAmount"] = "0.0" as AnyObject
                dict["budgetCurrentAmount"] = "0.0" as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 4
            {
                dict["totalAmount"] = totalBillsAmount as AnyObject
                dict["budgetAmount"] = "0.0" as AnyObject
                dict["budgetCurrentAmount"] = "0.0" as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 3
            {
                dict["totalAmount"] = String(accountCount) as AnyObject
                dict["budgetAmount"] = String(creditAmount) as AnyObject
                dict["budgetCurrentAmount"] = String(debitAmount) as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 2
            {
                let budgetAmountArray = NSMutableArray()
                for i in 0..<totalBudget.count
                {
                    budgetAmountArray.add(String((totalBudget[i] as! Budget).budgetAmount))
                }
                let usedBudgetAmountArray = NSMutableArray()
                for i in 0..<totalBudget.count
                {
                    usedBudgetAmountArray.add(String((totalBudget[i] as! Budget).currentAmount))
                }
                if budgetAmountArray.count == 0
                {
                    dict["budgetAmount"] = "0.0" as AnyObject
                }
                else
                {
                    dict["budgetAmount"] = String.sum(array: budgetAmountArray)! as AnyObject
                }
                
                
                if usedBudgetAmountArray.count == 0
                {
                    dict["budgetCurrentAmount"] = "0.0" as AnyObject
                }
                else
                {
                    dict["budgetCurrentAmount"] = String.sum(array: usedBudgetAmountArray)! as AnyObject
                }
                dict["totalAmount"] = String(Double(Double(String.sum(array: budgetAmountArray)!)!-Double(String.sum(array: usedBudgetAmountArray)!)!)) as AnyObject
                
                
                dict["isBudget"] = true as AnyObject
            }
            
            dict["backgroundImage"] = image[i] as AnyObject
            dict["btnbackgroundImage"] = img_Add[i] as AnyObject
            dict["categoryId"] = "0" as AnyObject
            
            dashboardArray.append(dict)
        }
        
        getDisplayCategoryList(date: date)

    }
    //UITableview Animation
    func RightTOleft(viewAnimation: UITableView) {
        
//        var napkinBottomFrame: CGRect = viewAnimation.frame
//        napkinBottomFrame.origin.x = 0
//        UITableView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
//            viewAnimation.frame = napkinBottomFrame
//        }, completion: {(_ finished: Bool) -> Void in
//            /*done*/
//
//        })
        
        let frame = viewAnimation.frame
        
        viewAnimation.frame.origin.x = +viewAnimation.frame.width
        
        UITableView.animate(withDuration: 0.5, animations: {
            viewAnimation.frame.origin.x = 0.0//-viewAnimation.frame.width
        }) { (_) in
            viewAnimation.frame = frame
//            UITableView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn], animations: {
//                viewAnimation.frame.origin.x = -viewAnimation.frame.width
////                viewAnimation.frame.origin.x = 0.0
//            })

        }
    }
    
    func LeftToRight(viewAnimation: UITableView)
    {
        let frame = viewAnimation.frame
        
        viewAnimation.frame.origin.x = -viewAnimation.frame.width
        
        UITableView.animate(withDuration: 0.5, animations: {
            viewAnimation.frame.origin.x = 0.0//-viewAnimation.frame.width
        }) { (_) in
            viewAnimation.frame = frame
        }
    }
    
    //UIView Animation
    func RightTOleftView(viewAnimation: UIView) {

        let frame = viewAnimation.frame

        viewAnimation.frame.origin.x = +viewAnimation.frame.width

        UIView.animate(withDuration: 0.5, animations: {
            viewAnimation.frame.origin.x = 0.0
        }) { (_) in
            viewAnimation.frame = frame

        }
  
    }
    func LeftToRightView(viewAnimation: UIView)
    {
        let frame = viewAnimation.frame

        viewAnimation.frame.origin.x = -viewAnimation.frame.width

        UIView.animate(withDuration: 0.5, animations: {
            viewAnimation.frame.origin.x = 0.0
            
        }) { (_) in
            viewAnimation.frame = frame
        }
    }
    
    //MARK:- Keyboard Method
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            
            let _ = self.currencyView.window?.frame {
            let viewHeight = self.currencyView.frame.height
            self.currencyView.frame = CGRect(x: self.currencyView.frame.origin.x,
                                             y: self.currencyView.frame.origin.y,
                                             width: self.currencyView.frame.width,
                                             height: viewHeight - keyboardSize.height)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.currencyView.frame.height
            self.currencyView.frame = CGRect(x: self.currencyView.frame.origin.x,
                                             y: self.currencyView.frame.origin.y,
                                             width: self.currencyView.frame.width,
                                             height: viewHeight + keyboardSize.height)
        }
        
    }

    
    //MARK:- Helper Method
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first!
        if(touch.view == savingView)
        {
            savingView.isHidden = true
        }
        
    }
    
    func getAIReport()
    {
        tblAIList.estimatedRowHeight = 44.0
        tblAIList.rowHeight = UITableViewAutomaticDimension
        tblAIList.tableFooterView = UIView.init(frame: CGRect.zero)
        
        lblAIViewTitle.text = "Hey below are the top expenses of your previous Months. \nSet budget for it."
        
        aiImageView.layer.cornerRadius = aiImageView.frame.size.width/2
        aiImageView.layer.masksToBounds = true
        aiContainerView.viewShadow()
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: Date())
        var priviousMonthArray = [[String:AnyObject]]()
        var currentMonthArray = [[String:AnyObject]]()
        print(previousMonth!)
        
        let previousMonthList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@ ", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0")
        
        let datePreviousList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0").value(forKey: "date") as! NSArray
        
        
        let uniquePreviousDateArray = NSMutableArray()
        var datePreviousArray : [NSDate] = []
        datePreviousArray = datePreviousList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        let categoryPreviousIDList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0").value(forKey: "categoryID") as! NSArray
        for i in 0..<categoryPreviousIDList.count
        {
            if(!uniquePreviousDateArray.contains(categoryPreviousIDList[i]))
            {
                uniquePreviousDateArray.add(categoryPreviousIDList[i])
            }
        }
        let uniquePreviousArray : NSMutableArray = []
        for i in 0..<datePreviousArray.count
        {
            let temp = String.convertFormatOfDate(date: datePreviousArray[i] as Date)
            if(!uniquePreviousArray.contains(temp!))
            {
                uniquePreviousArray.add(temp!)
            }
        }
        
        for j in 0..<uniquePreviousDateArray.count//uniqueArray
        {
            var totalAmount : Double = 0
            var category_name : String = ""
            var category_ID : String = ""
            
            for i in 0..<previousMonthList.count
            {
                if(uniquePreviousDateArray[j] as! String == String(describing: previousMonthList[i]["categoryID"]!))
                {
                    for k in 0..<uniquePreviousArray.count
                    {
                        if(uniquePreviousArray[k] as! String == String.convertFormatOfDate(date: previousMonthList[i]["date"]! as! Date))
                        {
                            totalAmount = totalAmount + Double(String(describing:previousMonthList[i]["amount"]!))!
                            category_name = String(describing:previousMonthList[i]["categoryName"]!)
                            category_ID = String(describing:previousMonthList[i]["categoryID"]!)
                        }
                    }
                }
            }
            var dict = [String:AnyObject]()
            dict["amount"] = String(totalAmount) as AnyObject
            dict["categoryName"] = category_name as AnyObject
            dict["categoryID"] = uniquePreviousDateArray[j] as AnyObject
            priviousMonthArray.append(dict)
        }
        
        print(priviousMonthArray)
        
        let currentMonthList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ AND repeatType = %@", Date().startOfMonth(), Date().endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0")
        
        let datecurrentList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@", Date().startOfMonth(), Date().endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0").value(forKey: "date") as! NSArray
        
        
        let uniquecurrentDateArray = NSMutableArray()
        var datecurrentArray : [NSDate] = []
        datecurrentArray = datecurrentList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        let categorycurrentIDList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@", Date().startOfMonth(), Date().endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0").value(forKey: "categoryID") as! NSArray
        for i in 0..<categorycurrentIDList.count
        {
            if(!uniquecurrentDateArray.contains(categorycurrentIDList[i]))
            {
                uniquecurrentDateArray.add(categorycurrentIDList[i])
            }
        }
        let uniquecurrentArray : NSMutableArray = []
        for i in 0..<datecurrentArray.count
        {
            let temp = String.convertFormatOfDate(date: datecurrentArray[i] as Date)
            if(!uniquecurrentArray.contains(temp!))
            {
                uniquecurrentArray.add(temp!)
            }
        }
        
        for j in 0..<uniquecurrentDateArray.count//uniqueArray
        {
            var totalcurrentAmount : Double = 0
            var categorycurrent_name : String = ""
            var categorycurrent_ID : String = ""
            
            for i in 0..<currentMonthList.count
            {
                if(uniquecurrentDateArray[j] as! String == String(describing: currentMonthList[i]["categoryID"]!))
                {
                    for k in 0..<uniquecurrentArray.count
                    {
                        if(uniquecurrentArray[k] as! String == String.convertFormatOfDate(date: currentMonthList[i]["date"]! as! Date))
                        {
                            totalcurrentAmount = totalcurrentAmount + Double(String(describing:currentMonthList[i]["amount"]!))!
                            categorycurrent_name = String(describing:currentMonthList[i]["categoryName"]!)
                            categorycurrent_ID = String(describing:currentMonthList[i]["categoryID"]!)
                        }
                    }
                }
            }
            var dict = [String:AnyObject]()
            dict["currentAmount"] = String(totalcurrentAmount) as AnyObject
            dict["priviousAmount"] = "0" as AnyObject
            dict["categoryName"] = categorycurrent_name as AnyObject
            dict["categoryID"] = categorycurrent_ID as AnyObject
            currentMonthArray.append(dict)
        }
        print("before Compare:",currentMonthArray)
        
        for i in 0..<currentMonthArray.count
        {
            for j in 0..<priviousMonthArray.count
            {
                if currentMonthArray[i]["categoryID"] as! String == priviousMonthArray[j]["categoryID"] as! String
                {
                    currentMonthArray[i]["priviousAmount"] = priviousMonthArray[j]["amount"]
                }
            }
        }
        print("After Compare:",currentMonthArray)
        
        
        var dict = [String:AnyObject]()
        dict["currentAmount"] = String.convertFormatOfDateToStringSpecificFormat(date: Date(), format: "MMM,yyyy") as AnyObject
        dict["priviousAmount"] = String.convertFormatOfDateToStringSpecificFormat(date: Calendar.current.date(byAdding: .month, value: -1, to: Date())!, format: "MMM,yyyy") as AnyObject
        dict["categoryName"] = "Category" as AnyObject
        dict["categoryID"] = "" as AnyObject
        finalAIArray.append(dict)
        
        for i in 0..<currentMonthArray.count
        {
            let temp = (120 * Double(currentMonthArray[i]["priviousAmount"] as! String)!)/100
            if temp < Double(currentMonthArray[i]["currentAmount"] as! String)!
            {
                finalAIArray.append(currentMonthArray[i])
            }
        }
        
        if finalAIArray.count > 1
        {
            tblAIListHeightConstraint.constant = CGFloat(finalAIArray.count * 50)
            showAnimate1(selectOption: "AIReport")
            tblAIList.reloadData()
        }
        else
        {
            
        }
    }
    /*
    func getDataMonth() {
        
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: Date())
        var priviousMonthArray = [[String:AnyObject]]()
        var currentMonthArray = [[String:AnyObject]]()
        print(previousMonth!)
        
        let previousMonthList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@ ", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0")
        
        let datePreviousList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType= %@ AND repeatType = %@", previousMonth!.startOfMonth(), previousMonth!.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0","0").value(forKey: "date") as! NSArray
        
        let TotalAmountList =
        
    }
    */
    
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
        }
        
        return currencySymbol
    }
    
    func showAnimate1(selectOption : String)
    {
        if selectOption == "Category"
        {
            bgView.isHidden = false
            addCategoryView.isHidden = false
            bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            bgView.alpha = 0.0;
            addCategoryView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            addCategoryView.alpha = 0.0;
            tblCategoryList.dataSource = self
            tblCategoryList.reloadData()
            UIView.animate(withDuration: 0.25, animations: {
                self.bgView.alpha = 0.7
                self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.addCategoryView.alpha = 1.0
                self.addCategoryView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
            });
        }
        else if selectOption == "AIReport"
        {
            bgView.isHidden = false
            aiView.isHidden = false
            bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            bgView.alpha = 0.0;
            aiView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            aiView.alpha = 0.0;
            UIView.animate(withDuration: 0.25, animations: {
                self.bgView.alpha = 0.7
                self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.aiView.alpha = 1.0
                self.aiView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
            });
        }
        else if selectOption == "Currency"
        {
            bgView.isHidden = false
            currencyView.isHidden = false
            bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            bgView.alpha = 0.0;
            currencyView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            currencyView.alpha = 0.0;
            tblcurrency.dataSource = self
            tblcurrency.reloadData()
            UIView.animate(withDuration: 0.25, animations: {
                self.bgView.alpha = 0.7
                self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.currencyView.alpha = 1.0
                self.currencyView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
            });
        }
    }
    
    func removeAnimate1(selectOption : String)
    {
        if selectOption == "Category"
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.addCategoryView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.addCategoryView.alpha = 0.0;
                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.addCategoryView.isHidden = true
                    self.bgView.isHidden = true
                    //                self.tblList.reloadData()
                }
            });
        }
        else if selectOption == "AIReport"
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.aiView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.aiView.alpha = 0.0;
                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.aiView.isHidden = true
                    self.bgView.isHidden = true
                }
            });
            
        }
        else if selectOption == "Currency"
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.currencyView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.currencyView.alpha = 0.0;
                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.currencyView.isHidden = true
                    self.bgView.isHidden = true
                    //                self.tblList.reloadData()
                }
            });
        }
    }
    
    func AddDailyTransaction(Dailycategory: [Category]){
        
        //for Entry in Transaction Table
        var dailySaving = Transaction()
        var autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
        
        if(autoCatId == nil)
        {
            autoCatId = 1
        }
        else
        {
            autoCatId = autoCatId! + 1
        }
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        
        //Fetch Current Account
        let AccountId = uiRealm.objects(Account.self).filter("name=%@",lblOutlays.text!).first
        
        //Entry into transaction table
        dailySaving = Transaction(value: [autoCatId!,
                                       Dailycategory[0].name,
                                       Dailycategory[0].image,
                                       Dailycategory[0].categoryID,
                                       Constant.TO_STRING(Dailycategory[0].subCategoryID),
                                       Dailycategory[0].name,
                                       Dailycategory[0].image,
                                       String(txtMonthlySaving!.text!),
                                       TodayDate,
                                       Dailycategory[0].name,
                                       AccountId!.id,
                                       String("1"),
                                       String("1"),
                                       String("0"),
                                       TodayDate,
                                       Double(0.0)
            
            ])
        
        try! uiRealm.write { () -> Void in
            uiRealm.add(dailySaving)
            
            savingView.isHidden = true
            totalAllAmounts(date: selectedMonthDate)
        }
    }
    
    //MARK:- Tableview Method
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == tblcurrency)
        {
            return 1
        }
        else if tableView == tblCategoryList
        {
            return 1
        }
        else if tableView == tblAIList
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tblcurrency)
        {
            if(searchActive) {
                return filtered.count
            }
            return countryArray.count
        }
        else if tableView == tblCategoryList
        {
            return categoryArray.count
        }
        else if tableView == tblAIList
        {
            if finalAIArray.count > 5
            {
                return 5
            }
            else
            {
                return finalAIArray.count
            }
        }
        else
        {
            return dashboardArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == tblcurrency)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCountry", for: indexPath as IndexPath)
            
            cell.imageView?.layer.cornerRadius = 5
            cell.imageView?.layer.masksToBounds = true
            
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                        
                    {
                        cell.imageView?.image = UIImage(named: "\(temp.alpha2).png")
                        cell.textLabel?.text = temp.name
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = countryname.currency
                
                cell.imageView?.image = UIImage(named: "\(countryname.alpha2).png")
                cell.textLabel?.text = countryname.name
            }
            
            //getCurrencySymbol(currencyCode: countryname.name)
            
            return cell
        }
        else if tableView == tblCategoryList
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath as IndexPath) as! dashboardCategoryCell
            
            cell.lblCategoryTitle.text = (categoryArray[indexPath.row] as! Category).name
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(DashboardViewController.btnSelectCategoryAction(_:)), for: .touchUpInside)
            
            if selectCategoryArray.contains((categoryArray[indexPath.row] as! Category)) {
                cell.btnCheck.setImage(UIImage(named:"ic_check"), for: UIControlState.normal)
            }
            else
            {
                cell.btnCheck.setImage(UIImage(named:"ic_uncheck"), for: UIControlState.normal)
            }
            return cell
        }
        else if tableView == tblAIList
        {
            
            print("Final AI Report:",finalAIArray[indexPath.row])
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AICell", for: indexPath as IndexPath) as! AILIstCell
            
            if indexPath.row == 0
            {
                cell.lblCategoryTitle.font = UIFont(name: "Helvetica-Regular", size: 13)
                cell.lblPriviousMonthAmount.font = UIFont(name: "Helvetica-Regular", size: 13)
                cell.lblCurrentMonthAmount.font = UIFont(name: "Helvetica-Regular", size: 13)
                
                cell.lblCategoryTitle.backgroundColor = UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 251.0/255.0, alpha: 1)
                cell.lblPriviousMonthAmount.backgroundColor = UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 251.0/255.0, alpha: 1)
                cell.lblCurrentMonthAmount.backgroundColor = UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 251.0/255.0, alpha: 1)
            }
            else
            {
                
            }
            cell.lblCategoryTitle.text = finalAIArray[indexPath.row]["categoryName"] as? String
            cell.lblPriviousMonthAmount.text = finalAIArray[indexPath.row]["priviousAmount"] as? String
            cell.lblCurrentMonthAmount.text = finalAIArray[indexPath.row]["currentAmount"] as? String
            
            return cell
        }
        else
        {
            //For Dashboard Tableview
            //No Set Budget
            if dashboardArray[indexPath.row]["isBudget"] as! Bool == false
            {
                //For Goals
                if indexPath.row == 4
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell5", for: indexPath as IndexPath) as! GoalsCell
                    cell.selectionStyle = .none
                    
                    cell.viewBG.layer.cornerRadius = 8
                    
                    cell.contentView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
                    cell.contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
                    cell.contentView.layer.cornerRadius = 8
                    cell.contentView.layer.shadowOpacity = 0.5
                    cell.contentView.layer.shadowRadius = 3
                    
                    cell.bgimage.layer.cornerRadius = 8
                    cell.bgimage.layer.masksToBounds = true
//
                    cell.bgimage?.image = UIImage(named: dashboardArray[indexPath.row]["backgroundImage"] as! String)
                    cell.bgimage.setImageColor(color: UIColor.white)
                    cell.lblGoalCount.text = "\(goalsCount)"
                    cell.lblGoalTotla.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(goalstotal)"
                    cell.lblGoalSaving.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(goalsSaving)"
                    cell.clipsToBounds = false
                    
                    cell.lblGoalCount.font = DashboardSubLabel_17
                    cell.lblGoalTotla.font = DashboardSubLabel_17
                    cell.lblGoalSaving.font = DashboardSubLabel_17
                    cell.lblGoalTotlaTitle.font = DashboardSubLabel_17
                    cell.lblGoalSavingTitle.font = DashboardSubLabel_17
                    cell.lblGoalCountTitle.font = DashboardSubLabel_17
                    cell.lblGoalTitle.font = DashboardTableLabelTitle
                    
                    cell.lblGoalTitle.textColor = UIColor.black
                    cell.lblGoalCount.textColor = UIColor.black
                    cell.lblGoalTotla.textColor = UIColor.black
                    cell.lblGoalSaving.textColor = UIColor.black
                    cell.lblGoalTotlaTitle.textColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1)
                    cell.lblGoalSavingTitle.textColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1)
                    cell.lblGoalCountTitle.textColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1)
                    return cell
                }
                
                
                //For Accounts
                else if indexPath.row == 3
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath as IndexPath) as! AccountCell
                    cell.selectionStyle = .none
                    
                    cell.viewBG.layer.cornerRadius = 8
                    
                    cell.contentView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
                    cell.contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
                    cell.contentView.layer.cornerRadius = 8
                    cell.contentView.layer.shadowOpacity = 0.5
                    cell.contentView.layer.shadowRadius = 3
                    
                    cell.bgimageBudget.layer.cornerRadius = 8
                    cell.bgimageBudget.layer.masksToBounds = true

                    let currentAmount1 : NSArray = uiRealm.objects(Account.self).filter("id = %@",UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "remainigAmount") as! NSArray
                    
                    cell.bgimageBudget?.image = UIImage(named: dashboardArray[indexPath.row]["backgroundImage"] as! String)
                    cell.bgimageBudget.setImageColor(color: UIColor.white)
                    cell.lblAccountCount.text = String(describing: dashboardArray[indexPath.row]["totalAmount"]!)
                    cell.lblCreditsAmount.text = String(describing: "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \((currentAmount1)[0])")
//                    cell.lblCreditsAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \((currentAmount1)[0])"))"
                    cell.lblDebitsAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["budgetCurrentAmount"]!))"
                    
                    
                    cell.clipsToBounds = false
                    cell.lblAccountTitle.text = dashboardArray[indexPath.row]["name"] as? String
                    cell.lblAccountCount.font = DashboardSubLabel_17
                    cell.lblCreditsAmount.font = DashboardSubLabel_17
                    cell.lblDebitsAmount.font = DashboardSubLabel_17
                    cell.lblDebitsTitle.font = DashboardSubLabel_17
                    cell.lblCreditsTitle.font = DashboardSubLabel_17
                    cell.lblAccountCountTitle.font = DashboardSubLabel_17
                    cell.lblAccountTitle.font = DashboardTableLabelTitle
                    
                    cell.lblAccountTitle.textColor = UIColor.black
                    cell.lblAccountCount.textColor = UIColor.black
                    cell.lblCreditsAmount.textColor = UIColor.black
                    cell.lblDebitsAmount.textColor = UIColor.black
                    cell.lblDebitsTitle.textColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1)
                    cell.lblCreditsTitle.textColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1)
                    cell.lblAccountCountTitle.textColor = UIColor(red: 187.0/255.0, green: 187.0/255.0, blue: 187.0/255.0, alpha: 1)
                    return cell
                }
                //For Bills
                /*
                if indexPath.row == 3
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath as IndexPath) as! BillsCell
                    cell.selectionStyle = .none
                    
                    cell.viewBG.layer.cornerRadius = 8
                    
                    cell.contentView.layer.shadowColor = UIColor(colorLiteralRed: 135.0/255.0, green: 135.0/255.0, blue: 135.0/255.0, alpha: 1).cgColor
                    cell.contentView.layer.cornerRadius = 8
                    cell.contentView.layer.shadowOpacity = 1.5
                    cell.contentView.layer.shadowRadius = 6
                    cell.contentView.layer.shadowOffset = CGSize.zero
                    
                    cell.bgimageBills.layer.cornerRadius = 8
                    cell.bgimageBills.layer.masksToBounds = true
                    
                    cell.bgimageBills?.image = UIImage(named: dashboardArray[indexPath.row]["backgroundImage"] as! String)
//                    cell.lblTotalAmount.text = String(describing: dashboardArray[indexPath.row]["totalAmount"]!)
                    cell.lblTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["totalAmount"]!))"
                    
                    cell.lblPaidBills.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["budgetAmount"]!))"
                    cell.lblRemainingBills.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["budgetCurrentAmount"]!))"
                    
                    cell.clipsToBounds = false
                    cell.lblTitle.text = dashboardArray[indexPath.row]["name"] as? String
                    cell.lblTotalAmount.font = DashboardAmount
                    cell.lblPaidBills.font = DashboardAmount
                    cell.lblRemainingBills.font = DashboardAmount
                    cell.lblTotalAmountTitle.font = DashboardSubLabel
                    cell.lblPaidBillsTitle.font = DashboardSubLabel
                    cell.lblRemainingBillsTitle.font = DashboardSubLabel
                    cell.lblTitle.font = DashboardTableLabelTitle
                    return cell
                }
                */
                    
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath as IndexPath) as! IncomeExpenseCell
                    cell.selectionStyle = .none
                    cell.viewBG.layer.cornerRadius = 8
                    
                    cell.contentView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
                    cell.contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
                    cell.contentView.layer.cornerRadius = 8
                    cell.contentView.layer.shadowOpacity = 0.5
                    cell.contentView.layer.shadowRadius = 3
//                    cell.contentView.layer.shadowOffset = CGSize.zero
                    
                    //Add Button Shadow
                    cell.btnAddTransaction.layer.masksToBounds = false
                    cell.btnAddTransaction.layer.cornerRadius = cell.btnAddTransaction.frame.size.width/2
                    cell.btnAddTransaction.layer.shadowColor = UIColor.lightGray.cgColor
                    cell.btnAddTransaction.layer.shadowOpacity = 1
                    cell.btnAddTransaction.layer.shadowOffset = CGSize.zero
                    cell.btnAddTransaction.layer.shadowRadius = 3
                    cell.bgimage.layer.cornerRadius = 8
                    cell.bgimage.layer.masksToBounds = true
                    
                    cell.bgimage?.image = UIImage(named: dashboardArray[indexPath.row]["backgroundImage"] as! String)
                    cell.bgimage.setImageColor(color: UIColor.white)
                    cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["totalAmount"]!))"
                   cell.btnAddTransaction.setImage(UIImage(named:dashboardArray[indexPath.row]["btnbackgroundImage"] as! String), for: .normal)
                    cell.clipsToBounds = false
                    cell.lblTitle.text = dashboardArray[indexPath.row]["name"] as? String
                    cell.lblTitle.textColor = UIColor.black
                    
                    cell.btnAddTransaction.tag = indexPath.row
                cell.btnAddTransaction.addTarget(self,action:#selector(btnAddTransactionAction(_:)),
                                                     for:.touchUpInside)
                    cell.lblTitle.font = DashboardTableLabelTitle
                    cell.lblAmount.font = DashboardAmount
                    cell.lblAmount.textColor = UIColor.black
                    return cell
                }
            }
            else
            {
                //Budget Set
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath as IndexPath) as! BudgetCell
                cell.selectionStyle = .none
                
                //View Shadow
                cell.viewBGBudget.layer.cornerRadius = 8
                cell.contentView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
                cell.contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
                cell.contentView.layer.cornerRadius = 8
                cell.contentView.layer.shadowOpacity = 0.5
                cell.contentView.layer.shadowRadius = 3
                cell.bgimageBudget.layer.cornerRadius = 8
                cell.bgimageBudget.layer.masksToBounds = true
                
                //Add Button Shadow
                cell.btnAddBudget.layer.masksToBounds = false
                cell.btnAddBudget.layer.cornerRadius = cell.btnAddBudget.frame.size.width/2
                cell.btnAddBudget.layer.shadowColor = UIColor.lightGray.cgColor
                cell.btnAddBudget.layer.shadowOpacity = 1
                cell.btnAddBudget.layer.shadowOffset = CGSize.zero
                cell.btnAddBudget.layer.shadowRadius = 3
                
                cell.btnAddBudget.setImage(UIImage(named:dashboardArray[indexPath.row]["btnbackgroundImage"] as! String), for: .normal)
                cell.lblTitle.text = dashboardArray[indexPath.row]["name"] as? String
                cell.lblDisplayBudget.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["budgetAmount"]!))"
                cell.lblUsedBudget.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["budgetCurrentAmount"]!))"
                cell.lblRemainBudget.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["totalAmount"]!))"
                
                if Double(dashboardArray[indexPath.row]["totalAmount"] as! String)! < 0
                {
                    cell.lblRemainBudget.textColor = UIColor.red
                }
                else
                {
                    cell.lblRemainBudget.textColor = UIColor.white
                }
                
                cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(String(describing: dashboardArray[indexPath.row]["totalAmount"]!))"
                
                if Double(String(describing: dashboardArray[indexPath.row]["budgetAmount"]!))! == 0.0//Double(dashboardArray[indexPath.row]["budgetAmount"] as! String) == 0.0
                {
                    cell.budgetProgress.progress = 0.0
                }
                else if Double(String(describing: dashboardArray[indexPath.row]["budgetCurrentAmount"]!))! == 0.0//Double(dashboardArray[indexPath.row]["budgetCurrentAmount"] as! String) == 0.0
                {
                    cell.budgetProgress.progress = 0.0
                }
                else
                {
                    cell.budgetProgress.progress = Float(Float(Double(String(describing: dashboardArray[indexPath.row]["budgetCurrentAmount"]!))! / Double(String(describing: dashboardArray[indexPath.row]["budgetAmount"]!))!))
                }
                
                cell.budgetProgress.progressTintColor = UIColor.darkGray
                cell.budgetProgress.gradientColors = [UIColor(red: 176.0/255.0, green: 106.0/255.0, blue: 179.0/255.0, alpha: 1).cgColor, UIColor(red: 69.0/255.0, green: 104.0/255.0, blue: 220.0/255.0, alpha: 1).cgColor]
                cell.bgimageBudget?.image = UIImage(named: dashboardArray[indexPath.row]["backgroundImage"] as! String)
                cell.bgimageBudget.setImageColor(color: UIColor.white)
                cell.btnAddBudget.tag = indexPath.row
                cell.btnAddBudget.addTarget(self,action:#selector(btnAddTransactionAction(_:)),
                                                 for:.touchUpInside)
                cell.lblTitle.font = DashboardTableLabelTitle
                cell.lblAmount.font = DashboardAmount
                cell.lblRemainBudget.font = DashboardSubLabel
                cell.lblUsedBudget.font = DashboardSubLabel
                cell.lblDisplayBudget.font = DashboardSubLabel
                cell.lblTotalBudget.font = DashboardSubLabel
                cell.lblSave.font = DashboardSubLabel
                
                cell.lblTitle.textColor = UIColor.black
                cell.lblTitle.textColor = UIColor.black
                cell.lblAmount.textColor = UIColor.black
                cell.lblRemainBudget.textColor = UIColor.lightGray
                cell.lblUsedBudget.textColor = UIColor.lightGray
                cell.lblDisplayBudget.textColor = UIColor.lightGray
                cell.lblTotalBudget.textColor = UIColor.lightGray
                cell.lblSave.textColor = UIColor.lightGray

                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(tableView == tblcurrency)
        {
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                    {
                        let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                        strCurrency = currencyCode
                        UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = getCurrencySymbol(currencyCode: countryname.currency)
                strCurrency = currencyCode
                UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
            }
            removeAnimate1(selectOption: "Currency")
            currencySearchBar.resignFirstResponder()
            viewWillAppear(true)
        }
        else if(tableView == tblCategoryList)
        {
            if selectCategoryArray.contains((categoryArray[indexPath.row] as! Category))
            {
                selectCategoryArray.remove((categoryArray[indexPath.row] as! Category))
            }
            else
            {
                selectCategoryArray.add(categoryArray[indexPath.row] as! Category)
            }
            tblCategoryList.reloadData()
        }
        else
        {
            if indexPath.row == 0 || indexPath.row == 1
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as! TransactionHistoryViewController
                if indexPath.row == 0
                {
                    vc.categoryType = "0"
                }
                else
                {
                    vc.categoryType = "1"
                }
                vc.strCategoryId = "0"
                vc.currentMonthDate = selectedMonthDate
                vc.passingDate = selectedMonthDate
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetViewController") as! BudgetViewController
                vc.viewSelection = "Dashboard"
                vc.passingDate = selectedMonthDate
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
                vc.passingDate = selectedMonthDate
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 4
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsVC") as! SetGoalsVC
//                vc.passingDate = selectedMonthDate
                self.navigationController?.pushViewController(vc, animated: true)
            }

            else
            {
                let catId = dashboardArray[indexPath.row]["categoryId"]!
                if("\(catId)" == BillCategoryID)
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillsVC") as! BillsVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as! TransactionHistoryViewController
                    vc.categoryType = "0"
                    vc.currentMonthDate = selectedMonthDate
                    vc.strCategoryId = dashboardArray[indexPath.row]["categoryId"] as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == tblcurrency)
        {
            return 44
        }
        else if(tableView == tblCategoryList)
        {
            return 44
        }
        else if tableView == tblList
        {
            if dashboardArray[indexPath.row]["isBudget"] as! Bool == false
            {
                if indexPath.row == 4
                {
                    return 170
                }
                else if indexPath.row == 3
                {
                    return 170
                }
                
                return 135
            }
            else
            {
                return 200
            }
        }
        else
        {
            return 44
        }
        
    }
    
    //MARK: - Admob Delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)

        BottomHeightConstraint.constant = 0
        
    }
    
    //MARK:- Action Method
    @objc func CheckPurchase(notification:NSNotification){
        if UserDefaults.standard.value(forKey: "Purchase") != nil {
            if UserDefaults.standard.bool(forKey: "Purchase") {
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    @IBAction func btnAccountAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainAccountListVC") as! MainAccountListVC
        //vc?.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnAIDoneAction(_ sender: Any) {
        removeAnimate1(selectOption: "AIReport")
    }
    
    @IBAction func btnAddCategoryAction(_ sender: UIButton) {
        titleView.setGradientColor(color1: UIColor(red: 209.0/255.0, green: 11.0/255.0, blue: 192.0/255.0, alpha: 1), color2: UIColor(red: 86.0/255.0, green: 42.0/255.0, blue: 162.0/255.0, alpha: 1))
        viewButtonBackground.setGradientColor(color1: UIColor(red: 209.0/255.0, green: 11.0/255.0, blue: 192.0/255.0, alpha: 1), color2: UIColor(red: 86.0/255.0, green: 42.0/255.0, blue: 162.0/255.0, alpha: 1))
        
        showAnimate1(selectOption: "Category")
        
    }
    
    @IBAction func btnSelectCategoryAction(_ sender: UIButton)
    {
        if selectCategoryArray.contains((categoryArray[sender.tag] as! Category))
        {
            selectCategoryArray.remove((categoryArray[sender.tag] as! Category))
        }
        else
        {
            selectCategoryArray.add(categoryArray[sender.tag] as! Category)
        }
        tblCategoryList.reloadData()
    }
    
    @IBAction func btnOKCategoryAction(_ sender: UIButton) {
        
        dashboardArray = []
//        if selectCategoryArray.count > 0
//        {
            for j in 0..<categoryArray.count//selectCategoryArray
            {
                if selectCategoryArray.count == 0
                {
                    let workouts = uiRealm.objects(Category.self).filter("categoryID = %@", (categoryArray[j] as! Category).categoryID)
                    
                    if let workout = workouts.first
                    {
                        try! uiRealm.write
                        {
                            workout.displayDashboard = 0
                        }
                    }
                }
                else
                {
                    for i in 0..<selectCategoryArray.count
                    {
                        if (categoryArray[j] as! Category).categoryID == (selectCategoryArray[i] as! Category).categoryID
                        {
                            let workouts = uiRealm.objects(Category.self).filter("categoryID = %@", (selectCategoryArray[i] as! Category).categoryID)
                            
                            if let workout = workouts.first
                            {
                                try! uiRealm.write
                                {
                                    workout.displayDashboard = 1
                                }
                            }
                            break
                        }
                        else
                        {
                            let workouts = uiRealm.objects(Category.self).filter("categoryID = %@", (categoryArray[j] as! Category).categoryID)
                            
                            if let workout = workouts.first
                            {
                                try! uiRealm.write
                                {
                                    workout.displayDashboard = 0
                                }
                            }
                        }
                        
                    }
                }
            }
//        }
        
        for i in 0..<array.count
        {
            var dict = [String:AnyObject]()
            dict["name"] = array[i] as AnyObject
            if i == 0
            {
                dict["totalAmount"] = totalExpense as AnyObject
                dict["budgetAmount"] = "0.0" as AnyObject
                dict["budgetCurrentAmount"] = "0.0" as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 1
            {
                dict["totalAmount"] = totalIncome as AnyObject
                dict["budgetAmount"] = "0.0" as AnyObject
                dict["budgetCurrentAmount"] = "0.0" as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 3
            {
                dict["totalAmount"] = totalBillsAmount as AnyObject
                dict["budgetAmount"] = "0.0" as AnyObject
                dict["budgetCurrentAmount"] = "0.0" as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 4
            {
                dict["totalAmount"] = String(accountCount) as AnyObject
                dict["budgetAmount"] = String(creditAmount) as AnyObject
                dict["budgetCurrentAmount"] = String(debitAmount) as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            else if i == 2
            {
                let budgetAmountArray = NSMutableArray()
                for i in 0..<totalBudget.count
                {
                    budgetAmountArray.add(String((totalBudget[i] as! Budget).budgetAmount))
                }
                let usedBudgetAmountArray = NSMutableArray()
                for i in 0..<totalBudget.count
                {
                    usedBudgetAmountArray.add(String((totalBudget[i] as! Budget).currentAmount))
                }
                if budgetAmountArray.count == 0
                {
                    dict["budgetAmount"] = "0.0" as AnyObject
                }
                else
                {
                    dict["budgetAmount"] = String.sum(array: budgetAmountArray)! as AnyObject
                }
                
                if usedBudgetAmountArray.count == 0
                {
                    dict["budgetCurrentAmount"] = "0.0" as AnyObject
                }
                else
                {
                    dict["budgetCurrentAmount"] = String.sum(array: usedBudgetAmountArray)! as AnyObject
                }
                dict["totalAmount"] = String(Double(Double(String.sum(array: budgetAmountArray)!)!-Double(String.sum(array: usedBudgetAmountArray)!)!)) as AnyObject
                dict["isBudget"] = true as AnyObject
            }
            
            dict["backgroundImage"] = image[i] as AnyObject
            dict["btnbackgroundImage"] = img_Add[i] as AnyObject
            dict["categoryId"] = "0" as AnyObject
            
            dashboardArray.append(dict)
        }
        
        let workouts = uiRealm.objects(Category.self).filter("displayDashboard = %d", 1)
        
        for i in 0..<workouts.count
        {
            var dict = [String:AnyObject]()
            dict["name"] = workouts[i]["name"] as AnyObject
            
            let totalAmount = uiRealm.objects(Transaction.self).filter("categoryID = %@ AND accountId = %@ AND date BETWEEN {%@,%@}",workouts[i]["categoryID"] as! String, UserDefaults.standard.integer(forKey: "AccountID"), Date().startOfMonth(), Date().endOfMonth()).value(forKey: "amount") as! NSArray
            
            dict["totalAmount"] = String.sum(array: totalAmount)! as AnyObject
            dict["categoryId"] = (workouts[i]["categoryID"] as! String) as AnyObject
            
            let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d AND date BETWEEN {%@,%@}",Int(workouts[i]["categoryID"] as! String)!, Date().startOfMonth(), Date().endOfMonth())
            
            if let budgetWorkout = budgetUpdate.last
            {
                dict["budgetAmount"] = budgetWorkout.budgetAmount as AnyObject
                dict["budgetCurrentAmount"] = budgetWorkout.currentAmount as AnyObject
                dict["isBudget"] = true as AnyObject
            }
            else
            {
                dict["budgetAmount"] = 0.0 as AnyObject
                dict["budgetCurrentAmount"] = 0.0 as AnyObject
                dict["isBudget"] = false as AnyObject
            }
            
            dict["backgroundImage"] = image[Int(arc4random_uniform(UInt32(image.count)))] as AnyObject
            dict["btnbackgroundImage"] = "ic_addDashboard" as AnyObject
            
            dashboardArray.append(dict)
        }
        print(dashboardArray)
        removeAnimate1(selectOption: "Category")
        tblList.reloadData()
        
    }
    
    @IBAction func btnIncomeAction(_ sender: UIButton) {
    }
    
    @IBAction func btnAddTransactionAction(_ sender: UIButton){
        
        if sender.tag == 2
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetDetailViewController") as! BudgetDetailViewController
            //                vc.viewSelection = "Dashboard"
            vc.passingDate = selectedMonthDate
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 3
        {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BillsViewController")as! BillsViewController
            
            let productDetailView = self.storyboard?.instantiateViewController(withIdentifier: "BillCategoryViewController")as! BillCategoryViewController
            self.navigationController?.pushViewController(viewController, animated: false)
            viewController.passingDate = selectedMonthDate
            productDetailView.passingDate = selectedMonthDate
            self.navigationController?.pushViewController(productDetailView, animated: true)
        }
        else if sender.tag == 4
        {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAccountViewController")as! AddAccountViewController
            viewController.passingDate = selectedMonthDate
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController
            vc.passingView = "addTransaction"
            if sender.tag == 1
            {
                vc.categoryType = "1"
                vc.strCategoryType = "Income"
            }
            else
            {
                vc.categoryType = "0"
                vc.strCategoryType = "Expense"
            }
            vc.strCategoryID = dashboardArray[sender.tag]["categoryId"] as! String
            self.navigationController?.pushViewController(vc, animated: true)
//            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func btnNextMonthAction(_ sender: UIButton) {
        let nextDate = Constant.getNextMonthDate(mydate: selectedMonthDate)
        selectedMonthDate = nextDate
        strCurrentMonth = Constant.getMonthYear(selectedMonthDate)
        lblCurrentMonthTitle.text = strCurrentMonth
        print(nextDate)
        totalAllAmounts(date: selectedMonthDate)
        RightTOleft(viewAnimation: tblList)
        RightTOleftView(viewAnimation: TotalAmountView)
    }
    
    @IBAction func btnPrevioudMonthAction(_ sender: UIButton) {
        let priviousDate = Constant.getPriviousMonthDate(mydate: selectedMonthDate)
        selectedMonthDate = priviousDate
        strCurrentMonth = Constant.getMonthYear(selectedMonthDate)
        lblCurrentMonthTitle.text = strCurrentMonth
        print(priviousDate)
        totalAllAmounts(date: selectedMonthDate)
        LeftToRight(viewAnimation: tblList)
        LeftToRightView(viewAnimation: TotalAmountView)
    }
    
    @IBAction func btnAddMonthlySavingAction(_ sender: UIButton) {
       
        //filter category of daily expence
        if(txtMonthlySaving.text != ""){
            let monthlySavingCategory = uiRealm.objects(Category.self).filter("categoryType = %@ AND categoryID = %@","1",dailySavingCategoryID).toArray!
            print("MonthlySavingcategoty = \(monthlySavingCategory)")
            
            //for Add Money to Transaction
            AddDailyTransaction(Dailycategory: monthlySavingCategory)
        }else{
            alertView(alertMessage: "Enter Some Amount")
        }
    }
    
    // MARK: - SearchBar Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = countryNewArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tblcurrency.reloadData()
        
    }

 }
extension DashboardViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (tblList != nil)
        {
            let scrollDiff = scrollView.contentOffset.y - self.previousScrollOffset
            
            let absoluteTop: CGFloat = 0;
            let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;
            
            let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
            let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom
            
            if canAnimateHeader(scrollView) {
                
                // Calculate new header height
                var newHeight = self.headerHeightConstraint.constant
                if isScrollingDown {
                    newHeight = max(self.minHeaderHeight, self.headerHeightConstraint.constant - abs(scrollDiff))
                } else if isScrollingUp {
                    newHeight = min(self.maxHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
                }
                
                // Header needs to animate
                if newHeight != self.headerHeightConstraint.constant {
                    self.headerHeightConstraint.constant = newHeight
                    self.updateHeader()
                    self.setScrollPosition(self.previousScrollOffset)
                }
                
                self.previousScrollOffset = scrollView.contentOffset.y
            }
        }
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (tblList != nil)
        {
            self.scrollViewDidStopScrolling()
        }
        
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if (tblList != nil)
        {
            if !decelerate {
                self.scrollViewDidStopScrolling()
            }
        }
    }
    
    func scrollViewDidStopScrolling() {
        
        if (tblList != nil)
        {
            let range = self.maxHeaderHeight - self.minHeaderHeight
            let midPoint = self.minHeaderHeight + (range / 3)
            
            if self.headerHeightConstraint.constant > midPoint {
                self.expandHeader()
            } else {
                self.collapseHeader()
            }
        }
        
        
    }
    
    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        
        if (tblList != nil)
        {
            // Calculate the size of the scrollView when header is collapsed
            let scrollViewMaxHeight = scrollView.frame.height + self.headerHeightConstraint.constant - minHeaderHeight
            
            // Make sure that when header is collapsed, there is still room to scroll
            return scrollView.contentSize.height > scrollViewMaxHeight
        }
        else
        {
            return false
        }
        
        
    }
    
    func collapseHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.minHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    func expandHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.maxHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    func setScrollPosition(_ position: CGFloat) {
        
        if (tblList != nil)
        {
            self.tblList.contentOffset = CGPoint(x: self.tblList.contentOffset.x, y: position)
        }
    }
    
    func updateHeader() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.headerHeightConstraint.constant - self.minHeaderHeight
        let percentage = openAmount / range
        
        if percentage == 0.0
        {
            self.lblTitle.alpha = 1
        }
        else{
            self.lblTitle.alpha = 0
        }
        
        //        self.titleTopConstraint.constant = -openAmount + 10
        print(self.titleTopConstraint.constant-openAmount + 10,percentage)
        self.lblCurrentBalance.alpha = percentage
        self.lblCurrentBalanceTitle.alpha = percentage
        self.lblOutlays.alpha = percentage
        self.TotalAmountView.alpha = percentage
        
    }
}
