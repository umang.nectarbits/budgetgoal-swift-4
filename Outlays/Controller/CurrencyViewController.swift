//
//  CurrencyViewController.swift
//  Outlays
//
//  Created by Cools on 11/2/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class CurrencyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet weak var tblcurrency: UITableView!
    @IBOutlet weak var currencySearchBar: UISearchBar!
    
    var countryArray = NSMutableArray()
    var countryNewArray : [String]! = []
    var searchActive: Bool = false
    var filtered: [String] = []
    var strCurrency : String = ""
    var selectedCurrency: String = ""
    var currencyNumber : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        print(selectedCurrency)
        lblHeaderTitle.font = headerTitleFont
        tblcurrency.tableFooterView = UIView.init(frame: CGRect.zero)
        countryArray = ((IsoCountries.allCountries as NSArray).mutableCopy() as! NSMutableArray)
        for i in 0..<countryArray.count {
            let temp = countryArray[i] as! IsoCountryInfo
            countryNewArray.append(temp.name)
            
        }
        print(countryNewArray)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    
    //MARK:- Helper Method
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
            print("Currency Symbol : \(currencySymbol)")
        }
        
        return currencySymbol
    }
    
    // MARK: - Tableview Delegate Method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(searchActive) {
            return filtered.count
        }
        return countryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath as IndexPath)
        
        cell.imageView?.layer.cornerRadius = 5
        cell.imageView?.layer.masksToBounds = true
        
        if(searchActive == true)
        {
            for i in 0..<countryArray.count
            {
                let temp = countryArray[i] as! IsoCountryInfo
                if(filtered[indexPath.row] as String == temp.name)
                {
                    cell.imageView?.image = UIImage(named: "\(temp.alpha2).png")
                    cell.textLabel?.text = temp.name
                }
            }
        }
        else {
            let countryname = countryArray[indexPath.row] as! IsoCountryInfo
            let currencyCode = countryname.currency
            
            cell.imageView?.image = UIImage(named: "\(countryname.alpha2).png")
            cell.textLabel?.text = countryname.name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(searchActive == true){
            for i in 0..<countryArray.count
            {
                let temp = countryArray[i] as! IsoCountryInfo
                
                if(filtered[indexPath.row] as String == temp.name)
                {
                    if(temp.currency == selectedCurrency){
                        print("Not Selected")
                        
                        let alert = UIAlertController(title: "Alert", message: "You have selected this country", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                        strCurrency = currencyCode
                        print(temp)
                        
                        var  temp1 = [String:AnyObject]()//NSMutableDictionary()
                        temp1["currencyCode"] = strCurrency as AnyObject//temp1.setValue(strCurrency, forKey: "currencyCode")
                        temp1["name"] = temp.name as AnyObject//temp1.setValue(temp.name, forKey: "name")
                        temp1["currency"] = temp.currency as AnyObject//temp1.setValue(temp.currency, forKey: "currency")
                        temp1["currencyFlag"] = "\(temp.alpha2).png" as AnyObject//temp1.setValue("\(temp.alpha2).png", forKey: "currencyFlag")
                        temp1["currencyNumber"] = currencyNumber as AnyObject//temp1.setValue(currencyNumber, forKey: "currencyNumber")
                        //                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getCountryCurrency"), object: temp1)
                        NotificationCenter.default.post(name: NSNotification.Name("getCountryCurrency"), object: temp1)
                        self.navigationController?.popViewController(animated: true)
                    }
                    //                    let  temp1 = NSMutableDictionary()
                    //                    temp1.setValue(strCurrency, forKey: "currencyCode")
                    //                    temp1.setValue(temp.name, forKey: "name")
                    //                    temp1.setValue(temp.currency, forKey: "currency")
                    //                    temp1.setValue("\(temp.alpha2).png", forKey: "currencyFlag")
                    //                    temp1.setValue(currencyNumber, forKey: "currencyNumber")
                    //                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getCountryCurrency"), object: temp1)
                    //                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else {
            let countryname = countryArray[indexPath.row] as! IsoCountryInfo
            let currencyCode = getCurrencySymbol(currencyCode: countryname.currency)
            strCurrency = currencyCode
            print(countryname)
            let  temp1 = NSMutableDictionary()
            temp1.setValue(strCurrency, forKey: "currencyCode")
            temp1.setValue(countryname.name, forKey: "name")
            temp1.setValue(countryname.currency, forKey: "currency")
            temp1.setValue("\(countryname.alpha2).png", forKey: "currencyFlag")
            temp1.setValue(currencyNumber, forKey: "currencyNumber")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getCountryCurrency"), object: temp1)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - SearchBar Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = countryNewArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tblcurrency.reloadData()
        
    }
    //MARK: - Action Method
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
