//
//  SettingCurrencyViewController.swift
//  Outlays
//
//  Created by harshesh on 28/09/1939 Saka.
//  Copyright © 1939 Cools. All rights reserved.
//

import UIKit

class SettingCurrencyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var HeaderView: UIView!
    @IBOutlet var HeaderTitle: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var tblcurrency: UITableView!
    @IBOutlet weak var currencySearchBar: UISearchBar!

    var countryArray = NSMutableArray()
    var countryNewArray : [String]! = []
    var filtered: [String] = []
    var searchActive: Bool = false
    var strCurrency : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        
        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)
        HeaderTitle.font = InnerHeadersFont
        HeaderTitle.textColor = HeaderTitleColor
        
        countryArray = (IsoCountries.allCountries as! NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<countryArray.count {
            let temp = countryArray[i] as! IsoCountryInfo
            countryNewArray.append(temp.name)
        }
        print(countryNewArray)
        strCurrency = UserDefaults.standard.value(forKey: "currencySymbol") as! String

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    //MARK: - Helper Method
    
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
            print("Currency Symbol : \(currencySymbol)")
        }
        
        return currencySymbol
    }
    
    //MARK: - TableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        
            return 1
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
            if(searchActive) {
                return filtered.count
            }
            return countryArray.count
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath as IndexPath)
        
        cell.imageView?.layer.cornerRadius = 5
        cell.imageView?.layer.masksToBounds = true
        
        if(searchActive == true)
        {
            for i in 0..<countryArray.count
            {
                let temp = countryArray[i] as! IsoCountryInfo
                if(filtered[indexPath.row] as String == temp.name)

                {
                    cell.imageView?.image = UIImage(named: "\(temp.alpha2).png")
                    cell.textLabel?.text = temp.name
                }
            }
        }
        else
        {
            let countryname = countryArray[indexPath.row] as! IsoCountryInfo
            let currencyCode = countryname.currency
            
            cell.imageView?.image = UIImage(named: "\(countryname.alpha2).png")
            cell.textLabel?.text = countryname.name
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(tableView == tblcurrency)
        {
            if(searchActive == true){
                for i in 0..<countryArray.count
                {
                    let temp = countryArray[i] as! IsoCountryInfo
                    if(filtered[indexPath.row] as String == temp.name)
                    {
                        let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                        strCurrency = currencyCode
                        UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
//                        tblSetting.reloadData()
                    }
                }
            }
            else {
                let countryname = countryArray[indexPath.row] as! IsoCountryInfo
                let currencyCode = getCurrencySymbol(currencyCode: countryname.currency)
                strCurrency = currencyCode
                UserDefaults.standard.set(strCurrency, forKey: "currencySymbol")
                print(currencyCode)
            }
            self.navigationController?.popViewController(animated: true)

        }
    }
    // MARK: - SearchBar Delegate Method
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //        searchActive = true;
        let textField: UITextField? = (searchBar.value(forKey: "_searchField") as? UITextField)
        textField?.clearButtonMode = .never
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = countryNewArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tblcurrency.reloadData()
        
    }
    
    //MARK: - Action Method
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
 
}

