//
//  AddAccountViewController.swift
//  Created by harshesh on 06/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AddAccountViewController: UIViewController, JBDatePickerViewDelegate, UITextFieldDelegate,GADBannerViewDelegate {
    
//    @IBOutlet var HeaderView : UIView!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var AccountView: UIView!
    @IBOutlet var BalanceView: UIView!
    @IBOutlet var calView: UIView!
    @IBOutlet var txtBalance: UITextField!
    @IBOutlet var btnCalander: UIButton!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var headerView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var btnSelectDate: UIButton!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblAccountNameTitle: UILabel!
    @IBOutlet var txtAccountName: UITextField!
    @IBOutlet var lblBalanceTitle: UILabel!
    @IBOutlet var lblDateTitle: UILabel!
    @IBOutlet var btnHomeAdd: UIButton!
    @IBOutlet var btnSetDefault: UIButton!
    @IBOutlet var img_back: UIImageView!
    @IBOutlet var img_back1: UIImageView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var selectedDate1 = Date()
    var dateToSelect: Date!
    var currentDate = Date()
    var HomeActive :Bool = true
    var DefultAmount : Bool = false
    var passingView = ""
    var transferView = ""
    var editData = [String:AnyObject]()
    var passingDate : Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        setFont()
        setViewShadow()
        self.calendarView.isHidden = true
        self.bgView.isHidden = true
        
        txtBalance.addDoneButtonToKeyboard(myAction:  #selector(txtBalance.resignFirstResponder))
        txtAccountName.addDoneButtonToKeyboard(myAction:  #selector(txtAccountName.resignFirstResponder))
        
        if transferView == "MainAccounts"
        {
            img_back.image = UIImage(named: "ic_cancel")?.maskWithColor(color: UIColor.black)
            //img_back1.isHidden = true
        }
        else
        {
           // img_back1.isHidden = false
            img_back.isHidden = true
        }
        
        if passingView == "Edit"
        {
            selectedDate1 =  Date().convertFormatOfDateToStringNoFormat(date: editData["date"] as! String)
            btnSelectDate.setTitle(String.convertFormatOfDate(date: selectedDate1), for: .normal)
            
            txtBalance.text = String(editData["amount"] as! Double)
            txtAccountName.text = editData["name"] as? String
            
            HomeActive = editData["isIncludeOnDashboard"] as! Bool
            DefultAmount = editData["isDefaultAccount"] as! Bool
            
            if (HomeActive == false)
            {
                btnHomeAdd.setBackgroundImage(#imageLiteral(resourceName: "ic_uncheck"), for: UIControlState.normal)
            }
            else
            {
                btnHomeAdd.setBackgroundImage(#imageLiteral(resourceName: "ic_check"), for: UIControlState.normal)
            }
            if (DefultAmount == false)
            {
                btnSetDefault.setBackgroundImage(#imageLiteral(resourceName: "ic_uncheck"), for: UIControlState.normal)
            }
            else
            {
                btnSetDefault.setBackgroundImage(#imageLiteral(resourceName: "ic_check"), for: UIControlState.normal)
            }
            btnSave.setTitle("Update", for: .normal)
        }
        else
        {
            selectedDate1 =  passingDate!
            btnSelectDate.setTitle(String.convertFormatOfDate(date: passingDate!), for: .normal)
            btnHomeAdd.setBackgroundImage(#imageLiteral(resourceName: "ic_check"), for: UIControlState.normal)
            btnSave.setTitle("Save", for: .normal)
        }
        
        datePickerView.delegate = self
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        headerView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        lblHeaderTitle.font = headerTitleFont
        lblHeaderTitle.textColor = UIColor.white
        
   //     BottomHeightConstraint.constant = 0
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    override func viewWillLayoutSubviews() {
        btnSave.viewShadow()
//        calView.viewShadow()
//        AccountView.viewShadow()
//        BalanceView.viewShadow()
    }

    //MARK:- Helper Method
    
    func setViewShadow()
    {
        AccountView.layer.masksToBounds = false
        AccountView.layer.cornerRadius = 0
        AccountView.layer.shadowColor = UIColor.lightGray.cgColor
        AccountView.layer.shadowOpacity = 0.6
        AccountView.layer.shadowOffset = CGSize(width: -1, height: 1)
        AccountView.layer.shadowRadius = 1.5
        
        calView.layer.masksToBounds = false
        calView.layer.cornerRadius = 0
        calView.layer.shadowColor = UIColor.lightGray.cgColor
        calView.layer.shadowOpacity = 0.6
        calView.layer.shadowOffset = CGSize(width: -1, height: 1)
        calView.layer.shadowRadius = 1.5
        
        BalanceView.layer.masksToBounds = false
        BalanceView.layer.cornerRadius = 0
        BalanceView.layer.shadowColor = UIColor.lightGray.cgColor
        BalanceView.layer.shadowOpacity = 0.6
        BalanceView.layer.shadowOffset = CGSize(width: -1, height: 1)
        BalanceView.layer.shadowRadius = 1.5
    }
    
    func setFont()
    {
//        lblHeader.font = TopHeaderFont
        lblHeaderTitle.font = headerTitleFont
        lblDateTitle.font = tableHeaderFont
        lblBalanceTitle.font = tableHeaderFont
        lblAccountNameTitle.font = tableHeaderFont
        txtAccountName.font = tableHeaderFont
        txtBalance.font = tableHeaderFont
        btnSave.titleLabel?.font = tableHeaderFont
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
    }

    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    //MARK: - JBDatePicker Delegate
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            
            btnSelectDate.setTitle(stringDate, for: .normal)
            selectedDate1 = selectedDate
            removeAnimate()
        }
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return passingDate//Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
    }
    
    //MARK:- Action Method
    
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        if transferView == "MainAccounts"
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnFirstLabelTap(_ sender: UIButton) {
        
        if (HomeActive == false)
        {
            HomeActive = true;
            
            btnHomeAdd.setBackgroundImage(#imageLiteral(resourceName: "ic_check"), for: UIControlState.normal)
        }
        else
        {
            HomeActive = false;
            btnHomeAdd.setBackgroundImage(#imageLiteral(resourceName: "ic_uncheck"), for: UIControlState.normal)
        }
    }
    @IBAction func btnDefultTap(_ sender: UIButton) {
        
        if (DefultAmount == false)
        {
            DefultAmount = true;
            btnSetDefault.setBackgroundImage(#imageLiteral(resourceName: "ic_check"), for: UIControlState.normal)
            
        }
        else
        {
            DefultAmount = false;
            btnSetDefault.setBackgroundImage(#imageLiteral(resourceName: "ic_uncheck"), for: UIControlState.normal)
        }
        
        
    }
    @IBAction func btnSaveAccountAction(_ sender: UIButton) {
        if txtAccountName.text != ""
        {
            if txtBalance.text != ""
            {
                
                if passingView == "Edit"
                {
                    let workouts = uiRealm.objects(Account.self).filter("id = %@", (editData["id"] as! Int))
                    
                    if Double(txtBalance.text!)!  >= 0
                    {
                        
                        if let workout = workouts.first
                        {
                            try! uiRealm.write
                            {
                                workout.amount = Double(txtBalance.text!)!
                                workout.remainigAmount = Double(txtBalance.text!)! - (Double(editData["amount"] as! Double) - Double(editData["remainigAmount"] as! Double))
                                workout.date = selectedDate1
                                workout.isIncludeOnDashboard = HomeActive
                                workout.isDefaultAccount = DefultAmount
                                workout.name = txtAccountName.text!
                            }
                            
                            if DefultAmount
                            {
                                if editData["id"] as! Int != UserDefaults.standard.integer(forKey: "AccountID")
                                {
                                    let workoutsEdit = uiRealm.objects(Account.self).filter("id = %@", (UserDefaults.standard.integer(forKey: "AccountID")))
                                    UserDefaults.standard.set(editData["id"] as! Int, forKey: "AccountID")
                                    if let workoutsEdit = workoutsEdit.first
                                    {
                                        try! uiRealm.write
                                        {
                                            workoutsEdit.isDefaultAccount = false
                                        }
                                    }
                                }
                            }
                            
                            if transferView == "MainAccounts"
                            {
                                self.dismiss(animated: true, completion: nil)
                            }
                            else
                            {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                    var taskListB = Account()
                    let autoCatId = uiRealm.objects(Account.self).max(ofProperty: "id") as Int?
                    
                    if(autoCatId == nil)
                    {
                        
                        taskListB = Account(value: [1,txtAccountName.text!,Double(txtBalance.text!)!,Double(txtBalance.text!)!,selectedDate1,HomeActive,DefultAmount])
                    }
                    else
                    {
                        let catId = (uiRealm.objects(Account.self).max(ofProperty: "id") as Int?)! + 1
                        taskListB = Account(value: [catId,txtAccountName.text!,Double(txtBalance.text!)!,Double(txtBalance.text!)!,selectedDate1,HomeActive,DefultAmount])
                    }
                    
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                    
                    if DefultAmount
                    {
                        let workouts = uiRealm.objects(Account.self).filter("id = %@", (UserDefaults.standard.integer(forKey: "AccountID")))
                        let catId = (uiRealm.objects(Account.self).max(ofProperty: "id") as Int?)!
                        UserDefaults.standard.set(catId, forKey: "AccountID")
                        if let workout = workouts.first
                        {
                            try! uiRealm.write
                            {
                                workout.isDefaultAccount = false
                            }
                        }
                    }
                    
                    if transferView == "MainAccounts"
                    {
                        self.dismiss(animated: true, completion: nil)
                    }
                    else
                    {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            else
            {
                alertView(alertMessage: AlertString.AddAmount)
            }
        }
        else
        {
            alertView(alertMessage: AlertString.AccountName)
        }
    }
    
    //MARK:- Textfield Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        print(string)
        
        
        if(textField == txtBalance)
        {
            let newText = (txtBalance.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.characters.count
            
            if numberOfChars <= 10
            {
                if((txtBalance.text?.characters.count)! == 1)
                {
                    if(txtBalance.text == "0")
                    {
                        if(string == "0")
                        {
                            return false
                        }
                        else if(string == "")
                        {
                            return false
                        }
                        else
                        {
                            txtBalance.text = ""
                            //                        txtAmount.text = string
                            return true
                        }
                    }
                    else
                    {
                        if(string == "")
                        {
                            txtBalance.text = "0"
                            return false
                        }
                        return true
                    }
                }
                else
                {
                    let newCharacters = CharacterSet(charactersIn: string)
                    let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacters)
                    if boolIsNumber == true {
                        return true
                    } else {
                        if string == "." {
                            let countdots = txtBalance.text!.components(separatedBy: ".").count - 1
                            let array = txtBalance.text?.characters.map { String($0) }
                            var decimalCount = 0
                            for character in array! {
                                if character == "." {
                                    decimalCount += 1
                                }
                            }
                            
                            if countdots == 0 {
                                return true
                            } else {
                                if countdots > 0 && string == "." {
                                    return false
                                } else {
                                    if decimalCount == 2 {
                                        return true
                                    } else {
                                        return false
                                    }
                                    //                                return true
                                }
                            }
                        } else {
                            return false
                        }
                    }
                }
            }
            else
            {
                return false
            }
            
            
        }
        else
        {
            return true
        }
        
        
        
    }
    
}
