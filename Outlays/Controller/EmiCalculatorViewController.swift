//
//  EmiCalculatorViewController.swift
//  Outlays
//
//  Created by Nectarbits on 12/09/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class EmiCalculatorViewController: UIViewController, UITextFieldDelegate, JBDatePickerViewDelegate,GADBannerViewDelegate {
   
    @IBOutlet var GradientView :  UIView!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var scrvEmi : UIScrollView!
    @IBOutlet var ContentView :  UIView!
    @IBOutlet var lblPrincipalAmount : UILabel!
    @IBOutlet var txtPrincipalAmount : UITextField!
    @IBOutlet var lblDownPayment : UILabel!
    @IBOutlet var txtDownPayment : UITextField!
    @IBOutlet var lblInterest : UILabel!
    @IBOutlet var txtInterest : UITextField!
    @IBOutlet var lblLoan : UILabel!
    @IBOutlet var txtLoan : UITextField!
    @IBOutlet var txtmonth : UITextField!
    @IBOutlet var btnYear : UIButton!
    @IBOutlet var btnEmiDate : UIButton!
    @IBOutlet var btnCalculate: UIButton!
    @IBOutlet var headerView : UIView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet var principalView :  UIView!
    @IBOutlet var downView :  UIView!
    @IBOutlet var interestView :  UIView!
    @IBOutlet var loanView :  UIView!
//    @IBOutlet var startDateView :  UIView!
    @IBOutlet var btnFinalcalculate :  UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var ScrlBgView: UIView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnCompare : UIButton!
    @IBOutlet weak var btnReset : UIButton!


    var xyz : Bool = false
    var passingType = ""
    var transactionDict = NSMutableDictionary()
    var selectedDate1 = Date()
    var currentDate = Date()
    var dateToSelect: Date!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        currentDate = Date()
        
        selectedDate1 =  Date()
        
        self.calendarView.isHidden = true
        self.bgView.isHidden = true

        txtPrincipalAmount.delegate = self
        txtDownPayment.delegate = self
        txtInterest.delegate = self
        txtLoan.delegate = self
        datePickerView.delegate = self
        
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription

        txtPrincipalAmount.placeholder = "Enter Principal Amount"
        txtDownPayment.placeholder = "Enter Down Payment"
        txtInterest.placeholder = "Enter Your Intrest"
        txtLoan.placeholder = "Years"
        
//        lblHeaderTitle.font = headerTitleFont
        txtPrincipalAmount.addDoneButtonToKeyboard(myAction:  #selector( txtPrincipalAmount.resignFirstResponder))
        txtDownPayment.addDoneButtonToKeyboard(myAction:  #selector( txtDownPayment.resignFirstResponder))
        txtInterest.addDoneButtonToKeyboard(myAction:  #selector(txtInterest.resignFirstResponder))
        txtLoan.addDoneButtonToKeyboard(myAction:  #selector( txtLoan.resignFirstResponder))
        NotificationCenter.default.addObserver(self, selector: #selector(EmiCalculatorViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EmiCalculatorViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
        
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        let Image = UIImage(named: "circular-arrow-in-rounded-button 2")?.withRenderingMode(.alwaysTemplate)
        btnReset.setImage(Image, for: .normal)
        btnReset.tintColor = UIColor.white
        
        let Image1 = UIImage(named: "ic_comparison")?.withRenderingMode(.alwaysTemplate)
        btnCompare.setImage(Image1, for: .normal)
        btnCompare.tintColor = UIColor.white
        
        let Image3 = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        btnMenu.setImage(Image3, for: .normal)
        btnMenu.tintColor = UIColor.white
        
        let Image4 = UIImage(named: "ic_calender")?.withRenderingMode(.alwaysTemplate)
        btnEmiDate.setImage(Image4, for: .normal)
        btnEmiDate.tintColor = SecondColor
        
        GradientView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
//        BottomHeightConstraint.constant = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    
    override func viewWillDisappear(_ animated: Bool) {
        print("End")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        principalView.viewShadow()
//        downView.viewShadow()
//        interestView.viewShadow()
//        loanView.viewShadow()
//        startDateView.viewShadow()
        btnFinalcalculate.viewShadow()
//        btnEmiDate.viewShadow()
        ScrlBgView.viewShadow()
        btnYear.viewShadow()
        
    }

    
//MARK: - Helper Method
    @objc func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if var keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardSize = self.view.convert(keyboardSize, from: nil)
            
            var contentInset:UIEdgeInsets = self.scrvEmi.contentInset
            contentInset.bottom = keyboardSize.size.height
            self.scrvEmi.contentInset = contentInset
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            
            let contentInset:UIEdgeInsets = .zero
            self.scrvEmi.contentInset = contentInset
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
        
    }
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
//MARK: - JBDatePicker Delegate
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            
            btnEmiDate.setTitle(stringDate!, for: .normal)
            
            removeAnimate()
        }
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
    
//MARK: - Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        print(string)
        
        
        if(textField == txtLoan)
        {
            let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.characters.count
            
            var number : Int = 0
            if btnYear.titleLabel?.text == "YEARS"
            {
                number = 2
            }
            else
            {
                number = 4
            }
            
            if numberOfChars <= number
            {
                if((textField.text?.characters.count)! == 1)
                {
                    if(textField.text == "0")
                    {
                        if(string == "0")
                        {
                            return false
                        }
                        else if(string == "")
                        {
                            return false
                        }
                        else
                        {
                            textField.text = ""
                            //                        txtAmount.text = string
                            return true
                        }
                    }
                    else
                    {
                        if(string == "")
                        {
                            textField.text = "0"
                            return false
                        }
                        return true
                    }
                }
                else
                {
                    let newCharacters = CharacterSet(charactersIn: string)
                    let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacters)
                    if boolIsNumber == true {
                        return true
                    } else {
                        if string == "." {
                            let countdots = textField.text!.components(separatedBy: ".").count - 1
                            let array = textField.text?.characters.map { String($0) }
                            var decimalCount = 0
                            for character in array! {
                                if character == "." {
                                    decimalCount += 1
                                }
                            }
                            
                            if countdots == 0 {
                                return true
                            } else {
                                if countdots > 0 && string == "." {
                                    return false
                                } else {
                                    if decimalCount == 2 {
                                        return true
                                    } else {
                                        return false
                                    }
                                    //                                return true
                                }
                            }
                        } else {
                            return false
                        }
                    }
                }
            }
            else
            {
                return false
            }
            
            
        }
        else
        {
            let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.characters.count
            
            if numberOfChars <= 15
            {
                if((textField.text?.characters.count)! == 1)
                {
                    if(textField.text == "0")
                    {
                        if(string == "0")
                        {
                            return false
                        }
                        else if(string == "")
                        {
                            return false
                        }
                        else
                        {
                            textField.text = ""
                            //                        txtAmount.text = string
                            return true
                        }
                    }
                    else
                    {
                        if(string == "")
                        {
                            textField.text = "0"
                            return false
                        }
                        return true
                    }
                }
                else
                {
                    let newCharacters = CharacterSet(charactersIn: string)
                    let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacters)
                    if boolIsNumber == true {
                        return true
                    } else {
                        if string == "." {
                            let countdots = textField.text!.components(separatedBy: ".").count - 1
                            let array = textField.text?.characters.map { String($0) }
                            var decimalCount = 0
                            for character in array! {
                                if character == "." {
                                    decimalCount += 1
                                }
                            }
                            
                            if countdots == 0 {
                                return true
                            } else {
                                if countdots > 0 && string == "." {
                                    return false
                                } else {
                                    if decimalCount == 2 {
                                        return true
                                    } else {
                                        return false
                                    }
                                    //                                return true
                                }
                            }
                        } else {
                            return false
                        }
                    }
                }
            }
            else
            {
                return false
            }

        }
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        BottomHeightConstraint.constant = 0
    }

    
// MARK:- Action Method
    @IBAction func EmiStatisticView(_ sender: UIButton)
    {
        if(txtPrincipalAmount.text == "0" || txtPrincipalAmount.text == "")
        {
            self.alertView(alertMessage: AlertString.PrincipalAmount)
        }
        else if(txtDownPayment.text == "")
        {
            self.alertView(alertMessage: AlertString.EnterDownPayment)
        }
        else if(txtInterest.text == "0" || txtInterest.text == "")
        {
            self.alertView(alertMessage: AlertString.EnterInterest)
        }
        else if(txtLoan.text == "0" || txtLoan.text == "")
        {
            self.alertView(alertMessage: AlertString.EnterYear)
        }
        else if(btnEmiDate.titleLabel?.text == "Emi Start Date")
        {
            self.alertView(alertMessage: AlertString.SelectDate)
        }
        else
        {
            let dict = NSMutableDictionary()
            
            dict.setValue(txtPrincipalAmount.text, forKey: "PrincipalAmount")
            dict.setValue(txtDownPayment.text, forKey: "DpAmount")
            dict.setValue(txtInterest.text, forKey: "InterestAmount")
            if xyz == false
            {
                dict.setValue(txtLoan.text, forKey: "LoanTime")
            }
            else
            {
                dict.setValue(String(format: "%.1f",(Double(txtLoan.text!)! / 12)), forKey: "LoanTime")
            }
            dict.setValue(btnEmiDate.titleLabel?.text, forKey: "EmiDate")
            
            if Double(dict.value(forKey: "DpAmount") as! String)! > Double(dict.value(forKey: "PrincipalAmount") as! String)!
            {
                self.alertView(alertMessage: AlertString.PrincipalHigh)
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmiStatisticsViewController") as! EmiStatisticsViewController
                vc.detailValue = dict
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    @IBAction func closeView(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }

    @IBAction func btnYearTap(_ sender: UIButton) {
        
        if xyz == false
        {
            btnYear.setTitle("MONTH", for: .normal)
            if txtLoan.text != ""
            {
                txtLoan.text = String(Int(Double(txtLoan.text!)! * 12))
            }
            else
            {
                txtLoan.placeholder = "Month"
            }
            xyz = true
        }
        else
        {
            btnYear.setTitle("YEARS", for: .normal)
            
            if txtLoan.text != ""
            {
                txtLoan.text = String(format: "%.1f",(Double(txtLoan.text!)! / 12))
            }
            else
            {
                txtLoan.placeholder = "Year"
            }
            
            xyz = false
        }
    }
    @IBAction func btnCompareAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CompareLoansVC") as! CompareLoansVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnResetTap(_ sender: UIButton) {
        
        txtPrincipalAmount.text = ""
        txtDownPayment.text = ""
        txtInterest.text = ""
        txtLoan.text = ""
        btnYear.setTitle("YEARS", for: .normal)
    }
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }

}
