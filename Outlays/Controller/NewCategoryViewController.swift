//
//  NewCategoryViewController.swift
//  Created by harshesh on 28/10/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI


class NewCategoryViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet var HeaderView: UIView!
    @IBOutlet var clevCategory: UICollectionView!
    //  @IBOutlet var categorySearchBar: UISearchBar!
    @IBOutlet var btncancel: UIButton!
    @IBOutlet var btnPushBack: UIButton!
//    @IBOutlet var categoryView: UIView!
    @IBOutlet var txtCategoryName: UITextField!
    @IBOutlet var img_selectedCategory: UIImageView!
    @IBOutlet var lblHeaderAddCategory: UILabel!
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var clevAddCategory: UICollectionView!
    @IBOutlet var img_pushBack: UIImageView!
    @IBOutlet var addTransactionView: UIView!
    @IBOutlet var txtAmmount: UITextField!
    @IBOutlet var txtSelectedAccountName: UITextField!
    @IBOutlet var lblSelectedCategoryName: UILabel!
    @IBOutlet weak var BgdatePickerView: UIView!
    @IBOutlet var txtDate : UITextField!
    @IBOutlet var txtReminder : UITextField!
    @IBOutlet var imgCategory: UIImageView!
    @IBOutlet var txtAddNote: UITextField!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnAddTransactionDone: UIButton!
    @IBOutlet var addTransactionHeader: UIView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var CategoryHeaderView: UIView!
    @IBOutlet var btnSplit: UIButton!

    var categoryArray : NSMutableArray = []
    var subcategoryArray : NSMutableArray = []
    var viewMode = ""
    var strCategoryID = ""
    var info = NSMutableDictionary()
    var categoryType : String = ""
    var sections = [Section]()
    var items = NSMutableArray()
    var filterSubCat = NSMutableArray()
    var searchActive: Bool = false
    var isEdited : Bool = false
    var isMainCategory : String = "Main"
    var img_icon : String = ""
    var editDictionary = NSMutableDictionary()
    var passingView : String = ""
    var mainCategoryName = ""
    var mainCategoryImage = ""
    var categoryImageArray = NSMutableArray()
    //    var passingType = ""
    var accountArray = [String]()
    var listAccountArray = [[String:AnyObject]]()
    var transactionDict = NSMutableDictionary()
    var dateToSelect: Date!
    var viewDatePicker : UIView!
    var viewReminderPicker : UIView!
    let donePicker = UIButton()
    let cancelPicker = UIButton()
    let doneReminderPicker = UIButton()
    let cancelReminderPicker = UIButton()
    var strPickerValue = ""
    var strReminderValue = ""
    var isTap = ""
    var reminderArray = [String]()
    var pickerHeight = CGFloat()
    var dict = NSMutableDictionary()
    var strSelectedCategory : String = ""
    var passingDate : Date!
    var selectedDate1 = Date()
    var currentDate = Date()
    var selectedAccountID : Int = 0
    var accountID = ""
    var strCategoryType = ""
    var addToListArray = NSMutableArray()
    var isPickerTap = Bool()
    let accountPicker = UIPickerView()
    let reminderPicker = UIPickerView()

    //MARK:- Views Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        //txtAmmount.inputAccessoryView = transactionInputView
        currentDate = Date()
        selectedDate1 =  Date()
        getAccountData()
        setUP()
        reminderArray = ["Never", "Daily", "Weekly", "Monthly"]
        strSelectedCategory = ""
        categoryImageArray = (Constant.categoryImage as! NSArray).mutableCopy() as! NSMutableArray
        BgdatePickerView.isHidden = true
        addTransactionView.isHidden = true
        clevCategory.layer.shadowColor = UIColor.lightGray.cgColor
        clevCategory.layer.shadowOpacity = 0.6
        clevCategory.layer.shadowOffset = CGSize(width: 1, height: 1)
        clevCategory.layer.shadowRadius = 1
        clevCategory.layer.cornerRadius = 4
        
        if passingView == "setting"
        {
            btnPushBack.isHidden = false
            img_pushBack.isHidden = false
            btncancel.isHidden = true
            
            categoryType = "1"
            self.getCategoryData(categoryType: categoryType)
        }
        else if passingView == "split"
        {
            btncancel.isHidden = false
            btnPushBack.isHidden = true
            img_pushBack.isHidden = true
            
            categoryType = "0"
            self.getCategoryData(categoryType: categoryType)
        }
        else if passingView == "edit"{
            btnDelete.isHidden = false
            showAddTransactionAnimate()
            btnAddTransactionDone.setTitle("Update", for: .normal)
            if(transactionDict["subCategoryID"] as! String == "0")
            {
                //                txtCategoryName.text = transactionDict["categoryName"] as! String
                lblSelectedCategoryName.text = (transactionDict["categoryName"] as! String)
                strSelectedCategory = transactionDict["categoryName"] as! String
//                imgCategory.image = UIImage(named: transactionDict["categoryImage"] as! String)
                imgCategory.image = UIImage(named: transactionDict["categoryImage"] as! String)!.withRenderingMode(.alwaysTemplate)
                imgCategory.tintColor = UIColor.white
                txtAmmount.text = (transactionDict["amount"] as! String)
                txtAddNote.text = (transactionDict["note"] as! String)
            }
            else
            {
                lblSelectedCategoryName.text = (transactionDict["categoryName"] as! String) //"\(transactionDict["categoryName"] as! String)/\(transactionDict["subCatName"] as! String)"
                strSelectedCategory = transactionDict["categoryName"] as! String//"\(transactionDict["categoryName"] as! String)/\(transactionDict["subCatName"] as! String)"
//                imgCategory.image = UIImage(named: transactionDict["categoryImage"] as! String)
                imgCategory.image = UIImage(named: transactionDict["categoryImage"] as! String)!.withRenderingMode(.alwaysTemplate)
                imgCategory.tintColor = UIColor.white
                txtAmmount.text = (transactionDict["amount"] as! String)
                txtAddNote.text = (transactionDict["note"] as! String)
            }
            
            txtDate.text = transactionDict["date"] as? String
            passingDate = String.convertFormatOfStringToDate(date: transactionDict["date"] as! String)
            
            if transactionDict["note"] as! String != ""
            {
                //                txtAddNote.text = transactionDict["note"] as! String
            }
            
            dict = NSMutableDictionary()
            
            dict.setValue(transactionDict["categoryID"] as! String, forKey: "categoryID")
            dict.setValue(transactionDict["setType"] as! String, forKey: "categoryType")
            dict.setValue(transactionDict["subCatImage"] as! String, forKey: "image")
            dict.setValue(transactionDict["categoryImage"] as! String, forKey: "categoryimage")
            dict.setValue(transactionDict["subCatName"] as! String, forKey: "name")
            dict.setValue(transactionDict["categoryName"] as! String, forKey: "categoryName")
            dict.setValue(transactionDict["subCategoryID"] as! String, forKey: "subCategoryID")
            
            var reminderType = String()
            selectedAccountID = transactionDict["accountId"] as! Int
            reminderType = transactionDict["repeatType"] as! String
            
            if(reminderType == "0")
            {
                self.txtReminder.text = "Never"
            }
            else if(reminderType == "1")
            {
                self.txtReminder.text = "Daily"
            }
            else if(reminderType == "2")
            {
                self.txtReminder.text = "Weekly"
            }
            else if(reminderType == "3")
            {
                self.txtReminder.text = "Monthly"
            }
        }
        else
        {
            btnAddTransactionDone.setTitle("Done", for: .normal)
            txtDate.text = String.convertFormatOfDate(date: currentDate)
            passingDate = currentDate
            btncancel.isHidden = false
            btnPushBack.isHidden = true
            img_pushBack.isHidden = true
            if strCategoryID == "0"
            {
                self.getCategoryData(categoryType: categoryType)
            }
            else
            {
                self.getCategoryDataWithID(categoryType: categoryType)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewCategoryViewController.showSpinningWheel(notification:)), name: Notification.Name(rawValue: "fetchCategoryData"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != addTransactionView && touch.view != view {
            IQKeyboardManager.sharedManager().enable = false
        }
    }
    
    func setUP(){
        HeaderView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        addTransactionHeader.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))

        btnAddTransactionDone.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))

        CategoryHeaderView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))

        btnOk.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        btnDelete.isHidden = true
        bgView.isHidden = true
        txtAddNote.isUserInteractionEnabled = false
        
        if strCategoryType == "Expense" {
            
            btnSplit.isHidden = false
        }else{
            
            btnSplit.isHidden = true
        }
        if passingView == "Edit"
        {
            for i in 0..<listAccountArray.count
            {
                if listAccountArray[i]["id"] as? Int == Int(transactionDict["accountId"] as! String)
                {
                    txtSelectedAccountName.text = accountArray[i]
                    selectedAccountID = listAccountArray[i]["id"] as! Int
                }
            }
        }
        else
        {
            for i in 0..<listAccountArray.count
            {
                if listAccountArray[i]["id"] as? Int == UserDefaults.standard.integer(forKey: "AccountID")
                {
                    txtSelectedAccountName.text = accountArray[i]
                    selectedAccountID = listAccountArray[i]["id"] as! Int
                }
            }
        }
    }
    
    //MARK:- Select Account Picker
//    func setupDatePicker() {
//
//        viewDatePicker = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-pickerHeight, width: self.view.frame.size.width, height: pickerHeight))
//        viewDatePicker.backgroundColor = UIColor.white
//        self.view.addSubview(viewDatePicker)
//
//        self.AccountPicker = UIPickerView(frame:CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: pickerHeight - 10))
//        self.AccountPicker.delegate = self
//        self.AccountPicker.dataSource = self
//        self.AccountPicker.backgroundColor = UIColor.white
//        viewDatePicker.addSubview(AccountPicker)
//
//        //done.buttonType = custom
//        donePicker.frame = CGRect(x: self.view.frame.width - 70, y: 0, width: 60, height: 50)
//        donePicker.setTitle("Done", for: .normal)
//        donePicker.setTitleColor(UIColor.black, for: .normal)
//        donePicker.addTarget(self, action: #selector(self.btnAccountPickerDone(_:)), for: .touchUpInside)
//        viewDatePicker.addSubview(donePicker)
//
//        cancelPicker.frame = CGRect(x: 10, y: 5, width: 60, height: 40)
//        cancelPicker.setTitle("Cancel", for: .normal)
//        cancelPicker.setTitleColor(UIColor.black, for: .normal)
//        cancelPicker.addTarget(self, action: #selector(self.btnAccountPickerCancel(_:)), for: .touchUpInside)
//        viewDatePicker.addSubview(cancelPicker)
//
////        viewDatePicker.isHidden = true
//    }
    
//    func hidePickerView(){
//        viewDatePicker.isHidden = true
//    }
//
//    func showPickerView() {
//        txtAmmount.resignFirstResponder()
//        txtAddNote.resignFirstResponder()
//        viewDatePicker.isHidden = false
//    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    func handleTap(sender: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    //MARK:- Helper Method
    func getCategoryData(categoryType : String)
    {
        categoryArray = []
        
        let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@","1",categoryType)
        
        if viewMode == "edit"
        {
            for i in 0..<categoryList.count
            {
                let dict = NSMutableDictionary()
                dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
                dict.setValue(categoryList[i]["name"]!, forKey: "name")
                dict.setValue(categoryList[i]["image"]!, forKey: "image")
                dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
                dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
                dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
                dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
                dict.setValue(false, forKey: "isCell")
                
                if (info["categoryID"] as? String)! == categoryList[i]["categoryID"] as! String
                {
                    dict.setValue(true, forKey: "isCell")
                    strCategoryID = categoryList[i]["categoryID"] as! String
                }
                else
                {
                    dict.setValue(false, forKey: "isCell")
                }
                categoryArray.add(dict)
            }
        }
        else
        {
            for i in 0..<categoryList.count
            {
                let dict = NSMutableDictionary()
                dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
                dict.setValue(categoryList[i]["name"]!, forKey: "name")
                dict.setValue(categoryList[i]["image"]!, forKey: "image")
                dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
                dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
                dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
                dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
                
                if i == 0
                {
                    dict.setValue(true, forKey: "isCell")
                    categoryArray.add(dict)
                    
                }
                else
                {
                    dict.setValue(false, forKey: "isCell")
                    categoryArray.add(dict)
                }
                
                
            }
        }
        print(categoryArray)
        clevCategory.reloadData()
        firstTimeCategoryView()
    }
    func getCategoryDataWithID(categoryType : String)
    {
        categoryArray = []
        let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND categoryID = %@","1",categoryType,strCategoryID)
        let mutableArray = NSMutableArray()
        sections = [Section]()
        
        for i in 0..<categoryList.count
        {
            let dict = NSMutableDictionary()
            dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
            dict.setValue(categoryList[i]["name"]!, forKey: "name")
            dict.setValue(categoryList[i]["image"]!, forKey: "image")
            dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
            dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
            dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
            dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
            
            if i == 0
            {
                dict.setValue(false, forKey: "isCell")
                categoryArray.add(dict)
            }
            else
            {
                dict.setValue(false, forKey: "isCell")
                categoryArray.add(dict)
            }
            
            
        }
        print(categoryArray)
        clevCategory.reloadData()
        firstTimeCategoryView()
    }
    
    func firstTimeCategoryView()
    {
        strCategoryID = (categoryArray[0] as! NSDictionary).value(forKey:"categoryID") as! String
        print(strCategoryID)
        
        mainCategoryName = (categoryArray[0] as! NSDictionary).value(forKey:"name") as! String
        mainCategoryImage = (categoryArray[0] as! NSDictionary).value(forKey:"image") as! String
        
        let subcategoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND categoryID = %@","0",categoryType,(self.categoryArray[0] as! NSDictionary).value(forKey: "categoryID") as! String)
        subcategoryArray = []
        for i in 0..<subcategoryList.count
        {
            let dict = NSMutableDictionary()
            dict.setValue(subcategoryList[i]["id"] as! Int, forKey: "id")
            dict.setValue(subcategoryList[i]["name"]!, forKey: "name")
            dict.setValue(subcategoryList[i]["image"]!, forKey: "image")
            dict.setValue(subcategoryList[i]["categoryID"]!, forKey: "categoryID")
            dict.setValue(subcategoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
            dict.setValue(subcategoryList[i]["categoryType"]!, forKey: "categoryType")
            dict.setValue(subcategoryList[i]["isCategory"]!, forKey: "isCategory")
            subcategoryArray.add(dict)
        }
        clevCategory.reloadData()
    }
    
    func getAccountData()
    {
        let puppies = uiRealm.objects(Account.self)
        listAccountArray = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["id"] = puppies[j]["id"]! as AnyObject
            dict1["name"] = puppies[j]["name"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["remainigAmount"] = puppies[j]["remainigAmount"]! as AnyObject
            dict1["date"] = String.convertFormatOfDate(date: puppies[j]["date"]! as! Date) as AnyObject
            dict1["isIncludeOnDashboard"] = puppies[j]["isIncludeOnDashboard"]! as AnyObject
            dict1["isDefaultAccount"] = puppies[j]["isDefaultAccount"]! as AnyObject
            accountArray.append(dict1["name"] as! String)
            listAccountArray.append(dict1)
        }
    }
    
    //MARK:- Animation View
    
    func showAnimate()
    {
        txtCategoryName.text = ""
        img_selectedCategory.image = UIImage(named: "ic_Add_blue.png")
        bgView.isHidden = false
    }
    
    func removeAnimate()
    {
        self.bgView.isHidden = true
    }
    
    func showAddTransactionAnimate()
    {
        addTransactionView.isHidden = false
//        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        txtAmmount.becomeFirstResponder()
    }
    
    func removeAddTransactionAnimate()
    {
//        IQKeyboardManager.sharedManager().enable = false
        txtAmmount.resignFirstResponder()
        self.reminderPicker.isHidden = true
        self.accountPicker.isHidden = true
        self.addTransactionView.isHidden = true
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        
        if viewMode == "edit"
        {
            self.alertView(alertMessage: AlertString.NotChangeCategory)
        }
        else
        {
            for i in 0...categoryArray.count-1 {
                
                if sender.tag == i {
                    
                    
                    (categoryArray[i] as! NSDictionary).setValue(true, forKey: "isCell")
                    strCategoryID = (categoryArray[i] as! NSDictionary).value(forKey:"categoryID") as! String
                    print(strCategoryID)
                }
                else {
                    
                    (categoryArray[i] as! NSDictionary).setValue(false, forKey: "isCell")
                }
                
                
                print(i)
            }
            
            clevCategory.reloadData()
        }
    }
    
    // MARK:- CollectionView Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == clevCategory
        {
            return categoryArray.count
        }
        else
        {
            return categoryImageArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == clevCategory
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCategory", for: indexPath) as! CategoryCollectionViewCell
            cell.categoryImage.image = UIImage(named: (self.categoryArray[indexPath.row] as! NSDictionary).value(forKey: "image") as! String)
            cell.btnSelect.tag = indexPath.row
            cell.btnSelect.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
            
            cell.lblCatName.text =  (self.categoryArray[indexPath.row] as! NSDictionary).value(forKey: "name") as? String
            
            let isCellSelected : Bool = (self.categoryArray[indexPath.row] as! NSDictionary).value(forKey: "isCell") as! Bool
            //            clevCategory.selectItem(at: isCellSelected, animated: false, scrollPosition: .left)
            print(cell.lblCatName.frame)
            if isCellSelected {
                // cell.bgView.backgroundColor = UIColor.blue
                //                print(cell.imgvBudget.frame)
                //                cell.imgvBudget.layer.masksToBounds = true
                //                cell.imgvBudget.layer.cornerRadius = 25
                //                cell.imgvBudget.layer.borderWidth = 2
                //                cell.imgvBudget.layer.borderColor = UIColor.blue.cgColor
            }
            else {
                //cell.bgView.backgroundColor = UIColor.clear
                //                cell.imgvBudget.layer.masksToBounds = true
                //                cell.imgvBudget.layer.cornerRadius = 0
                //                cell.imgvBudget.layer.borderWidth = 0
                //                cell.imgvBudget.layer.borderColor = UIColor.clear.cgColor
            }
            
            cell.categoryImage.setImageColor(color: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1))
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryimagecell", for: indexPath) as! categoryImageCell
            
            cell.categoryImg.image = UIImage(named: ((Constant.categoryImage[indexPath.row] as NSDictionary).value(forKey: "name") as! String))
            cell.layer.cornerRadius = cell.frame.size.width / 2
            cell.layer.masksToBounds = true
            cell.layer.borderWidth = 0.3
            cell.layer.borderColor = UIColor.black.cgColor
            
            let isCellSelected : Bool = (categoryImageArray[indexPath.row] as! NSDictionary).value(forKey: "isCell") as! Bool
            if isCellSelected {
                // cell.bgView.backgroundColor = UIColor.blue
                cell.layer.borderColor = UIColor.blue.cgColor
            }
            else {
                //cell.bgView.backgroundColor = UIColor.clear
                cell.layer.borderColor = UIColor.black.cgColor
            }
            
            cell.categoryImg.setImageColor(color: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1))
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == clevCategory
        {
            for i in 0...categoryArray.count-1 {
                
                if indexPath.row == i {
                    
                    if passingView == "addTransaction"
                    {
                        let temp = categoryArray[indexPath.row] as! NSMutableDictionary
                        
                        mainCategoryName = (categoryArray[i] as! NSDictionary).value(forKey:"name") as! String
                        mainCategoryImage = (categoryArray[i] as! NSDictionary).value(forKey:"image") as! String
                        
                        temp.setValue(mainCategoryName, forKey: "categoryName")
                        temp.setValue(mainCategoryImage, forKey: "categoryimage")
                        if(categoryType == "1")
                        {
                            temp.setValue("1", forKey: "categoryType")
                        }
                        else{
                            temp.setValue("0", forKey: "categoryType")
                        }
                        
                        if("\(temp["categoryID"]!)" == BillCategoryID){
                           
                            if(UserDefaults.standard.value(forKey: "BillCategoryData") == nil){
                                UserDefaults.standard.set(temp, forKey: "BillCategoryData")
                            }else{
                                UserDefaults.standard.set(temp, forKey: "BillCategoryData")
                            }
                            
                            let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
                            self.navigationController?.pushViewController(VC, animated: true)
                            
                        }else if("\(temp["categoryID"]!)" == LoanCategoryID){
                            if(UserDefaults.standard.value(forKey: "LoanCategoryData") == nil){
                                UserDefaults.standard.set(temp, forKey: "LoanCategoryData")
                            }else{
                                UserDefaults.standard.set(temp, forKey: "LoanCategoryData")
                            }
                            
                            let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLoanVC") as! AddNewLoanVC
                            self.navigationController?.pushViewController(VC, animated: true)
                        }
                        else{
                            NotificationCenter.default.post(name: Notification.Name("fetchCategoryData"), object: temp)
                            showAddTransactionAnimate()
                            clevCategory.reloadData()
                        }
                    }
                    else{
                        let temp = categoryArray[indexPath.row] as! NSMutableDictionary
                        
                        mainCategoryName = (categoryArray[i] as! NSDictionary).value(forKey:"name") as! String
                        mainCategoryImage = (categoryArray[i] as! NSDictionary).value(forKey:"image") as! String
                        
                        temp.setValue(mainCategoryName, forKey: "categoryName")
                        temp.setValue(mainCategoryImage, forKey: "categoryimage")
                        if(categoryType == "1")
                        {
                            temp.setValue("1", forKey: "categoryType")
                        }
                        else{
                            temp.setValue("0", forKey: "categoryType")
                        }
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetchSplitCategoryData"), object: temp)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else {
                    
                    (categoryArray[i] as! NSDictionary).setValue(false, forKey: "isCell")
                }
                print(i)
            }
            
            clevCategory.reloadData()
        }
        else
        {
            img_selectedCategory.image = UIImage(named: ((categoryImageArray[indexPath.row] as! NSDictionary).value(forKey: "name") as! String))
            img_icon = ((categoryImageArray[indexPath.row] as! NSDictionary).value(forKey: "name") as! String)
            
            
            for i in 0...categoryImageArray.count-1 {
                
                if indexPath.row == i {
                    
                    let temp = categoryImageArray[i] as! NSDictionary
                    let temp1 = temp.mutableCopy() as! NSMutableDictionary
                    temp1.setValue(true, forKey: "isCell")
                    
                    
                    categoryImageArray.replaceObject(at: i, with: temp1)
                    
                    //                    (categoryImageArray[i] as! NSDictionary).setValue(true, forKey: "isCell")
                }
                else
                {
                    let temp = categoryImageArray[i] as! NSDictionary
                    let temp1 = temp.mutableCopy() as! NSMutableDictionary
                    temp1.setValue(false, forKey: "isCell")
                    
                    
                    categoryImageArray.replaceObject(at: i, with: temp1)
                    
                    //                    (categoryImageArray[i] as! NSDictionary).setValue(false, forKey: "isCell")
                }
                clevAddCategory.reloadData()
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView == clevCategory){
            return CGSize(width: CGFloat((clevCategory.frame.size.width / 3) - 20), height: CGFloat((clevCategory.frame.size.width / 3) - 20))
        }
        else{
            return CGSize(width: CGFloat((clevCategory.frame.size.width / 5) - 20), height: CGFloat((clevCategory.frame.size.width / 5) - 20))
        }
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        if(passingView == "split" || passingView == "addTransaction")
        {
            
            sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed! = !sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed!
        }
        else
        {
            if(isEdited == true)
            {
                let temp = sections[Int(gestureRecognizer.accessibilityHint!)!]
                print(temp)
                
                
                editDictionary = NSMutableDictionary()
                editDictionary.setValue(temp.id, forKey: "id")
                editDictionary.setValue(temp.categoryID, forKey: "categoryID")
                editDictionary.setValue(temp.categoryType, forKey: "categoryType")
                editDictionary.setValue(temp.image, forKey: "image")
                editDictionary.setValue(temp.isCategory, forKey: "isCategory")
                editDictionary.setValue(temp.name, forKey: "name")
                editDictionary.setValue(temp.subCategoryID, forKey: "subCategoryID")
                print(editDictionary)
                isMainCategory = "editMain"
                lblHeaderAddCategory.text = "Edit Main Category"
//                showAnimate()
                txtCategoryName.text = temp.name
                img_selectedCategory.image = UIImage(named: temp.image!)
                img_icon = temp.image!
            }
            else
            {
                sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed! = !sections[Int(gestureRecognizer.accessibilityHint!)!].collapsed!
            }
        }
    }
    
    //MARK:- Textfield Delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if textField == txtReminder{
            txtReminder.inputView = reminderPicker
            reminderPicker.delegate = self
            isTap = "Reminder"
        }else if textField == txtSelectedAccountName{
            txtSelectedAccountName.inputView = accountPicker
            accountPicker.delegate = self
            isTap = "Account"
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:_ Reminder Method
    func getDateIsProper(type:String)
    {
        if(selectedDate1 < currentDate)
        {
            self.alertView(alertMessage: AlertString.PastDate)
            let stringDate = String.convertFormatOfDate(date:  Date())
            
            txtDate.text = stringDate
            selectedDate1 =  passingDate//Date()
        }
        else
        {
            txtReminder.text = type
        }
    }
    
    //MARK:- PickerView Delaget
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if (isTap == "Account"){
            return listAccountArray.count
        }else if (isTap == "Reminder"){
            return reminderArray.count
        }else{
            return listAccountArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if (isTap == "Account"){
            return "\(listAccountArray[row]["name"]!)"
        }else if (isTap == "Reminder"){
            return reminderArray[row]
        }else{
            return "\(listAccountArray[row]["name"]!)"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if (isTap == "Account"){
            txtSelectedAccountName.text = "\(listAccountArray[row]["name"]!)"
            strPickerValue = "\(listAccountArray[row]["name"]!)"
            accountID = "\(listAccountArray[row]["id"]!)"
        }else if (isTap == "Reminder") {
            txtReminder.text = "\(reminderArray[row])"
            strReminderValue = "\(reminderArray[row])"
            self.getDateIsProper(type: "\(reminderArray[row])")
        }else{
        }
    }
    
    //MARK:- Action Method
    @IBAction func textFieldEditing(sender: UITextField) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        txtDate.text = dateFormatter.string(from: sender.date)
        selectedDate1 = sender.date
    }

    @objc func btnAccountPickerCancel(_ sender: UIButton)
    {
        txtAmmount.becomeFirstResponder()
//        self.hidePickerView()
    }
    
//    @objc func btnAccountPickerDone(_ sender: UIButton)
//    {
//        if (isTap == "Account"){
//            txtSelectedAccountName.text = strPickerValue
//            self.selectedAccountID = Int(accountID)!
//        }else{
//            txtReminder.text = strReminderValue
//        }
//        txtAmmount.becomeFirstResponder()
////        self.hidePickerView()
//    }
    
    @objc func showSpinningWheel(notification: NSNotification) {
        dict = notification.object as! NSMutableDictionary
        if(dict["subCategoryID"] as! String == "0")
        {
            lblSelectedCategoryName.text = (dict["categoryName"] as! String)
            strSelectedCategory = dict["categoryName"] as! String
//            imgCategory.image = UIImage(named: dict["image"] as! String)
            imgCategory.image = UIImage(named: dict["image"] as! String)!.withRenderingMode(.alwaysTemplate)
            imgCategory.tintColor = UIColor.white
        }
        else
        {
            lblSelectedCategoryName.text = "\(dict["categoryName"] as! String)/\(dict["name"] as! String)"
            strSelectedCategory = "\(dict["categoryName"] as! String)/\(dict["name"] as! String)"
//            imgCategory.image = UIImage(named: dict["image"] as! String)
            imgCategory.image = UIImage(named: dict["image"] as! String)!.withRenderingMode(.alwaysTemplate)
            imgCategory.tintColor = UIColor.white
        }
        print(dict)
    }
    
    @IBAction func btnAddNoteAction(_ sender: UIButton)
    {
        txtAddNote.isUserInteractionEnabled = true
        txtAddNote.becomeFirstResponder()
    }
    
    @IBAction func btnAccountAction(_ sender: UIButton){
        isTap = "Account"
//        setupDatePicker()
//        showPickerView()
    }
    
    @IBAction func btnReminderAction(_ sender: UIButton){
        isTap = "Reminder"
    }
    
    @IBAction func btnSplitAction(_ sender: UIButton){
        
        if passingView == "edit"
        {
            self.alertView(alertMessage: AlertString.EditingMode)
        }
        else
        {
            if(txtAmmount.text == "0" || txtAmmount.text == "")
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplitViewController") as! SplitViewController
                vc.amount = txtAmmount.text!
                vc.modalPresentationStyle = UIModalPresentationStyle.popover
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackDismiss(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCategoryAction(_ sender: UIButton) {
        isMainCategory = "Main"
        lblHeaderAddCategory.text = "Add New Category"
        showAnimate()
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomCategoryListingVC") as! CustomCategoryListingVC
//        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func addSubCategoryAction(_ sender: UIButton) {
        
        if strCategoryID != "" && strCategoryID != "0"
        {
            btnOk.tag = sender.tag
            isMainCategory = "Sub"
            lblHeaderAddCategory.text = "Add New Sub Category"
        }
        else
        {
            alertView(alertMessage: AlertString.SelectCategory)
        }
    }
    
    @IBAction func editAction(_ sender: UIButton)
    {
        if(isEdited == false)
        {
            isEdited = true
        }
        else
        {
            isEdited = false
        }
    }
    
    @IBAction func deleteSubCategoryAction(_ sender: UIButton) {
        //
        let temp = subcategoryArray[sender.tag] as! NSMutableDictionary
        let categoryList = uiRealm.objects(Category.self).filter("id = %@",temp["id"] as! Int)
        try! uiRealm.write {
            uiRealm.delete(categoryList)
        }
        subcategoryArray.removeObject(at: sender.tag)
        //        getCategoryDataWithID(categoryType: categoryType)
        
    }
    
    @IBAction func btnCloseAddTransactionTap(_ sender: UIButton) {
        
        if passingView == "edit"{
            self.dismiss(animated: true, completion: nil)
        }else{
            removeAddTransactionAnimate()
        }
    }
    
    @IBAction func btnClosePopUp(_ sender: UIButton) {
        btnOk.tag = 0
        removeAnimate()
    }
    
    @IBAction func addNewSubCategory(_ sender: UIButton) {
        
        if isMainCategory == "Main"
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let catId = (uiRealm.objects(Category.self).max(ofProperty: "id") as Int?)! + 1
                    let count = categoryArray.count - 1
                    let categoryId = uiRealm.objects(Category.self).value(forKey: "categoryID") as! NSArray
                    var uniqueDateArray = [Int]()
                    for i in 0..<categoryId.count
                    {
                        //                        if(!uniqueDateArray.contains(categoryId[i] as! Int))
                        //                        {
                        //                            uniqueDateArray.append(Int(categoryId[i] as! String)!)
                        //                        }
                        
                        if !uniqueDateArray.contains(Int(categoryId[i] as! String)!) {
                            uniqueDateArray.append(Int(categoryId[i] as! String)!)
                        }
                    }
                    
                    let max = uniqueDateArray.max()
                    
                    let categoryID : Int!
                    if(count == -1)
                    {
                        categoryID = 1
                    }
                    else
                    {
                        categoryID = max! + 1
                    }
                    
                    var taskListB = Category()
                    taskListB = Category(value: [catId,txtCategoryName.text!,img_icon,String(categoryID),String(0),categoryType,"1"])
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                    strCategoryID = ""
                    getCategoryData(categoryType: categoryType)
                    removeAnimate()
                }
                else{
                    self.alertView(alertMessage: AlertString.SelectIcon)
                }
            }
            else{
                self.alertView(alertMessage: AlertString.CategoryName)
            }
        }
        else if isMainCategory == "editSub"
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let workouts = uiRealm.objects(Category.self).filter("id = %@", (editDictionary["id"] as! Int))
                    //
                    if let workout = workouts.first {
                        try! uiRealm.write {
                            workout.categoryID = editDictionary["categoryID"] as! String
                            workout.categoryType = editDictionary["categoryType"] as! String
                            workout.image = img_icon
                            workout.isCategory = editDictionary["isCategory"] as! String
                            workout.name = txtCategoryName.text!
                            workout.subCategoryID = editDictionary["subCategoryID"] as! String
                        }
                        
                        for i in 0..<subcategoryArray.count
                        {
                            if ((subcategoryArray[i] as! NSDictionary).value(forKey: "id") as! Int) == editDictionary["id"] as! Int
                            {
                                
                                
                                (subcategoryArray[i] as! NSDictionary).setValue(txtCategoryName.text!, forKey: "name")
                                (subcategoryArray[i] as! NSDictionary).setValue(img_icon, forKey: "image")
                            }
                        }
                    }
                }
                else{
                    self.alertView(alertMessage: AlertString.SelectIcon)
                }
            }
            else{
                self.alertView(alertMessage: AlertString.CategoryName)
            }
        }
        else if isMainCategory == "editMain"
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let workouts = uiRealm.objects(Category.self).filter("id = %@", (Int(editDictionary["id"] as! String)!))
                    //
                    if let workout = workouts.first {
                        try! uiRealm.write {
                            workout.categoryID = editDictionary["categoryID"] as! String
                            workout.categoryType = editDictionary["categoryType"] as! String
                            workout.image = img_icon
                            workout.isCategory = editDictionary["isCategory"] as! String
                            workout.name = txtCategoryName.text!
                            workout.subCategoryID = editDictionary["subCategoryID"] as! String
                        }
                        getCategoryData(categoryType: categoryType)
                        removeAnimate()
                    }
                }
                else{
                    self.alertView(alertMessage: AlertString.SelectIcon)
                }
            }
            else{
                self.alertView(alertMessage: AlertString.SelectCategory)
            }
        }
        else
        {
            if(txtCategoryName.text != "")
            {
                if(img_icon != "")
                {
                    let catId = (uiRealm.objects(Category.self).max(ofProperty: "id") as Int?)! + 1
                    let count = subcategoryArray.count - 1//sections[sender.tag].items.count - 1
                    //                    let count = items.count - 1
                    let subCatId : Int!
                    if(count == -1)
                    {
                        subCatId = 1
                    }
                    else
                    {
                        subCatId = Int((subcategoryArray[count] as! NSMutableDictionary).value(forKey: "subCategoryID") as! String)! + 1
                    }
                    
                    var taskListB = Category()
                    taskListB = Category(value: [catId,txtCategoryName.text!,img_icon,strCategoryID,String(subCatId),categoryType,"0"])
                    try! uiRealm.write { () -> Void in
                        uiRealm.add(taskListB)
                    }
                    // getCategoryData(categoryType: categoryType)
                    
                    let subcategoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ AND categoryID = %@","0",categoryType,strCategoryID)
                    subcategoryArray = []
                    for i in 0..<subcategoryList.count
                    {
                        let dict = NSMutableDictionary()
                        dict.setValue(subcategoryList[i]["id"] as! Int, forKey: "id")
                        dict.setValue(subcategoryList[i]["name"]!, forKey: "name")
                        dict.setValue(subcategoryList[i]["image"]!, forKey: "image")
                        dict.setValue(subcategoryList[i]["categoryID"]!, forKey: "categoryID")
                        dict.setValue(subcategoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
                        dict.setValue(subcategoryList[i]["categoryType"]!, forKey: "categoryType")
                        dict.setValue(subcategoryList[i]["isCategory"]!, forKey: "isCategory")
                        subcategoryArray.add(dict)
                    }
                    print(subcategoryArray)
                    removeAnimate()
                }
                else{
                    self.alertView(alertMessage: AlertString.SelectIcon)
                }
            }
            else{
                self.alertView(alertMessage: AlertString.CategoryName)
            }
        }
    }
    
    @IBAction func btnAddTransactionAction(_ sender: UIButton)
    {
        if passingView == "edit"
        {
            if(txtAmmount.text != "0")
            {
                if(strSelectedCategory != "")
                {
                    
                    let budgetTbl = uiRealm.objects(Budget.self)
                    
                    if budgetTbl.count > 0
                    {
                        print(uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(transactionDict["categoryID"] as! String)!))
                        
                        if Int(transactionDict["categoryID"] as! String)! == Int(dict.value(forKey: "categoryID") as! String)!
                        {
                            let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(transactionDict["categoryID"] as! String)!)
                            
                            if let budgetWorkout = budgetUpdate.first
                            {
                                try! uiRealm.write
                                {
                                    budgetWorkout.currentAmount = Double(txtAmmount.text!)!
                                }
                            }
                        }
                        else
                        {
                            let budgetUpdate1 = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(transactionDict["categoryID"] as! String)!)
                            
                            if let budgetWorkout = budgetUpdate1.first
                            {
                                try! uiRealm.write
                                {
                                    budgetWorkout.currentAmount = budgetWorkout.currentAmount - Double(txtAmmount.text!)!
                                }
                            }
                            
                            let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(dict.value(forKey: "categoryID") as! String)!)
                            
                            if let budgetWorkout = budgetUpdate.first
                            {
                                try! uiRealm.write
                                {
                                    budgetWorkout.currentAmount = Double(txtAmmount.text!)!
                                }
                            }
                        }
                    }
                    
                    let workouts = uiRealm.objects(Transaction.self).filter("id = %@", (transactionDict["id"] as! Int))
                    
                    if let workout = workouts.first
                    {
                        try! uiRealm.write
                        {
                            var reminderType = String()
                            
                            if(self.txtReminder.text == "Never")
                            {
                                reminderType = "0"
                            }
                            else if(self.txtReminder.text == "Daily")
                            {
                                reminderType = "1"
                            }
                            else if(self.txtReminder.text == "Weekly")
                            {
                                reminderType = "2"
                            }
                            else if(self.txtReminder.text == "Monthly")
                            {
                                reminderType = "3"
                            }
                            
                            let reminderTypes = transactionDict["repeatType"] as! String
                            
                            if(reminderTypes == reminderType)
                            {
                                workout.isUpdateByService = transactionDict["isUpdateByService"] as! String
                                workout.repeatType = reminderTypes
                            }
                            else
                            {
                                workout.isUpdateByService = "0"
                                workout.repeatType = reminderType
                                
                            }
                            
                            
                            workout.amount = txtAmmount.text!
                            workout.categoryID = dict.value(forKey: "categoryID") as! String
                            workout.categoryImage = dict.value(forKey: "categoryimage") as! String
                            workout.categoryName = dict.value(forKey: "categoryName") as! String
                            workout.accountId = selectedAccountID
                            //                            selectedDate1 = Constant.changeDate(txtDate.text!)
                            
                            workout.date = passingDate
                            workout.updatedDate = passingDate
                            
                            if(txtAddNote.text == "Add Note..")
                            {
                                workout.note = ""
                            }
                            else
                            {
                                workout.note = txtAddNote.text!
                            }
                            workout.setType = dict.value(forKey: "categoryType") as! String
                            workout.subCategoryID = dict.value(forKey: "subCategoryID") as! String
                            workout.subCatImage = dict.value(forKey: "image") as! String
                            workout.subCatName = dict.value(forKey: "name") as! String
                        }
                        
                        if selectedAccountID == transactionDict.value(forKey: "accountId") as! Int
                        {
                            let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID))
                            if let workoutsAccount = workoutsAccount.first
                            {
                                try! uiRealm.write
                                {
                                    if strCategoryType == "Expense"
                                    {
                                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - (Double(transactionDict.value(forKey: "amount") as! String)! - Double(txtAmmount.text!)!)
                                    }
                                    else
                                    {
                                        workoutsAccount.amount = (workoutsAccount.amount - Double(transactionDict.value(forKey: "amount") as! String)!) + Double(txtAmmount.text!)!
                                        workoutsAccount.remainigAmount = (workoutsAccount.remainigAmount - Double(transactionDict.value(forKey: "amount") as! String)!) + Double(txtAmmount.text!)!
                                    }
                                    
                                }
                            }
                        }
                        else
                        {
                            //Main Account Update
                            let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (transactionDict.value(forKey: "accountId") as! Int))
                            if let workoutsAccount = workoutsAccount.first
                            {
                                try! uiRealm.write
                                {
                                    if strCategoryType == "Expense"
                                    {
                                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Double(txtAmmount.text!)!
                                    }
                                    else
                                    {
                                        workoutsAccount.amount = workoutsAccount.amount - Double(txtAmmount.text!)!
                                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(txtAmmount.text!)!
                                    }
                                    
                                }
                            }
                            
                            //Change Account Update
                            let workoutsChangeAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID))
                            if let workoutsChangeAccount = workoutsChangeAccount.first
                            {
                                try! uiRealm.write
                                {
                                    if strCategoryType == "Expense"
                                    {
                                        workoutsChangeAccount.remainigAmount = workoutsChangeAccount.remainigAmount - (Double(txtAmmount.text!)!)
                                    }
                                    else
                                    {
                                        workoutsChangeAccount.amount = workoutsChangeAccount.amount + Double(txtAmmount.text!)!
                                        workoutsChangeAccount.remainigAmount = workoutsChangeAccount.remainigAmount + Double(txtAmmount.text!)!
                                    }
                                    
                                }
                            }
                        }
                        removeAddTransactionAnimate()
//                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else
                {
                    self.alertView(alertMessage: AlertString.SelectCategory)
                }
            }
            else
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
        }
        else
        {
            if(txtAmmount.text != "0")
            {
                if(strSelectedCategory != "")
                {
                    let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int((dict["categoryID"] as? String)!)!)
                    
                    if let budgetWorkout = budgetUpdate.first
                    {
                        try! uiRealm.write
                        {
                            budgetWorkout.currentAmount = budgetWorkout.currentAmount + Double(txtAmmount.text!)!
                        }
                    } //
                    
                    let dict1 = NSMutableDictionary()
                    dict1.setValue(txtAmmount.text, forKey: "amount")
                    dict1.setValue(dict["name"], forKey: "subcategoryName")
                    dict1.setValue(dict["categoryName"], forKey: "categoryName")
                    dict1.setValue(dict["image"], forKey: "subcategoryImage")
                    dict1.setValue(dict["categoryimage"], forKey: "categoryimage")
                    
                    if(txtAddNote.text == "Add Note..")
                    {
                        dict1.setValue("", forKey: "description")
                    }
                    else
                    {
                        dict1.setValue(txtAddNote.text, forKey: "description")
                    }
                    dict1.setValue(passingDate, forKey: "date")
                    dict1.setValue(dict["categoryType"], forKey: "categoryType")
                    dict1.setValue(dict["subCategoryID"], forKey: "subCategoryID")
                    //                    dict1.setValue(dict["isCategory"], forKey: "isCategory")
                    dict1.setValue(dict["categoryID"], forKey: "categoryID")
                    addToListArray.add(dict1)
                    
                    var reminderType = String()
                    
                    if(self.txtReminder.text == "Never")
                    {
                        reminderType = "0"
                    }
                    else if(self.txtReminder.text == "Daily")
                    {
                        reminderType = "1"
                    }
                    else if(self.txtReminder.text == "Weekly")
                    {
                        reminderType = "2"
                    }
                    else if(self.txtReminder.text == "Monthly")
                    {
                        reminderType = "3"
                    }
                    
                    
                    for i in 0..<addToListArray.count
                    {
                        var taskListB = Transaction()
                        let autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
                        
                        let dictCat : NSMutableDictionary = addToListArray[i] as! NSMutableDictionary
                        print(dictCat)
                        
                        ///For Set Milisecond Code
//                        let currentTimeInMiliseconds = Date().timeIntervalSince1970
//                        let milliseconds = currentTimeInMiliseconds * 1000.0
                        
                        if(autoCatId == nil)
                        {
                            
                            
                            taskListB = Transaction(value: [1,dictCat.value(forKey: "categoryName"),dictCat.value(forKey: "categoryimage"),dictCat.value(forKey: "categoryID"),dictCat.value(forKey: "subCategoryID"),dictCat.value(forKey: "subcategoryName"),dictCat.value(forKey: "subcategoryImage"),dictCat.value(forKey: "amount"),dictCat.value(forKey: "date"),dictCat.value(forKey: "description"),selectedAccountID,dictCat.value(forKey: "categoryType"),reminderType,"0",dictCat.value(forKey: "date")])
                        }
                        else
                        {
                            let catId = (uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?)! + 1
                            taskListB = Transaction(value: [catId,dictCat.value(forKey: "categoryName"),dictCat.value(forKey: "categoryimage"),dictCat.value(forKey: "categoryID"),dictCat.value(forKey: "subCategoryID"),dictCat.value(forKey: "subcategoryName"),dictCat.value(forKey: "subcategoryImage"),dictCat.value(forKey: "amount"),dictCat.value(forKey: "date"),dictCat.value(forKey: "description"),selectedAccountID,dictCat.value(forKey: "categoryType"),reminderType,"0",dictCat.value(forKey: "date")])
                            
                        }
                        
                        try! uiRealm.write { () -> Void in
                            uiRealm.add(taskListB)
                        }
                        
                        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID ))
                        if let workoutsAccount = workoutsAccount.first
                        {
                            try! uiRealm.write
                            {
                                if strCategoryType == "Expense"
                                {
                                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(txtAmmount.text!)!
                                }
                                else
                                {
                                    workoutsAccount.amount = workoutsAccount.amount + Double(txtAmmount.text!)!
                                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Double(txtAmmount.text!)!
                                }
                            }
                        }
                        
//                        self.navigationController?.popViewController(animated: true)
//                        self.dismiss(animated: true, completion: nil)
                        removeAddTransactionAnimate()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else
                {
                    self.alertView(alertMessage: AlertString.SelectCategory)
                }
            }
            else
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
        }
//        removeAddTransactionAnimate()
    }
    
    @IBAction func btnDeleteTransactionAction(_ sender: UIButton)
    {
        let categoryList = uiRealm.objects(Transaction.self).filter("id = %@",transactionDict["id"] as! Int)
        
        let budgetData = uiRealm.objects(Budget.self).filter("mainCatID = %@",Int(transactionDict["categoryID"] as! String)!)
        
        if let budgetWorkout = budgetData.first
        {
            try! uiRealm.write
            {
                budgetWorkout.currentAmount = budgetWorkout.currentAmount - Double(transactionDict["amount"] as! String)!
            }
        }
        
        try! uiRealm.write {
            uiRealm.delete(categoryList)
        }
        
        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID))
        if let workoutsAccount = workoutsAccount.first
        {
            try! uiRealm.write
            {
                if strCategoryType == "Expense"
                {
                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Double(txtAmmount.text!)!
                }
                else
                {
                    workoutsAccount.amount = workoutsAccount.amount - Double(txtAmmount.text!)!
                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(txtAmmount.text!)!
                }
                
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}
