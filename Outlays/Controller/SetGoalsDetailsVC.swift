//
//  SetGoalsDetailsVC.swift
//  Created by harshesh on 14/06/1940 Saka.
//  Copyright © 1940 Cools. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class SetGoalsDetailsVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, JBDatePickerViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var progresssView: MBCircularProgressBarView!
    @IBOutlet var  txtName: UITextField!
    @IBOutlet var  txtPrice: UITextField!
    @IBOutlet var  txtEditName: UITextField!
    @IBOutlet var  txtEditPrice: UITextField!
    @IBOutlet var  txtEditContribution: UITextField!
    @IBOutlet var  lblCurrentTime: UILabel!
    @IBOutlet var  btnEdit: UIButton!
    @IBOutlet var  btnDone: UIButton!
    @IBOutlet var imageGoal: UIImageView!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var btnSelectDate: UIButton!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var lblMonth : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var btnDaily: UIButton!
    @IBOutlet var btnWeekly: UIButton!
    @IBOutlet var btnMonthly: UIButton!
    @IBOutlet var editGoalNameView : UIView!
    @IBOutlet var editGoalPriceView : UIView!
    @IBOutlet var addGoalNameView : UIView!
    @IBOutlet var addGoalPriceView : UIView!
    @IBOutlet var historyView : UIView!
    @IBOutlet var bankDetailView : UIView!
    @IBOutlet var dateView : UIView!
    @IBOutlet var reminderView : UIView!
    @IBOutlet var lblContribution : UILabel!
    @IBOutlet var lblCurrency : UILabel!
    @IBOutlet var btnContribution: UIButton!
    @IBOutlet var viewBankDetail: UIView!
    @IBOutlet var btnBankName: UIButton!
    @IBOutlet var lblSelectBankName: UILabel!
    @IBOutlet var btnBankPicker: UIButton!
//    @IBOutlet var BankPicker: UIPickerView!
    @IBOutlet var  txtBankName: UITextField!
    @IBOutlet var ImagePicView: UIView!
    @IBOutlet var txtSelectBankName: UITextField!

    var imagePicker = UIImagePickerController()
    var selectedDate1 = Date()
    var dateToSelect: Date!
    var passingDate : Date!
    var strData = ""
    var strGoalReminder = ""
    var passingView = ""
    var editData = [String:AnyObject]()
    var passGoalId = ""
    var goalsData = [[String:AnyObject]]()
    var contributData = [[String:AnyObject]]()
    var AccountData = [[String:AnyObject]]()
    var accountID = ""
    var passAcountId = 0
    var BankPicker : UIPickerView!
    var viewDatePicker : UIView!
    let donePicker = UIButton()
    let cancelPicker = UIButton()
    var cancelbgView : UIView!
    var strLabelText = ""
    var strPickerValue = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
         UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.doInitilization()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    override func viewWillAppear(_ animated: Bool) {
//        getAccountInfo()
    }

    func doInitilization ()
    {
        self.setLayout()
        getAccountInfo()
        imagePicker.delegate = self
        editGoalNameView.isHidden = true
        editGoalPriceView.isHidden = true
        progresssView.isHidden = true
        btnEdit.isHidden = true
        btnDone.isHidden = false
        datePickerView.delegate = self
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription
        lblCurrentTime.text = currentTime()
        
        viewBankDetail.isHidden = true
        viewBankDetail.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(viewBankDetail)

        //Edit View
        if passingView == "Edit"
        {
            editGoalPriceView.isHidden = false
            editGoalNameView.isHidden = false
            progresssView.isHidden = false
            addGoalNameView.isHidden = true
            addGoalPriceView.isHidden = true
            btnDone.setImage(#imageLiteral(resourceName: "ic_editGoal"), for: .normal)
            txtEditName.text = editData["name"] as? String
            lblCurrency.text = (UserDefaults.standard.value(forKey: "currencySymbol") as! String)
            txtEditPrice.text = "\((editData["amount"])!)"
            lblSelectBankName.text = "\(editData["acountName"]!)"
            let getDate = String.convertFormatOfStringToDate(date: "\(editData["createDate"]!)")
            let editDate = String.convertFormatOfDateToStringSpecificFormat(date: getDate!, format: "dd MMM")
            self.lblMonth.text = self.sanitizeDateString(editDate!, format: "MMMM")
            self.lblDate.text = self.sanitizeDateString(editDate!, format: "dd")
            
            //get Contribution Total Amount
            let totalAmount: Int = uiRealm.objects(Contribution.self).filter("goalId = %d", Int("\(editData["goalid"]!)")!).sum(ofProperty: "amount")
            print(totalAmount)
            self.lblContribution.text = "Contribute: \((UserDefaults.standard.value(forKey: "currencySymbol") as! String))\("\(totalAmount)")"//"\(totalAmount)"

            //Set Progress Value
            let goalPrice = editData["amount"] as! Int //Int(editData["amount"] as! String)
            let progPercentage =  Float(((100 * totalAmount)/goalPrice))
            
            if (progPercentage >= 100)
            {
                progresssView.value = CGFloat(100)
            }
            else
            {
                progresssView.value = CGFloat(progPercentage)
            }
            
            //Set Reminder Type Daily,Weekly,Monthly
            if ("\(editData["reminderType"]!)" == "1")
            {
                btnDaily.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0)
                btnWeekly.backgroundColor = UIColor.groupTableViewBackground
                btnMonthly.backgroundColor = UIColor.groupTableViewBackground
                btnDaily.setTitleColor(UIColor.white, for: .normal)
                btnWeekly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
                btnMonthly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
                self.btnDaily.setTitle("Daily", for: .normal)
                strGoalReminder = "Daily"
            }
            else if("\(editData["reminderType"]!)" == "2")
            {
                btnDaily.backgroundColor = UIColor.groupTableViewBackground
                btnWeekly.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0)
                btnMonthly.backgroundColor = UIColor.groupTableViewBackground
                
                btnDaily.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
                btnWeekly.setTitleColor(UIColor.white, for: .normal)
                btnMonthly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
                
                self.btnWeekly.setTitle("Weekly", for: .normal)
                strGoalReminder = "Weekly"
            }
            else //if("\(editData["reminderType"]!)" == "3")
            {
                btnDaily.backgroundColor = UIColor.groupTableViewBackground
                btnWeekly.backgroundColor = UIColor.groupTableViewBackground
                btnMonthly.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0)
                
                btnDaily.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
                btnWeekly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
                btnMonthly.setTitleColor(UIColor.white, for: .normal)
                
                self.btnMonthly.setTitle("Monthly", for: .normal)
                strGoalReminder = "Monthly"
            }
        }
        else
        {
            let tempDate = String.convertFormatOfDateToStringSpecificFormat(date: self.passingDate, format: "dd MMM")
            self.lblMonth.text = self.sanitizeDateString(tempDate!, format: "MMMM")
            self.lblDate.text = self.sanitizeDateString(tempDate!, format: "dd")
        }
    }
    
    func setupDatePicker() {
        viewDatePicker = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 250))
        viewDatePicker.backgroundColor = UIColor.white
        self.view.addSubview(viewDatePicker)
        
        self.BankPicker = UIPickerView(frame:CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: 200))
        self.BankPicker.delegate = self
        self.BankPicker.dataSource = self
        self.BankPicker.backgroundColor = UIColor.white
        viewDatePicker.addSubview(BankPicker)
        
        //done.buttonType = custom
        donePicker.frame = CGRect(x: self.view.frame.width - 70, y: 0, width: 60, height: 50)
        donePicker.setTitle("Done", for: .normal)
        donePicker.setTitleColor(UIColor.black, for: .normal)
        donePicker.addTarget(self, action: #selector(self.btndone(_:)), for: .touchUpInside)
        viewDatePicker.addSubview(donePicker)
        
        cancelPicker.frame = CGRect(x: 10, y: 5, width: 60, height: 40)
        cancelPicker.setTitle("Cancel", for: .normal)
        cancelPicker.setTitleColor(UIColor.black, for: .normal)
//        cancelPicker.contentHorizontalAlignment = .center
//        cancelPicker.backgroundColor = UIColor.lightGray
        cancelPicker.addTarget(self, action: #selector(self.btnCancel(_:)), for: .touchUpInside)
        viewDatePicker.addSubview(cancelPicker)
        
        viewDatePicker.isHidden = true
    }
    
    func hidePickerView(){
        viewDatePicker.isHidden = true
    }
    
    func showPickerView() {
        viewDatePicker.isHidden = false
    }
    
    //Sewt Layout Of Views
    func setLayout()
    {
        //View
        historyView.layer.shadowColor = UIColor.lightGray.cgColor
        historyView.layer.masksToBounds = false
        historyView.layer.shadowOpacity = 1
        historyView.layer.shadowRadius = 3
        historyView.layer.shadowOffset = CGSize(width: 0, height: 4)

        bankDetailView.layer.shadowColor = UIColor.lightGray.cgColor
        bankDetailView.layer.masksToBounds = false
        bankDetailView.layer.shadowOpacity = 1
        bankDetailView.layer.shadowRadius = 3
        bankDetailView.layer.shadowOffset = CGSize(width: 0, height: 4)

        dateView.layer.shadowColor = UIColor.lightGray.cgColor
        dateView.layer.masksToBounds = false
        dateView.layer.shadowOpacity = 1
        dateView.layer.shadowRadius = 3
        dateView.layer.shadowOffset = CGSize(width: 0, height: 4)

        reminderView.layer.shadowColor = UIColor.lightGray.cgColor
        reminderView.layer.masksToBounds = false
        reminderView.layer.shadowOpacity = 1
        reminderView.layer.shadowRadius = 3
        reminderView.layer.shadowOffset = CGSize(width: 0, height: 4)
        
        //Button
        lblMonth.layer.cornerRadius = 5
        lblMonth.layer.masksToBounds = true
        lblDate.layer.cornerRadius = 20
        lblDate.layer.masksToBounds = true
        btnSelectDate.layer.cornerRadius = 10
        btnSelectDate.layer.masksToBounds = true
        
        //ImageView
        imageGoal.layer.cornerRadius = 60
        ImagePicView.layer.cornerRadius = 60
        
        btnBankName.layer.cornerRadius = 5
        btnBankName.layer.masksToBounds = true
    }
    
    //Get Contribution Data
    func getcontributInfo()
    {
        let puppies = uiRealm.objects(Contribution.self).filter("goalId = %d", Int("\(passGoalId)")!)
        contributData = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["contributid"] = puppies[j]["contributid"]! as AnyObject
            dict1["goalId"] = puppies[j]["goalId"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["note"] = puppies[j]["note"]! as AnyObject
            dict1["createDate"] = String.convertFormatOfDate(date: puppies[j]["createDate"]! as! Date) as AnyObject
            contributData.append(dict1)
        }
        
        if contributData.count > 0
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContributionListVC") as! ContributionListVC
            vc.getgoalId = passGoalId
            let getDate = String.convertFormatOfStringToDate(date: "\(editData["createDate"]!)")
            vc.setGoalDate = getDate //(editData["createDate"]! as! Date)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContributionVC") as! ContributionVC
            vc.getgoalId = passGoalId
            vc.getpassingDate = passingDate
            let getDate = String.convertFormatOfStringToDate(date: "\(editData["createDate"]!)")
            vc.setGoalDate = getDate //(editData["createDate"]! as! Date)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    //MARK:-
    func getAccountInfo()
    {
        let puppies = uiRealm.objects(GoalAccount.self)
        AccountData = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["gAccountid"] = puppies[j]["gAccountid"]! as AnyObject
            dict1["gName"] = puppies[j]["gName"]! as AnyObject
            AccountData.append(dict1)
        }
        
        if AccountData.count == 0
        {
            lblSelectBankName.text = "Add Account"
        }
        else if (strLabelText == "")
        {
            lblSelectBankName.text = "Select Bank Name"
        }
        else
        {
            lblSelectBankName.text = strLabelText
        }
    }

    //MARK:- UIImagePickerControllerDelegate Methods
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageGoal.contentMode = .scaleAspectFit
            imageGoal.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    //MARK:- Helper Method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view == viewBankDetail
        {
            viewBankDetail.isHidden = true
        }
        else if touch.view != datePickerView && touch.view != dateHeaderView{
            removeAnimate()
        }
    }
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
                self.strData = String.convertFormatOfDateToStringSpecificFormat(date: self.selectedDate1, format: "dd MMM")
                self.lblMonth.text = self.sanitizeDateString(self.strData, format: "MMMM")
                self.lblDate.text = self.sanitizeDateString(self.strData, format: "dd")
            }
        });
    }

//    func pickUp(_ textField : UITextField){
//
//        // UIPickerView
//        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//        self.myPickerView.delegate = self
//        self.myPickerView.dataSource = self
//        self.myPickerView.backgroundColor = UIColor.white
//        txtSelectBankName.inputView = self.myPickerView
//
//        // ToolBar
////        let toolBar = UIToolbar(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y-66, width: self.view.frame.size.width, height: 50))
//        let toolBar = UIToolbar()
//        toolBar.barStyle = .default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor.blue
//        toolBar.sizeToFit()
//
//        // Adding Button ToolBar
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
//        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        textField.inputAccessoryView = toolBar
//    }
    
    //Date Convert in String
    func sanitizeDateString(_ dateString:String, format:String) -> String?
    {
        let dateFormatter = DateFormatter()
        if format == "MMMM"
        {
            dateFormatter.dateFormat = "dd MMM"
            let newFormat = DateFormatter()
            newFormat.dateFormat = "MMMM"
            if let date = dateFormatter.date(from: dateString)
            {
                print(date)
                return newFormat.string(from: date)
            }
            else
            {
                return nil
            }
        }
        else{
            dateFormatter.dateFormat = "dd MMM"
            let newFormat = DateFormatter()
            newFormat.dateFormat = "dd"
            if let date = dateFormatter.date(from: dateString)
            {
                print(date)
                return newFormat.string(from: date)
            }
            else
            {
                return nil
            }
        }
    }
    
    //Get Time
    func currentTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return "\(hour):\(minutes)"
    }
    
    //MARK:- PickerView Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return AccountData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(AccountData[row]["gName"]!)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        strPickerValue = "\(AccountData[row]["gName"]!)"
        accountID = "\(AccountData[row]["gAccountid"]!)"
        print(accountID)
//        self.view.endEditing(true)
    }

    //MARK: - JBDatePicker Delegate
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            _ = String.convertFormatOfDate(date: selectedDate)
            selectedDate1 = selectedDate
            removeAnimate()
        }
    }
    
//    func shouldAllowSelectionOfDay(_ date: Date?) -> Bool {
//
//        //this code example disables selection for dates older then today
//        guard let date = date else {return true}
//        let comparison = NSCalendar.current.compare(date, to: Date().stripped()!, toGranularity: .day)
//
//        if comparison == .orderedAscending {
//            return false
//        }
//        return true
//    }

    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        if let date = dateToSelect {
            return date
        }
        else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
    //MARK:- Action Method
    @objc func btnCancel(_ sender: UIButton)
    {
        self.hidePickerView()
    }
    
    @objc func btndone(_ sender: UIButton)
    {
        lblSelectBankName.text = strPickerValue
        self.hidePickerView()
    }
    
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }
    
    @available(iOS 10.0, *)
    @available(iOS 10.0, *)
    @available(iOS 10.0, *)
    @IBAction func btnDailyTap(_ sender: Any)
    {
        btnDaily.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0)
        btnWeekly.backgroundColor = UIColor.groupTableViewBackground
        btnMonthly.backgroundColor = UIColor.groupTableViewBackground
        btnDaily.setTitleColor(UIColor.white, for: .normal)
        btnWeekly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
        btnMonthly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
        self.btnDaily.setTitle("Daily", for: .normal)
        strGoalReminder = "Daily"
    }
    
    @available(iOS 10.0, *)
    @available(iOS 10.0, *)
    @available(iOS 10.0, *)
    @IBAction func btnWeeklyTap(_ sender: Any)
    {
        btnDaily.backgroundColor = UIColor.groupTableViewBackground
        btnWeekly.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0)
        btnMonthly.backgroundColor = UIColor.groupTableViewBackground
        btnWeekly.setTitleColor(UIColor.white, for: .normal)
        btnMonthly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
        btnDaily.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
        self.btnWeekly.setTitle("Weekly", for: .normal)
        strGoalReminder = "Weekly"
    }
    
    @IBAction func btnMonthlyTap(_ sender: Any)
    {
        btnDaily.backgroundColor = UIColor.groupTableViewBackground
        btnWeekly.backgroundColor = UIColor.groupTableViewBackground
        btnMonthly.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0)
        btnDaily.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
        btnWeekly.setTitleColor(UIColor(displayP3Red: 0/255, green: 0/255, blue: 139/255, alpha: 1.0), for: .normal)
        btnMonthly.setTitleColor(UIColor.white, for: .normal)
        self.btnMonthly.setTitle("Monthly", for: .normal)
        strGoalReminder = "Monthly"
    }

//    @IBAction func btnImgSelect(_ sender: UIButton)
//    {
//        imagePicker.allowsEditing = false
//        imagePicker.sourceType = .photoLibrary
//        present(imagePicker, animated: true, completion: nil)
//    }
    
    @IBAction func btnDoneAction(_ sender: UIButton)
    {
        if passingView == "Edit"
        {
            let workouts = uiRealm.objects(Goals.self).filter("goalid = %@", (editData["goalid"] as! Int))
            
            var type = 1
            
            if(strGoalReminder == "Daily")
            {
                type =  1
                strGoalReminder = "Daily"
            }
            else if(strGoalReminder == "Weekly")
            {
                type =  2
                strGoalReminder = "Weekly"
            }
            else if(strGoalReminder == "Monthly")
            {
                type =  3
                strGoalReminder = "Monthly"
            }
            
            if let workout = workouts.first
            {
                try! uiRealm.write
                {
                    workout.name = String(txtEditName.text!)
                    workout.amount = Int(txtEditPrice.text!)!
                    if (workout.amount == 0)
                    {
                        self.alertView(alertMessage: "Do not Add Zero Value.")
                    }
                    workout.createDate = selectedDate1
                    workout.reminderType = "\(type)"
                    workout.acountName = String(lblSelectBankName.text!)
                    workout.gAccountid = Int(passAcountId);
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is SetGoalsVC {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
                    self.alertView(alertMessage: AlertString.SucessFullyUpdate)
//                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        else
        {
            if (txtName.text == "")
            {
                self.alertView(alertMessage: AlertString.GoalName)
                return
            }
            else if (txtPrice.text == "")
            {
                self.alertView(alertMessage: AlertString.GoalPrice)
                return
            }
            else if (lblSelectBankName.text == "Select Bank Name")
            {
                self.alertView(alertMessage: "Please Select Bank")
                return
            }
            else if (txtPrice.text == "0")
            {
                self.alertView(alertMessage: "Do Not Add Zero Value")
                return
            }
              else if strGoalReminder == ""
            {
                self.alertView(alertMessage: AlertString.GoalReminder)
                return
            }
            else if AccountData.count == 0
            {
                self.alertView(alertMessage: "Please Add Bank Information")
            }
            else
            {
                var goalsInfo = Goals()
                var autoCatId = uiRealm.objects(Goals.self).max(ofProperty: "goalid") as Int?
                
                if(autoCatId == nil)
                {
                    autoCatId = 1
                }
                else
                {
                    autoCatId = autoCatId! + 1
                }
                
                var type = 1
                
                if(strGoalReminder == "Daily")
                {
                    type =  1
                }
                else if(strGoalReminder == "Weekly")
                {
                    type =  2
                }
                else if(strGoalReminder == "Monthly")
                {
                    type =  3
                }
                
                goalsInfo = Goals(value: [autoCatId!,
                                          String(txtName.text!),
                                          Int(txtPrice.text!)!,
                                          passingDate!,
                                          String(lblSelectBankName.text!),
                                          String(type),
                                          Int(passAcountId)])
                
                try! uiRealm.write { () -> Void in
                    uiRealm.add(goalsInfo)
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is SetGoalsVC {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
                }
                self.alertView(alertMessage: AlertString.SucessAdd)
            }
        }
    }
    
    @IBAction func btnContributionTAP(_ sender: Any)
    {
        if (editData.count == 0)
        {
            alertView(alertMessage: AlertString.AddGoal)
        }
        else
        {
            self.getcontributInfo()
        }
    }
    
    @IBAction func btnAddBankAction(_ sender: Any)
    {
        viewBankDetail.isHidden = false
        txtBankName.text = nil
        txtBankName.placeholder = "Enter Bank Name"
    }
    
    @IBAction func btnBankDetailAddTap(_ sender: Any)
    {
        if (txtBankName.text == "")
        {
            self.alertView(alertMessage: "Please Add Bank Name")
            return
        }
        else
        {
                var accountInfo = GoalAccount()
                var autoCatId = uiRealm.objects(GoalAccount.self).max(ofProperty: "gAccountid") as Int?
                
                if(autoCatId == nil)
                {
                    autoCatId = 1
                }
                else
                {
                    autoCatId = autoCatId! + 1
                }
            
            accountInfo = GoalAccount(value: [autoCatId!,String(txtBankName.text!)])
                strLabelText = txtBankName.text!
            
                try! uiRealm.write { () -> Void in
                    uiRealm.add(accountInfo)
                    viewBankDetail.isHidden = true
                    doInitilization()

//                    lblSelectBankName.text = strLblText
//                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//                    for aViewController in viewControllers {
//                        if aViewController is SetGoalsVC {
//                            self.navigationController!.popToViewController(aViewController, animated: true)
//                        }
//                    }
                }
        }
    }
    
    @IBAction func btnBankPickerAction(_ sender: Any)
    {
        if AccountData.count == 0
        {
            self.alertView(alertMessage: "Tap on Pluse Button and Add Saving Account Name")
        }
        else
        {
            setupDatePicker()
            self.showPickerView()
        }
    }

    
    @IBAction func btnCancelTAP(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK: textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fulltext = textField.text! + (string)
        if fulltext.count == 2 {
            if (((fulltext as? NSString)?.substring(to: 1)) == "0") && !(fulltext == "0.") {
                textField.text = (fulltext as NSString).substring(with: NSRange(location: 1, length: 1))
                return false
            }
        }
        return true
    }
}
