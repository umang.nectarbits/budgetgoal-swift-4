		//
//  AddTransactionVC.swift
//  Finance
//
//  Created by Cools on 6/20/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMobileAds

class AddTransactionVC: UIViewController,UITextFieldDelegate,JBDatePickerViewDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate,CalculatorDelegate,GADBannerViewDelegate {
  
    //MARK: - Define IBoutlet
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet weak var btnClose : UIButton!
    @IBOutlet weak var txtAmount : UITextField!
    @IBOutlet weak var txtAddNote : UITextView!
    @IBOutlet weak var btnCategory : UIButton!
    @IBOutlet weak var btnDate : UIButton!
    @IBOutlet var btnDaily: UIButton!	
    @IBOutlet var btnWeekly: UIButton!
    @IBOutlet var btnMonthly: UIButton!
    @IBOutlet var bgScrollview: UIScrollView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var headerView : UIView!
    @IBOutlet var TopheaderView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var lblheader : UILabel!
    @IBOutlet var txtCategoryName : UITextField!
    @IBOutlet var txtDate : UITextField!
    @IBOutlet var lblReminder : UILabel!
    @IBOutlet var btnCategory_img: UIButton!
    @IBOutlet var img_category: UIImageView!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnTransactrionAdd: UIButton!
    @IBOutlet var btnCalender: UIButton!
    @IBOutlet var btnSplit: UIButton!
    @IBOutlet var btnReminder: UIButton!
    @IBOutlet var img_note: UIImageView!
    @IBOutlet var scrillviewInnerView: UIView!
    @IBOutlet weak var btnSplitAmount: UIButton!
    @IBOutlet weak var splitHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var selectAccountPicker: AAPickerView!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Define Variable
    let reminderPicker: UIPickerView = UIPickerView()
    var checkedDaily : Bool = false
    var checkedWeekly : Bool = false
    var checkedMonthly : Bool = false
    var strSelectedCategory : String = ""
    var dict = NSMutableDictionary()
    var selectedDate1 = Date()
    var currentDate = Date()
    var addToListArray = NSMutableArray()
    var longPress = UILongPressGestureRecognizer()
    var dateToSelect: Date!
    var transactionDict = NSMutableDictionary()
    var passingType = ""
    var reminderArray = ["Never","Daily","Weekly","Monthly"]
    var strCategoryType = ""
    var strCategoryID = ""
    var accountArray = [String]()
    var selectedAccountID : Int = 0
    var listAccountArray = [[String:AnyObject]]()
    var passingDate : Date!
    //MARK: - Viewdidload
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        currentDate = Date()
        selectedDate1 =  Date()

        btnDelete.isHidden = true

        self.calendarView.isHidden = true
        self.bgView.isHidden = true
        txtAmount.delegate = self
        strSelectedCategory = ""
        
        getAccountData()
        setLayout()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddTransactionVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddTransactionVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        bgScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height-headerView.frame.size.height)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddTransactionVC.showSpinningWheel(notification:)), name: NSNotification.Name(rawValue: "fetchCategoryData"), object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
        
        if passingType == "edit"
        {
            btnDelete.isHidden = false
            
            txtAmount.text = transactionDict.value(forKey: "amount") as? String
            if(transactionDict["subCategoryID"] as! String == "0")
            {
                txtCategoryName.text = transactionDict["categoryName"] as? String
                strSelectedCategory = transactionDict["categoryName"] as! String
                btnCategory_img.setBackgroundImage(UIImage(named: transactionDict["categoryImage"] as! String), for: .normal)
                img_category.image = UIImage(named: transactionDict["categoryImage"] as! String)
            }
            else
            {
                txtCategoryName.text = "\(transactionDict["categoryName"] as! String)/\(transactionDict["subCatName"] as! String)"
                strSelectedCategory = "\(transactionDict["categoryName"] as! String)/\(transactionDict["subCatName"] as! String)"
                btnCategory_img.setBackgroundImage(UIImage(named: transactionDict["categoryImage"] as! String), for: .normal)
                img_category.image = UIImage(named: transactionDict["subCatImage"] as! String)
            }
            
            txtDate.text = transactionDict["date"] as? String
            passingDate = String.convertFormatOfStringToDate(date: transactionDict["date"] as! String)
            
            if transactionDict["note"] as! String != ""
            {
                txtAddNote.text = transactionDict["note"] as! String
            }
            
            dict = NSMutableDictionary()
            
            dict.setValue(transactionDict["categoryID"] as! String, forKey: "categoryID")
            dict.setValue(transactionDict["setType"] as! String, forKey: "categoryType")
            dict.setValue(transactionDict["subCatImage"] as! String, forKey: "image")
            dict.setValue(transactionDict["categoryImage"] as! String, forKey: "categoryimage")
            dict.setValue(transactionDict["subCatName"] as! String, forKey: "name")
            dict.setValue(transactionDict["categoryName"] as! String, forKey: "categoryName")
            dict.setValue(transactionDict["subCategoryID"] as! String, forKey: "subCategoryID")
            
            var reminderType = String()
            selectedAccountID = transactionDict["accountId"] as! Int
            reminderType = transactionDict["repeatType"] as! String
            
            if(reminderType == "0")
            {
                self.lblReminder.text = "Never"
            }
            else if(reminderType == "1")
            {
                self.lblReminder.text = "Daily"
            }
            else if(reminderType == "2")
            {
               self.lblReminder.text = "Weekly"
            }
            else if(reminderType == "3")
            {
                self.lblReminder.text = "Monthly"
            }
        }
        else
        {
            btnDelete.isHidden = true
            txtDate.text = String.convertFormatOfDate(date: currentDate)
        }
        datePickerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        
        IQKeyboardManager.sharedManager().enable = true
        txtAmount.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = false
        
    }
    override func viewWillLayoutSubviews() {
        
        if strCategoryType == "Expense"
        {
            headerView.setGradientColor(color1: UIColor(red: 153.0/255.0, green: 91.0/255.0, blue: 196.0/255.0, alpha: 1), color2: UIColor(red: 222.0/255.0, green: 89.0/255.0, blue: 154.0/255.0, alpha: 1))
            
            
            TopheaderView.setGradientColor(color1: UIColor(red: 153.0/255.0, green: 91.0/255.0, blue: 196.0/255.0, alpha: 1), color2: UIColor(red: 222.0/255.0, green: 89.0/255.0, blue: 154.0/255.0, alpha: 1))
        }
        else
        {
            headerView.setGradientColor(color1: UIColor(red: 48.0/255.0, green: 184.0/255.0, blue: 125.0/255.0, alpha: 1), color2: UIColor(red: 140.0/255.0, green: 200.0/255.0, blue: 67.0/255.0, alpha: 1))
            TopheaderView.setGradientColor(color1: UIColor(red: 48.0/255.0, green: 184.0/255.0, blue: 125.0/255.0, alpha: 1), color2: UIColor(red: 140.0/255.0, green: 200.0/255.0, blue: 67.0/255.0, alpha: 1))
            btnCalender.setBackgroundImage(UIImage(named:"icon_calendar")?.maskWithColor(color: UIColor(red: 48.0/255.0, green: 184.0/255.0, blue: 125.0/255.0, alpha: 1)), for: .normal)
            btnSplit.setBackgroundImage(UIImage(named:"ic_split")?.maskWithColor(color: UIColor(red: 48.0/255.0, green: 184.0/255.0, blue: 125.0/255.0, alpha: 1)), for: .normal)
            btnReminder.setBackgroundImage(UIImage(named:"icon_reminder")?.maskWithColor(color: UIColor(red: 48.0/255.0, green: 184.0/255.0, blue: 125.0/255.0, alpha: 1)), for: .normal)
            img_note.image = UIImage(named: "ic_note")?.maskWithColor(color: UIColor(red: 48.0/255.0, green: 184.0/255.0, blue: 125.0/255.0, alpha: 1))
            
        }
        scrillviewInnerView.viewShadow()
        
    }
    
    // MARK: - RFCalculatorKeyboard delegate
    func calculator(_ calculator: CalculatorKeyboard, didChangeValue value: String) {
        txtAmount.text = value
    }
    
    //MARK: - Helper Method
    
    func setLayout()
    {
        btnTransactrionAdd.layer.masksToBounds = false
        btnTransactrionAdd.layer.cornerRadius = btnTransactrionAdd.frame.size.width/2
        btnTransactrionAdd.layer.shadowColor = UIColor.lightGray.cgColor
        btnTransactrionAdd.layer.shadowOpacity = 1
        btnTransactrionAdd.layer.shadowOffset = CGSize.zero
        btnTransactrionAdd.layer.shadowRadius = 3
        
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription
        
        if strCategoryType == "Expense"
        {
            btnSplitAmount.isHidden = false
            splitHeightConstraint.constant = 50.0
        }
        else
        {
            btnSplitAmount.isHidden = true
            splitHeightConstraint.constant = 0.0
        }
        
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 300)
        let keyboard = CalculatorKeyboard(frame: frame)
        keyboard.delegate = self
        keyboard.showDecimal = true
        txtAmount.inputView = keyboard
        txtAmount.delegate = self
        txtAmount.addDoneButtonToKeyboard(myAction:  #selector(txtAmount.resignFirstResponder))
        
        let stringData = ["Monthly","Bi-Monthly","Quaterly","Half Year","Yearly"]
        selectAccountPicker.stringPickerData = accountArray
        selectAccountPicker.pickerType = .StringPicker
        selectAccountPicker.pickerRow.font = UIFont(name: "Helvatica-Regular", size: 30)
        
        selectAccountPicker.toolbar.barTintColor = .darkGray
        selectAccountPicker.toolbar.tintColor = .black
        
        if passingType == "Edit"
        {
            for i in 0..<listAccountArray.count
            {
                if listAccountArray[i]["id"] as? Int == Int(transactionDict["accountId"] as! String)
                {
                    selectAccountPicker.text = accountArray[i]
                    selectedAccountID = listAccountArray[i]["id"] as! Int
                }
            }
        }
        else
        {
            for i in 0..<listAccountArray.count
            {
                if listAccountArray[i]["id"] as? Int == UserDefaults.standard.integer(forKey: "AccountID")
                {
                    selectAccountPicker.text = accountArray[i]
                    selectedAccountID = listAccountArray[i]["id"] as! Int
                }
            }
//            selectAccountPicker.text = accountArray[0]
//            selectedAccountID = listAccountArray[0]["id"] as! Int
        }
        
        selectAccountPicker.stringDidChange = { index in
            print("selectedString ", stringData[index])
            self.selectedAccountID = self.listAccountArray[index]["id"] as! Int
        }
    }
    
    func getAccountData()
    {
        let puppies = uiRealm.objects(Account.self)
        listAccountArray = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["id"] = puppies[j]["id"]! as AnyObject
            dict1["name"] = puppies[j]["name"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["remainigAmount"] = puppies[j]["remainigAmount"]! as AnyObject
            dict1["date"] = String.convertFormatOfDate(date: puppies[j]["date"]! as! Date) as AnyObject
            dict1["isIncludeOnDashboard"] = puppies[j]["isIncludeOnDashboard"]! as AnyObject
            dict1["isDefaultAccount"] = puppies[j]["isDefaultAccount"]! as AnyObject
            accountArray.append(dict1["name"] as! String)
            listAccountArray.append(dict1)
        }
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if var keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardSize = self.view.convert(keyboardSize, from: nil)
            
            var contentInset:UIEdgeInsets = self.bgScrollview.contentInset
            contentInset.bottom = keyboardSize.size.height
            self.bgScrollview.contentInset = contentInset

            
//            if bgScrollview.frame.size.height == self.view.frame.size.height-headerView.frame.size.height{
//                bgScrollview.frame.size.height -= (keyboardSize.height)
//                bgScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height-(keyboardSize.height))
//            }
        }
    }
    @objc  
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            let contentInset:UIEdgeInsets = .zero
            self.bgScrollview.contentInset = contentInset
            
//            if bgScrollview.frame.size.height != self.view.frame.size.height-headerView.frame.size.height{
//                bgScrollview.frame.size.height += (keyboardSize.height)
//                bgScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - headerView.frame.size.height)
//            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }

    }
    
    @objc func showSpinningWheel(notification: NSNotification) {
        dict = notification.object as! NSMutableDictionary
        btnCategory_img.layer.cornerRadius = btnCategory_img.frame.size.width / 2
//        btnCategory_img.layer.borderWidth = 1
        btnCategory_img.layer.borderColor = UIColor.black.cgColor
        btnCategory_img.layer.masksToBounds = true
        img_category.layer.cornerRadius = img_category.frame.size.width / 2
        img_category.layer.borderWidth = 1
        img_category.layer.borderColor = UIColor.white.cgColor
        img_category.layer.masksToBounds = true
        if(dict["subCategoryID"] as! String == "0")
        {
            txtCategoryName.text = dict["categoryName"] as! String
            strSelectedCategory = dict["categoryName"] as! String
            btnCategory_img.setBackgroundImage(UIImage(named: dict["categoryimage"] as! String), for: .normal)
            img_category.image = UIImage(named: dict["categoryimage"] as! String)
        }
        else
        {
            txtCategoryName.text = "\(dict["categoryName"] as! String)/\(dict["name"] as! String)"
            strSelectedCategory = "\(dict["categoryName"] as! String)/\(dict["name"] as! String)"
            btnCategory_img.setBackgroundImage(UIImage(named: dict["categoryimage"] as! String), for: .normal)
            img_category.image = UIImage(named: dict["image"] as! String)
        }
        print(dict)
    }
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
    }

    
    //MARK: - Action Method
    
    @IBAction func btnDeleteTransactionAction(_ sender: UIButton)
    {
        let categoryList = uiRealm.objects(Transaction.self).filter("id = %@",transactionDict["id"] as! Int)
        
        let budgetData = uiRealm.objects(Budget.self).filter("mainCatID = %@",Int(transactionDict["categoryID"] as! String)!)
        
        if let budgetWorkout = budgetData.first
        {
            try! uiRealm.write
            {
                budgetWorkout.currentAmount = budgetWorkout.currentAmount - Double(transactionDict["amount"] as! String)!
            }
        }
        
        try! uiRealm.write {
            uiRealm.delete(categoryList)
        }
        
        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID))
        if let workoutsAccount = workoutsAccount.first
        {
            try! uiRealm.write
            {
                if strCategoryType == "Expense"
                {
                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Double(txtAmount.text!)!
                }
                else
                {
                    workoutsAccount.amount = workoutsAccount.amount - Double(txtAmount.text!)!
                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(txtAmount.text!)!
                }
                
            }
        }
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }
    @IBAction func btnSplitAction(_ sender: UIButton) {
        
        if passingType == "edit"
        {
            self.alertView(alertMessage: AlertString.EditingMode)
        }
        else
        {
            if(txtAmount.text == "0")
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
            else
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplitViewController") as! SplitViewController
                vc.amount = txtAmount.text!
                vc.modalPresentationStyle = UIModalPresentationStyle.popover
                present(vc, animated: true, completion: nil)
                
            }
        }
    }

    @IBAction func closeView(_ sender: UIButton)
    {
        txtAmount.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTransactionAction(_ sender: UIButton)
    {
        if passingType == "edit"
        {
            if(txtAmount.text != "0")
            {
                if(strSelectedCategory != "")
                {
                    
                    let budgetTbl = uiRealm.objects(Budget.self)
                    
                    if budgetTbl.count > 0
                    {
                        print(uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(transactionDict["categoryID"] as! String)!))
                        
                        if Int(transactionDict["categoryID"] as! String)! == Int(dict.value(forKey: "categoryID") as! String)!
                        {
                            let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(transactionDict["categoryID"] as! String)!)
                            
                            if let budgetWorkout = budgetUpdate.first
                            {
                                try! uiRealm.write
                                {
                                    budgetWorkout.currentAmount = Double(txtAmount.text!)!
                                }
                            }
                        }
                        else
                        {
                            let budgetUpdate1 = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(transactionDict["categoryID"] as! String)!)
                            
                            if let budgetWorkout = budgetUpdate1.first
                            {
                                try! uiRealm.write
                                {
                                    budgetWorkout.currentAmount = budgetWorkout.currentAmount - Double(txtAmount.text!)!
                                }
                            }
                            
                            let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(dict.value(forKey: "categoryID") as! String)!)
                            
                            if let budgetWorkout = budgetUpdate.first
                            {
                                try! uiRealm.write
                                {
                                    budgetWorkout.currentAmount = Double(txtAmount.text!)!
                                }
                            }
                        }
                    }
                    
                    let workouts = uiRealm.objects(Transaction.self).filter("id = %@", (transactionDict["id"] as! Int))
                    
                    if let workout = workouts.first
                    {
                        try! uiRealm.write
                        {
                            var reminderType = String()
                            
                            if(self.lblReminder.text == "Never")
                            {
                                reminderType = "0"
                            }
                            else if(self.lblReminder.text == "Daily")
                            {
                                reminderType = "1"
                            }
                            else if(self.lblReminder.text == "Weekly")
                            {
                                reminderType = "2"
                            }
                            else if(self.lblReminder.text == "Monthly")
                            {
                                reminderType = "3"
                            }
                            
                            let reminderTypes = transactionDict["repeatType"] as! String
 
                            if(reminderTypes == reminderType)
                            {
                                workout.isUpdateByService = transactionDict["isUpdateByService"] as! String
                                workout.repeatType = reminderTypes
                            }
                            else
                            {
                                workout.isUpdateByService = "0"
                                workout.repeatType = reminderType

                            }
                            

                            workout.amount = txtAmount.text!
                            workout.categoryID = dict.value(forKey: "categoryID") as! String
                            workout.categoryImage = dict.value(forKey: "categoryimage") as! String
                            workout.categoryName = dict.value(forKey: "categoryName") as! String
                            workout.accountId = selectedAccountID
//                            selectedDate1 = Constant.changeDate(txtDate.text!)
                        
                            workout.date = passingDate
                            workout.updatedDate = passingDate
                           
                            if(txtAddNote.text == "Add Note..")
                            {
                                workout.note = ""
                            }
                            else
                            {
                                workout.note = txtAddNote.text!
                            }
                            workout.setType = dict.value(forKey: "categoryType") as! String
                            workout.subCategoryID = dict.value(forKey: "subCategoryID") as! String
                            workout.subCatImage = dict.value(forKey: "image") as! String
                            workout.subCatName = dict.value(forKey: "name") as! String
                        }
                        
                        
                        
                        if selectedAccountID == transactionDict.value(forKey: "accountId") as! Int
                        {
                            let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID))
                            if let workoutsAccount = workoutsAccount.first
                            {
                                try! uiRealm.write
                                {
                                    if strCategoryType == "Expense"
                                    {
                                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - (Double(transactionDict.value(forKey: "amount") as! String)! - Double(txtAmount.text!)!)
                                    }
                                    else
                                    {
                                        workoutsAccount.amount = (workoutsAccount.amount - Double(transactionDict.value(forKey: "amount") as! String)!) + Double(txtAmount.text!)!
                                        workoutsAccount.remainigAmount = (workoutsAccount.remainigAmount - Double(transactionDict.value(forKey: "amount") as! String)!) + Double(txtAmount.text!)!
                                    }
                                    
                                }
                            }
                        }
                        else
                        {
                            //Main Account Update
                            let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (transactionDict.value(forKey: "accountId") as! Int))
                            if let workoutsAccount = workoutsAccount.first
                            {
                                try! uiRealm.write
                                {
                                    if strCategoryType == "Expense"
                                    {
                                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Double(txtAmount.text!)!
                                    }
                                    else
                                    {
                                        workoutsAccount.amount = workoutsAccount.amount - Double(txtAmount.text!)!
                                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(txtAmount.text!)!
                                    }
                                    
                                }
                            }
                            
                            //Change Account Update
                            let workoutsChangeAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID))
                            if let workoutsChangeAccount = workoutsChangeAccount.first
                            {
                                try! uiRealm.write
                                {
                                    if strCategoryType == "Expense"
                                    {
                                        workoutsChangeAccount.remainigAmount = workoutsChangeAccount.remainigAmount - (Double(txtAmount.text!)!)
                                    }
                                    else
                                    {
                                        workoutsChangeAccount.amount = workoutsChangeAccount.amount + Double(txtAmount.text!)!
                                        workoutsChangeAccount.remainigAmount = workoutsChangeAccount.remainigAmount + Double(txtAmount.text!)!
                                    }
                                    
                                }
                            }
                        }
                        
                        
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else
                {
                    self.alertView(alertMessage: AlertString.SelectCategory)
                }
            }
            else
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
                    }
        else
        {
            if(txtAmount.text != "0")
            {
                if(strSelectedCategory != "")
                {
                    let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int((dict["categoryID"] as? String)!)!)
                    
                    if let budgetWorkout = budgetUpdate.first
                    {
                        try! uiRealm.write
                        {
                            budgetWorkout.currentAmount = budgetWorkout.currentAmount + Double(txtAmount.text!)!
                        }
                    }
                    
                    let dict1 = NSMutableDictionary()
                    dict1.setValue(txtAmount.text, forKey: "amount")
                    dict1.setValue(dict["name"], forKey: "subcategoryName")
                    dict1.setValue(dict["categoryName"], forKey: "categoryName")
                    dict1.setValue(dict["image"], forKey: "subcategoryImage")
                    dict1.setValue(dict["categoryimage"], forKey: "categoryimage")
                    
                    if(txtAddNote.text == "Add Note..")
                    {
                        dict1.setValue("", forKey: "description")
                    }
                    else
                    {
                        dict1.setValue(txtAddNote.text, forKey: "description")
                    }
                    dict1.setValue(passingDate, forKey: "date")
                    dict1.setValue(dict["categoryType"], forKey: "categoryType")
                    dict1.setValue(dict["subCategoryID"], forKey: "subCategoryID")
//                    dict1.setValue(dict["isCategory"], forKey: "isCategory")
                    dict1.setValue(dict["categoryID"], forKey: "categoryID")
                    addToListArray.add(dict1)
                    
                    var reminderType = String()
                    
                    if(self.lblReminder.text == "Never")
                    {
                        reminderType = "0"
                    }
                    else if(self.lblReminder.text == "Daily")
                    {
                        reminderType = "1"
                    }
                    else if(self.lblReminder.text == "Weekly")
                    {
                        reminderType = "2"
                    }
                    else if(self.lblReminder.text == "Monthly")
                    {
                        reminderType = "3"
                    }
 
                    
                    for i in 0..<addToListArray.count
                    {
                        var taskListB = Transaction()
                        let autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
                        
                        let dictCat : NSMutableDictionary = addToListArray[i] as! NSMutableDictionary
                        print(dictCat)
                        if(autoCatId == nil)
                        {
                            
                            taskListB = Transaction(value: [1,dictCat.value(forKey: "categoryName"),dictCat.value(forKey: "categoryimage"),dictCat.value(forKey: "categoryID"),dictCat.value(forKey: "subCategoryID"),dictCat.value(forKey: "subcategoryName"),dictCat.value(forKey: "subcategoryImage"),dictCat.value(forKey: "amount"),dictCat.value(forKey: "date"),dictCat.value(forKey: "description"),selectedAccountID,dictCat.value(forKey: "categoryType"),reminderType,"0",dictCat.value(forKey: "date")])
                        }
                        else
                        {
                            let catId = (uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?)! + 1
                            taskListB = Transaction(value: [catId,dictCat.value(forKey: "categoryName"),dictCat.value(forKey: "categoryimage"),dictCat.value(forKey: "categoryID"),dictCat.value(forKey: "subCategoryID"),dictCat.value(forKey: "subcategoryName"),dictCat.value(forKey: "subcategoryImage"),dictCat.value(forKey: "amount"),dictCat.value(forKey: "date"),dictCat.value(forKey: "description"),selectedAccountID,dictCat.value(forKey: "categoryType"),reminderType,"0",dictCat.value(forKey: "date")])
                            
                        }
                        
                        try! uiRealm.write { () -> Void in
                            uiRealm.add(taskListB)
                        }
                        
                        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (selectedAccountID ))
                        if let workoutsAccount = workoutsAccount.first
                        {
                            try! uiRealm.write
                            {
                                if strCategoryType == "Expense"
                                {
                                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(txtAmount.text!)!
                                }
                                else
                                {
                                    workoutsAccount.amount = workoutsAccount.amount + Double(txtAmount.text!)!
                                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Double(txtAmount.text!)!
                                }
                                
                            }
                        }
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else
                {
                    self.alertView(alertMessage: AlertString.SelectCategory)
                }
            }
            else
            {
                self.alertView(alertMessage: AlertString.AddAmount)
            }
            
        }
    }
    
    @IBAction func categoryAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController//CategoryViewController
        vc.passingView = "addTransaction"
        if strCategoryType == "Expense"
        {
            vc.categoryType = "0"
        }
        else
        {
            vc.categoryType = "1"
        }
        vc.strCategoryID = strCategoryID
        self.present(vc, animated: true, completion: nil)
    }
    
    override var previewActionItems : [UIPreviewActionItem] {
        
        print(transactionDict)
        txtAmount.text = transactionDict.value(forKey: "amount") as? String
        if(transactionDict["subCategoryID"] as! String == "0")
        {
            txtCategoryName.text = transactionDict["categoryName"] as? String
            strSelectedCategory = transactionDict["categoryName"] as! String
            btnCategory_img.setBackgroundImage(UIImage(named: transactionDict["categoryImage"] as! String), for: .normal)
            img_category.image = UIImage(named: transactionDict["categoryImage"] as! String)
        }
        else
        {
            txtCategoryName.text = "\(transactionDict["categoryName"] as! String)/\(transactionDict["subCatName"] as! String)"
            strSelectedCategory = "\(transactionDict["categoryName"] as! String)/\(transactionDict["subCatName"] as! String)"
            btnCategory_img.setBackgroundImage(UIImage(named: transactionDict["categoryImage"] as! String), for: .normal)
            img_category.image = UIImage(named: transactionDict["subCatImage"] as! String)
        }
        
        txtDate.text = transactionDict["date"] as? String
        
        if transactionDict["note"] as! String != ""
        {
            txtAddNote.text = transactionDict["note"] as! String
        }
        
        dict = NSMutableDictionary()
        
        dict.setValue(transactionDict["categoryID"] as! String, forKey: "categoryID")
        dict.setValue(transactionDict["setType"] as! String, forKey: "categoryType")
        dict.setValue(transactionDict["subCatImage"] as! String, forKey: "image")
        dict.setValue(transactionDict["categoryImage"] as! String, forKey: "categoryimage")
        dict.setValue(transactionDict["subCatName"] as! String, forKey: "name")
        dict.setValue(transactionDict["categoryName"] as! String, forKey: "categoryName")
        dict.setValue(transactionDict["subCategoryID"] as! String, forKey: "subCategoryID")

        
        
        let deleteAction = UIPreviewAction(title: "Delete", style: .destructive) { (action, viewController) -> Void in
            print("You deleted the photo")
            let categoryList = uiRealm.objects(Transaction.self).filter("id = %@",self.transactionDict["id"] as! Int)
            try! uiRealm.write {
                uiRealm.delete(categoryList)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "detectAfterDeleteEvent"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "detectAfterDeleteEventOverview"), object: nil)
        }
        
        return [deleteAction]
        
    }
    
    @IBAction func btnReminderAction(_ sender: UIButton) {
                
        let optionMenu = UIAlertController(title: nil, message: "Choose Reminder", preferredStyle: .actionSheet)
        
        let NeverAction = UIAlertAction(title: "Never", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.lblReminder.text = "Never"
        })
        let DailyAction = UIAlertAction(title: "Daily", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.getDateIsProper(type: "Daily")
        })
        let WeeklyAction = UIAlertAction(title: "Weekly", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.getDateIsProper(type: "Weekly")
        })
        let MonthlyAction = UIAlertAction(title: "Monthly", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.getDateIsProper(type: "Monthly")
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(NeverAction)
        optionMenu.addAction(DailyAction)
        optionMenu.addAction(WeeklyAction)
        optionMenu.addAction(MonthlyAction)
        optionMenu.addAction(cancelAction)
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func getDateIsProper(type:String)
    {
        if(selectedDate1 < currentDate)
        {
            self.alertView(alertMessage: AlertString.PastDate)
            let stringDate = String.convertFormatOfDate(date:  Date())
                
            txtDate.text = stringDate
            selectedDate1 =  passingDate//Date()
        }
        else
        {
            lblReminder.text = type
        }
    }
 
    //MARK: - UIPickerview Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @available(iOS 2.0, *)
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        inputTaxRate.text = stateInfo[row].tax
        lblReminder.text = reminderArray[row]
        
        reminderPicker.removeFromSuperview()
        
     }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return reminderArray[row]
    }

    
    //MARK: - Textview Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add Note.." {
            textView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note.."
        }
    }
    
    //MARK: - Textfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtAmount = textField
    }
    
    /*
     
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        print(string)
        
        
        if(textField == txtAmount)
        {
            let newText = (txtAmount.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.characters.count
            
            if numberOfChars <= 10
            {
                if((txtAmount.text?.characters.count)! == 1)
                {
                    if(txtAmount.text == "0")
                    {
                        if(string == "0")
                        {
                            return false
                        }
                        else if(string == "")
                        {
                            return false
                        }
                        else
                        {
                            txtAmount.text = ""
                            //                        txtAmount.text = string
                            return true
                        }
                    }
                    else
                    {
                        if(string == "")
                        {
                            txtAmount.text = "0"
                            return false
                        }
                        return true
                    }
                }
                else
                {
                    let newCharacters = CharacterSet(charactersIn: string)
                    let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacters)
                    if boolIsNumber == true {
                        return true
                    } else {
                        if string == "." {
                            let countdots = txtAmount.text!.components(separatedBy: ".").count - 1
                            let array = txtAmount.text?.characters.map { String($0) }
                            var decimalCount = 0
                            for character in array! {
                                if character == "." {
                                    decimalCount += 1
                                }
                            }
                            
                            if countdots == 0 {
                                return true
                            } else {
                                if countdots > 0 && string == "." {
                                    return false
                                } else {
                                    if decimalCount == 2 {
                                        return true
                                    } else {
                                        return false
                                    }
                                    //                                return true
                                }
                            }
                        } else {
                            return false
                        }
                    }
                }
            }
            else
            {
                return false
            }
            

        }
        else
        {
            return true
        }
        
        
        
    }
    */
    
    //MARK: - JBDatePicker Delegate
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            
            if(lblReminder.text != "Never")
            {
                if(selectedDate <= currentDate)
                {
                    self.alertView(alertMessage: AlertString.PastDate)
                }
                else
                {
                    txtDate.text = stringDate
                    selectedDate1 = selectedDate
                    passingDate = selectedDate1
                }
            }
            else
            {
                txtDate.text = stringDate
                selectedDate1 = selectedDate
                passingDate = selectedDate1
            }
            removeAnimate()
        }
    }
    
//    func shouldAllowSelectionOfDay(_ date: Date?) -> Bool {
//
//        //this code example disables selection for dates older then today
//        guard let date = date else {return true}
//        let comparison = NSCalendar.current.compare(date, to: Date().stripped()!, toGranularity: .day)
//
//        if comparison == .orderedAscending {
//            return false
//        }
//        return true
//    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {

        if let date = dateToSelect {
            return date
        }
        else{
            return Date() //passingDate/
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
}
