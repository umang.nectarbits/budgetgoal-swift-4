//
//  CurrencyConverterVC.swift
//  Created by Cools on 11/17/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds
import IQKeyboardManagerSwift
import UserNotifications
import UserNotificationsUI


class currencyCell: UITableViewCell {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblCurrencyCode : UIButton!
    @IBOutlet var btnSelectCurrency : UIButton!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var imgFlag : UIButton!
    @IBOutlet var lblCurrencySymbol : UILabel!
    @IBOutlet var BgView: UIView!
}

class CurrencyConverterVC: UIViewController,CalculatorDelegate,UITextFieldDelegate,
GADBannerViewDelegate {
    
    @IBOutlet var HeaderView : UIView!
    @IBOutlet var lblCurrencyConverter : UILabel!
    @IBOutlet weak var calculatorView : CalculatorKeyboard!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imgFirstFlag : UIImageView!
    @IBOutlet var imgSecondFlag : UIImageView!
    @IBOutlet var txtFirstAmount: UITextField!
    @IBOutlet var txtSecondAmount: UITextField!
    @IBOutlet var lblFirstCurrencySymbol : UILabel!
    @IBOutlet var lblSecondCurrencySymbol : UILabel!
    @IBOutlet var btnFirstSelectCurrency : UIButton!
    @IBOutlet var btnSecondSelectCurrency : UIButton!
    @IBOutlet var bgView : UIView!
    @IBOutlet var cardView : UIView!
    
    var currencyArray = [[String:AnyObject]]()
    var strCurrency : String = ""
    var secondCurrencyAmount = 0.0
    var thirdCurrencyAmount = 0.0
    var mainSeletedCurrency = "INR"
    var isExchange = ""
    var progress = MBProgressHUD()
    var getCurrency = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HeaderView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        lblCurrencyConverter.font = headerFont
        lblCurrencyConverter.textColor = UIColor.white
        
        bgView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
        bgView.layer.shadowOffset = CGSize(width: 0, height: 3)
        bgView.layer.cornerRadius = 8
        bgView.layer.shadowOpacity = 0.7
        bgView.layer.shadowRadius = 6
        
        cardView.layer.cornerRadius = 5
        cardView.layer.masksToBounds = true
        
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        let currentLocale = NSLocale.current
        let countryCodeTemp = currentLocale.localizedString(forCurrencyCode: "USA")//currentLocale.objectForKey(NSLocaleCountryCode) as? String
        let countrySymbol = currentLocale.currencySymbol
        
        txtFirstAmount.text = "1"
        print(countryCodeTemp)
        print(countrySymbol)
        
        isExchange = "0"
        
        getCountryDict(countryName: "India")
        getCountryDict(countryName: "United States")
        
        txtFirstAmount.delegate = self
        imgFirstFlag.image = (UIImage(named: currencyArray[0]["currencyFlag"] as! String))
        lblFirstCurrencySymbol.text = currencyArray[0]["currency"] as? String
        
        txtSecondAmount.delegate = self
        imgSecondFlag.image = (UIImage(named: currencyArray[1]["currencyFlag"] as! String))
        lblSecondCurrencySymbol.text = currencyArray[1]["currency"] as? String
        
        getCurrentConverter(strFirstCurrency: currencyArray[0]["currency"] as! String, strsecondCurrency: currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 999)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(CurrencyConverterViewController.showSpinningWheel(notification:)), name: NSNotification.Name(rawValue: "getCountryCurrency"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CurrencyConverterVC.showSpinningWheel(notification:)), name: Notification.Name(rawValue: "getCountryCurrency"), object: nil)
        
        // Do any additional setup after loading the view.
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        txtFirstAmount.becomeFirstResponder()
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    
    @objc func showSpinningWheel(notification: NSNotification) {
        let dict = notification.object as! [String:AnyObject]
        
        if dict["currencyNumber"] as! Int == 0
        {
            print("Index 0 : \(dict)")
            currencyArray[0]["currencyFlag"] = "\(String(describing: dict["currencyFlag"]!)).png" as AnyObject
            currencyArray[0]["currencyCode"] = dict["currencyCode"]
            currencyArray[0]["name"] = dict["name"]
            currencyArray[0]["currency"] = dict["currency"]
            mainSeletedCurrency = dict["currency"] as! String
            self.btnFirstSelectCurrency.tag = 1
            getCurrentConverter(strFirstCurrency: currencyArray[0]["currency"] as! String, strsecondCurrency: currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 0)
        }
        else
        {
            print("Index 1 : \(dict)")
            currencyArray[1]["currencyFlag"] = "\(String(describing: dict["currencyFlag"]!)).png" as AnyObject
            currencyArray[1]["currencyCode"] = dict["currencyCode"]
            currencyArray[1]["name"] = dict["name"]
            currencyArray[1]["currency"] = dict["currency"]
            //            mainSeletedCurrency = dict["currency"] as! String
            self.btnSecondSelectCurrency.tag = 2
            getCurrentConverter(strFirstCurrency: currencyArray[0]["currency"] as! String, strsecondCurrency: currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 1)
        }
    }
    
    func getCurrentConverter(strFirstCurrency: String, strsecondCurrency: String, strthirdCurrency: String, index : Int)
    {
        let rechability = Reachability()!
        
        if rechability.isReachableViaWiFi || rechability.isReachableViaWWAN    {
            let config = URLSessionConfiguration.default // Session Configuration
            let session = URLSession(configuration: config) // Load configuration into Session
            //            let url = URL(string: "https://free.currencyconverterapi.com/api/v4/convert?q=\(strFirstCurrency)_\(strsecondCurrency),\(strFirstCurrency)_\(strthirdCurrency)&compact=y")!
            //            let url = URL(string: "https://free.currconv.com/api/v7/convert?q=\(strFirstCurrency)_\(strsecondCurrency),\(strFirstCurrency)_\(strthirdCurrency)&compact=ultra&apiKey=7b3d44ba16278d9b202c")!
            
            progress = MBProgressHUD.showAdded(to: view, animated: true)
            let url = URL(string: "https://free.currconv.com/api/v7/convert?q=\(strFirstCurrency)_\(strsecondCurrency)&compact=ultra&apiKey=7b3d44ba16278d9b202c")!
            
            let task = session.dataTask(with: url, completionHandler: {
                (data, response, error) in
                
                if error != nil {
                    DispatchQueue.main.async {
                        self.progress.hide(animated: true)
                    }
                    print(error!.localizedDescription)
                    
                } else {
                    
                    do {
                        
                        if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : AnyObject]
                        {
                            //Implement your logic
                            print(json)
                            if (json["error"] != nil){
                                
                                DispatchQueue.main.async {
                                    self.progress.hide(animated: true)
                                    self.txtFirstAmount.resignFirstResponder()
                                    let alert = UIAlertController(title: "Alert", message: json["error"]! as? String, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.progress.hide(animated: true)
                                    if json.count > 0
                                    {
                                        if(self.txtFirstAmount.text == "1"){
                                            self.currencyArray[1]["amount"] = Double("1")! * Double(String(describing: (json["\(strFirstCurrency)_\(strsecondCurrency)"]!)))! as AnyObject
                                            self.secondCurrencyAmount = (Double(String(describing: (json["\(strFirstCurrency)_\(strsecondCurrency)"]!))) as AnyObject) as! Double
                                            
                                            self.txtSecondAmount.text = String(format: "%.2f", Double("\(self.currencyArray[1]["amount"]!)")!)
                                            self.txtFirstAmount.becomeFirstResponder()
                                        }else{
                                            self.currencyArray[1]["amount"] = Double(String(describing: (json["\(strFirstCurrency)_\(strsecondCurrency)"]!))) as AnyObject
                                            self.secondCurrencyAmount = (Double(String(describing: (json["\(strFirstCurrency)_\(strsecondCurrency)"]!))) as AnyObject) as! Double
                                            
                                            self.txtSecondAmount.text = String(format: "%.2f", Double(self.txtFirstAmount.text!)!*Double("\(self.currencyArray[1]["amount"]!)")!)
                                            
                                        }
                                        
                                        if(index == 0){
                                            self.txtFirstAmount.delegate = self
                                            self.imgFirstFlag.image = (UIImage(named: self.currencyArray[0]["currencyFlag"] as! String))
                                            self.lblFirstCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                                            self.btnFirstSelectCurrency.tag = 1
                                            
                                        }else if(index == 1){
                                            
                                            self.txtSecondAmount.delegate = self
                                            self.imgSecondFlag.image = (UIImage(named: self.currencyArray[1]["currencyFlag"] as! String))
                                            self.lblSecondCurrencySymbol.text = self.currencyArray[1]["currency"] as? String
                                            self.btnSecondSelectCurrency.tag = 2
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            self.progress.hide(animated: true)
                        }
                        print("error in JSONSerialization")
                        
                    }
                    
                }
                
            })
            task.resume()
        } else {
            alertView(alertMessage: AlertString.CurrencyConverter)
        }
        
    }
    
    func getCountryDict(countryName: String)
    {
        let tempCountryArray = (IsoCountries.allCountries as NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<tempCountryArray.count
        {
            let temp = tempCountryArray[i] as! IsoCountryInfo
            if(countryName == temp.name)
            {
                
                let currencyCode = getCurrencySymbol(currencyCode: temp.currency)
                strCurrency = currencyCode
                var tempDict = [String:AnyObject]()
                tempDict["currencyFlag"] = "\(temp.alpha2).png" as AnyObject
                tempDict["currencyCode"] = strCurrency as AnyObject
                tempDict["name"] = temp.name as AnyObject
                tempDict["currency"] = temp.currency as AnyObject
                tempDict["amount"] = "1" as AnyObject
                currencyArray.append(tempDict)
            }
        }
    }
    
    //MARK:- Helper Method
    func getCurrencySymbol(currencyCode : String) -> String
    {
        
        var currencySymbol: String = ""
        
        if(currencyCode == "GGP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "IMP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "JEP")
        {
            currencySymbol = "£"
        }
        else if(currencyCode == "TVD")
        {
            currencySymbol = "A$"
        }
        else{
            let locale = NSLocale(localeIdentifier: currencyCode)
            currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
            print("Currency Symbol : \(currencySymbol)")
        }
        
        return currencySymbol
    }
    
    func calculator(_ calculator: CalculatorKeyboard, didChangeValue value: String) {
        
        if value == "0"
        {
            txtFirstAmount.text = ""
            txtFirstAmount.placeholder = "1"
            
            currencyArray[1]["amount"] = Double("1")! * secondCurrencyAmount as AnyObject
            
            currencyArray[2]["amount"] = Double("1")! * thirdCurrencyAmount as AnyObject
        }
        else
        {
            txtFirstAmount.text = value
            currencyArray[1]["amount"] = Double(value)! * secondCurrencyAmount as AnyObject
            
            currencyArray[2]["amount"] = Double(value)! * thirdCurrencyAmount as AnyObject
        }
        
        
        
        //        txtAmount.text = value
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(string)
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print(txtAfterUpdate)
        if txtAfterUpdate.count == 0 {
            self.txtSecondAmount.text = String(format: "%.2f", Double(1)*Double("\(self.currencyArray[1]["amount"]!)")!)
        }else{
            self.txtSecondAmount.text = String(format: "%.2f", Double(txtAfterUpdate)!*Double("\(self.currencyArray[1]["amount"]!)")!)
        }
        return true
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }
        
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
    }
    
    
    //MARK: - Action Method
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func btnExchangeTAP(_ sender: UIButton) {
        
        if(self.txtFirstAmount.text == "1" || self.txtFirstAmount.text == "0.01"){
            if isExchange == "0"{
                print("Before : \(self.currencyArray)")
                self.currencyArray.swapAt(0, 1)
                print("After : \(self.currencyArray)")
                
                self.imgFirstFlag.image = (UIImage(named: self.currencyArray[0]["currencyFlag"] as! String))
                self.lblFirstCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                self.imgSecondFlag.image = (UIImage(named: self.currencyArray[1]["currencyFlag"] as! String))
                self.lblSecondCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                getCurrentConverter(strFirstCurrency: self.currencyArray[0]["currency"] as! String, strsecondCurrency: self.currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 999)
                isExchange = "1"
                
            }else{
                print("Before : \(self.currencyArray)")
                self.currencyArray.swapAt(0, 1)
                print("After : \(self.currencyArray)")
                
                self.imgFirstFlag.image = (UIImage(named: self.currencyArray[0]["currencyFlag"] as! String))
                self.lblFirstCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                self.imgSecondFlag.image = (UIImage(named: self.currencyArray[1]["currencyFlag"] as! String))
                self.lblSecondCurrencySymbol.text = self.currencyArray[1]["currency"] as? String
                getCurrentConverter(strFirstCurrency: self.currencyArray[0]["currency"] as! String, strsecondCurrency: self.currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 999)
                isExchange = "0"
            }
        }else{
            if isExchange == "0"{
                print("Before : \(self.currencyArray)")
                self.currencyArray.swapAt(0, 1)
                print("After : \(self.currencyArray)")
                
                self.imgFirstFlag.image = (UIImage(named: self.currencyArray[0]["currencyFlag"] as! String))
                self.lblFirstCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                self.imgSecondFlag.image = (UIImage(named: self.currencyArray[1]["currencyFlag"] as! String))
                self.lblSecondCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                getCurrentConverter(strFirstCurrency: self.currencyArray[0]["currency"] as! String, strsecondCurrency: self.currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 999)
                isExchange = "1"
            }else{
                print("Before : \(self.currencyArray)")
                self.currencyArray.swapAt(0, 1)
                print("After : \(self.currencyArray)")
                
                self.imgFirstFlag.image = (UIImage(named: self.currencyArray[0]["currencyFlag"] as! String))
                self.lblFirstCurrencySymbol.text = self.currencyArray[0]["currency"] as? String
                self.imgSecondFlag.image = (UIImage(named: self.currencyArray[1]["currencyFlag"] as! String))
                self.lblSecondCurrencySymbol.text = self.currencyArray[1]["currency"] as? String
                getCurrentConverter(strFirstCurrency: self.currencyArray[0]["currency"] as! String, strsecondCurrency: self.currencyArray[1]["currency"] as! String, strthirdCurrency: "", index : 999)
                isExchange = "0"
            }
        }
    }
    
    @IBAction func btnFirstSelectedCountryAction(_ sender: UIButton){
        btnFirstSelectCurrency.tag = 1
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CurrencyViewController") as! CurrencyViewController
        vc.selectedCurrency = mainSeletedCurrency
        vc.currencyNumber = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSecondSelectedCountryAction(_ sender: UIButton){
        btnSecondSelectCurrency.tag = 2
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CurrencyViewController") as! CurrencyViewController
        vc.currencyNumber = 1
        vc.selectedCurrency = mainSeletedCurrency
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
