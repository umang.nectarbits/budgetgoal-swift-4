//
//  TransferViewController.swift
//  Created by harshesh on 06/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class TransferViewController: UIViewController,JBDatePickerViewDelegate,GADBannerViewDelegate {
    
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var scrvTransfer: UIScrollView!
    @IBOutlet var SelectAccountView: UIView!
    @IBOutlet var SecondView: UIView!
    @IBOutlet var InfoView: UIView!
    @IBOutlet var btnformAccount: UIButton!
    @IBOutlet var btnToAccount: UIButton!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var txtNote: UITextView!
    @IBOutlet var btnTransfer: UIButton!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var headerView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var btnSelectDate: UIButton!
    @IBOutlet var lblFromAccountTitle: UILabel!
    @IBOutlet var lblFromAccount: UILabel!
    @IBOutlet var lblRemainingAccountTitle: UILabel!
    @IBOutlet var lblRemainingAccount: UILabel!
    @IBOutlet var lblToAccountTitle: UILabel!
    @IBOutlet var lblToRemainingAccountTitle: UILabel!
    @IBOutlet var lblAmountTitle: UILabel!
    @IBOutlet var lblDateTitle: UILabel!
    @IBOutlet var lblNoteTitle: UILabel!
    @IBOutlet var lblRemAccount: UILabel!
    @IBOutlet var lblToAccount: UILabel!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var ScrlBg: UIView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var selectedDate1 = Date()
    var dateToSelect: Date!
    var currentDate = Date()
    var fromDict = [String:AnyObject]()
    var toDict = [String:AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

//        IQKeyboardManager.sharedManager().enable = true
        setFont()
        self.calendarView.isHidden = true
        self.bgView.isHidden = true
        selectedDate1 =  Date()
        btnSelectDate.setTitle(String.convertFormatOfDate(date: Date()), for: .normal)
        datePickerView.delegate = self
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription
        txtAmount.addDoneButtonToKeyboard(myAction:  #selector(txtAmount.resignFirstResponder))
        
        headerView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(TransferViewController.setAccountData(notification:)), name: NSNotification.Name(rawValue: "getAccountData"), object: nil)

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewWillLayoutSubviews() {
        
        ScrlBg.viewShadow()
        btnTransfer.viewShadow()
//        SelectAccountView.viewShadow()
//        SecondView.viewShadow()
//        InfoView.viewShadow()
//        headerView.viewShadow()
    }

    
    //MARK:- Helper Method
    
    @objc func setAccountData(notification: NSNotification) {
        let dict = notification.object as! [String:AnyObject]
        if dict["transferAccount"] as! String == "From"
        {
            print(dict["AccountDetail"]!)
            fromDict = dict["AccountDetail"] as! [String : AnyObject]
            lblFromAccount.text = fromDict["name"] as? String
            lblFromAccount.textColor = UIColor.lightGray
            lblRemainingAccount.text = String(fromDict["remainigAmount"] as! Double)
            
        }
        else if dict["transferAccount"] as! String == "To"
        {
            print(dict["AccountDetail"]!)
            toDict = dict["AccountDetail"] as! [String : AnyObject]
            lblToAccount.text = toDict["name"] as? String
            lblToAccount.textColor = UIColor.lightGray
            lblRemAccount.text = String(toDict["remainigAmount"] as! Double)
        }
    }
    
    func setFont()
    {
        btnSelectDate.titleLabel?.font = tableHeaderFont
        lblHeaderTitle.font = headerTitleFont
        
        lblFromAccountTitle.font = tableHeaderFont
        lblFromAccount.font = tableHeaderFont
        lblRemainingAccountTitle.font = smallFont
        lblRemainingAccount.font = tableHeaderFont
        lblToAccountTitle.font = tableHeaderFont
        lblToRemainingAccountTitle.font = smallFont
        lblAmountTitle.font = tableHeaderFont
        lblDateTitle.font = tableHeaderFont
        lblNoteTitle.font = tableHeaderFont
        lblRemAccount.font = tableHeaderFont
        lblToAccount.font = tableHeaderFont
        txtNote.font = tableHeaderFont
        txtAmount.font = tableHeaderFont
        btnTransfer.titleLabel?.font = mediumBiggerFont
        txtNote.text = "Add Note.."
        
        lblHeaderTitle.textColor = UIColor.white
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
    }
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    
    //MARK: - JBDatePicker Delegate
    
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            
            btnSelectDate.setTitle(stringDate, for: .normal)
            selectedDate1 = selectedDate
            removeAnimate()
        }
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        BottomHeightConstraint.constant = 0
    }
    
    //MARK:- Action Method
    
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFromAccountSelectAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        vc.accountSelection = "From"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnToAccountSelectAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        vc.accountSelection = "To"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnTransferAction(_ sender: UIButton) {
        
        if txtAmount.text != ""
        {
            if (txtAmount.text != "0")
            {
                if (Double(txtAmount.text!)! <= Double(lblRemainingAccount.text!)!)
                {
                        //Add Transfer Data
                        var taskListB = Transfer()
                        let autoCatId = uiRealm.objects(Transfer.self).max(ofProperty: "id") as Int?
                        
                        if(autoCatId == nil)
                        {
                            taskListB = Transfer(value: [1,lblFromAccount.text!,lblToAccount.text!,Double(txtAmount.text!)!,Double(txtAmount.text!)!,selectedDate1,txtNote.text!])
                        }
                        else
                        {
                            let catId = (uiRealm.objects(Transfer.self).max(ofProperty: "id") as Int?)! + 1
                            taskListB = Transfer(value: [catId,lblFromAccount.text!,lblToAccount.text!,Double(txtAmount.text!)!,Double(txtAmount.text!)!,selectedDate1,txtNote.text!])
                        }
                        try! uiRealm.write { () -> Void in
                            uiRealm.add(taskListB)
                        }
                        
                        //Update Value in Account From to Account To
                        //Account From Update
                        let workoutsFrom = uiRealm.objects(Account.self).filter("id = %@", (fromDict["id"] as! Int))
                        
                        if let workoutsFrom = workoutsFrom.first
                        {
                            try! uiRealm.write
                            {
                                workoutsFrom.amount = workoutsFrom.amount - Double(txtAmount.text!)!
                                workoutsFrom.remainigAmount = workoutsFrom.remainigAmount - Double(txtAmount.text!)!
                            }
                            
                        }
                        //Account To Update
                        let workoutsTo = uiRealm.objects(Account.self).filter("id = %@", (toDict["id"] as! Int))
                        
                        if let workoutsTo = workoutsTo.first
                        {
                            try! uiRealm.write
                            {
                                workoutsTo.amount = workoutsTo.amount + Double(txtAmount.text!)!
                                workoutsTo.remainigAmount = workoutsTo.remainigAmount + Double(txtAmount.text!)!
                            }
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                else
                {
                self.alertView(alertMessage: "Enter Value Less then Total Amount")
                }
            }
            else
            {
                self.alertView(alertMessage: AlertString.GreaterZero)
            }
        }
         else
         {
            alertView(alertMessage: AlertString.AddAmount)
         }
    }

    //MARK: - Textview Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add Note.." {
            textView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note.."
        }
    }
}
