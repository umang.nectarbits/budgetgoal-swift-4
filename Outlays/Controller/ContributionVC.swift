//
//  ContributionVC.swift
//  Created by harshesh on 15/06/1940 Saka.
//  Copyright © 1940 Cools. All rights reserved.
//

import UIKit

class ContributionVC: UIViewController, JBDatePickerViewDelegate,UITextFieldDelegate {

    @IBOutlet var txtAMount: UITextField!
    @IBOutlet var txtNote: UITextField!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var btnSelectDate: UIButton!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var bgView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var lblMonth : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var contributDetailView : UIView!

    var selectedDate1 = Date()
    var dateToSelect: Date!
    var getpassingDate : Date!
    var setGoalDate : Date!
    var strData = ""
    var getgoalId = ""
    var passingView = ""
    var contributEditInfo = [String: AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLayout()
        
         UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.calendarView.isHidden = true
        self.bgView.isHidden = true
        datePickerView.delegate = self
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription
        lblMonth.layer.cornerRadius = 5
        lblMonth.layer.masksToBounds = true
        lblDate.layer.cornerRadius = 20
        lblDate.layer.masksToBounds = true
        // Do any additional setup after loading the view.
        //Edit View
        if passingView == "Edit"
        {
            txtAMount.text = "\(contributEditInfo["amount"]!)"
            txtNote.text = contributEditInfo["note"] as? String
            let getDate = String.convertFormatOfStringToDate(date: "\(contributEditInfo["createDate"]!)")//"\(editData["createDate"]!)"
            let editDate = String.convertFormatOfDateToStringSpecificFormat(date: getDate!, format: "dd MMM")
            self.lblMonth.text = self.sanitizeDateString(editDate!, format: "MMMM")
            self.lblDate.text = self.sanitizeDateString(editDate!, format: "dd")
        }
        else
        {
            let tempDate = String.convertFormatOfDateToStringSpecificFormat(date: datePickerView.selectedDateView.date!, format: "dd MMM")
            self.lblMonth.text = self.sanitizeDateString(tempDate!, format: "MMMM")
            self.lblDate.text = self.sanitizeDateString(tempDate!, format: "dd")
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    //MARK:- Helper Method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
    }
    
    func setLayout()
    {
        //View
        contributDetailView.layer.cornerRadius = 10
        contributDetailView.layer.masksToBounds = true
        contributDetailView.layer.shadowColor = UIColor.lightGray.cgColor
        contributDetailView.layer.masksToBounds = false
        contributDetailView.layer.shadowOpacity = 1
        contributDetailView.layer.shadowRadius = 3
        contributDetailView.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }

    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
                self.strData = String.convertFormatOfDateToStringSpecificFormat(date: self.selectedDate1, format: "dd MMM")
                self.lblMonth.text = self.sanitizeDateString(self.strData, format: "MMMM")
                self.lblDate.text = self.sanitizeDateString(self.strData, format: "dd")
                
            }
        });
    }
    
    func sanitizeDateString(_ dateString:String, format:String) -> String?
    {
        let dateFormatter = DateFormatter()
        if format == "MMMM"
        {
            dateFormatter.dateFormat = "dd MMM"
            let newFormat = DateFormatter()
            newFormat.dateFormat = "MMMM"
            if let date = dateFormatter.date(from: dateString)
            {
                print(date)
                return newFormat.string(from: date)
            }
            else
            {
                return nil
            }
        }
        else{
            dateFormatter.dateFormat = "dd MMM"
            let newFormat = DateFormatter()
            newFormat.dateFormat = "dd"
            if let date = dateFormatter.date(from: dateString)
            {
                print(date)
                return newFormat.string(from: date)
            }
            else
            {
                return nil
            }
        }
    }
    
    //MARK: - JBDatePicker Delegate
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            selectedDate1 = selectedDate
            removeAnimate()
        }
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }

    
    //MARK:- Action Method
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }

    @available(iOS 10.0, *)
    @IBAction func btnDoneTAP(_ sender: Any)
    {
        if passingView == "Edit"
        {
            let workouts = uiRealm.objects(Contribution.self).filter("contributid = %@", (contributEditInfo["contributid"] as! Int))

            if let workout = workouts.first
            {
                try! uiRealm.write
                {
                    workout.note = String(txtNote.text!)
                    workout.amount = Int(txtAMount.text!)!
                    if (workout.amount == 0)
                    {
                        self.alertView(alertMessage: "Do Not Add Zero Value")
                    }
                    
                    if (setGoalDate! <= selectedDate1)
                    {
                        workout.createDate = selectedDate1
                    }
                    else
                    {
                        self.alertView(alertMessage: "You can not select past date for Goal")
                    }
                    
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is SetGoalsVC {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
//                    self.navigationController?.popToRootViewController(animated: true)
                }
//                self.alertView(alertMessage: AlertString.SucessFullyUpdate)
            }
        }
        else
        {
            var contributInfo = Contribution()
            let autoCatId = uiRealm.objects(Contribution.self).max(ofProperty: "contributid") as Int?
            
            if(autoCatId == nil)
            {
                if (txtAMount.text! == "" && txtNote.text! == "")
                {
                    self.alertView(alertMessage: "Please Add Amount And ")
                }
                    
                else if (txtAMount.text! == "0")
                {
                    self.alertView(alertMessage: "Do Not Add Zero Value")
                    return
                }

                else if (txtNote.text! == "")
                {
                    self.alertView(alertMessage: "Please Add Note")
                    return
                }

                else
                {
                    if (setGoalDate! <= selectedDate1)
                    {
                        contributInfo = Contribution(value: [1,Int(getgoalId)!,Int(txtAMount.text!)!,String(txtNote.text!),selectedDate1])
                        
                        try! uiRealm.write{ () -> Void in
                            uiRealm.add(contributInfo)
                            //                self.navigationController?.popToRootViewController(animated: true)
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                            for aViewController in viewControllers {
                                if aViewController is SetGoalsVC {
                                    self.navigationController!.popToViewController(aViewController, animated: true)
                                }
                            }
                        }
                    }
                    else
                    {
                        self.alertView(alertMessage: "You can not select past date for Goal")
                    }
                }
            }
                
            else
            {
                if (txtAMount.text! == "" && txtNote.text! == "")
                {
                    self.alertView(alertMessage: "Please Add Value")
                }

                else if (txtAMount.text! == "0")
                {
                    self.alertView(alertMessage: "Do Not Add Zero Value")
                    return
                }

                else if (txtNote.text! == "")
                {
                    self.alertView(alertMessage: "Please Add Note")
                    return
                }

                else if (setGoalDate! <= selectedDate1)
                {
                    let catId = (uiRealm.objects(Contribution.self).max(ofProperty: "contributid") as Int?)! + 1
                    contributInfo = Contribution(value: [catId,Int(getgoalId)!,Int(txtAMount.text!)!,"\(txtNote.text!)",selectedDate1])
                    
                    try! uiRealm.write{ () -> Void in
                        uiRealm.add(contributInfo)
                        //                self.navigationController?.popToRootViewController(animated: true)
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        for aViewController in viewControllers {
                            if aViewController is SetGoalsVC {
                                self.navigationController!.popToViewController(aViewController, animated: true)
                            }
                        }
                    }
                }
                else
                {
                    self.alertView(alertMessage: "You can not select past date for Goal")
                }
            }
            

        }
    }
    
    @IBAction func btnCancelTAP(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fulltext = textField.text! + (string)
        if fulltext.count == 2 {
            if (((fulltext as? NSString)?.substring(to: 1)) == "0") && !(fulltext == "0.") {
                textField.text = (fulltext as NSString).substring(with: NSRange(location: 1, length: 1))
                return false
            }
        }
        return true
    }
}
