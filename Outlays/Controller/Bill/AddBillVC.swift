//
//  AddBillVC.swift
//  Budget Goals
//
//  Created by nectarbits on 6/13/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit
import DropDown

class AddBillVC: UIViewController, UITextFieldDelegate{

    @IBOutlet var HeaderView : UIView!
    @IBOutlet var BillCategoryView : UIView!
    @IBOutlet var lblBillAccountTittle: UILabel!
    @IBOutlet var btnBillAccount: UIButton!
    @IBOutlet var lblBillCategoryTittle: UILabel!
    @IBOutlet var btnBillCategory: UIButton!
    @IBOutlet var lblBillNameTittle: UILabel!
    @IBOutlet var txtBillName: UITextField!
    @IBOutlet var lblBillAmountTittle: UILabel!
    @IBOutlet var txtBillAmount: UITextField!
    @IBOutlet var lblBillCycleTittle: UILabel!
    @IBOutlet var btnBillCycle: UIButton!
    @IBOutlet var lblBillDateTittle: UILabel!
    @IBOutlet var txtBillDate: UITextField!
    @IBOutlet var lblReminder: UILabel!

    @IBOutlet var btnback: UIButton!
    @IBOutlet var btnElectricity: UIButton!
    @IBOutlet var btnGas: UIButton!
    @IBOutlet var btnPhone: UIButton!
    @IBOutlet var btnTax: UIButton!
    @IBOutlet var btnOther: UIButton!
    @IBOutlet var btnReminder: UIButton!
    @IBOutlet var btnAddBill: UIButton!
    
    var selectedDate1 = Date()
    var CategoryImages = ["ic_electricity","ic_fuel","ic_call","ic_tax","ic_OtherCat"]
    var listAccountArray = [Account]()
    var AccountId = Int()
    var isEditBill = false
    var isFrom = ""
    var billDetail = NSMutableDictionary()
    var selectedCycleType = ""
    var selectedCategoryType = ""
    var MainBillUpdateData = NewBills()
    
    
    let AccountDropDown = DropDown()
    let CategoryDropDown = DropDown()
    let CycleDropDown = DropDown()
//    var CategoryDict = [String:AnyObject]()
    
    let categoryArray = ["Electricity","Gas","Phone","Tax","Other"]
    let cycleArray = ["Monthly","2 Months","3 Months","6 Months","12 Months"]
    var accountArray = [String]()
    var Maincategory = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if(UserDefaults.standard.value(forKey: "BillCategoryData") != nil)
        {
            Maincategory = UserDefaults.standard.value(forKey: "BillCategoryData")! as! NSDictionary
        }
        
        fetchAccounts()
        
        setupAccountDropDown()
        setupCycleDropDown()
        setupCategoryDropDown()
        
        if(isEditBill){
            if(isFrom == "MainBill"){
                btnAddBill.setTitle("UPDATE BILL", for: .normal)
                btnBillAccount.setTitle(listAccountArray[Int(MainBillUpdateData.accountID)-1].name, for: .normal)
                AccountId = MainBillUpdateData.accountID
                btnBillCategory.setTitle("\(MainBillUpdateData.BillSubCategoryName)", for: .normal)
                selectedCategoryType = MainBillUpdateData.BillSubCategoryID
                txtBillName.text = MainBillUpdateData.BillName
                txtBillAmount.text = "\(MainBillUpdateData.BillAmount)"
                btnBillCycle.setTitle(cycleArray[Int(MainBillUpdateData.BillCycle)! - 3], for: .normal)
                selectedCycleType = "\(MainBillUpdateData.BillCycle)"
                selectedDate1 = String.convertFormatOfStringToDate(date: (String.convertFormatOfDate(date: MainBillUpdateData.BillDate)))
                txtBillDate.text = String.convertFormatOfDate(date: selectedDate1)
            }else{
                btnAddBill.setTitle("UPDATE BILL", for: .normal)
                
                btnBillAccount.setTitle(listAccountArray[Int(billDetail["accountId"]! as! Int)-1].name, for: .normal)
                AccountId = billDetail["accountId"] as! Int
                btnBillCategory.setTitle("\(billDetail["subCatName"]!)", for: .normal)
                selectedCategoryType = billDetail["subCategoryID"]! as! String
                txtBillName.text = billDetail["note"]! as? String
                txtBillAmount.text = billDetail["amount"]! as? String
                btnBillCycle.setTitle(cycleArray[Int(billDetail["repeatType"]! as! String)! - 3], for: .normal)
                selectedCycleType = billDetail["repeatType"]! as! String
                selectedDate1 = String.convertFormatOfStringToDate(date: (billDetail["date"]! as? String)!)
                txtBillDate.text = String.convertFormatOfDate(date: selectedDate1)
            }
            
        }
        else{
            btnBillAccount.setTitle(listAccountArray[0].name, for: .normal)
            btnBillCategory.setTitle(categoryArray[0], for: .normal)
            btnBillCycle.setTitle(cycleArray[0], for: .normal)
            
            self.AccountId = self.listAccountArray[0].id
            self.selectedCategoryType = "\(1)"
            self.selectedCycleType = "\(3)"
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy h:mm a"
            let result = formatter.string(from: date)
            let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
            print("Todays Date:- \(TodayDate)")
            
            txtBillDate.text = String.convertFormatOfDate(date: TodayDate)
            selectedDate1 = TodayDate
        }
        
        HeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        let image = UIImage(named: "ic_uncheck")?.withRenderingMode(.alwaysTemplate)
        let image1 = UIImage(named: "ic_check")?.withRenderingMode(.alwaysTemplate)
        btnReminder.setImage(image, for: .normal)
        btnReminder.setImage(image1, for: .selected)
        btnReminder.tintColor = UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1)
        btnReminder.isSelected = true
        
        // Do any additional setup after loading the view.
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtBillDate){
            if(isEditBill){
                txtBillDate.resignFirstResponder()
                alertView(alertMessage: "Do not Change Bill Date")
            }
        }
        
    }
    
    
    //MARK:- Fetch Account
    func fetchAccounts(){
        
        let accounts = uiRealm.objects(Account.self)
        if(accounts.count != 0){
            
            for j in 0..<accounts.count
            {
                accountArray.append(accounts[j].name)
            }
            
            listAccountArray = accounts.toArray!
        }else{
            print("no acount Found")
        }
        
    }
    
    //MARK:- SetUp DropDown
    func setupAccountDropDown() {
        AccountDropDown.anchorView = btnBillAccount
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        AccountDropDown.dataSource = accountArray
        
        AccountDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.btnBillAccount.setTitle(item, for: .normal)
            
            self.AccountId = self.listAccountArray[index].id
            
        }
    }
    
    func setupCategoryDropDown() {
        CategoryDropDown.anchorView = btnBillCategory
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        CategoryDropDown.dataSource = categoryArray
        
        CategoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.btnBillCategory.setTitle(item, for: .normal)
            self.selectedCategoryType = "\(index + 1)"
        }
    }
    
    func setupCycleDropDown() {
        CycleDropDown.anchorView = btnBillCycle
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        CycleDropDown.dataSource = cycleArray
        
        CycleDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.btnBillCycle.setTitle(item, for: .normal)
            self.selectedCycleType = "\(index + 3)"
        }
    }
    
    //MARK:
    //MARK:- Validation
    func Validation() -> Bool{
        
        if(txtBillName.text == ""){
            alertView(alertMessage: "Please Enter Bill Name")
            return false
        }
        else if(txtBillAmount.text == "") {
            alertView(alertMessage: "Please Enter Bill Amount")
            return false
        }
        else if(btnBillCycle.titleLabel?.text == "") {
            alertView(alertMessage: "Please Enter Bill Cycle")
            return false
        }
        else if(txtBillDate.text == "") {
            alertView(alertMessage: "Please Enter Bill Date")
            return false
        }
        return true
    }
    
    //MARK:- Entry in NewBills Table
    func AddBilltoNewBills(){
        
        //for Entry in Transaction Table
        var NewBillsInfo = NewBills()
        var autoCatId = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
        
        if(autoCatId == nil)
        {
            autoCatId = 1
        }
        else
        {
            autoCatId = autoCatId! + 1
        }
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        print("Todays Date:- \(TodayDate)")
        
        NewBillsInfo = NewBills(value: [autoCatId!,
                                        Maincategory["categoryName"]!,
                                        Maincategory["categoryimage"]!,
                                        Maincategory["categoryID"]!,
                                        String((btnBillCategory.titleLabel?.text!)!),
                                        Maincategory["categoryimage"]!,
                                        String(selectedCategoryType),
                                        String(txtBillAmount!.text!),
                                        selectedDate1,
                                        String(txtBillName.text!),
                                        AccountId,
                                        1,
                                        0,
                                        selectedCycleType,
                                        selectedDate1
            ])
        
        
        try! uiRealm.write { () -> Void in
            uiRealm.add(NewBillsInfo)
            
//            self.navigationController?.popViewController(animated: true)
        }
        
        if selectedCycleType == "3" {
            for i in 0 ..< 12 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo = NewBills(value: [autoCatIdNew!,
                                                Maincategory["categoryName"]!,
                                                Maincategory["categoryimage"]!,
                                                Maincategory["categoryID"]!,
                                                String((btnBillCategory.titleLabel?.text!)!),
                                                Maincategory["categoryimage"]!,
                                                String(selectedCategoryType),
                                                String(txtBillAmount!.text!),
                                                Calendar.current.date(byAdding: .month, value: i + 1, to: selectedDate1)!,
                                                String(txtBillName.text!),
                                                AccountId,
                                                0,
                                                autoCatId!,
                                                selectedCycleType,
                                                selectedDate1
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo)
                    
                    //            self.navigationController?.popViewController(animated: true)
                }
            }
        }else if selectedCycleType == "4" {
            for i in 0 ..< 6 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo1 = NewBills(value: [autoCatIdNew!,
                                                Maincategory["categoryName"]!,
                                                Maincategory["categoryimage"]!,
                                                Maincategory["categoryID"]!,
                                                String((btnBillCategory.titleLabel?.text!)!),
                                                Maincategory["categoryimage"]!,
                                                String(selectedCategoryType),
                                                String(txtBillAmount!.text!),
                                                Calendar.current.date(byAdding: .month, value: (i*2) + 2, to: selectedDate1)!,
                                                String(txtBillName.text!),
                                                AccountId,
                                                0,
                                                autoCatId!,
                                                selectedCycleType,
                                                selectedDate1
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo1)
                    
                    //            self.navigationController?.popViewController(animated: true)
                }
            }
        }else if selectedCycleType == "5" {
            for i in 0 ..< 4 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo2 = NewBills(value: [autoCatIdNew!,
                                                Maincategory["categoryName"]!,
                                                Maincategory["categoryimage"]!,
                                                Maincategory["categoryID"]!,
                                                String((btnBillCategory.titleLabel?.text!)!),
                                                Maincategory["categoryimage"]!,
                                                String(selectedCategoryType),
                                                String(txtBillAmount!.text!),
                                                Calendar.current.date(byAdding: .month, value: (i*3) + 3, to: selectedDate1)!,
                                                String(txtBillName.text!),
                                                AccountId,
                                                0,
                                                autoCatId!,
                                                selectedCycleType,
                                                selectedDate1
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo2)
                    
                    //            self.navigationController?.popViewController(animated: true)
                }
            }
        }else if selectedCycleType == "6" {
            for i in 0 ..< 2 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo3 = NewBills(value: [autoCatIdNew!,
                                                Maincategory["categoryName"]!,
                                                Maincategory["categoryimage"]!,
                                                Maincategory["categoryID"]!,
                                                String((btnBillCategory.titleLabel?.text!)!),
                                                Maincategory["categoryimage"]!,
                                                String(selectedCategoryType),
                                                String(txtBillAmount!.text!),
                                                Calendar.current.date(byAdding: .month, value: (i*6) + 6, to: selectedDate1)!,
                                                String(txtBillName.text!),
                                                AccountId,
                                                0,
                                                autoCatId!,
                                                selectedCycleType,
                                                selectedDate1
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo3)
                    
                    //            self.navigationController?.popViewController(animated: true)
                }
            }
        }else if selectedCycleType == "7" {
            for i in 0 ..< 1 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo4 = NewBills(value: [autoCatIdNew!,
                                                Maincategory["categoryName"]!,
                                                Maincategory["categoryimage"]!,
                                                Maincategory["categoryID"]!,
                                                String((btnBillCategory.titleLabel?.text!)!),
                                                Maincategory["categoryimage"]!,
                                                String(selectedCategoryType),
                                                String(txtBillAmount!.text!),
                                                Calendar.current.date(byAdding: .month, value: (i*12) + 12, to: selectedDate1)!,
                                                String(txtBillName.text!),
                                                AccountId,
                                                0,
                                                autoCatId!,
                                                selectedCycleType,
                                                selectedDate1
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo4)
                    
                    //            self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Action Methods
    @IBAction func btnBackTap(_ sender: UIButton) {
        if(isEditBill){
            self.navigationController?.popViewController(animated: true)
            return
        }
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textFieldEditing(sender: UITextField) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        txtBillDate.text = dateFormatter.string(from: sender.date)
        selectedDate1 = sender.date
    }
    
    @IBAction func btnBillAccountTap(_ sender: UIButton) {
        if(!isEditBill){
            AccountDropDown.show()
        }
    }
    @IBAction func btnBillCategoryTap(_ sender: UIButton) {
        CategoryDropDown.show()
    }
    @IBAction func btnBillCycleTap(_ sender: UIButton) {
        if(!isEditBill){
            CycleDropDown.show()
        }
    }

    @IBAction func btnReminderTap(_ sender: UIButton) {
        if sender.isSelected{
            btnReminder.isSelected = false
        }
        else
        {
            btnReminder.isSelected = true
        }
    }
    
    @IBAction func btnAddBill(_ sender: UIButton) {
        
        if(isEditBill){
            
            if(Validation()){
                
                if(isFrom == "MainBill"){
                    let MainBillUpdate = uiRealm.objects(NewBills.self).filter("BillID = %@", MainBillUpdateData.BillID)
                    
                    if let MainBill = MainBillUpdate.first
                    {
                        try! uiRealm.write {
                            
                            let date = Date()
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd-MM-yyyy h:mm a"
                            let result = formatter.string(from: date)
                            let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
                            print("Todays Date:- \(TodayDate)")
                            
                            MainBill.BillUpdatedDate = TodayDate
                            MainBill.BillSubCategoryID = String(selectedCategoryType)
                            MainBill.BillSubCategoryName = String((btnBillCategory.titleLabel?.text!)!)
                            MainBill.BillName = String(txtBillName.text!)
                            MainBill.BillAmount = String(txtBillAmount!.text!)
                        }
                        
                        let UpcommingBills = uiRealm.objects(NewBills.self).filter("MainBillID=%@",MainBill.BillID)
                        
                        if(UpcommingBills.count != 0){
                            for i in 0 ..< UpcommingBills.count{
                                
                                try! uiRealm.write {
                                    UpcommingBills[i].BillUpdatedDate = MainBill.BillUpdatedDate
                                    UpcommingBills[i].BillSubCategoryID = String(selectedCategoryType)
                                    UpcommingBills[i].BillSubCategoryName = String((btnBillCategory.titleLabel?.text!)!)
                                    UpcommingBills[i].BillName = String(txtBillName.text!)
                                    UpcommingBills[i].BillAmount = String(txtBillAmount!.text!)
                                }
                                
                            }
                        }
                        
                    }
                    
                }else{
                    
                    let BillUpdate = uiRealm.objects(Transaction.self).filter("id = %@", billDetail["id"]!)
                    if let BillData = BillUpdate.first
                    {
                        //Update Transaction
                        try! uiRealm.write
                        {
                            
                            let date = Date()
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd-MM-yyyy h:mm a"
                            let result = formatter.string(from: date)
                            let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
                            print("Todays Date:- \(TodayDate)")
                            
                            //                        BillData.id = billDetail["id"]! as! Int
                            BillData.categoryName = (billDetail["categoryName"]! as? String)!
                            BillData.categoryImage = (billDetail["categoryImage"]! as? String)!
                            BillData.categoryID = (billDetail["categoryID"] as? String)!
                            BillData.subCategoryID = String(selectedCategoryType)
                            BillData.subCatName = String((btnBillCategory.titleLabel?.text!)!)
                            BillData.subCatImage = (billDetail["categoryImage"] as? String)!
                            BillData.amount = String(txtBillAmount!.text!)
                            BillData.date = selectedDate1
                            BillData.note = String(txtBillName.text!)
                            BillData.accountId = AccountId
                            BillData.setType = String("0")
                            BillData.repeatType = selectedCycleType
                            BillData.isUpdateByService = String("")
                            BillData.updatedDate = TodayDate
                            BillData.milliseconds = Double(0.0)
                            
                        }
                        
                        //For Deduct Amout From Selected Account
                        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (BillData.accountId))
                        if let workoutsAccount = workoutsAccount.first
                        {
                            print("Total:- \(workoutsAccount.remainigAmount + (Constant.TO_DOUBLE(billDetail["amount"]!) - Double(txtBillAmount!.text!)!))")
                            try! uiRealm.write
                            {
                                workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + (Constant.TO_DOUBLE(billDetail["amount"]!) - Double(txtBillAmount!.text!)!)
                            }
                        }
                    }
                }
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        else{
            if(Validation()){
                
                //for Entry in Transaction Table
                var BillInfo = Transaction()
                var autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
                
                if(autoCatId == nil)
                {
                    autoCatId = 1
                }
                else
                {
                    autoCatId = autoCatId! + 1
                }
                
                var reminder = false
                if(btnReminder.isSelected){
                    reminder = true
                }
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy h:mm a"
                let result = formatter.string(from: date)
                let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
                print("Todays Date:- \(TodayDate)")
                
                BillInfo = Transaction(value: [autoCatId!,
                                               Maincategory["categoryName"]!,
                                               Maincategory["categoryimage"]!,
                                               Maincategory["categoryID"]!,
                                               String(selectedCategoryType),
                                               String((btnBillCategory.titleLabel?.text!)!),
                                               Maincategory["categoryimage"]!,
                                               String(txtBillAmount!.text!),
                                               selectedDate1,
                                               String(txtBillName.text!),
                                               AccountId,
                                               String("0"),
                                               selectedCycleType,
                                               String(""),
                                               selectedDate1,
                                               Double(0.0)
                                               
                    ])
                
                try! uiRealm.write { () -> Void in
                    uiRealm.add(BillInfo)
                    
//                    self.navigationController?.popViewController(animated: true)
//                    self.dismiss(animated: true, completion: nil)
                }
                
                //For Deduct Amout From Selected Account
                let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (BillInfo.accountId))
                if let workoutsAccount = workoutsAccount.first
                {
                    try! uiRealm.write
                    {
                        workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(BillInfo.amount)!
                    }
                }
                
                //for Entry in Bill Table
                self.AddBilltoNewBills()
            }
        }
    }
}
