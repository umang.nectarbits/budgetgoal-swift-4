//
//  BillsVC.swift
//  Budget Goals
//
//  Created by nectarbits on 7/2/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit

class BillsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var HeaderView: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var tblBillList: UITableView!
    @IBOutlet var lblNoBillsFound: UILabel!
    
    var BillsData = [NewBills]()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.GetBills()
        self.tblBillList.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        HeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
//        self.GetBills()
//        self.tblBillList.tableFooterView = UIView(frame: .zero)
        // Do any additional setup after loading the view.
    }

    //MARK:- Get Bills
    func GetBills(){
        
        BillsData.removeAll()
        
        let Bill = uiRealm.objects(NewBills.self)
        if(Bill.count != 0){
            for i in 0 ..< Bill.count
            {
                if(Bill[i].isMain == 1){
                    BillsData.append(Bill[i])
                }
            }
            if(BillsData.count != 0){
                tblBillList.isHidden = false
                lblNoBillsFound.isHidden = true
                tblBillList.reloadData()
            }else{
                tblBillList.isHidden = true
                lblNoBillsFound.isHidden = false
            }
        }else{
            tblBillList.isHidden = true
            lblNoBillsFound.isHidden = false
        }
    }
    
    
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BillsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoanListCell", for: indexPath) as! LoanListCell
        
        cell.lblAmount.textColor = UIColor.red
        var Cycle = Int()
        if("\(BillsData[indexPath.row].BillCycle)" == "3"){
            Cycle = 1
        }else if("\(BillsData[indexPath.row].BillCycle)" == "4"){
            Cycle = 2
        }else if("\(BillsData[indexPath.row].BillCycle)" == "5"){
            Cycle = 3
        }else if("\(BillsData[indexPath.row].BillCycle)" == "6"){
            Cycle = 6
        }else if("\(BillsData[indexPath.row].BillCycle)" == "7"){
            Cycle = 12
        }
        
        cell.lblAmount.text  = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(BillsData[indexPath.row].BillAmount)\n\(Cycle) Monthly"
        cell.lblLoanDate.text = String.convertFormatOfDateToStringSpecificFormat(date: BillsData[indexPath.row].BillDate, format: "dd-MMM-yyyy")
        cell.lblLoanType.text = BillsData[indexPath.row].BillSubCategoryName
        cell.lblLoanName.text = BillsData[indexPath.row].BillName
        cell.imgLoanType.image = UIImage(named: BillsData[indexPath.row].BillMainCategoryImage)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let BillVC = storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
        BillVC.isEditBill = true
        BillVC.MainBillUpdateData = BillsData[indexPath.row]
        BillVC.isFrom = "MainBill"
        self.navigationController?.pushViewController(BillVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete){
            
            //for delete all Upcomming Bills from main Bill Id
            let allBills = uiRealm.objects(NewBills.self).filter("MainBillID=%@",BillsData[indexPath.row].BillID)
            if(allBills.count != 0){
                try! uiRealm.write {
                    uiRealm.delete(allBills)
                }
            }
            
            //for delete main bill Id
            let deleteMainBill = uiRealm.objects(NewBills.self).filter("BillID=%@",BillsData[indexPath.row].BillID)
            try! uiRealm.write {
                uiRealm.delete(deleteMainBill)
            }
            
            tblBillList.beginUpdates()
            BillsData.remove(at: indexPath.row)
            tblBillList.deleteRows(at: [indexPath as IndexPath], with: .fade)
            tblBillList.endUpdates()
            
            if(BillsData.count != 0){
                lblNoBillsFound.isHidden = true
                tblBillList.isHidden = false
                
            }else{
                lblNoBillsFound.isHidden = false
                tblBillList.isHidden = true
            }
            
        }
    }
    
    
    //MARK:- Action Methods
    @IBAction func btnBackTap(_ sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
