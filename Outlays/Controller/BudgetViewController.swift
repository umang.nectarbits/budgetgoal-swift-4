//
//  BudgetViewController.swift

//  Created by Nectarbits on 21/07/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BudgetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SambagMonthYearPickerViewControllerDelegate,GADBannerViewDelegate
{
    @IBOutlet var HeaderView : UIView!
    @IBOutlet var btnFilter : UIButton!
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var lblNoData : UILabel!
    @IBOutlet var btnSideMenu : UIButton!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tblCategory : UITableView!
    @IBOutlet var lblheader : UILabel!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var sections : NSMutableArray = NSMutableArray()
    var sectionsByDate : NSMutableArray = NSMutableArray()
    var category : NSMutableArray = []
    var categoryByDate : NSMutableArray = []
    var selectDateMonth = Date()
    var viewSelection = ""
    var isDate = ""
    var passingDate : Date!
    
    
    //MARK:- Views Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        if viewSelection == "Dashboard"
        {
            btnSideMenu.isHidden = true
            btnBack.isHidden = false
        }
        else
        {
            btnSideMenu.isHidden = false
            btnBack.isHidden = true
        }
        tblCategory.estimatedRowHeight = 100.0
        tblCategory.rowHeight = UITableViewAutomaticDimension
        tblCategory.tableFooterView = UIView.init(frame: CGRect.zero)
//        lblHeaderTitle.font = headerTitleFont
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        if passingDate == nil
        {
            passingDate = Date()
        }
        
        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        let Image = UIImage(named: "ic_filter_overview")?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(Image, for: .normal)
        btnFilter.tintColor = UIColor.white
        
        let Image1 = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        btnSideMenu.setImage(Image1, for: .normal)
        btnSideMenu.tintColor = UIColor.white
        
        let Image2 = UIImage(named: "ic_NewBack")
        btnBack.setImage(Image2, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        btnAdd.layer.masksToBounds = false
        btnAdd.layer.cornerRadius = btnAdd.frame.size.width/2
        btnAdd.layer.shadowColor = UIColor.lightGray.cgColor
        btnAdd.layer.shadowOpacity = 0.7
        btnAdd.layer.shadowOffset = CGSize(width: -1, height: 1)
        btnAdd.layer.shadowRadius = 1
        
        let fileManager = FileManager.default
        // We need just to get the documents folder url
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        print(documentsUrl)
        
        self.selectDateMonth = passingDate
        if(isDate == "")
        {
            self.getCategoryData()
        }
        else
        {
            self.getCategoryByDate()
        }
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    func showSpinningWheel(notification: NSNotification) {
        passingDate = notification.object as! Date
    }
    
    //MARK:- Helper Methods
    func getCategoryData()
    {
        sections = []
        category = []
        
        let categoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ ","1","0")
        
        for i in 0..<categoryList.count
        {
            let dict = NSMutableDictionary()
            dict.setValue(categoryList[i]["id"] as! Int, forKey: "id")
            dict.setValue(categoryList[i]["name"]!, forKey: "name")
            dict.setValue(categoryList[i]["image"]!, forKey: "image")
            dict.setValue(categoryList[i]["categoryID"]!, forKey: "categoryID")
            dict.setValue(categoryList[i]["subCategoryID"]!, forKey: "subCategoryID")
            dict.setValue(categoryList[i]["categoryType"]!, forKey: "categoryType")
            dict.setValue(categoryList[i]["isCategory"]!, forKey: "isCategory")
            
            let budgetUpdate = uiRealm.objects(Budget.self).filter("date BETWEEN {%@,%@} AND mainCatID = %d", selectDateMonth.startOfMonth(), selectDateMonth.endOfMonth(), Int(categoryList[i]["categoryID"] as! String)!)
            if budgetUpdate.count == 0
            {
            }
            else
            {
                sections.add(dict)
                
                let track : Category =  categoryList[i]
                
                category.add(track)
            }
            
        }
        if sections.count > 0
        {
            tblCategory.isHidden = false
            lblNoData.isHidden = true
            lblNoData.text = "You have not added any Budget yet, To add Budget tap on + button"
            tblCategory.reloadData()
        }
        else
        {
            tblCategory.isHidden = true
            lblNoData.isHidden = false
            lblNoData.text = "You have not added any Budget yet, To add Budget tap on + button"
        }
        
        tblCategory.reloadData()
    }
    
    
    func getCategoryByDate()
    {
 
//        sectionsByDate = []
//        categoryByDate = []
        
        sections = []
        category = []
        
        let categoryList = uiRealm.objects(Budget.self).filter("date BETWEEN {%@,%@}",selectDateMonth.startOfMonth(), selectDateMonth.endOfMonth())
        
        if(categoryList.count > 0 )
        {
            for i in 0..<categoryList.count
            {
                
                let tempCategoryList = uiRealm.objects(Category.self).filter("isCategory = %@ AND categoryType = %@ ","1","0")
                let track : Budget =  categoryList[i]
                
                for j in 0..<tempCategoryList.count
                {
                    if track.mainCatID == Int(tempCategoryList[j]["categoryID"] as! String)!
                    {
                        let dict = NSMutableDictionary()
                        dict.setValue(tempCategoryList[j]["id"] as! Int, forKey: "id")
                        dict.setValue(tempCategoryList[j]["name"]!, forKey: "name")
                        dict.setValue(tempCategoryList[j]["image"]!, forKey: "image")
                        dict.setValue(tempCategoryList[j]["categoryID"]!, forKey: "categoryID")
                        dict.setValue(tempCategoryList[j]["subCategoryID"]!, forKey: "subCategoryID")
                        dict.setValue(tempCategoryList[j]["categoryType"]!, forKey: "categoryType")
                        dict.setValue(tempCategoryList[j]["isCategory"]!, forKey: "isCategory")
                        
                        sections.add(dict)
                        
                        let track : Category =  tempCategoryList[j]
                        
                        category.add(track)
                    }
                }
                
            }
        }
        else
        {
            
            
//            tblCategory.isHidden = true
//            lblNoData.isHidden = false
        }
        tblCategory.reloadData()
    }

    
    //MARK:- UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetViewCell") as! BudgetViewCell
       
        let temp = sections[indexPath.row] as! NSMutableDictionary
        
        cell.lblName.text = temp["name"] as? String
        cell.lblName.font = tableMainFont
        cell.imgv.image = UIImage(named: temp["image"] as! String)
        cell.imgv.layer.borderWidth = 0.3
        cell.imgv.layer.borderColor = Constant.tableTextColorCode.cgColor
        cell.imgv.layer.cornerRadius = cell.imgv.frame.size.width / 2
        cell.imgv.layer.masksToBounds = true
        cell.lblTotalPrice.text = ""
        cell.lblRemainPrice.text = ""
        cell.viewRemain.isHidden = true
        cell.viewTotal.backgroundColor = UIColor.lightGray

        let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int((temp["categoryID"] as? String)!)!)
     
        let symbols = UserDefaults.standard.value(forKey: "currencySymbol") as! String

        cell.lblTotalPrice.text =  String(format:"0.0 %@", symbols)
        cell.lblRemainingAmount.text =  String(format:"0.0 %@", symbols)
        cell.lblTime.text = ""
       
        if let budgetWorkout = budgetUpdate.last
        {
            try! uiRealm.write
            {
               let symbol = UserDefaults.standard.value(forKey: "currencySymbol") as! String
                
                cell.lblTotalPrice.text =  String(format:"%.2f %@",budgetWorkout.budgetAmount,symbol)
                cell.lblRemainPrice.text = String(format:"%.2f %@",budgetWorkout.currentAmount, symbol)
                
                cell.lblRemainingAmount.text = String(format:"%.2f %@",(budgetWorkout.budgetAmount - budgetWorkout.currentAmount),UserDefaults.standard.value(forKey: "currencySymbol") as! String)

                var type =  budgetWorkout.cycleType
                
                if(type == 1)
                {
                     cell.lblTime.text = "Daily"
                }
                else if(type == 2)
                {
                    cell.lblTime.text = "Weekly"
                }
                else if(type == 3)
                {
                    cell.lblTime.text = "Monthly"
                }
                
                let width = cell.viewTotal.frame.size.width
                
                if(budgetWorkout.budgetAmount < budgetWorkout.currentAmount)
                {
                    let widths = (budgetWorkout.budgetAmount * Double(width)) /  budgetWorkout.currentAmount
                    
                    cell.viewTotal.backgroundColor = UIColor.red
                    cell.viewRemain.backgroundColor = UIColor.lightGray
                    cell.viewRemain.isHidden = false
//                    cell.viewRemain.frame = CGRect(x: cell.viewRemain.frame.origin.x, y: cell.viewRemain.frame.origin.y, width: CGFloat(widths), height: cell.viewRemain.frame.size.height)
                    cell.viewRemain.frame.size.width = CGFloat(widths)
                    
                    
                }
                else
                {
                    if budgetWorkout.budgetAmount > 0 && budgetWorkout.currentAmount == 0
                    {
                        cell.viewTotal.backgroundColor = UIColor(red: 0/255, green: 108/255, blue: 59/255, alpha: 1)
                        cell.lblRemainingAmount.textColor = UIColor(red: 0/255, green: 108/255, blue: 59/255, alpha: 1)
                    }
                    else{
                        let widths = (budgetWorkout.currentAmount * Double(width)) /  budgetWorkout.budgetAmount
                        cell.lblRemainingAmount.textColor = UIColor(red: 0/255, green: 108/255, blue: 59/255, alpha: 1)
                        cell.viewTotal.backgroundColor = UIColor(red: 0/255, green: 108/255, blue: 59/255, alpha: 1)
                        cell.viewRemain.backgroundColor = UIColor.lightGray
                        cell.viewRemain.isHidden = false
                        cell.viewRemain.frame = CGRect(x: cell.viewRemain.frame.origin.x, y: cell.viewRemain.frame.origin.y, width: CGFloat(widths), height: cell.viewRemain.frame.size.height)
                        
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let temp = sections[indexPath.row] as! NSMutableDictionary

        var budgetAmonut = 0
        var budgetInfo = Budget()
        
        let budgetUpdate = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int((temp["categoryID"] as? String)!)!)
        
        if let budgetWorkout = budgetUpdate.last
        {
            try! uiRealm.write
            {
                budgetAmonut = Int(budgetWorkout.budgetAmount)
                budgetInfo = budgetWorkout
            }
        }
        
        if(budgetAmonut > 0)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetHistoryViewController") as! BudgetHistoryViewController
            
            vc.budgetInfo = budgetInfo
            vc.passingDate = passingDate
            vc.selectDate = self.selectDateMonth
            
            //vc.budgetAmountDateWise = budgetUpdate
            
            vc.info = temp
            
            let list : Category = category[indexPath.row] as! Category
            
            vc.categoryInfo = list
            
            vc.isBudget = budgetAmonut
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetDetailViewController") as! BudgetDetailViewController
            
            vc.info = temp
            
            let list : Category = category[indexPath.row] as! Category
            
            vc.categoryInfo = list
            
            vc.isBudget = budgetAmonut
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    //MARK: - Admob Delegate
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
        
    }

    //MARK:- Action Methods
    @IBAction func btnFilterTap(_ sender: Any)
    {
        let vc = SambagMonthYearPickerViewController()
        vc.delegate = self
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnAddAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetDetailViewController") as! BudgetDetailViewController
        vc.passingDate = passingDate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnOverviewAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OverviewViewController") as! OverviewViewController
        self.navigationController?.setViewControllers([vc], animated: false)
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    //MARK: - SambagDate Picker Method
    func sambagMonthYearPickerDidSet(_ viewController: SambagMonthYearPickerViewController, result: SambagMonthYearPickerResult) {
        
//                let resultDate = Constant.convertDate(String(describing: result))
                let resultDate = Constant.getStringToDate(String(describing: result))
                self.selectDateMonth = resultDate
                self.getCategoryByDate()
                self.dismiss(animated: true, completion: nil)
    }
    
    func sambagMonthYearPickerDidCancel(_ viewController: SambagMonthYearPickerViewController) {
        
                self.dismiss(animated: true, completion: nil)
    }

//    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult)
//    {
//        let resultDate = Constant.convertDate(String(describing: result))
//        self.selectDateMonth = resultDate
//        self.getCategoryByDate()
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
}
