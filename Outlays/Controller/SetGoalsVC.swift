//
//  SetGoalsVC.swift
//  Created by harshesh on 13/06/1940 Saka.
//  Copyright © 1940 Cools. All rights reserved.
//

import UIKit
import MBCircularProgressBar
class setGoalsCell: UITableViewCell {
    @IBOutlet weak var tblprogresssView: MBCircularProgressBarView!
    @IBOutlet var lblGoalName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgGolas: UIImageView!
    @IBOutlet var lblContribution: UILabel!
}

class goalsCollectionTypeCell: UICollectionViewCell {
    @IBOutlet weak var ClevprogresssView: MBCircularProgressBarView!
    @IBOutlet var lblGoalName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgGolas: UIImageView!
    @IBOutlet var lblContribution: UILabel!
    @IBOutlet var bgView: UIView!

}

@available(iOS 10.0, *)
class SetGoalsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK:- Outlate
    @IBOutlet var tblGoals: UITableView!
    @IBOutlet var btnTableType: UIButton!
    @IBOutlet var btnCollectionType: UIButton!
    @IBOutlet var lblTableType: UILabel!
    @IBOutlet var lblCollectionType: UILabel!
    @IBOutlet var clevSetGoals: UICollectionView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var goalsData = [[String:AnyObject]]()
    var displayDate : Date!
    var goalId = ""
    var contributData = [[String:AnyObject]]()
    var progPercentage = Float()
    
    @IBOutlet weak var datepicker: ScrollableDatepicker!{
    didSet {
        var dates = [Date]()
        for day in -15...15 {
            dates.append(Date(timeIntervalSinceNow: Double(day * 86400)))
    }
    
        datepicker.dates = dates
        datepicker.selectedDate = Date()
        datepicker.delegate = self
        var configuration = Configuration()
        
        // weekend customization
        configuration.weekendDayStyle.dateTextColor =  UIColor.black//UIColor(red: 242.0/255.0, green: 93.0/255.0, blue: 28.0/255.0, alpha: 1.0)
        configuration.weekendDayStyle.dateTextFont = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.thin)
        configuration.weekendDayStyle.weekDayTextColor = UIColor.black//UIColor(red: 242.0/255.0, green: 93.0/255.0, blue: 28.0/255.0, alpha: 1.0)
        configuration.weekendDayStyle.weekDayTextFont = UIFont.systemFont(ofSize: 8, weight: UIFont.Weight.light)
    
        // selected date customization
        configuration.selectedDayStyle.backgroundColor = UIColor(white: 0.9, alpha: 1)
        configuration.daySizeCalculation = .numberOfVisibleItems(5)
        
        datepicker.configuration = configuration
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        DispatchQueue.main.async {
            self.showSelectedDate()
            self.datepicker.scrollToSelectedDate(animated: false)
        }
        
        clevSetGoals.isHidden = true
        //set Background
        lblTableType.backgroundColor = UIColor(red: 84.0/255.0, green: 139.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        lblCollectionType.backgroundColor = UIColor.clear
        tblGoals.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewWillAppear(_ animated: Bool) {
        getGoalsInfo()
        lblNoData.isHidden = true
        if (goalsData.count == 0)
        {
            lblNoData.isHidden = false
            tblGoals.isHidden = true
        }
        else
        {
            lblNoData.isHidden = true
            tblGoals.isHidden = false
        }
    }

    //MARK:- Helper Methods
    func getGoalsInfo()
    {
        let puppies = uiRealm.objects(Goals.self)
        goalsData = []
        for j in 0..<puppies.count
        {
            var dict1 = [String:AnyObject]()
            dict1["goalid"] = puppies[j]["goalid"]! as AnyObject
            dict1["name"] = puppies[j]["name"]! as AnyObject
            dict1["amount"] = puppies[j]["amount"]! as AnyObject
            dict1["createDate"] = String.convertFormatOfDate(date: puppies[j]["createDate"]! as! Date) as AnyObject
            dict1["acountName"] = puppies[j]["acountName"]! as AnyObject
            dict1["reminderType"] = puppies[j]["reminderType"]! as AnyObject
            dict1["gAccountid"] = puppies[j]["gAccountid"]! as AnyObject
            
            goalsData.append(dict1)
        }
        print(goalsData)
        tblGoals.reloadData()
        clevSetGoals.reloadData()
    }
    
//    //Get Contribution Data
//    func getcontributInfo()
//    {
//        let puppies = uiRealm.objects(Contribution.self)
//        contributData = []
//        for j in 0..<puppies.count
//        {
//            var dict1 = [String:AnyObject]()
//            dict1["contributid"] = puppies[j]["contributid"]! as AnyObject
//            dict1["goalId"] = puppies[j]["goalId"]! as AnyObject
//            dict1["amount"] = puppies[j]["amount"]! as AnyObject
//            dict1["note"] = puppies[j]["note"]! as AnyObject
//            dict1["createDate"] = String.convertFormatOfDate(date: puppies[j]["createDate"]! as! Date) as AnyObject
//            contributData.append(dict1)
//        }
//    }
    
    //MARK:- Calaender Methods
    fileprivate func showSelectedDate() {
        guard let selectedDate = datepicker.selectedDate else {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM YYYY"
        self.displayDate = selectedDate
//        selectedDateLabel.text = formatter.string(from: selectedDate)
    }
    
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return goalsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "setGoalsCell", for: indexPath) as! setGoalsCell
        
        // set the corner radius in Cell
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        cell.selectionStyle = .none
        // set the shadow properties
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowRadius = 4.0
        
        cell.imgGolas.layer.cornerRadius = 5
        cell.imgGolas.clipsToBounds = true
        cell.lblGoalName.text = goalsData[indexPath.row]["name"] as? String
        cell.lblPrice.text = "\((UserDefaults.standard.value(forKey: "currencySymbol") as! String))\(goalsData[indexPath.row]["amount"]!)"
        //get Total Amount Of Contribution
        let totalAmount: Int = uiRealm.objects(Contribution.self).filter("goalId = %d", Int("\(goalsData[indexPath.row]["goalid"]!)")!).sum(ofProperty: "amount")
        print(totalAmount)
        cell.lblContribution.text = " Contribute: \((UserDefaults.standard.value(forKey: "currencySymbol") as! String))\("\(totalAmount)")"
        
        //Set Progress Value
        let goalPrice = goalsData[indexPath.row]["amount"] as! Int//Int(goalsData[indexPath.row]["amount"] as! String)
        let progPercentage =  Float(((100 * totalAmount)/goalPrice))  //"\((100 * totalAmount) / (goalsData[indexPath.row]["amount"]))"
        if (progPercentage >= 100)
        {
            cell.tblprogresssView.value = CGFloat(100)

        }
        else
        {
            cell.tblprogresssView.value = CGFloat(progPercentage)
        }
        
        return cell
    }
    
    @available(iOS 10.0, *)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
        vc.passingView = "Edit"
        vc.editData = goalsData[indexPath.row]
        vc.passGoalId = "\(goalsData[indexPath.row]["goalid"]!)"
        vc.passAcountId = goalsData[indexPath.row]["gAccountid"] as! Int
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return goalsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "goalsCollectionTypeCell", for: indexPath) as! goalsCollectionTypeCell
        cell.backgroundColor = UIColor.clear
        cell.bgView.layer.cornerRadius = 10
        cell.bgView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.masksToBounds = false
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 3
        cell.layer.shadowOffset = CGSize(width: 0, height: 4)
        
        cell.imgGolas.layer.cornerRadius = 40
        cell.imgGolas.clipsToBounds = true
        cell.lblGoalName.text = goalsData[indexPath.row]["name"] as? String
        cell.lblPrice.text = "\((UserDefaults.standard.value(forKey: "currencySymbol") as! String))\(goalsData[indexPath.row]["amount"]!)"
        
        //get Total Amount Of Contribution
        let totalAmount: Int = uiRealm.objects(Contribution.self).filter("goalId = %d", Int("\(goalsData[indexPath.row]["goalid"]!)")!).sum(ofProperty: "amount")
        cell.lblContribution.text = " Contribute: \((UserDefaults.standard.value(forKey: "currencySymbol") as! String))\("\(totalAmount)")"

        //set
        let goalPrice = goalsData[indexPath.row]["amount"] as! Int //Int(goalsData[indexPath.row]["amount"] as! Int)
        let progPercentage =  Float(((100 * totalAmount)/goalPrice))  //"\((100 * totalAmount) / (goalsData[indexPath.row]["amount"]))"
        
        if (progPercentage >= 100)
        {
            cell.ClevprogresssView.value = CGFloat(100)
        }
        else
        {
            cell.ClevprogresssView.value = CGFloat(progPercentage)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
        vc.passingView = "Edit"
        vc.editData = goalsData[indexPath.row]
        vc.passGoalId = "\(goalsData[indexPath.row]["goalid"]!)"
        vc.passAcountId = goalsData[indexPath.row]["gAccountid"] as! Int
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(20, 20, 20, 0);
    }
    
    //MARK:- Action Method
    @IBAction func btnTableTAP(_ sender: Any)
    {
        if (goalsData.count == 0)
        {
            lblNoData.isHidden = false
            tblGoals.isHidden = true
        }
        else
        {
            lblNoData.isHidden = true
            lblTableType.backgroundColor = UIColor(red: 84.0/255.0, green: 139.0/255.0, blue: 84.0/255.0, alpha: 1.0)
            lblCollectionType.backgroundColor = UIColor.clear
            clevSetGoals.isHidden = true
            tblGoals.isHidden = false
        }
    }
    
    @IBAction func btnCollectionTAP(_ sender: Any)
    {
        if (goalsData.count == 0)
        {
            lblNoData.isHidden = false
            clevSetGoals.isHidden = true
        }
        else
        {
            lblNoData.isHidden = true
            lblCollectionType.backgroundColor = UIColor(red: 84.0/255.0, green: 139.0/255.0, blue: 84.0/255.0, alpha: 1.0)
            lblTableType.backgroundColor = UIColor.clear
            clevSetGoals.isHidden = false
            tblGoals.isHidden = true
        }
    }
    
    @IBAction func btnPlusTAP(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "Purchase") != nil {
            if UserDefaults.standard.bool(forKey: "Purchase") {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
                vc.passingDate = displayDate!
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                if (goalsData.count == 1){
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseSubscribtionVC") as! PurchaseSubscribtionVC
                    vc.strScreenTag = "SetGoal"
                    self.present(vc, animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
                    vc.passingDate = displayDate!
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            if (goalsData.count == 1){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseSubscribtionVC") as! PurchaseSubscribtionVC
                vc.strScreenTag = "SetGoal"
                self.present(vc, animated: true, completion: nil)
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
                vc.passingDate = displayDate!
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
//        if ((UserDefaults.standard.value(forKey: "GetAllFeature") != nil)){
//            if(UserDefaults.standard.bool(forKey: "GetAllFeature")){
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
//                vc.passingDate = displayDate!
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else{
//
//            }
//        }else{
//            if(UserDefaults.standard.value(forKey: "GetGoalFeature") != nil){
//                if(UserDefaults.standard.bool(forKey: "GetGoalFeature")){
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
//                    vc.passingDate = displayDate!
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//            }else{
//
//                if (goalsData.count == 1){
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseSubscribtionVC") as! PurchaseSubscribtionVC
//                    vc.strScreenTag = "SetGoal"
//                    self.present(vc, animated: true, completion: nil)
//                }else{
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetGoalsDetailsVC") as! SetGoalsDetailsVC
//                    vc.passingDate = displayDate!
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//            }
//        }
    }

    @IBAction func btnBackTAP(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - ScrollableDatepickerDelegate
@available(iOS 10.0, *)
extension SetGoalsVC: ScrollableDatepickerDelegate {
    
    func datepicker(_ datepicker: ScrollableDatepicker, didSelectDate date: Date) {
        showSelectedDate()
    }
}
