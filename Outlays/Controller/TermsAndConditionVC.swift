//
//  TermsAndConditionVC.swift
//  Budget Goals
//
//  Created by Jay on 20/03/20.
//  Copyright © 2020 Cools. All rights reserved.
//

import UIKit

class TermsAndConditionVC: UIViewController, UIWebViewDelegate{

    @IBOutlet var HeaderView : UIView!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet weak var webvPricayPolicy: UIWebView!
    @IBOutlet var btnBack : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        webvPricayPolicy.delegate = self
        
        lblHeaderTitle.text = "Terms And Condition"
        lblHeaderTitle.font = InnerHeadersFont
        lblHeaderTitle.textColor = HeaderTitleColor
        
        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)

        //1. Load web site into my web view
        let myURL = URL(string: "http://iappbits.in/budgetgoal/Terms.html")
        let myURLRequest:URLRequest = URLRequest(url: myURL!)
        webvPricayPolicy.loadRequest(myURLRequest)
        
        
        let Image = UIImage(named: "ic_NewBack")
        btnBack.setImage(Image, for: .normal)

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Action Method
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
