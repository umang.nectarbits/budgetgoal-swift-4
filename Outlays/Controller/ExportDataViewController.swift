//
//  ExportDataViewController.swift
//  Created by harshesh on 15/11/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import MessageUI
import GoogleMobileAds

class ExportDataViewController: UIViewController,SambagDatePickerViewControllerDelegate,
MFMailComposeViewControllerDelegate,GADBannerViewDelegate{
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var Headerview: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var viewCategoryFilter: UIView!
    @IBOutlet var btnLeftMonth: UIButton!
    @IBOutlet var btnRightMonth: UIButton!
    @IBOutlet var btnApply: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!

    var dateBtnSelection = ""
    var startDateofMonth = Date()
    var endDateofMonth = Date()
    var animatedViewSelection = ""
    var viewSelection = ""
    var searchActive: Bool = false
    var filtered: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        setButtonShadow()
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        lblHeaderTitle.font = InnerHeadersFont
        lblHeaderTitle.textColor = HeaderTitleColor
        
//        btnApply.layer.shadowColor = UIColor(colorLiteralRed: 242.0/255.0, green: 241.0/255.0, blue: 252.0/255.0, alpha: 1).cgColor
        btnApply.layer.shadowColor = UIColor(red: 242.0/255.0, green: 241.0/255.0, blue: 252.0/255.0, alpha: 1).cgColor
        btnApply.layer.cornerRadius = 4
        btnApply.layer.shadowOpacity = 1.5
        btnApply.layer.shadowRadius = 6
        btnApply.layer.shadowOffset = CGSize.zero
        
//        viewCategoryFilter.layer.shadowColor = UIColor(colorLiteralRed: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1).cgColor
        viewCategoryFilter.layer.shadowColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1).cgColor
        
        viewCategoryFilter.layer.cornerRadius = 8
        viewCategoryFilter.layer.shadowOpacity = 0.7
        viewCategoryFilter.layer.shadowRadius = 2
        viewCategoryFilter.layer.shadowOffset = CGSize.zero
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    override func viewWillLayoutSubviews() {
        viewCategoryFilter.viewShadow()
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    
    //MARK:- Helper Method
    func setButtonShadow()
    {
        btnLeftMonth.layer.masksToBounds = false
        btnLeftMonth.layer.cornerRadius = 0
        btnLeftMonth.layer.shadowColor = UIColor.lightGray.cgColor
        btnLeftMonth.layer.shadowOpacity = 0.6
        btnLeftMonth.layer.shadowOffset = CGSize(width: -1, height: 1)
        btnLeftMonth.layer.shadowRadius = 1.5
        
        btnRightMonth.layer.masksToBounds = false
        btnRightMonth.layer.cornerRadius = 0
        btnRightMonth.layer.shadowColor = UIColor.lightGray.cgColor
        btnRightMonth.layer.shadowOpacity = 0.6
        btnRightMonth.layer.shadowOffset = CGSize(width: -1, height: 1)
        btnRightMonth.layer.shadowRadius = 1.5
    }
    func removeAnimate(strViewSelection : String)
    {
            filtered = []
            searchActive = false;
            UIView.animate(withDuration: 0.25, animations: {
                self.viewCategoryFilter.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.viewCategoryFilter.alpha = 0.0;
//                self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//                self.bgView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.viewCategoryFilter.isHidden = true
//                    self.bgView.isHidden = true
                }
            });
        
    }
    func creatCSV() -> Void {
        
        let puppies = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@}", startDateofMonth, endDateofMonth)//uiRealm.objects(Transaction.self)
        
        let transactionArray = NSMutableArray()
        
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            transactionArray.add(dict1)
        }
        
        if transactionArray.count > 0
        {
            let fileName = "Tasks.csv"
            let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
            var csvText = "Date,Category Type,Category Name,Sub Category Name,Amount,Note\n"
            
            for i in 0..<transactionArray.count {
                var typeOfCategory = ""
                if (transactionArray[i] as! NSMutableDictionary).value(forKey: "setType") as! String == "1"
                {
                    typeOfCategory = "Income"
                }
                else
                {
                    typeOfCategory = "Expense"
                }
                
                let newLine = "\((transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String),\(typeOfCategory),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryName") as! String),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "subCatName") as! String),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String),\((transactionArray[i] as! NSMutableDictionary).value(forKey: "note") as! String)\n"
                csvText.append(newLine)
            }
            
            do {
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                
                if MFMailComposeViewController.canSendMail() {
                    let emailController = MFMailComposeViewController()
                    emailController.mailComposeDelegate = self
                    emailController.setToRecipients([])
                    emailController.setSubject("data export")
                    //                    emailController.setMessageBody("Hi,\n\nThe .csv data export is attached\n\n\nSent from the MPG app: http://www.justindoan.com/mpg-fuel-tracker", isHTML: false)
                    let tempData = try NSData(contentsOf: path!) as Data
                    
                    emailController.addAttachmentData(tempData, mimeType: "text/csv", fileName: "\(fileName)")
                    present(emailController, animated: true, completion: nil)
                }
                
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        }
        else {
            alertView(alertMessage: AlertString.ExportDataView)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        
        if animatedViewSelection == "currencyView"
        {
            if touch.view != viewCategoryFilter {
                removeAnimate(strViewSelection: "categoryFilter")
            }
        }
    }
    //MARK: - SambagDate Picker Method
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        
        let resultDate = Constant.convertDate(String(describing: result))
        if dateBtnSelection == "left"
        {
            startDateofMonth = resultDate.startOfMonth()
            btnLeftMonth.setTitle(Constant.getMonthYear(resultDate), for: .normal)
        }
        else if dateBtnSelection == "right"
        {
            endDateofMonth = resultDate.startOfMonth()
            btnRightMonth.setTitle(Constant.getMonthYear(resultDate), for: .normal)
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            //self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        //BottomHeightConstraint.constant = 0
    }

    //MARK: - Action Method
    @IBAction func btnApplyDateSelection(_ sender: UIButton) {
        
        if startDateofMonth < endDateofMonth
        {
//            removeAnimate(strViewSelection: "categoryFilter")
            creatCSV()
        }
        else
        {
            self.alertView(alertMessage: AlertString.ExportDataSelectmonth)
        }
        
        
    }
    
    @IBAction func btnLeftDateSelection(_ sender: UIButton) {
        dateBtnSelection = "left"
        let vc = SambagDatePickerViewController()
        vc.delegate = self as! SambagDatePickerViewControllerDelegate
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnRightDateSelection(_ sender: UIButton) {
        dateBtnSelection = "right"
        let vc = SambagDatePickerViewController()
        vc.delegate = self as? SambagDatePickerViewControllerDelegate
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
