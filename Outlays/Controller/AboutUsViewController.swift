//
//  AboutUsViewController.swift
//  Finance
//
//  Created by Cools on 7/8/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds


class AboutUsViewController: UIViewController,UIWebViewDelegate,GADBannerViewDelegate {

//@IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet var HeaderView : UIView!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet weak var webvAboutUs: UIWebView!
    @IBOutlet var btnSideMenu : UIButton!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!

    var viewSelection = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        if viewSelection == "SideMenuVC"
        {
            btnSideMenu.isHidden = true
            btnBack.isHidden = false
        }
        else
        {
            btnSideMenu.isHidden = false
            btnBack.isHidden = true
        }
        webvAboutUs.delegate = self
        lblHeaderTitle.font = InnerHeadersFont
        lblHeaderTitle.textColor = HeaderTitleColor
        
        //1. Load web site into my web view
        let myURL = URL(string: "http://www.nectarbits.com")
        let myURLRequest:URLRequest = URLRequest(url: myURL!)
        webvAboutUs.loadRequest(myURLRequest)

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        let Image = UIImage(named: "ic_NewBack")
        btnBack.setImage(Image, for: .normal)
        
        let Image1 = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        btnSideMenu.setImage(Image1, for: .normal)
        btnSideMenu.tintColor = UIColor.white
        
//        BottomHeightConstraint.constant = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    func webViewDidStartLoad(_ webView: UIWebView)
    {
        print("Webview started Loading")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        print("Webview did finish load")
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
        
    }

 
//MARK: - Action Method
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
}
