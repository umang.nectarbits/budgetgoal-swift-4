//
//  TransactionHistoryViewController.swift
//  Outlays
//
//  Created by Cools on 10/5/17.
//  Copyright © 2017 Cools. All rights reserved.
//


class headerCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
}

class mainCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var img_icon: UIImageView!
    @IBOutlet var lblAmount : UICountingLabel!
    @IBOutlet var lblNotes : UILabel!
    @IBOutlet var shadowView : UIView!
}

import UIKit
import GoogleMobileAds
//struct SectionTransaction
//{
//    //    setType
//    //    repeatType
//    var amount: String!
//    var date: String!
//    var items: NSMutableArray!
//    var collapsed: Bool!
//
//    init(amount: String,date: String, items: NSMutableArray, collapsed: Bool = false)
//    {
//        self.amount = amount
//        self.date = date
//        self.items = items
//        self.collapsed = collapsed
//    }
//}

class TransactionHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,GADBannerViewDelegate  {
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var headerView: UIView!
    @IBOutlet var tblTransaction : UITableView!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    //    @IBOutlet var lblNotIncome : UILabel!
    //    @IBOutlet var lblNotExpense : UILabel!
    @IBOutlet var lblNoData : UILabel!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var sections = [SectionTransaction]()
    var dateArray : [NSDate] = []
    var uniqueDateArray : NSMutableArray = []
    var transactionArray = NSMutableArray()
    var categoryType = ""
    var strCategoryId = ""
    var currentMonthDate : Date!
    var passingDate : Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        tblTransaction.estimatedRowHeight = 80.0
        
        tblTransaction.rowHeight = UITableViewAutomaticDimension
        
        headerView.setGradientColor(color1: UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1), color2: UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1))

        tblTransaction.tableFooterView = UIView.init(frame: CGRect.zero)
        lblHeaderTitle.font = HeaderFont_Bold_15
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        //        BottomHeightConstraint.constant = 0
        //        lblNotIncome.isHidden = true
        //        lblNotExpense.isHidden = true
        lblNoData.isHidden = true
        
        self.tblTransaction.contentInset.bottom = 66
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnAdd.layer.masksToBounds = false
        btnAdd.layer.cornerRadius = btnAdd.frame.size.width/2
        btnAdd.layer.shadowColor = UIColor.lightGray.cgColor
        btnAdd.layer.shadowOpacity = 0.7
        btnAdd.layer.shadowOffset = CGSize(width: -1, height: 1)
        btnAdd.layer.shadowRadius = 1
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
        
        if categoryType == "0"
        {
            //            btnAdd.dropShadow()
            lblHeaderTitle.text = "Expense"
            btnAdd.setBackgroundImage(UIImage(named:"ic_Tableadd"), for: .normal)
        }
        else
        {
            //            btnAdd.dropShadow()
            lblHeaderTitle.text = "Income"
            btnAdd.setBackgroundImage(UIImage(named:"ic_Tableadd"), for: .normal)
            if sections.count > 0
            {
                tblTransaction.isHidden = false
                //                lblNotIncome.isHidden = true
                lblNoData.isHidden = true
                lblNoData.text = "You have not added any Income yet, To add Income tap on + button"
                tblTransaction.reloadData()
            }
            else
            {
                tblTransaction.isHidden = true
                //                lblNotIncome.isHidden = false
                lblNoData.isHidden = false
                lblNoData.text = "You have not added any Income yet,To add Income tap on + button"
            }
        }
        
        let puppies = uiRealm.objects(Transaction.self).filter("accountId = %@",UserDefaults.standard.integer(forKey: "AccountID"))
        transactionArray = []
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            dict1.setValue(puppies[j]["accountId"]!, forKey: "accountId")
            dict1.setValue(puppies[j]["repeatType"]!, forKey: "repeatType")
            dict1.setValue(puppies[j]["isUpdateByService"]!, forKey: "isUpdateByService")
            dict1.setValue(puppies[j]["updatedDate"]!, forKey: "updatedDate")
            dict1.setValue(String(describing: puppies[j]["milliseconds"]!), forKey: "milliseconds")
            transactionArray.add(dict1)
        }
        if strCategoryId == "0"
        {
            filterByDate(selectDate: currentMonthDate)
        }
        else
        {
            filterByCategoryID(selectDate : currentMonthDate)
        }
    }
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    func filterByCategoryID(selectDate : Date)
    {
        sections = [SectionTransaction]()
        uniqueDateArray = []
        dateArray = []
        
        let dateList = uiRealm.objects(Transaction.self).filter("setType = %@ AND categoryID = %@ AND date BETWEEN {%@,%@} AND accountId = %@",categoryType, strCategoryId, selectDate.startOfMonth(), selectDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID")).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueDateArray.contains(temp!))
            {
                uniqueDateArray.add(temp!)
            }
        }
        
        for j in 0..<uniqueDateArray.count
        {
            var totalAmount : Double = 0.0
            let temp1 : NSMutableArray = []
            
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && categoryType == (transactionArray[i] as! NSMutableDictionary).value(forKey: "setType") as! String && strCategoryId == (transactionArray[i] as! NSMutableDictionary).value(forKey: "categoryID") as! String)
                {
                    
                    totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                    print(totalAmount)
                    var dict = NSMutableDictionary()
                    dict = transactionArray[i] as! NSMutableDictionary
                    print(dict)
                    temp1.add(dict)
                }
            }
            let temp = SectionTransaction(amount: String(totalAmount), date: uniqueDateArray[j] as! String, items: temp1)
            print(temp)
            sections.append(temp)
        }
        tblTransaction.reloadData()
    }
    
    func filterByDate(selectDate : Date)
    {
        sections = [SectionTransaction]()
        uniqueDateArray = []
        dateArray = []
        
        let dateList = uiRealm.objects(Transaction.self).filter("setType = %@ AND accountId = %@ AND date BETWEEN {%@,%@}",categoryType,UserDefaults.standard.integer(forKey: "AccountID"), selectDate.startOfMonth(), selectDate.endOfMonth()).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
        
        for i in 0..<dateArray.count
        {
            let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
            if(!uniqueDateArray.contains(temp!))
            {
                uniqueDateArray.add(temp!)
            }
        }
        
        let milisecondArray = NSMutableArray()
        
        for j in 0..<uniqueDateArray.count
        {
            for i in 0..<transactionArray.count
            {
                if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String
                    )
                {
                    if !milisecondArray.contains((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                    {
                        milisecondArray.add((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                    }
                }
            }
        }
//        for k in 0..<milisecondArray.count
//        {
            for j in 0..<uniqueDateArray.count
            {
                var totalAmount : Double = 0.0
                var totalSplitedAmount : Double = 0.0
                let temp1 : NSMutableArray = []
                let temp2 : NSMutableArray = []
                
                
                
                for i in 0..<transactionArray.count
                {
                    if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && categoryType == (transactionArray[i] as! NSMutableDictionary).value(forKey: "setType") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String == "0")
                    {
                        
                        totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                        print(totalAmount)
                        var dict = NSMutableDictionary()
                        dict = transactionArray[i] as! NSMutableDictionary
                        print(dict)
                        temp1.add(dict)
                    }
                }
                let temp = SectionTransaction(amount: String(totalAmount), date: uniqueDateArray[j] as! String, items: temp1)
                print(temp)
                sections.append(temp)
                
                
//                for k in 0..<milisecondArray.count
//                {
                
                for i in 0..<transactionArray.count
                {
                    
                    if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String && categoryType == (transactionArray[i] as! NSMutableDictionary).value(forKey: "setType") as! String && (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String != "0")
                    {
                        
//                        if milisecondArray[k] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String
//                        {
                            totalSplitedAmount = totalSplitedAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                            print(totalSplitedAmount)
                            var dict = NSMutableDictionary()
                            dict = transactionArray[i] as! NSMutableDictionary
                            print(dict)
                            temp2.add(dict)
//                        }
                        
                    }
                }
                
                if temp2.count > 0
                {
                    let tempSplit = SectionTransaction(amount: String(totalSplitedAmount), date: "Split Amount", items: temp2)
                    print(tempSplit)
                    sections.append(tempSplit)
                }
            
//            }
        }
        
        if categoryType == "0"
        {
            lblHeaderTitle.text = "Expense"
            btnAdd.setBackgroundImage(UIImage(named:"ic_Tableadd"), for: .normal)
            if sections.count > 0
            {
                tblTransaction.isHidden = false
                //                lblNotExpense.isHidden = true
                lblNoData.isHidden = true
                lblNoData.text = "You have not added any Expense yet, To add Expense tap on + button"
                tblTransaction.reloadData()
            }
            else
            {
                tblTransaction.isHidden = true
                //                lblNotExpense.isHidden = false
                lblNoData.isHidden = false
                lblNoData.text = "You have not added any Expense yet, To add Expense tap on + button"
            }
        }
        else
        {
            lblHeaderTitle.text = "Income"
            btnAdd.setBackgroundImage(UIImage(named:"ic_Tableadd"), for: .normal)
            if sections.count > 0
            {
                tblTransaction.isHidden = false
                //                lblNotIncome.isHidden = true
                lblNoData.isHidden = true
                tblTransaction.reloadData()
            }
            else
            {
                tblTransaction.isHidden = true
                //                lblNotIncome.isHidden = false
                lblNoData.isHidden = false
            }
            
        }
        tblTransaction.reloadData()
    }
    
    //MARK:- Tableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! headerCell
        
        headerCell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
        headerCell.lblTitle.text = sections[section].date
        headerCell.lblTitle.font = tableHeaderFont
        headerCell.lblTitle.textColor = Constant.tableSubTextColorCode
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapAction))
        tapRecognizer.delegate = self
        tapRecognizer.accessibilityHint = String(section)
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        headerCell.addGestureRecognizer(tapRecognizer)
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell") as! mainCell
        
        var temp = NSMutableDictionary()
        cell.selectionStyle = .none
        temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        cell.lblTitle.textColor = Constant.tableTextColorCode
        cell.lblTitle.font = tableMainFont
        cell.lblAmount.font = tableMainFont
        
        cell.lblTitle.text = temp["subCatName"] as? String
        
        cell.lblNotes.textColor = Constant.tableSubTextColorCode
        
        cell.lblNotes.text = temp["note"] as? String
        cell.lblNotes.font = tableSubFont
        
        cell.img_icon.layer.cornerRadius = 5
        cell.img_icon.backgroundColor = UIColor(red: 244.0 / 255.0, green: 92.0 / 255.0, blue: 67.0 / 255.0, alpha: 1.0)
        if("\(temp["subCatImage"]!)" != ""){
            cell.img_icon.image = UIImage(named: temp["subCatImage"] as! String)!.withRenderingMode(.alwaysTemplate)
            cell.img_icon.tintColor = .white
            cell.img_icon.contentMode = .scaleAspectFit
        }
        
        cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(temp["amount"] as! String)"
        
        cell.shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.shadowView.layer.shadowOpacity = 0.4
        cell.shadowView.layer.shadowRadius = 1
        cell.shadowView.layer.shadowOffset = CGSize.zero
        cell.shadowView.layer.masksToBounds = false
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        cell.lblAmount.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        cell.lblAmount.method = .easeOut
        
        cell.lblAmount.count(from: 0, to: CGFloat(Double(temp["amount"] as! String)!), withDuration: 0.0)
        
        if(temp["setType"] as! String == "0")
        {
            cell.lblAmount.textColor = Constant.expenseColorCode
        }
        else
        {
            cell.lblAmount.textColor = Constant.incomeColorCode
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var temp1 = NSMutableDictionary()
        temp1 = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        
        if("\(temp1["categoryID"]!)" == BillCategoryID){
            
            let BillVC = storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
            BillVC.isEditBill = true
            BillVC.billDetail = temp1
            self.navigationController?.pushViewController(BillVC, animated: true)
        }else if("\(temp1["categoryID"]!)" == LoanCategoryID){
            print("Loan Transaction Tap")
        }
        else{
            
            let detailVC = storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController
            detailVC.modalPresentationStyle = .fullScreen
            
            detailVC.transactionDict = temp1
            detailVC.passingView = "edit"
            detailVC.strCategoryID = strCategoryId
            if categoryType == "0"
            {
                detailVC.strCategoryType = "Expense"
            }
            else
            {
                detailVC.strCategoryType = "Income"
            }
            
            present(detailVC, animated: true, completion: nil)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.delete){
        
            var temp1 = NSMutableDictionary()
            temp1 = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
            
//            //For Deduct Amout From Selected Account
            let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (temp1["accountId"]!))
            if let workoutsAccount = workoutsAccount.first
            {
                try! uiRealm.write
                {
                    workoutsAccount.remainigAmount = workoutsAccount.remainigAmount + Constant.TO_DOUBLE(temp1["amount"]!)
                }
            }
            
            let categoryList = uiRealm.objects(Transaction.self).filter("id = %@",temp1["id"] as! Int)
        
            try! uiRealm.write {
                uiRealm.delete(categoryList)
            }
            
            //For Remove price from Budget Table
            let budgetAccount = uiRealm.objects(Budget.self).filter("mainCatID = %d", Int(temp1["categoryID"] as! String)!)
            if let budgetAccount = budgetAccount.first
            {
                try! uiRealm.write
                {
                    budgetAccount.currentAmount = budgetAccount.currentAmount -  Constant.TO_DOUBLE(temp1["amount"]!)
                }
            }

            tblTransaction.beginUpdates()
            sections[indexPath.section].items.removeObject(at: indexPath.row)
            tblTransaction.deleteRows(at: [indexPath as IndexPath], with: .fade)
            tblTransaction.endUpdates()
            
        }
    }
    
    
    
    //MARK:- Helper Method
    @objc func handleTapAction(gestureRecognizer: UIGestureRecognizer)
    {
        print(sections[Int(gestureRecognizer.accessibilityHint!)!])
        
        if sections[Int(gestureRecognizer.accessibilityHint!)!].date == "Split Amount"
        {
            let detailVC = storyboard?.instantiateViewController(withIdentifier: "SplitViewController") as! SplitViewController
            detailVC.editSplitTransaction = sections[Int(gestureRecognizer.accessibilityHint!)!]
            detailVC.passingView = "EditSplitTransaction"
            present(detailVC, animated: true, completion: nil)
        }
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            //            self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
        
    }
    
    
    
    //MARK:- Action Method
    @IBAction func btnAddTransactionAction(_ sender: UIButton) {
        
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController
        detailVC.passingView = "addTransaction"
        if categoryType == "0"
        {
            detailVC.strCategoryType = "Expense"
            detailVC.categoryType = "0"
        }
        else
        {
            detailVC.strCategoryType = "Income"
            detailVC.categoryType = "1"
        }
        
        
        detailVC.strCategoryID = strCategoryId
        detailVC.passingDate = passingDate
        
        self.navigationController?.pushViewController(detailVC, animated: true)
//        present(detailVC, animated: true, completion: nil)
    }
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
