//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var secondView: UIView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnInsta: UIButton!
    @IBOutlet var btnTwit: UIButton!
    @IBOutlet var imgTitle: UIImageView!
    
    var flag = 0
    var instaflaf = 0
    var twitflag = 0
    
    let aData : NSArray = ["Dashboard","All Transaction","Overview","Budget","Currency Converter","Emi Calculator","Upcoming Bills/Loans","Setting"]
    let aImage : NSArray = ["side_home","side_Trans","side_overview","side_budget","side_convert","side_cal","ic_bill","side_setting"]
    override func viewDidLoad() {
        super.viewDidLoad()
//        
//        bgView.isHidden = false
//        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//        bgView.alpha = 0.0;
//        UIView.animate(withDuration: 0.25, animations: {
//        self.bgView.alpha = 0.7
//        self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)

//        });
        
        imgTitle.setImageColor(color: UIColor.darkGray)
        
        let image1 = UIImage(named: "side_face") as! UIImage
//        self.btnFacebook.setBackgroundImage(image1, for: .normal)
        self.btnFacebook.setImage(image1, for: .normal)
        self.btnFacebook.tintColor = UIColor.darkGray
        
        let image2 = UIImage(named: "side_insta") as! UIImage
//        self.btnInsta.setBackgroundImage(image2, for: .normal)
        self.btnInsta.setImage(image2, for: .normal)
        self.btnInsta.tintColor = UIColor.darkGray
        
        let image3 = UIImage(named: "side_twit") as! UIImage
//        self.btnTwit.setBackgroundImage(image3, for: .normal)
        self.btnTwit.setImage(image3, for: .normal)
        self.btnTwit.tintColor = UIColor.darkGray
        
        tableView.tableFooterView = UIView()
        self.tableView.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
//MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let aCell = tableView.dequeueReusableCell(withIdentifier: "kCell", for: indexPath)
        let cellBGView = UIView()
        cellBGView.backgroundColor = UIColor.lightGray
        aCell.selectedBackgroundView = cellBGView
            let aLabel : UILabel = aCell.viewWithTag(10) as! UILabel
            aLabel.text = aData[indexPath.row] as? String
            aLabel.textColor = UIColor.black
            let imageIcon : UIImageView = aCell.viewWithTag(100) as! UIImageView
            imageIcon.image = UIImage(named: aImage[indexPath.row] as! String)
            imageIcon.setImageColor(color: UIColor.darkGray)
            return aCell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.lightGray
        if indexPath.row == 0 {
            kConstantObj.SetIntialMainViewController("DashboardViewController") // firstVC is storyboard ID
        }else if indexPath.row == 1 {
            kConstantObj.SetIntialMainViewController("AllTransactionViewController")
        }else if indexPath.row == 2 {
            kConstantObj.SetIntialMainViewController("OverviewViewController")
        }else if indexPath.row == 3 {
            kConstantObj.SetIntialMainViewController("BudgetViewController")
        }else if indexPath.row == 4 {
            kConstantObj.SetIntialMainViewController("CurrencyConverterVC")
        }
        else if indexPath.row == 5 {
            kConstantObj.SetIntialMainViewController("EmiCalculatorViewController")
        }
        else if indexPath.row == 6 {
            kConstantObj.SetIntialMainViewController("LoanListVC")
//            kConstantObj.SetIntialMainViewController("AddBillVC")
        }
        else if indexPath.row == 7 {
            kConstantObj.SetIntialMainViewController("SettingViewController")
        }

    }
    
    //MARK: - Action Method
    @IBAction func btnFacebookTap(_ sender: Any)
    {
        if let url = NSURL(string: "https://www.facebook.com/CustomMobileApplicationDevelopment"){
            UIApplication.shared.openURL(url as URL)
        }

    }
    
    @IBAction func btnInstaTap(_ sender: Any)
    {
        if let url = NSURL(string: "https://www.instagram.com"){
            UIApplication.shared.openURL(url as URL)
        }
 
    }
    
    @IBAction func btnTwiterTap(_ sender: Any)
    {
        if let url = NSURL(string: "https://twitter.com/ios_AppDevloper"){
            UIApplication.shared.openURL(url as URL)
        }
    }
}
