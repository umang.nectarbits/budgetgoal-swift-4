//
//  BudgetHistoryViewController.swift
//  Created by Nectarbits on 24/07/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds

class BudgetHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,GADBannerViewDelegate
{
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var lblBudgetAmount: UILabel!
    @IBOutlet var lblCurrentAmount: UILabel!
    @IBOutlet var lblSaveAmount: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblFromToDate: UILabel!
    @IBOutlet var tblv: UITableView!
    @IBOutlet var HistoryView : UIView!
     @IBOutlet var btnEdit: UIButton!
    
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var selectDate = Date()
    
    var budgetInfo: Budget!

    var budgetValues : NSMutableArray = NSMutableArray()
    var budgetAmountDateWise : NSMutableArray = NSMutableArray()

    var categoryInfo = Category()
    var isBudget = 0
    var info = NSMutableDictionary()
    
    var uniqueDateArray : NSMutableArray = []
    var values : NSMutableArray = []
    var sections = [SectionTransaction]()
    var sectionsCat = [SectionCategory]()
    var dateArray : [NSDate] = []
    var transactionArray = NSMutableArray()
    var totalAmount : Double = 0.0
    var totalAmounts : Double = 0.0
    var passingDate : Date!
    var selectedAccountID : Int = 0

    @IBOutlet var incomePieChart : PieChartView!
 
    //MARK:- Views Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        
        lblHeaderTitle.font = headerTitleFont
        lblHeaderTitle.textColor = UIColor.white
        
        HeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        btnEdit.setImage(UIImage(named: "ic_edit_category")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnEdit.tintColor = UIColor.white
        
        HistoryView.layer.cornerRadius = 10;
        //HistoryView.masksToBounds = true;
        
        HistoryView.layer.borderColor = UIColor.lightGray.cgColor;
        HistoryView.layer.borderWidth = 2.0;
        
        
//        let categoryList = uiRealm.objects(Budget.self).filter("date BETWEEN {%@,%@}",selectDate.startOfMonth(), selectDate.endOfMonth())
//        
//        for i in 0..<categoryList.count
//        {
//            let track : Budget =  categoryList[i]
//            
//            if(track.mainCatID == budgetInfo.mainCatID)
//            {
//                budgetValues.add(categoryList[i])
        
 //     let temp = String.convertFormatOfDate(date: categoryList[i].value(forKey: "date") as! Date)
//
//                if(sections.contains(temp!))
//                {
//                    values.add(categoryList[i])
//                }
//                else
//                {
//                    values.add(categoryList[i])
//                }
//            }
//       }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let puppies = uiRealm.objects(Transaction.self).filter("categoryID = %@",categoryInfo.categoryID)
        
        let from =  String.convertFormatOfDate(date: selectDate.startOfMonth())
        let to =  String.convertFormatOfDate(date: selectDate.endOfMonth() )
        
        lblFromToDate.text = String(format: "From: %@ - To: %@", from!,to!)
        
        // setUpValue(selectDate: Date())
        transactionArray = []
        for j in 0..<puppies.count
        {
            let dict1 = NSMutableDictionary()
            dict1.setValue(puppies[j]["id"] as! Int, forKey: "id")
            dict1.setValue(puppies[j]["categoryName"]!, forKey: "categoryName")
            dict1.setValue(puppies[j]["categoryImage"]!, forKey: "categoryImage")
            dict1.setValue(puppies[j]["subCatName"]!, forKey: "subCatName")
            dict1.setValue(puppies[j]["subCatImage"]!, forKey: "subCatImage")
            dict1.setValue(puppies[j]["categoryID"]!, forKey: "categoryID")
            dict1.setValue(puppies[j]["subCategoryID"]!, forKey: "subCategoryID")
            dict1.setValue(puppies[j]["amount"]!, forKey: "amount")
            dict1.setValue(puppies[j]["setType"]!, forKey: "setType")
            dict1.setValue(puppies[j]["accountId"]!, forKey: "accountId")
            dict1.setValue(String.convertFormatOfDate(date: puppies[j]["date"]! as! Date), forKey: "date")
            dict1.setValue(puppies[j]["note"]!, forKey: "note")
            dict1.setValue(puppies[j]["repeatType"]!, forKey: "repeatType")
            dict1.setValue(puppies[j]["isUpdateByService"]!, forKey: "isUpdateByService")
            dict1.setValue(puppies[j]["updatedDate"]!, forKey: "updatedDate")
            dict1.setValue(String(describing: puppies[j]["milliseconds"]!), forKey: "milliseconds")
            transactionArray.add(dict1)
        }
        
        self.filterByDate(selectDate: selectDate)
        print(budgetValues)
        
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    func setChart(_ dataPoints: [String], values: [Double])
    {
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = BarChartDataEntry(x: Double(i+2), y:values[i], data: nil )
            
            dataEntries.append(dataEntry)
        }
 
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "Units Sold")
        
        let pieChartData = PieChartData(dataSets: [pieChartDataSet])
        
        incomePieChart.data = pieChartData
        self.incomePieChart.legend.enabled = false
        self.incomePieChart.chartDescription?.text = ""

        
        let colors: [UIColor] = [UIColor(hexString:"006400"),
                                 UIColor(hexString:"ff0000")]
        
        pieChartDataSet.colors = colors
        pieChartDataSet.drawValuesEnabled = false
    }
    
    func filterByDate(selectDate : Date)
    {
        sections = [SectionTransaction]()
        uniqueDateArray = []
        dateArray = []
        let dateList = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND categoryID = %@", selectDate.startOfMonth(), selectDate.endOfMonth(),categoryInfo.categoryID).value(forKey: "date") as! NSArray
        
        dateArray = dateList.sorted(by: { ($0 as! Date).compare($1 as! Date) == .orderedDescending }) as! [NSDate]
       
        let symbol = UserDefaults.standard.value(forKey: "currencySymbol") as! String
        
        if(dateArray.count > 0)
        {
            let calendar = Calendar.autoupdatingCurrent
            let components = calendar.dateComponents([.day, .hour, .minute], from: dateArray.last! as Date)
            let day = components.day
            
//            lblBudgetAmount.text =  String(format:"Budget : %.2f %@",(budgetInfo.budgetAmount * Double(day!)),symbol)
            lblBudgetAmount.text =  String(format:"Budget : %.2f %@", budgetInfo.budgetAmount,symbol)

            
            for i in 0..<dateArray.count
            {
                let temp = String.convertFormatOfDate(date: dateArray[i] as Date)
                if(!uniqueDateArray.contains(temp!))
                {
                    uniqueDateArray.add(temp!)
                }
            }
            
            let milisecondArray = NSMutableArray()
            
            for j in 0..<uniqueDateArray.count
            {
                for i in 0..<transactionArray.count
                {
                    if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String
                        )
                    {
                        if !milisecondArray.contains((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                        {
                            milisecondArray.add((transactionArray[i] as! NSMutableDictionary).value(forKey: "milliseconds") as! String)
                        }
                    }
                }
            }
            print(milisecondArray)
            
            
            for j in 0..<uniqueDateArray.count
            {
                let temp1 : NSMutableArray = []
                for i in 0..<transactionArray.count
                {
                    if(uniqueDateArray[j] as! String == (transactionArray[i] as! NSMutableDictionary).value(forKey: "date") as! String)
                    {
                        totalAmount = totalAmount + Double((transactionArray[i] as! NSMutableDictionary).value(forKey: "amount") as! String)!
                        print(totalAmount)
                        var dict = NSMutableDictionary()
                        dict = transactionArray[i] as! NSMutableDictionary
                        print(dict)
                        temp1.add(dict)
                    }
                }
                
                totalAmounts = totalAmounts + totalAmount
                
                lblCurrentAmount.text = String(format:"Used : %.2f %@",totalAmounts, symbol)
                
                lblTotalAmount.text = String(format:"Total Expense : %.2f %@",totalAmounts, symbol)
                
                let temp = SectionTransaction(amount: String(totalAmount), date: uniqueDateArray[j] as! String, items: temp1)
                
                print(temp)
                
             //   let difference = (budgetInfo.budgetAmount * Double(day!)) -  totalAmounts
                
                let difference =  budgetInfo.budgetAmount  -  totalAmounts

                lblSaveAmount.text = String(format:"Saved Amount : %.2f %@", difference, symbol)
                
                sections.append(temp)
            }
           // self.setChart(["Red","Green"], values: [(budgetInfo.budgetAmount * Double(day!)),totalAmounts])
            self.setChart(["Red","Green"], values: [budgetInfo.budgetAmount,totalAmounts])
            
            tblv.reloadData()
            print(sections.count)
            tblv.isHidden = false
        }
        else
        {
            lblBudgetAmount.text =  String(format:"Budget : %.2f %@",(budgetInfo.budgetAmount * 1.0),symbol)
            
            lblCurrentAmount.text = String(format:"Used : 0.0 %@", symbol)
            lblTotalAmount.text = String(format:"Total Expense : 0.0 %@",symbol)
            lblSaveAmount.text = String(format:"Saved Amount : %.2f %@", budgetInfo.budgetAmount, symbol)

            tblv.isHidden = true
        }
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)        
        BottomHeightConstraint.constant = 0
    }

    //MARK:- Action Method
    @IBAction func btnBackTap(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEditTap(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BudgetDetailViewController") as! BudgetDetailViewController
        vc.viewMode = "edit"
        vc.info = info
        vc.categoryInfo = categoryInfo
        vc.isBudget = isBudget
        vc.budgetInfo = budgetInfo
        vc.isComeFromHistory = 1
        vc.passingDate = passingDate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sections.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! TransactionHeaderCell
        
        headerCell.viewByDate.backgroundColor = UIColor.clear
        
        headerCell.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 252.0/255.0, alpha: 1)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        headerCell.lblTotalAmount.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        
        headerCell.img_Category.isHidden = true
        headerCell.lblCategoryName.isHidden = true
        headerCell.lblCategoryTotalAmount.isHidden = true
        headerCell.viewByDate.isHidden = false
        headerCell.lblDate.text = sections[section].date
        headerCell.lblDate.font = tableHeaderFont
        headerCell.lblTotalAmount.font = tableHeaderFont
        headerCell.lblDate.textColor = Constant.tableSubTextColorCode
        headerCell.lblTotalAmount.textColor = Constant.tableTextColorCode
        //            headerCell.lblTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(sections[section].amount!)"//sections[section].amount
        
        headerCell.lblTotalAmount.method = .easeOut
        
        headerCell.lblTotalAmount.count(from: 0, to: CGFloat(Double(sections[section].amount!)!), withDuration: 0.0)

        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
         return 30.0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subCell") as! TransactionCell
        
        var temp = NSMutableDictionary()
        
        cell.backgroundColor = UIColor.white
        temp = sections[indexPath.section].items[indexPath.row] as! NSMutableDictionary
        cell.lblSubCategoryName.textColor = Constant.tableTextColorCode
        cell.lblSubCategoryName.font = tableMainFont
        cell.lblAmount.font = tableMainFont
        
        cell.lblSubCategoryName.text = temp["subCatName"] as? String
        
        cell.lblDescription.textColor = Constant.tableSubTextColorCode
        
        cell.lblDescription.text = temp["note"] as? String
        cell.lblDescription.font = tableSubFont
        cell.img_subCategory.image = UIImage(named: temp["subCatImage"] as! String)
        cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(temp["amount"] as! String)"
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        cell.lblAmount.formatBlock = {(_ value: CGFloat) -> String in
            let formatted: String? = formatter.string(for: Double(value))
            return "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatted!)"
        }
        cell.lblAmount.method = .easeOut
        
        cell.lblAmount.count(from: 0, to: CGFloat(Double(temp["amount"] as! String)!), withDuration: 0.0)
        
        if(temp["setType"] as! String == "0")
        {
            cell.lblAmount.textColor = Constant.expenseColorCode
        }
        else
        {
            cell.lblAmount.textColor = Constant.incomeColorCode
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}

extension UIColor {
    convenience init(hexString:String) {
        let hexString:NSString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 020
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}

