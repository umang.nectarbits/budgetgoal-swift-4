//
//  LoanDetailVC.swift
//  Budget Goals
//
//  Created by nectarbits on 6/24/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit

class LoanDetailVC: UIViewController {

    
    //For Loan Detail Card
    @IBOutlet weak var LoanDetailHeaderView: UIView!
    @IBOutlet weak var LoanDetailCardView: UIView!
    @IBOutlet weak var CardHeaderView: UIView!
    @IBOutlet weak var lblLoanName1: UILabel!
    @IBOutlet weak var lblLoanAccount: UILabel!
    @IBOutlet weak var lblStartLoanTitle: UILabel!
    @IBOutlet weak var lblStartLoan: UILabel!
    @IBOutlet weak var lblTotalAmountTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblLoanTenureTitle: UILabel!
    @IBOutlet weak var lblLoanTenure: UILabel!
    @IBOutlet weak var lblInterestTitle: UILabel!
    @IBOutlet weak var lblInterest: UILabel!
    @IBOutlet weak var lblPayOnTitle: UILabel!
    @IBOutlet weak var lblPayOn: UILabel!
    @IBOutlet weak var lblNextEMIDateTitle: UILabel!
    @IBOutlet weak var lblNextEMIDate: UILabel!
    @IBOutlet weak var lblNoOfInstallmentTitle: UILabel!
    @IBOutlet weak var lblNoOfInstallment: UILabel!
    @IBOutlet weak var lblMainAmountTitle: UILabel!
    @IBOutlet weak var lblMainAmount: UILabel!
    @IBOutlet weak var lblTotalPaidPercentage: UILabel!
    @IBOutlet var LoanProgress: GradientProgressBar!
    @IBOutlet weak var lblThisInstallmentTitle: UILabel!
    @IBOutlet weak var lblPaidUnpaid: UILabel!
    @IBOutlet weak var btnPayLoan: UIButton!
    @IBOutlet weak var btnViewStatement: UIButton!
    @IBOutlet var btnPaidHeightConstraint: NSLayoutConstraint!
    
    var listAccountArray = [Account]()
    var LoanData = Loans()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchAccounts()
        
        LoanProgress.gradientColors = [UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1).cgColor, UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1).cgColor]
        LoanProgress.tintColor = UIColor.gray
        
        CardHeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        LoanDetailHeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        LoanDetails(LoanData: LoanData)
        // Do any additional setup after loading the view.
    }

    
    //MARK:- Fetch Account
    func fetchAccounts(){
        
        let accounts = uiRealm.objects(Account.self)
        if(accounts.count != 0){
            
            listAccountArray = accounts.toArray!
        }else{
            print("no acount Found")
        }
        
    }
    //MARK:- Loan Details
    func LoanDetails(LoanData: Loans){
        
        var dict = Loans()
        dict = LoanData

        var LoanTime = Double()
        var dailyTime = Double()
//        var monthlyPayment = Double()
        
        if dict.loanPayOn == "Daily"{
            dailyTime = Double(LoanTime/12) * 365
            LoanTime = dailyTime
        }
        
        if("\(LoanData.loanYear)" != "0")
        {
            let LoanYear = Constant.TO_DOUBLE((LoanData.loanYear))
            LoanTime = Double(LoanYear)
        }
        if("\(LoanData.loanMonth)" != "0")
        {
            let LoanMonth = Constant.TO_DOUBLE((Double(LoanData.loanMonth)/12))
            LoanTime = Double(LoanMonth)
        }
        
        let r = Double(LoanData.loanIntrest) / 1200
        let r1 = pow(r+1, Double(LoanTime) * 12)
        let monthlyPayment = (r + (r/(r1 - 1))) * Double(LoanData.loanAmount)
        let totalPayment = monthlyPayment * Double(LoanTime) * 12
        let totalInterest = totalPayment - Double(LoanData.loanAmount)
        
//        if(dict.loanIntrest != 0.0){
//            let r =  Constant.TO_DOUBLE(dict.loanIntrest) / (Double(LoanTime)*100)
//            let r1 = pow(r+1, Double(LoanTime))
//            monthlyPayment = (r + (r/(r1 - 1))) * Constant.TO_DOUBLE(dict.loanAmount)
//        }
//        else{
//            monthlyPayment = Constant.TO_DOUBLE(dict.loanAmount) / LoanTime
//        }
        
        print("EMI Amount:- \(monthlyPayment)")
        print("total Payment :- \(totalPayment)")
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        print("Todays Date:- \(TodayDate)")
        
        var Past = Int()
        if(LoanData.loanPayOn == "Daily")
        {
            Past = TodayDate.days(from: LoanData.NextEmiDate)
            
        }else{
            Past = TodayDate.months(from: LoanData.NextEmiDate)
        }
        print("Past :- \(Past)")
        
        if (dict.NextEmiDate.timeIntervalSinceNow.sign == .minus) {
            //is Future Date
            btnPaidHeightConstraint.constant = 30
            lblPaidUnpaid.text = "Unpaid"
            
        } else {
            //is Erlier Date
            btnPaidHeightConstraint.constant = 0
            lblPaidUnpaid.text = "Paid"
        }
        
        lblLoanName1.text = "\(dict.loanName)"
        lblLoanAccount.text = listAccountArray[dict.accountID].name
        lblStartLoan.text = String.convertFormatOfDate(date: dict.loanStartDate)
        lblTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(dict.loanAmount)"
        if(dict.loanYear == 0){
            lblLoanTenure.text = "\(dict.loanMonth) Months"
        }
        else{
            lblLoanTenure.text = "\(dict.loanYear) Years"
        }
        
        lblInterest.text = "\(dict.loanIntrest)%"
        
        if(dict.loanPayOn == "1"){
            lblPayOn.text = "Monthly"
        }else{
            lblPayOn.text = "Yearly"
        }
        
        lblNextEMIDate.text = "\(String.convertFormatOfDate(date:dict.NextEmiDate)!)"
        lblNoOfInstallment.text = "\(dict.PaidEmiCount) from \(Int(LoanTime))"
        lblMainAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(String(format: "%.2f", totalPayment))"
        LoanProgress.progress = Float(((( Double(monthlyPayment) * Double(dict.PaidEmiCount)) / Double(totalPayment)) ))
        LoanProgress.tintColor = UIColor.lightGray
        lblTotalPaidPercentage.text = "\(String(format: "%.f", LoanProgress.progress * 100))% Paid"
    }
    
    func convertNextDate(dateString : Date, payType : String, value: Int) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
        let myDate = dateString
        //        let myDate = dateFormatter.date(from: dateString)!
        var tomorrow = Date()
        if(payType == "Daily")
        {
            tomorrow = Calendar.current.date(byAdding: .day, value: value, to: myDate)!
        }else{
            tomorrow = Calendar.current.date(byAdding: .month, value: value, to: myDate)!
        }
        //        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
        let somedateString = dateFormatter.string(from: tomorrow)
        print("your next Date is \(somedateString)")
        return tomorrow
    }
    
    //MARK:- Action Methods
    @IBAction func btnBack(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayLoanTap(_ sender: UIButton) {
        
        //For Update in Loan table
        let LoanUpdate = uiRealm.objects(Loans.self).filter("loanID = %@", LoanData.loanID)
        var TodayDate = Date()
        if let LoanData = LoanUpdate.first
        {
            try! uiRealm.write
            {
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy h:mm a"
                let result = formatter.string(from: date)
                TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
                print("Todays Date:- \(TodayDate)")
                
                var Past = Int()
                if(LoanData.loanPayOn == "Daily")
                {
                    Past = TodayDate.days(from: LoanData.NextEmiDate)
                    
                }else{
                    Past = TodayDate.months(from: LoanData.NextEmiDate)
                }
                print("Past :- \(Past)")
                
                LoanData.NextEmiDate = convertNextDate(dateString: LoanData.NextEmiDate, payType: LoanData.loanPayOn, value: (Past+1))
                LoanData.PaidEmiCount = LoanData.PaidEmiCount + (Past + 1)
                LoanData.EmiUpdatedDate = TodayDate
            }
        }
        
        //for Entry in Transaction Table
        var autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
        if(autoCatId == nil){
            autoCatId = 1
        }else{
            autoCatId = autoCatId! + 1
        }
        var transaction = Transaction()
        transaction = Transaction(value: [autoCatId!,
                                       LoanData.loanMainCategoryName,
                                       LoanData.loanMainCategoryImage,
                                       LoanData.loanMainCategoryID,
                                       LoanData.loanSubCategoryID,
                                       LoanData.loanSubCategoryName,
                                       LoanData.loanSubCategoryImage,
                                       String(format: "%.2f", LoanData.EmiValue),
                                       TodayDate,
                                       LoanData.loanName,
                                       LoanData.accountID,
                                       String("0"),
                                       LoanData.loanPayOn,
                                       String(""),
                                       TodayDate,
                                       Double(0.0)
            
            ])
        
        try! uiRealm.write { () -> Void in
            uiRealm.add(transaction)
            
        }
        
        //For Deduct Amout From Selected Account
        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (LoanData.accountID))
        if let workoutsAccount = workoutsAccount.first
        {
            try! uiRealm.write
            {
                workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(LoanData.EmiValue)
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnViewLoanStatement(_ sender: UIButton) {
        
        let dict = NSMutableDictionary()
        
        dict.setValue("\(LoanData.loanAmount)", forKey: "PrincipalAmount")
        dict.setValue("0", forKey: "DpAmount")
        dict.setValue("\(LoanData.loanIntrest)", forKey: "InterestAmount")
        
        var LoanTime = Double()
        if("\(LoanData.loanYear)" != "0")
        {
            let LoanYear = Constant.TO_DOUBLE((LoanData.loanYear))
            LoanTime = Double(LoanYear)
        }
        if("\(LoanData.loanMonth)" != "0")
        {
            let LoanMonth = Constant.TO_DOUBLE((Double(LoanData.loanMonth)/12))
            LoanTime = Double(LoanMonth)
        }
       
        dict.setValue(String(format: "%.1f",(LoanTime)), forKey: "LoanTime")
        
        let stringDate = String.convertFormatOfDate(date: LoanData.EmiDate)
        
        dict.setValue(stringDate, forKey: "EmiDate")
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmiStatisticsViewController") as! EmiStatisticsViewController
        vc.detailValue = dict
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
