//
//  BillVC.swift
//  Budget Goals
//
//  Created by nectarbits on 6/11/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds
import UserNotifications
import UserNotificationsUI

class LoanListCell: UITableViewCell
{
    @IBOutlet var imgLoanType: UIImageView!
    @IBOutlet var lblLoanType: UILabel!
    @IBOutlet var lblLoanDate: UILabel!
    @IBOutlet var lblLoanName: UILabel!
    @IBOutlet var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
}

class LoanListVC: UIViewController , GADBannerViewDelegate, UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var ExpencesHeaderView: UIView!
    @IBOutlet weak var tblLoanList: UITableView!
    @IBOutlet weak var tblBillList: UITableView!
    @IBOutlet weak var tblAllList: UITableView!
    @IBOutlet weak var lblNoLoanFound: UILabel!
    @IBOutlet weak var lblNoBillsFound: UILabel!
    @IBOutlet weak var LoanBillControll: UISegmentedControl!
    @IBOutlet var btnPaidHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnMenu: UIButton!
    
    //For Loan Detail Card
    @IBOutlet weak var LoanDetailView: UIView!
    @IBOutlet weak var LoanDetailCardView: UIView!
    @IBOutlet weak var CardHeaderView: UIView!
    @IBOutlet weak var lblLoanName1: UILabel!
    @IBOutlet weak var lblLoanType: UILabel!
    @IBOutlet weak var btnEditLoan: UIButton!
    @IBOutlet weak var lblStartLoanTitle: UILabel!
    @IBOutlet weak var lblStartLoan: UILabel!
    @IBOutlet weak var lblTotalAmountTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblLoanTenureTitle: UILabel!
    @IBOutlet weak var lblLoanTenure: UILabel!
    @IBOutlet weak var lblInterestTitle: UILabel!
    @IBOutlet weak var lblInterest: UILabel!
    @IBOutlet weak var lblPayOnTitle: UILabel!
    @IBOutlet weak var lblPayOn: UILabel!
    @IBOutlet weak var lblEmiTitle: UILabel!
    @IBOutlet weak var lblEMI: UILabel!
    @IBOutlet weak var lblNoOfInstallmentTitle: UILabel!
    @IBOutlet weak var lblNoOfInstallment: UILabel!
    @IBOutlet weak var lblMainAmountTitle: UILabel!
    @IBOutlet weak var lblMainAmount: UILabel!
    @IBOutlet weak var lblTotalPaidPercentage: UILabel!
    @IBOutlet var LoanProgress: GradientProgressBar!
    @IBOutlet weak var lblThisInstallmentTitle: UILabel!
    @IBOutlet weak var lblPaidUnpaid: UILabel!
    @IBOutlet weak var btnPaidLoan: UIButton!
    
    //For Bill Detail Card
    @IBOutlet weak var BillDetailView: UIView!
    @IBOutlet weak var BillDetailCardView: UIView!
    @IBOutlet weak var BillHeaderView: UIView!
    @IBOutlet weak var btnEditBill: UIButton!
    @IBOutlet weak var lblBillCategoryTitle: UILabel!
    @IBOutlet weak var lblBillName: UILabel!
    @IBOutlet weak var lblBillDateTitle: UILabel!
    @IBOutlet weak var lblBillDate: UILabel!
    @IBOutlet weak var lblBillAmountTitle: UILabel!
    @IBOutlet weak var lblBillAmount: UILabel!
    @IBOutlet weak var lblBillCycleTitle: UILabel!
    @IBOutlet weak var lblBillCycle: UILabel!
    @IBOutlet weak var lblBillReminderTitle: UILabel!
    @IBOutlet weak var lblBillReminder: UILabel!
    @IBOutlet weak var btnPayBill: UIButton!
    @IBOutlet weak var txtbillAmount: UITextField!
    
    //MARK:- Variable declaration
    fileprivate let actionButton = JJFloatingActionButton()
    var ExtraExpence = [Transaction]()
    var LoanData = [Loans]()
    var BillsData = [BillModel]()
    
    var ThisMonthBillList = [Transaction]()
    
    var AllExpenceData = [AllExpences]()
    var Maincategory = NSDictionary()
    
    //MARK:
    override func viewDidLoad() {
        super.viewDidLoad()

//        getExtraExpence()
//        getLoanInfo()
//        getBillInfo()
//        getAllExpence()
        
        if(UserDefaults.standard.value(forKey: "BillCategoryData") != nil)
        {
            Maincategory = UserDefaults.standard.value(forKey: "BillCategoryData")! as! NSDictionary
        }
        
        var ImageMenu = UIImage(named: "ic_menuBlack")?.withRenderingMode(.alwaysTemplate)
        ImageMenu = ImageMenu!.maskWithColor(color: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1))
        btnMenu.setImage(ImageMenu, for: .normal)
        
        LoanDetailView.frame = self.view.frame
        self.view.addSubview(LoanDetailView)
        LoanDetailView.isHidden = true
        
        LoanDetailCardView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
        LoanDetailCardView.layer.shadowOffset = CGSize(width: 0, height: 3)
        LoanDetailCardView.layer.cornerRadius = 8
        LoanDetailCardView.layer.shadowOpacity = 0.5
        LoanDetailCardView.layer.shadowRadius = 3
        
        CardHeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        LoanProgress.gradientColors = [UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1).cgColor, UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1).cgColor]
        
        ExpencesHeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        btnPaidLoan.layer.cornerRadius = 5
        
        
        BillDetailView.frame = self.view.frame
        self.view.addSubview(BillDetailView)
        BillDetailView.isHidden = true
        
        BillDetailCardView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.32).cgColor
        BillDetailCardView.layer.shadowOffset = CGSize(width: 0, height: 3)
        BillDetailCardView.layer.cornerRadius = 8
        BillDetailCardView.layer.shadowOpacity = 0.5
        BillDetailCardView.layer.shadowRadius = 3
        
        BillHeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        self.tblAllList.tableFooterView = UIView(frame: .zero)
        self.tblLoanList.tableFooterView = UIView(frame: .zero)
        self.tblBillList.tableFooterView = UIView(frame: .zero)
        
        self.tblAllList.contentInset.bottom = 66
        self.tblLoanList.contentInset.bottom = 66
        self.tblBillList.contentInset.bottom = 66
        self.tblLoanList.isHidden = true
        self.tblBillList.isHidden = true
        lblNoLoanFound.isHidden = true
        
        let font = UIFont(name: "HelveticaNeue-Bold", size: 15)
        LoanBillControll.setTitleTextAttributes([NSAttributedStringKey.font: font!], for: .normal)
        LoanBillControll.setTitleTextAttributes([NSAttributedStringKey.font: font!], for: .selected)
        
        /* // Action Buttons For Add Bills/Loans
        actionButton.addItem(title: "Extra", image: UIImage(named: "ic_OtherCat")) { item in
            print("Extra")
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "NewCategoryViewController") as! NewCategoryViewController
            VC.passingView = "addTransaction"
            VC.categoryType = "0"
            VC.strCategoryType = "Expense"
            VC.strCategoryID = "0"
            self.present(VC, animated: true, completion: nil)
        }
        
        //for Image Tint Color of Add Expence Buttons
        var Image = UIImage(named: "ic_OtherIncome")?.withRenderingMode(.alwaysTemplate)
        Image = Image?.maskWithColor(color: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1))
        
        actionButton.addItem(title: "Loan", image: Image) { item in
            print("Loan")
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLoanVC") as! AddNewLoanVC
            self.present(VC, animated: true, completion: nil)
        }
        
        actionButton.addItem(title: "Bill", image: UIImage(named: "ic_bill")) { item in
            
            print("Bill")
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
            self.present(VC, animated: true, completion: nil)
        }
        actionButton.buttonColor = UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1)
        actionButton.display(inViewController: self)
        actionButton.configureDefaultItem { (item) in
            item.titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
        }*/
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        getExtraExpence()
        getLoansInfo()
        getBillInfo()
//        getAllExpence()
    }
    
    //MARK:- Helper Methods
    func setUp()
    {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view == LoanDetailView {
            actionButton.isHidden = false
            self.LoanDetailView.transform = .identity
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                self.LoanDetailCardView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: {(finished: Bool) -> Void in
                // do something once the animation finishes, put it here
                self.LoanDetailView.isHidden = true
            })
            
        }
        else if(touch.view == BillDetailView) {
            actionButton.isHidden = false
            self.BillDetailView.transform = .identity
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                self.BillDetailCardView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: {(finished: Bool) -> Void in
                // do something once the animation finishes, put it here
                self.BillDetailView.isHidden = true
            })
        }
        
    }
    
    //for Get Bill Data
    func getLoansInfo(){
        
        let loans = uiRealm.objects(Loans.self)
        
        if(loans.count != 0){
            
            LoanData = loans.toArray!
            
        }
    }
    
    func getBillInfo(){
        
        BillsData.removeAll()
        
        let MainBill = uiRealm.objects(NewBills.self).filter("isMain=%@",1)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        print("Todays Date:- \(TodayDate)")
        
        for w in 0 ..< MainBill.count
        {
        
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "dd-MM-yyyy h:mm a"
            let billDate = Date.convertStringToDateWithUTC(strDate: "\(formatter1.string(from: MainBill[w].BillDate))", dateFormate: "dd-MM-yyyy h:mm a")
            
            let Diff = Calendar.current.dateComponents([.month], from: billDate, to: TodayDate).month
            if(Diff == 12){
                //If 12 Month Complete than fetch new 12 Month Bill
                self.AddBilltoNewBills(Bill: MainBill[w], todayDate: TodayDate)
            }
            
            let bills = uiRealm.objects(NewBills.self).filter("isMain=%@",0)
            var billModelArray = [BillModel]()
            for i in 0 ..< bills.count {
                let tempBill = BillModel(BillID: bills[i].BillID, BillMainCategoryName: bills[i].BillMainCategoryName, BillMainCategoryImage: bills[i].BillMainCategoryImage, BillMainCategoryID: bills[i].BillMainCategoryID, BillSubCategoryName: bills[i].BillSubCategoryName, BillSubCategoryImage: bills[i].BillSubCategoryImage, BillSubCategoryID: bills[i].BillSubCategoryID, BillAmount: bills[i].BillAmount, BillDate: bills[i].BillDate, BillName: bills[i].BillName, accountID: bills[i].accountID, isMain: bills[i].isMain, MainBillID: bills[i].MainBillID, BillCycle: bills[i].BillCycle, BillUpdatedDate: bills[i].BillUpdatedDate)
                billModelArray.append(tempBill)
            }
            if(billModelArray.count != 0) {
                
                for i in 0..<billModelArray.count{
                    
                    if(billModelArray[i].BillMainCategoryID == BillCategoryID){
                        
    //                    let billDate = Date.convertStringToDateWithUTC(strDate: "\(formatter.string(from: billModelArray[i].BillDate!))", dateFormate: "dd-MM-yyyy h:mm a")
                        
                        if(TodayDate > billDate){
                            
                            let thisMonth = Date().GetCurrentMonth(date: TodayDate)
                            let BillMonth = Date().GetCurrentMonth(date: billModelArray[i].BillDate!)
                            let monthdiff = Calendar.current.dateComponents([.month], from: billModelArray[i].BillDate!.startOfMonth(), to: TodayDate.startOfMonth()).month
                            var repeattype = 0
                            if monthdiff == 1 {
                                repeattype = 3
                            }else if monthdiff == 2 {
                                repeattype = 4
                            }else if monthdiff == 3 {
                                repeattype = 5
                            }else if monthdiff == 6 {
                                repeattype = 6
                            }else if monthdiff == 12 {
                                repeattype = 7
                            }
                            //   && repeattype == Int(bills[i].BillCycle)
                            if (TodayDate.startOfMonth() >= billModelArray[i].BillDate!.startOfMonth()){
                                
                                if Int(billModelArray[i].BillCycle!) == 3 {
                                    if monthdiff! > 1 {
    //                                    for j in 0 ..< monthdiff! {
                                            let tempbill1 = getUpdatedBillModel(bill: billModelArray[i], billDate: billDate, index: 0, diff: 1)
                                            BillsData.append(tempbill1)
    //                                    }
                                    }else{
                                        BillsData.append(billModelArray[i])
                                    }
                                }else if Int(billModelArray[i].BillCycle!) == 4 {
                                    if monthdiff! > 2 {
                                        let count = Int(monthdiff!/2)
    //                                    for j in 0 ..< count {
                                            let tempbill1 = getUpdatedBillModel(bill: billModelArray[i], billDate: billDate, index: 0, diff: 2)
                                            BillsData.append(tempbill1)
    //                                    }
                                    }else{
                                        BillsData.append(billModelArray[i])
                                    }
                                }else if Int(billModelArray[i].BillCycle!) == 5 {
                                    if monthdiff! > 3 {
                                        let count = Int(monthdiff!/3)
    //                                    for j in 0 ..< count {
                                            let tempbill1 = getUpdatedBillModel(bill: billModelArray[i], billDate: billDate, index: 0, diff: 3)
                                            BillsData.append(tempbill1)
    //                                    }
                                    }else{
                                        BillsData.append(billModelArray[i])
                                    }
                                }else if Int(billModelArray[i].BillCycle!) == 6 {
                                    if monthdiff! > 6 {
                                        let count = Int(monthdiff!/6)
    //                                    for j in 0 ..< count {
                                            let tempbill1 = getUpdatedBillModel(bill: billModelArray[i], billDate: billDate, index: 0, diff: 6)
                                            BillsData.append(tempbill1)
    //                                    }
                                    }else{
                                        BillsData.append(billModelArray[i])
                                    }
                                }else if Int(billModelArray[i].BillCycle!) == 7 {
                                    if monthdiff! > 12 {
                                        let count = Int(monthdiff!/12)
    //                                    for j in 0 ..< count {
                                            let tempbill1 = getUpdatedBillModel(bill: billModelArray[i], billDate: billDate, index: 0, diff: 12)
                                            BillsData.append(tempbill1)
    //                                    }
                                    }else{
                                        BillsData.append(billModelArray[i])
                                    }
                                }
                            }
                        }
                    }
                }
                
                if(BillsData.count != 0){
                    tblAllList.isHidden = false
                    lblNoBillsFound.isHidden = true
                    print(ThisMonthBillList)
                    tblBillList.reloadData()
                }else{
                    tblAllList.isHidden = true
                    lblNoBillsFound.isHidden = false
                }
                
            }else{
                tblAllList.isHidden = true
                lblNoBillsFound.isHidden = true
            }
            
        }
    }
    
    func getUpdatedBillModel(bill:BillModel,billDate:Date,index:Int,diff:Int) -> BillModel {
        let temp = BillModel(
            BillID: bill.BillID!,
            BillMainCategoryName: bill.BillMainCategoryName!,
            BillMainCategoryImage: bill.BillMainCategoryImage!,
            BillMainCategoryID: bill.BillMainCategoryID!,
            BillSubCategoryName: bill.BillSubCategoryName!,
            BillSubCategoryImage: bill.BillSubCategoryImage!,
            BillSubCategoryID: bill.BillSubCategoryID!,
            BillAmount: bill.BillAmount!,
            BillDate: bill.BillDate!,//Calendar.current.date(byAdding: .month, value: index + diff, to: billDate)!,
            BillName: bill.BillName!,
            accountID: bill.accountID!,
            isMain: bill.isMain!,
            MainBillID: bill.MainBillID!,
            BillCycle: bill.BillCycle!,
            BillUpdatedDate: bill.BillUpdatedDate)//Calendar.current.date(byAdding: .month, value: index + diff, to: billDate)!)
        return temp
    }
    
    //MARK:- Entry in NewBills Table
    func AddBilltoNewBills(Bill:NewBills , todayDate: Date){
        
        //for Entry in Transaction Table
      
        let updateMainBill = uiRealm.objects(NewBills.self).filter("BillID=%@",Bill.BillID).first
        
        try! uiRealm.write {
            updateMainBill?.BillDate = todayDate
        }
        
        if Bill.BillCycle == "3" {
            for i in 0 ..< 12 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo = NewBills(value: [autoCatIdNew!,
                                                     Maincategory["categoryName"]!,
                                                     Maincategory["categoryimage"]!,
                                                     Maincategory["categoryID"]!,
                                                     String(Bill.BillSubCategoryName),
                                                     Bill.BillSubCategoryImage,
                                                     Bill.BillSubCategoryID,
                                                     String(Bill.BillAmount),
                                                     Calendar.current.date(byAdding: .month, value: i + 1, to: Bill.BillDate)!,
                                                     String(Bill.BillName),
                                                     Bill.accountID,
                                                     0,
                                                     Bill.BillID,
                                                     Bill.BillCycle,
                                                     Bill.BillUpdatedDate
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo)
                    
                }
            }
        }else if Bill.BillCycle == "4" {
            for i in 0 ..< 6 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo1 = NewBills(value: [autoCatIdNew!,
                                                      Maincategory["categoryName"]!,
                                                      Maincategory["categoryimage"]!,
                                                      Maincategory["categoryID"]!,
                                                      String(Bill.BillSubCategoryName),
                                                      Bill.BillSubCategoryImage,
                                                      Bill.BillSubCategoryID,
                                                      String(Bill.BillAmount),
                                                      Calendar.current.date(byAdding: .month, value: (i*2) + 2, to: Bill.BillDate)!,
                                                      String(Bill.BillName),
                                                      Bill.accountID,
                                                      0,
                                                      Bill.BillID,
                                                      Bill.BillCycle,
                                                      Bill.BillUpdatedDate
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo1)
                    
                }
            }
        }else if Bill.BillCycle == "5" {
            for i in 0 ..< 4 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo2 = NewBills(value: [autoCatIdNew!,
                                                      Maincategory["categoryName"]!,
                                                      Maincategory["categoryimage"]!,
                                                      Maincategory["categoryID"]!,
                                                      String(Bill.BillSubCategoryName),
                                                      Bill.BillSubCategoryImage,
                                                      Bill.BillSubCategoryID,
                                                      String(Bill.BillAmount),
                                                      Calendar.current.date(byAdding: .month, value: (i*3) + 3, to: Bill.BillDate)!,
                                                      String(Bill.BillName),
                                                      Bill.accountID,
                                                      0,
                                                      Bill.BillID,
                                                      Bill.BillCycle,
                                                      Bill.BillUpdatedDate
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo2)
                    
                }
            }
        }else if Bill.BillCycle == "6" {
            for i in 0 ..< 2 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo3 = NewBills(value: [autoCatIdNew!,
                                                      Maincategory["categoryName"]!,
                                                      Maincategory["categoryimage"]!,
                                                      Maincategory["categoryID"]!,
                                                      String(Bill.BillSubCategoryName),
                                                      Bill.BillSubCategoryImage,
                                                      Bill.BillSubCategoryID,
                                                      String(Bill.BillAmount),
                                                      Calendar.current.date(byAdding: .month, value: (i*6) + 6, to: Bill.BillDate)!,
                                                      String(Bill.BillName),
                                                      Bill.accountID,
                                                      0,
                                                      Bill.BillID,
                                                      Bill.BillCycle,
                                                      Bill.BillUpdatedDate
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo3)
                    
                }
            }
        }else if Bill.BillCycle == "7" {
            for i in 0 ..< 1 {
                var autoCatIdNew = uiRealm.objects(NewBills.self).max(ofProperty: "BillID") as Int?
                
                if(autoCatIdNew == nil)
                {
                    autoCatIdNew = 1
                }
                else
                {
                    autoCatIdNew = autoCatIdNew! + 1
                }
                let TempBillsInfo4 = NewBills(value: [autoCatIdNew!,
                                                      Maincategory["categoryName"]!,
                                                      Maincategory["categoryimage"]!,
                                                      Maincategory["categoryID"]!,
                                                      String(Bill.BillSubCategoryName),
                                                      Bill.BillSubCategoryImage,
                                                      Bill.BillSubCategoryID,
                                                      String(Bill.BillAmount),
                                                      Calendar.current.date(byAdding: .month, value: (i*12) + 12, to: Bill.BillDate)!,
                                                      String(Bill.BillName),
                                                      Bill.accountID,
                                                      0,
                                                      Bill.BillID,
                                                      Bill.BillCycle,
                                                      Bill.BillUpdatedDate
                    ])
                try! uiRealm.write { () -> Void in
                    uiRealm.add(TempBillsInfo4)
                    
                }
            }
        }
        
    }
    
   
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == tblAllList){
         return BillsData.count
        }else if(tableView == tblLoanList){
            return LoanData.count
        }else {
            return BillsData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoanListCell", for: indexPath) as! LoanListCell
        if(tableView == tblAllList){
            
            cell.lblAmount.textColor = UIColor.red
            
            cell.lblAmount.text  = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(BillsData[indexPath.row].BillAmount!)"
//            print("Cell Date : \(BillsData[indexPath.row].BillDate!)")
            cell.lblLoanDate.text = String.convertFormatOfDateToStringSpecificFormat(date: BillsData[indexPath.row].BillDate!, format: "dd-MMM-yyyy")
            cell.lblLoanType.text = BillsData[indexPath.row].BillSubCategoryName
            cell.lblLoanName.text = BillsData[indexPath.row].BillName
            cell.imgLoanType.image = UIImage(named: BillsData[indexPath.row].BillMainCategoryImage!)
            
        }else if(tableView == tblLoanList) {
           cell.lblAmount.textColor = UIColor.red
            
            cell.lblLoanType.text = LoanData[indexPath.row].loanSubCategoryName
            cell.lblLoanName.text = LoanData[indexPath.row].loanName
            cell.lblLoanDate.text = String.convertFormatOfDateToStringSpecificFormat(date: LoanData[indexPath.row].EmiDate, format: "dd-MMM-yyyy")
            cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(String(format: "%.2f", LoanData[indexPath.row].EmiValue))"
            
            cell.imgLoanType.image = UIImage(named: LoanData[indexPath.row].loanSubCategoryImage)
        }else {
            cell.lblAmount.textColor = UIColor.red
            cell.lblLoanType.text = BillsData[indexPath.row].BillSubCategoryName
            cell.lblLoanName.text = BillsData[indexPath.row].BillName
            cell.lblLoanDate.text = String.convertFormatOfDateToStringSpecificFormat(date: BillsData[indexPath.row].BillDate!, format: "dd-MMM-yyyy")
            cell.lblAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(BillsData[indexPath.row].BillAmount!)"
            
            cell.imgLoanType.image = UIImage(named: BillsData[indexPath.row].BillMainCategoryImage!)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if(tableView == tblAllList){

            actionButton.isHidden = true
            btnEditBill.tag = indexPath.row
            
            BillDetailView.isHidden = false
            UIView.animate(withDuration: 1,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.BillDetailCardView.transform = .identity
                },completion: nil)
            
//            let data = uiRealm.objects(NewBills.self).filter("BillID = %@",BillsData[indexPath.row].BillID as! Int).first
            
            BillsDetails(billData: BillsData[indexPath.row], indexPath: indexPath)
            
        }else if(tableView == tblLoanList) {
            
//            actionButton.isHidden = true
//
//            btnPaidLoan.tag = indexPath.row
//
//            LoanDetailView.isHidden = false
//            UIView.animate(withDuration: 1,
//                           delay: 0,
//                           usingSpringWithDamping: 0.2,
//                           initialSpringVelocity: 6.0,
//                           options: .allowUserInteraction,
//                           animations: { [weak self] in
//                            self?.LoanDetailCardView.transform = .identity
//                },completion: nil)
//            LoanDetails(LoanData: LoanData[indexPath.row])
            ViewLoanStatistic(indexpath: indexPath, data: LoanData[indexPath.row])
        }else {
            
            actionButton.isHidden = true
            btnEditBill.tag = indexPath.row
            
            BillDetailView.isHidden = false
            UIView.animate(withDuration: 1,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.BillDetailCardView.transform = .identity
                },
                           completion: nil)
//            BillsDetails(billData: BillsData[indexPath.row], indexPath: indexPath)
            
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete){
            
            if(tableView == tblAllList){
                
                //For Table All Expence List
                let BillList = uiRealm.objects(NewBills.self).filter("BillID = %@", BillsData[indexPath.row].BillID)
                
                try! uiRealm.write {
                    uiRealm.delete(BillList)
                }
                tblAllList.beginUpdates()
                BillsData.remove(at: indexPath.row)
                tblAllList.deleteRows(at: [indexPath as IndexPath], with: .fade)
                tblAllList.endUpdates()
                
                if(BillsData.count != 0){
                    lblNoLoanFound.isHidden = true
                    tblAllList.isHidden = false
                    
                }else{
                    lblNoLoanFound.isHidden = false
                    lblNoLoanFound.text = "No Expences Found"
                    tblAllList.isHidden = true
                }
                
                
             
            }
            
            //for Table Loan List
            else if(tableView == tblLoanList) {
                let LoanListList = uiRealm.objects(Loans.self).filter("loanID = %@",LoanData[indexPath.row].loanID)
            
                try! uiRealm.write {
                    
                    uiRealm.delete(LoanListList)
                }
                tblLoanList.beginUpdates()
                LoanData.remove(at: indexPath.row)
                tblLoanList.deleteRows(at: [indexPath as IndexPath], with: .fade)
                tblLoanList.endUpdates()
                
                if(LoanData.count != 0){
                    lblNoLoanFound.isHidden = true
                    tblLoanList.isHidden = false
                   
                }else{
                    lblNoLoanFound.isHidden = false
                    tblLoanList.isHidden = true
                }
                
            }
                
            //for Table Bill List
            else {
                let BillList = uiRealm.objects(NewBills.self).filter("BillID = %@", BillsData[indexPath.row].BillID)
                
                try! uiRealm.write {
                    uiRealm.delete(BillList)
                }
                tblBillList.beginUpdates()
                BillsData.remove(at: indexPath.row)
                tblBillList.deleteRows(at: [indexPath as IndexPath], with: .fade)
                tblBillList.endUpdates()
                
                if(BillsData.count != 0){
                    lblNoBillsFound.isHidden = true
                    tblBillList.isHidden = false
                    
                }else{
                    lblNoBillsFound.isHidden = false
                    tblBillList.isHidden = true
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    //MARK:- Helping Methods
    func ViewLoanStatistic(indexpath: IndexPath, data: Loans){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoanDetailVC") as! LoanDetailVC
        vc.LoanData = data
        self.navigationController?.pushViewController(vc, animated: true)
        
        /*
        let dict = NSMutableDictionary()
        
        dict.setValue("\(data.loanAmount)", forKey: "PrincipalAmount")
        dict.setValue("0", forKey: "DpAmount")
        dict.setValue("\(data.loanIntrest)", forKey: "InterestAmount")
        
        var LoanTime = Double()
        if("\(data.loanYear)" != "0")
        {
            let LoanYear = Constant.TO_DOUBLE((data.loanYear))
            LoanTime = Double(LoanYear)
        }
        if("\(data.loanMonth)" != "0")
        {
            let LoanMonth = Constant.TO_DOUBLE((Double(data.loanMonth)/12))
            LoanTime = Double(LoanMonth)
        }
        
//        if LoanData[indexpath.row].loanPayOn == "Daily"{
//            dailyTime = Double(LoanTime/12) * 365
//            LoanTime = dailyTime
//        }
        
        dict.setValue(String(format: "%.1f",(LoanTime)), forKey: "LoanTime")
       
        let stringDate = String.convertFormatOfDate(date: data.EmiDate)
        
        dict.setValue(stringDate, forKey: "EmiDate")
        
      
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmiStatisticsViewController") as! EmiStatisticsViewController
        vc.detailValue = dict
        self.navigationController?.pushViewController(vc, animated: true)
 */
}
    
    func LoanDetails(LoanData: Loans){
        
        var dict = Loans()
        dict = LoanData
        var pay = 0
        if dict.loanPayOn == "Daily"{
            pay = 365
        }else if dict.loanPayOn == "Monthly"{
            pay = 12
        }
        var LoanTime = Double()
        var dailyTime = Double()
        var monthlyPayment = Double()
        
        if("\(dict.loanYear)" != "0")
        {
            let LoanYear = Constant.TO_INT(dict.loanYear)
            LoanTime = Double(LoanYear * 12)
        }
        if("\(dict.loanMonth)" != "0")
        {
            let LoanMonth = Constant.TO_INT(dict.loanMonth)
            LoanTime = LoanTime + Double(LoanMonth)
        }
        
        if dict.loanPayOn == "Daily"{
            dailyTime = Double(LoanTime/12) * 365
            LoanTime = dailyTime
        }
        
        if(dict.loanIntrest != 0.0){
            let r =  Constant.TO_DOUBLE(dict.loanIntrest) / (Double(LoanTime)*100)
            let r1 = pow(r+1, Double(LoanTime))
            monthlyPayment = (r + (r/(r1 - 1))) * Constant.TO_DOUBLE(dict.loanAmount)
        }
        else{
            monthlyPayment = Constant.TO_DOUBLE(dict.loanAmount) / LoanTime
        }
        
        print("EMI Amount:- \(monthlyPayment)")
        
        let TotalAmount = LoanTime * monthlyPayment
        print("Total Amount: \(TotalAmount)")
        print("nunber of EMI: \(LoanTime)")
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        print("Todays Date:- \(TodayDate)")
        
        var Past = Int()
        if(LoanData.loanPayOn == "Daily")
        {
            Past = TodayDate.days(from: LoanData.NextEmiDate)
            
        }else{
            Past = TodayDate.months(from: LoanData.NextEmiDate)
        }
        print("Past :- \(Past)")
        
        if (dict.NextEmiDate.timeIntervalSinceNow.sign == .minus) {
            //is Future Date
            btnPaidHeightConstraint.constant = 30
            lblPaidUnpaid.text = "Unpaid\nNextEmiOn:  \(String.convertFormatOfDate(date:dict.NextEmiDate)!)"
            
        } else {
            //is Erlier Date
            btnPaidHeightConstraint.constant = 0
            lblPaidUnpaid.text = "Paid\nNextEmiOn:  \(String.convertFormatOfDate(date:dict.NextEmiDate)!)"
        }
        
        lblLoanName1.text = "\(dict.loanName)"
        lblLoanType.text = "\(dict.accountID)"
        lblStartLoan.text = String.convertFormatOfDate(date: dict.loanStartDate)
        lblTotalAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(dict.loanAmount)"
        if(dict.loanYear == 0){
            lblLoanTenure.text = "\(dict.loanMonth) Months"
        }
        else{
            lblLoanTenure.text = "\(dict.loanYear) Years"
        }
        
        lblInterest.text = "\(dict.loanIntrest)%"
        lblPayOn.text = dict.loanPayOn
        lblEMI.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(String(format: "%.2f", monthlyPayment))"
        lblNoOfInstallment.text = "\(dict.PaidEmiCount) from \(Int(LoanTime))"
        lblMainAmount.text = "\(UserDefaults.standard.value(forKey: "currencySymbol")!) \(String(format: "%.2f", TotalAmount))"
        LoanProgress.progress = Float(((( Double(monthlyPayment) * Double(dict.PaidEmiCount)) / Double(TotalAmount)) ))
        LoanProgress.tintColor = UIColor.lightGray
        lblTotalPaidPercentage.text = "\(String(format: "%.f", LoanProgress.progress * 100))% Paid"
    }
    
    func convertNextDate(dateString : Date, payType : String, value: Int) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
        let myDate = dateString
        //        let myDate = dateFormatter.date(from: dateString)!
        var tomorrow = Date()
        if(payType == "Daily")
        {
            tomorrow = Calendar.current.date(byAdding: .day, value: value, to: myDate)!
        }else{
            tomorrow = Calendar.current.date(byAdding: .month, value: value, to: myDate)!
        }
        //        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
        let somedateString = dateFormatter.string(from: tomorrow)
        print("your next Date is \(somedateString)")
        return tomorrow
    }
    
    
    func BillsDetails(billData: BillModel,indexPath: IndexPath) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        print("Todays Date:- \(TodayDate)")
        
        let temp = Date().months(from: billData.BillDate!)
        
        let monthdiff = Calendar.current.dateComponents([.month], from: billData.BillDate!, to: TodayDate).month
        var repeattype = 0
        if monthdiff == 1 {
            repeattype = 3
        }else if monthdiff == 2 {
            repeattype = 4
        }else if monthdiff == 3 {
            repeattype = 5
        }else if monthdiff == 6 {
            repeattype = 6
        }else if monthdiff == 12 {
            repeattype = 7
        }

            txtbillAmount.isHidden = false
            lblBillAmount.isHidden = true
            
            btnPayBill.isEnabled = true
            btnPayBill.backgroundColor = UIColor(red: 65/255.0, green: 117/255.0, blue: 5/255.0, alpha: 1)
            
            lblBillDate.text = String.convertFormatOfDate(date: billData.BillDate!)
            txtbillAmount.text = "\(billData.BillAmount!)"
            txtbillAmount.becomeFirstResponder()

        lblBillCategoryTitle.text = billData.BillSubCategoryName
        lblBillName.text = billData.BillName
        lblBillCycle.text = "\(Int(billData.BillCycle!)! - 2) months"
        if(billData.BillCycle != "0"){
            lblBillReminder.text = "Yes"
        }else {
            lblBillReminder.text = "No"
        }
        
        btnEditBill.isHidden = true
        btnEditBill.tag = indexPath.row
        btnPayBill.tag = indexPath.row
    }
    
    
    //MARK:- Update Bill Table Entry
    func UpdateBillEntry(index: Int, date: Date, Amount: String){
     
        let BillUpdate = uiRealm.objects(NewBills.self).filter("BillID = %@", BillsData[index].BillID)
        
        if let BillData = BillUpdate.first
        {
            try! uiRealm.write
            {
               
                BillData.BillDate = date
                BillData.BillAmount = Amount
                BillData.BillUpdatedDate = date
                
                actionButton.isHidden = false
                self.BillDetailView.transform = .identity
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                    self.BillDetailCardView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                }, completion: {(finished: Bool) -> Void in
                    // do something once the animation finishes, put it here
                    self.BillDetailView.isHidden = true
                })
                actionButton.removeFromSuperview()
                
                getBillInfo()
                
            }
        }
        
    }
    
    //MARK:- Action Methods
    @IBAction func indexChanged(_ sender: Any) {
        switch LoanBillControll.selectedSegmentIndex
        {
        case 0:
            tblBillList.isHidden = true
            tblLoanList.isHidden = true
            lblNoLoanFound.isHidden = true
            lblNoBillsFound.isHidden = true
            if(BillsData.count == 0){
                tblAllList.isHidden = true
                lblNoLoanFound.isHidden = false
                lblNoLoanFound.text = "No Expences Found"
            }
            else{
                tblAllList.isHidden = false
                lblNoLoanFound.isHidden = true
            }
            tblAllList.isHidden = false
            tblAllList.reloadData()
        case 1:
            tblBillList.isHidden = true
            tblAllList.isHidden = true
            lblNoBillsFound.isHidden = true
            if(LoanData.count == 0){
                lblNoLoanFound.isHidden = false
                lblNoLoanFound.text = "No Loans Found"
                tblLoanList.isHidden = true
            }else {
                lblNoLoanFound.isHidden = true
                tblLoanList.isHidden = false
            }
            tblLoanList.reloadData()
        case 2:
            tblLoanList.isHidden = true
            tblAllList.isHidden = true
            lblNoLoanFound.isHidden = true
            if(BillsData.count == 0){
                lblNoBillsFound.isHidden = false
                tblBillList.isHidden = true
            }else {
                lblNoBillsFound.isHidden = true
                tblBillList.isHidden = false
            }
            
            tblBillList.reloadData()
        default:
            break
        }
    }
    
    
    @IBAction func closeView(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func btnAddLoan(_ sender: UIButton) {
        
        if (LoanBillControll.selectedSegmentIndex == 0){
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLoanVC") as! AddNewLoanVC
            self.present(VC, animated: true, completion: nil)
        }
        else{
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
            self.present(VC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnEditLoan(_ sender: UIButton) {
//        LoanDetailView.isHidden = true
    }
    
    @IBAction func btnEditBill(_ sender: UIButton) {
        BillDetailView.isHidden = true
        
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddBillVC") as! AddBillVC
        VC.isEditBill = true
        if(tblAllList.isHidden){
//            VC.billDetail = BillsData[sender.tag]
        }else{
            let data = uiRealm.objects(NewBills.self).filter("BillID = %@",AllExpenceData[sender.tag].BillID as! Int).first
//            VC.billDetail = data!
        }
        
        actionButton.isHidden = false
        self.BillDetailView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.BillDetailCardView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
            self.BillDetailView.isHidden = true
        })
        
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
    
     @IBAction func btnPaidLoan(_ sender: UIButton) {
        
        let LoanUpdate = uiRealm.objects(Loans.self).filter("loanID = %@", LoanData[sender.tag].loanID)
        
        if let LoanData = LoanUpdate.first
        {
            try! uiRealm.write
            {
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy h:mm a"
                let result = formatter.string(from: date)
                let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
                print("Todays Date:- \(TodayDate)")
                
                var Past = Int()
                if(LoanData.loanPayOn == "Daily")
                {
                    Past = TodayDate.days(from: LoanData.NextEmiDate)
                    
                }else{
                    Past = TodayDate.months(from: LoanData.NextEmiDate)
                }
                print("Past :- \(Past)")
                
                LoanData.NextEmiDate = convertNextDate(dateString: LoanData.NextEmiDate, payType: LoanData.loanPayOn, value: (Past+1))
                LoanData.PaidEmiCount = LoanData.PaidEmiCount + (Past + 1)
                LoanData.EmiUpdatedDate = TodayDate
            }
        }
        
        self.LoanDetailView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.LoanDetailCardView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
            self.LoanDetailView.isHidden = true
        })
    }
    
    @IBAction func btnPayBill(_ sender: UIButton) {
     
        print("Pay bill Click")
        var Bill : BillModel!
        
        Bill = BillsData[sender.tag]

        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        print("Todays Date:- \(TodayDate)")
            
        var autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
        
        if(autoCatId == nil)
        {
            autoCatId = 1
        }
        else
        {
            autoCatId = autoCatId! + 1
        }
        
        var Billdate = String.convertFormatOfStringToDate(date: lblBillDate.text!)
        var BillInfo = Transaction()
        let catName = Maincategory["categoryimage"]!
        let catImage = Maincategory["categoryimage"]!
        let categoryID = Maincategory["categoryID"]!
        let billSubCatID = String(Bill.BillSubCategoryID!)
        let billSubCatName = String(Bill.BillSubCategoryName!)
        let billCatImage = String(Bill.BillMainCategoryImage!)
        let billAmount = String(txtbillAmount.text!)
        let billName = String(Bill.BillName!)
        let accountId = Bill.accountID
        let billCycle = Bill.BillCycle
        
        BillInfo = Transaction(value:[autoCatId!,
                                      catName,
                                      catImage,
                                      categoryID,
                                      billSubCatID,
                                      billSubCatName,
                                      billCatImage,
                                      billAmount,
                                      Billdate!,
                                      billName,
                                      accountId!,
                                      String("0"),
                                      billCycle!,
                                      String(""),
                                      TodayDate,
                                      Double(0.0)])
        
        try! uiRealm.write { () -> Void in
            uiRealm.add(BillInfo)
            
        }
        
        //For Deduct Amout From Selected Account
        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (BillInfo.accountId))
        if let workoutsAccount = workoutsAccount.first
        {
            try! uiRealm.write
            {
                workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(BillInfo.amount)!
            }
        }
        
        //For Delete Bill Data From NewBill Table & Update BillUpdateDate
        let data = uiRealm.objects(NewBills.self).filter("BillID=%@",Bill.BillID!).first
        let UpdateMainBill = uiRealm.objects(NewBills.self).filter("BillID=%@",Bill.MainBillID!).first
        if(UpdateMainBill!.BillUpdatedDate < Bill.BillDate!){
            try! uiRealm.write {
                UpdateMainBill?.BillUpdatedDate = Bill.BillDate!
            }
        }
        try! uiRealm.write {
            uiRealm.delete(data!)
        }
        
        //For Hide PopUp
        self.BillDetailView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.BillDetailCardView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
            self.BillDetailView.isHidden = true
        })
        
        //Reload Data
        self.getBillInfo()
        
    }
}
