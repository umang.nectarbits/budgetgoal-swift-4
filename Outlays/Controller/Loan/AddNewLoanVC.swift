//
//  AddNewLoanVC.swift
//  Budget Goals
//
//  Created by nectarbits on 6/11/19.
//  Copyright © 2019 Cools. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import DropDown


class AddNewLoanVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var HeaderView: UIView!
    @IBOutlet var lblLoanAccount: UILabel!
    @IBOutlet var btnLoanAccount: UIButton!
    @IBOutlet var lblLoanCategory: UILabel!
    @IBOutlet var btnLoanCategory: UIButton!
    @IBOutlet var lblLoanAmount: UILabel!
    @IBOutlet var txtLoanAmount: UITextField!
    @IBOutlet var lblLoanIntrest: UILabel!
    @IBOutlet var txtLoanIntrest: UITextField!
    @IBOutlet var lblLoanTenure: UILabel!
    @IBOutlet var txtLoanYears: UITextField!
    @IBOutlet var txtLoanMonths: UITextField!
    @IBOutlet var lblLoanStartDate: UILabel!
    @IBOutlet var txtLoanStartDate: UITextField!
    @IBOutlet var lblLoanName: UILabel!
    @IBOutlet var txtLoanName: UITextField!
    @IBOutlet var lblLoanPayOn: UILabel!
    @IBOutlet var txtLoanPayOn: UITextField!
    @IBOutlet var btnback: UIButton!
    @IBOutlet var lblReminder: UILabel!
    @IBOutlet var btnReminder: UIButton!
    @IBOutlet var btnAddLoan: UIButton!
    @IBOutlet var ic_DownArrow1: UIImageView!
    
    var strData = ""
    var selectedDate1 = Date()
    var dateToSelect: Date!
    var LoanCategory = ""
    var LoanCategoryImage = ""
    var reminder: Reminder?
    var listAccountArray = [Account]()
    var AccountId = Int()
    let AccountDropDown = DropDown()
    let CategoryDropDown = DropDown()
    
    var accountArray = [String]()
    var Maincategory = NSDictionary()
    let categoryArray = ["Home","Car","Student","Personal","Other"]
    var selectedCategoryID = ""
    
    let thePicker = UIPickerView()
    let CategoryImage = ["ic_HomeCat","ic_CarCat","ic_StudentLoan","ic_Personal","ic_OtherCat"]
    var TodayDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(UserDefaults.standard.value(forKey: "LoanCategoryData") != nil)
        {
            Maincategory = UserDefaults.standard.value(forKey: "LoanCategoryData")! as! NSDictionary
        }
        
        
        self.fetchAccounts()
        self.setupAccountDropDown()
        self.setupCategoryDropDown()
        
        HeaderView.setGradientColor(color1: UIColor(red: 244/255, green: 92/255, blue: 67/255, alpha: 1), color2: UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1))
        
        btnAddLoan.layer.cornerRadius = 5
        
        LoanCategoryImage = CategoryImage[0]
        btnLoanAccount.setTitle(accountArray[0], for: .normal)
        btnLoanCategory.setTitle(categoryArray[0], for: .normal)
        
        let image11 = UIImage(named: "ic_uncheck")?.withRenderingMode(.alwaysTemplate)
        let image12 = UIImage(named: "ic_check")?.withRenderingMode(.alwaysTemplate)
        btnReminder.setImage(image11, for: .normal)
        btnReminder.setImage(image12, for: .selected)
        btnReminder.tintColor = UIColor(red: 235/255, green: 51/255, blue: 73/255, alpha: 1)
        btnReminder.isSelected = true
        
//        txtLoanType.inputView = thePicker
//        thePicker.delegate = self
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy h:mm a"
        let result = formatter.string(from: date)
        TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
        txtLoanStartDate.text = String.convertFormatOfDate(date: TodayDate)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        
    }
    
    //MARK:- Fetch Accounts
    func fetchAccounts(){
        
        let accounts = uiRealm.objects(Account.self)
        if(accounts.count != 0){
            
            for j in 0..<accounts.count
            {
                accountArray.append(accounts[j].name)
            }
            AccountId = accounts[0].id
            listAccountArray = accounts.toArray!
        }else{
            print("no acount Found")
        }
    }
    
    //MARK:- SetUp DropDown
    func setupAccountDropDown() {
        AccountDropDown.anchorView = btnLoanAccount
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        AccountDropDown.dataSource = accountArray
        
        AccountDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.btnLoanAccount.setTitle(item, for: .normal)
            
            self.AccountId = self.listAccountArray[index].id
            
        }
    }
    
    func setupCategoryDropDown() {
        CategoryDropDown.anchorView = btnLoanCategory
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        CategoryDropDown.dataSource = categoryArray
        
        CategoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.btnLoanCategory.setTitle(item, for: .normal)
            self.selectedCategoryID = "\(index + 1)"
            self.LoanCategoryImage = self.CategoryImage[index]
        }
    }
    
    //MARK:- Text Field Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        if(textField == txtLoanYears){
            if(txtLoanMonths.text != ""){
                alertView(alertMessage: "Set only One Tenue either Year or Month")
                return
            }
        }
        else if(textField == txtLoanMonths){
            if(txtLoanYears.text != ""){
                alertView(alertMessage: "Set only One Tenue either Year or Month")
                return
            }
        }
        else if textField == txtLoanPayOn
        {
            print("You edit txtLoanPayOn")
            txtLoanPayOn.resignFirstResponder()
            //For Selection of Loan Payment method
            let alert = UIAlertController(title: "Pay On", message: "Please Select your payment method", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Yearly", style: .default , handler:{ (UIAlertAction)in
                self.txtLoanPayOn.text = "Yearly"
            }))
            
            alert.addAction(UIAlertAction(title: "Monthly", style: .default , handler:{ (UIAlertAction)in
                self.txtLoanPayOn.text = "Monthly"
            }))
        
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func textFieldEditing(sender: UITextField) {
       
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        txtLoanStartDate.text = dateFormatter.string(from: sender.date)
        selectedDate1 = sender.date
    }
    
    //AMRK:- Calculate EMI
    func EMICalculate(LoanData : Loans)
    {
        
        var dict = Loans()
        dict = LoanData
        var pay = 0
        if dict.loanPayOn == "Daily"{
           pay = 365
        }else if dict.loanPayOn == "Monthly"{
            pay = 12
        }
        var LoanTime = Double()
        var dailyTime = Double()
        
        if("\(dict.loanYear)" != "0")
        {
            let LoanYear = Constant.TO_INT(dict.loanYear)
            LoanTime = Double(LoanYear)
        }
        if("\(dict.loanMonth)" != "0")
        {
            let LoanMonth = (Constant.TO_INT(dict.loanMonth))/12
            LoanTime = LoanTime + Double(LoanMonth)
        }
        
        let r =  Constant.TO_DOUBLE(dict.loanIntrest) / 1200//(Double(LoanTime)*100)
        let r1 = pow(r+1, Double(LoanTime) * 12)
        var monthlyPayment = (r + (r/(r1 - 1))) * Constant.TO_DOUBLE(dict.loanAmount)
        
        print("EMI Amount:- \(monthlyPayment)")
        var TotalAmount = Double()
        if(dict.loanPayOn == "2"){
            monthlyPayment = monthlyPayment * 12
            TotalAmount = LoanTime * monthlyPayment
        }
        else{
            TotalAmount = monthlyPayment * Double(LoanTime) * 12
        }
        
        print("Total Amount: \(TotalAmount)")
        print("nunber of EMI: \(LoanTime)")
        
        dict.PaidEmiCount = 1
        dict.EmiValue = monthlyPayment
        dict.TotalAmount = TotalAmount
        
        if(dict.loanIsReminder){
//            SetReminder(EmiDate: dict.EmiDate, Id: dict.loanID, name: "Reminder for your \(dict.loanCategory)-\(dict.loanName) Loan", isDaily: dict.loanReminderType)
        }
        
        try! uiRealm.write { () -> Void in
            uiRealm.add(dict)
            
        }
        
        //for Entry in Transaction Table
        var autoCatId = uiRealm.objects(Transaction.self).max(ofProperty: "id") as Int?
        if(autoCatId == nil){
            autoCatId = 1
        }else{
            autoCatId = autoCatId! + 1
        }
        
        var repeatType = ""
        if(LoanData.loanPayOn == "1"){
            repeatType = "3"
        }else {
            repeatType = "7"
        }
        
        var transaction = Transaction()
        transaction = Transaction(value: [autoCatId!,
                                          LoanData.loanMainCategoryName,
                                          LoanData.loanMainCategoryImage,
                                          LoanData.loanMainCategoryID,
                                          LoanData.loanSubCategoryID,
                                          LoanData.loanSubCategoryName,
                                          LoanData.loanSubCategoryImage,
                                          String(format: "%.2f", LoanData.EmiValue),
                                          LoanData.loanStartDate,//TodayDate,
                                          LoanData.loanName,
                                          LoanData.accountID,
                                          String("0"),
                                          repeatType,
                                          String(""),
                                          LoanData.loanStartDate,
                                          Double(0.0)
            
            ])
        
        try! uiRealm.write { () -> Void in
            uiRealm.add(transaction)
            
        }
        
        //For Deduct Amout From Selected Account
        let workoutsAccount = uiRealm.objects(Account.self).filter("id = %@", (LoanData.accountID))
        if let workoutsAccount = workoutsAccount.first
        {
            try! uiRealm.write
            {
                workoutsAccount.remainigAmount = workoutsAccount.remainigAmount - Double(LoanData.EmiValue)
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func convertNextDate(dateString : Date, payType : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
        let myDate = dateString
//        let myDate = dateFormatter.date(from: dateString)!
        var tomorrow = Date()
        if(payType == "Yearly")
        {
            tomorrow = Calendar.current.date(byAdding: .year, value: 1, to: myDate)!
        }else{
            tomorrow = Calendar.current.date(byAdding: .month, value: 1, to: myDate)!
        }
//        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
        let somedateString = dateFormatter.string(from: tomorrow)
        print("your next Date is \(somedateString)")
        return tomorrow
    }
    
    func Validation() -> Bool {
        
        if(btnLoanAccount.titleLabel?.text == ""){
            alertView(alertMessage: "Enter Accoount")
            return false
        }
        else if(btnLoanCategory.titleLabel?.text == ""){
            alertView(alertMessage: "Enter Loan Category")
            return false
        }
        else if(txtLoanAmount.text == ""){
            alertView(alertMessage: "Enter Loan Amount")
            return false
        }
        else if(txtLoanIntrest.text == ""){
            alertView(alertMessage: "Enter Loan Intrest")
            return false
        }
        else if(txtLoanYears.text == "" && txtLoanMonths.text == ""){
            alertView(alertMessage: "Enter Loan Tenue")
            return false
        }
        
        else if(txtLoanStartDate.text == ""){
            alertView(alertMessage: "Enter Loan Start Date")
            return false
        }
        else if(txtLoanName.text == "") {
            alertView(alertMessage: "Enter Loan Name")
            return false
        }
        else if(txtLoanPayOn.text == ""){
            alertView(alertMessage: "Enter Loan Payment Method")
            return false
        }
        return true
    }
    
    //MARK:- Notification Fire
    func SetReminder(EmiDate: Date, Id: Int, name: String, isDaily: Int){
        
        var time = EmiDate
//        let timeInterval = floor(time.timeIntervalSinceReferenceDate/60)*60
//        time = Date(timeIntervalSinceReferenceDate: timeInterval)
        
        
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Reminder", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: name, arguments: nil)
        content.sound = UNNotificationSound(named: UNNotificationSoundName(string: "ChillingMusic.wav") as String)
        let notificationCategory = "\(time)"
        content.categoryIdentifier = notificationCategory
        
        let calendar = Calendar.current
        let day = calendar.component(.day, from: time)
        let month = calendar.component(.month, from: time)
        let year = calendar.component(.year, from: time)
        
        var dateComponents = DateComponents()
        
        if(isDaily == 1){
            dateComponents.hour = 10
            dateComponents.minute = 00
        }else{
            dateComponents.day = day
            dateComponents.month = month
            dateComponents.year = year
            dateComponents.hour = 10
            dateComponents.minute = 00
        }
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest.init(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request)
        
        reminder = Reminder(name: name, time: time, notification: notificationCategory)
        
    }
    
    //MARK:- Action methods
    @IBAction func btnback(_ sender: UIButton) {
        
       self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnAccountTap(_ sender: UIButton) {
        AccountDropDown.show()
    }
    
    @IBAction func btnLoanCategoryTap(_ sender: UIButton) {
        CategoryDropDown.show()
    }
    
    @IBAction func btnReminder(_ sender: UIButton) {
        if sender.isSelected{
            btnReminder.isSelected = false
        }
        else
        {
            btnReminder.isSelected = true
        }
        
    }
    
    @IBAction func btnAddLoan(_ sender: UIButton) {
        
        if(Validation()){
            
            var LoanInfo = Loans()
            var autoCatId = uiRealm.objects(Loans.self).max(ofProperty: "loanID") as Int?
            
            if(autoCatId == nil)
            {
                autoCatId = 1
            }
            else
            {
                autoCatId = autoCatId! + 1
            }
            
            if(txtLoanYears.text == ""){
                txtLoanYears.text = "0"
            }
            if(txtLoanMonths.text == ""){
                txtLoanMonths.text = "0"
            }
            
            var type = 1
            
            if(txtLoanPayOn.text == "Monthly")
            {
                type =  1
            }
            else if(txtLoanPayOn.text == "Yearly")
            {
                type =  2
            }
            
            var reminder = false
            if(btnReminder.isSelected){
                reminder = true
            }
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy h:mm a"
            let result = formatter.string(from: date)
            let TodayDate = Date.convertStringToDateWithUTC(strDate: "\(result)", dateFormate: "dd-MM-yyyy h:mm a")
            print("Todays Date:- \(TodayDate)")
            
            let nextDate = convertNextDate(dateString: selectedDate1, payType: txtLoanPayOn.text!)
            
            LoanInfo = Loans(value: [autoCatId!,
                                     Maincategory["categoryID"]!,
                                     Maincategory["categoryName"]!,
                                     Maincategory["categoryimage"]!,
                                     selectedCategoryID,
                                     String((btnLoanCategory.titleLabel?.text!)!),
                                     LoanCategoryImage,
                                     AccountId,
                                     Int(txtLoanAmount.text!)!,
                                     Float(txtLoanIntrest.text!)!,
                                     Int(txtLoanYears.text!)!,
                                     Int(txtLoanMonths.text!)!,
                                     selectedDate1,
                                     String(txtLoanName.text!),
                                     String(type),
                                     reminder,
                                     0,
                                     false,
                                     0,
                                     0,
                                     selectedDate1,
                                     selectedDate1,
                                     nextDate,
                                     TodayDate
                ])
            
            self.EMICalculate(LoanData: LoanInfo)
        }
    }

}
