	//
//  AddBillViewController.swift
//  Created by Cools on 10/13/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AddBillViewController: UIViewController,JBDatePickerViewDelegate,GADBannerViewDelegate,UITextViewDelegate {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var bgView : UIView!
    @IBOutlet var calendarView : UIView!
    @IBOutlet var dateHeaderView : UIView!
    @IBOutlet var headerView : UIView!
    @IBOutlet var presentMonth : UILabel!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var scrView: UIScrollView!
    @IBOutlet var dateView: UIView!
    @IBOutlet var amountView: UIView!
    @IBOutlet var billIdView: UIView!
    @IBOutlet var noteView: UIView!
    @IBOutlet var timeView: UIView!
    @IBOutlet var txtNote: UITextView!
    @IBOutlet var lblSubCategoryBill: UILabel!
    @IBOutlet var lblCategoryBill: UILabel!
    @IBOutlet var txtBillTime: AAPickerView!
    @IBOutlet var txtBillId: UITextField!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var btnSelectDate: UIButton!
    @IBOutlet var TopheaderView : UIView!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var selectedDate1 = Date()
    var dateToSelect: Date!
    var categoryData = [String:AnyObject]()
    var subCategoryData = [String:AnyObject]()
    var editBillData = [String:AnyObject]()
    var passingView = ""
    var selectBillFrequency = ""
    var passingDate : Date!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        IQKeyboardManager.sharedManager().enable = true
        setlayout()
        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddTransactionVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddTransactionVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if var keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardSize = self.view.convert(keyboardSize, from: nil)
            
            var contentInset:UIEdgeInsets = self.scrView.contentInset
            contentInset.bottom = keyboardSize.size.height
            self.scrView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            let contentInset:UIEdgeInsets = .zero
            self.scrView.contentInset = contentInset
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view != datePickerView && touch.view != dateHeaderView {
            removeAnimate()
        }
        
    }

    
    func setlayout()
    {
//        btnBack.setImage(UIImage(named : "ic_backWhite")?.maskWithColor(color: UIColor.white), for: .normal)

        self.calendarView.isHidden = true
        self.bgView.isHidden = true
        
        if passingView != ""
        {
            selectedDate1 =  String.convertFormatOfStringToDate(date: editBillData["dueDate"] as! String)
            passingDate = String.convertFormatOfStringToDate(date: editBillData["dueDate"] as! String)
            btnSelectDate.setTitle(String.convertFormatOfDate(date: String.convertFormatOfStringToDate(date: editBillData["dueDate"] as! String)), for: .normal)
        }
        else
        {
            selectedDate1 =  passingDate!
            btnSelectDate.setTitle(String.convertFormatOfDate(date: passingDate!), for: .normal)
        }
        
        
        datePickerView.delegate = self
        presentMonth.text = datePickerView.presentedMonthView?.monthDescription
        
        if passingView != ""
        {
            lblCategoryBill.text = editBillData["categoryName"] as? String
            lblSubCategoryBill.text = editBillData["subcategoryName"] as? String
        }
        else
        {
            lblCategoryBill.text = categoryData["vCatName"] as? String
            lblSubCategoryBill.text = subCategoryData["vSubCatName"] as? String
        }
        
        if passingView != ""
        {
            txtAmount.text = editBillData["amount"] as? String
            txtBillId.text = editBillData["billId"] as? String
            txtNote.text = editBillData["note"] as? String
        }
        
        
//        txtNote.addDoneButtonToKeyboard(myAction:  #selector(txtNote.resignFirstResponder))
//        txtAmount.addDoneButtonToKeyboard(myAction:  #selector(txtAmount.resignFirstResponder))
//        txtBillId.addDoneButtonToKeyboard(myAction:  #selector(txtBillId.resignFirstResponder))
        
        let stringData = ["Monthly","Bi-Monthly","Quaterly","Half Year","Yearly"]
        txtBillTime.stringPickerData = stringData
        txtBillTime.pickerType = .StringPicker
        txtBillTime.pickerRow.font = UIFont(name: "Helvatica-Regular", size: 30)
        
        txtBillTime.toolbar.barTintColor = .darkGray
        txtBillTime.toolbar.tintColor = .black
        
        txtNote.text = "Add Note.."
        txtNote.textColor = UIColor.lightGray
        
        if passingView != ""
        {
            selectBillFrequency = editBillData["repeatType"] as! String
            txtBillTime.text = stringData[Int(editBillData["repeatType"] as! String)!]
        }
        else
        {
            txtBillTime.text = stringData[0]
            selectBillFrequency = "0"
        }
        
        txtBillTime.stringDidChange = { index in
            
            print("selectedString ", stringData[index])
            if index == 0
            {
                self.selectBillFrequency = "0"
            }
            else if index == 1
            {
                self.selectBillFrequency = "1"
            }
            else if index == 2
            {
                self.selectBillFrequency = "2"
            }
            else if index == 3
            {
                self.selectBillFrequency = "3"
            }
            else{
                self.selectBillFrequency = "4"
            }
        }
        
        
    }
    
    override func viewWillLayoutSubviews() {
        btnDone.viewShadow()
        dateView.viewShadow()
        amountView.viewShadow()
        billIdView.viewShadow()
        noteView.viewShadow()
        timeView.viewShadow()
    }
    
    //MARK: - Admob Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        //        AddHeightConstraint.constant = 50
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
//            self.BottomHeightConstraint.constant = 50
            
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        
        BottomHeightConstraint.constant = 0
    }

    
    //MARK:- Action Method
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCalendarAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        showAnimate()
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton)
    {
        if passingView != ""
        {
            if txtAmount.text == "" || txtAmount.text == "0"
            {
                alertView(alertMessage: AlertString.ProperAmount)
            }
            else
            {
                let workouts = uiRealm.objects(Bills.self).filter("id = %@", (editBillData["id"] as! Int))
                if let workout = workouts.first
                {
                    try! uiRealm.write
                    {
                        workout.amount = txtAmount.text!
                        workout.dueDate = selectedDate1
                        workout.note = txtNote.text
                        workout.repeatType = selectBillFrequency
                        workout.billId = txtBillId.text!
                    }
                }
            }
        }
        else
        {
            if txtAmount.text == "" || txtAmount.text == "0"
            {
                alertView(alertMessage: AlertString.ProperAmount)
            }
            else
            {
                var taskListB = Bills()
                let autoCatId = uiRealm.objects(Bills.self).max(ofProperty: "id") as Int?
                var strNote = ""
                
                if txtNote.text == "Add Note.."
                {
                    strNote = ""
                }
                else
                {
                    strNote = txtNote.text!
                }
                
                if(autoCatId == nil)
                {
                    
                    taskListB = Bills(value: [1,categoryData["iCatId"] as! String,categoryData["vCatName"] as! String,subCategoryData["id"] as! String,subCategoryData["vSubCatName"] as! String,selectedDate1,Date(),txtAmount.text!,txtBillId.text!,selectBillFrequency,strNote,subCategoryData["imagePath"] as! String,false])
                }
                else
                {
                    let catId = (uiRealm.objects(Bills.self).max(ofProperty: "id") as Int?)! + 1
                    taskListB = Bills(value: [catId,categoryData["iCatId"] as! String,categoryData["vCatName"] as! String,subCategoryData["id"] as! String,subCategoryData["vSubCatName"] as! String,selectedDate1,Date(),txtAmount.text!,txtBillId.text!,selectBillFrequency,strNote,subCategoryData["imagePath"] as! String,false])
                }
                
                try! uiRealm.write { () -> Void in
                    uiRealm.add(taskListB)
                }
            }
        }
    
        let dashboardVC = navigationController!.viewControllers.filter { $0 is BillsViewController }.first!
        navigationController!.popToViewController(dashboardVC, animated: true)
    }
    
    //MARK:- Helper Method
    func showAnimate()
    {
        bgView.isHidden = false
        calendarView.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        calendarView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.calendarView.alpha = 1.0
            self.calendarView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.calendarView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.calendarView.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.calendarView.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    //MARK: - Textview Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add Note.." {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note.."
            textView.textColor = UIColor.lightGray
        }
    }
    
    //MARK: - JBDatePicker Delegate
    
    func didSelectDay(_ dayView: JBDatePickerDayView)
    {
        if let selectedDate = datePickerView.selectedDateView.date {
            
            let stringDate = String.convertFormatOfDate(date: selectedDate)
            
            btnSelectDate.setTitle(stringDate, for: .normal)
            selectedDate1 = selectedDate
            removeAnimate()
        }
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        presentMonth.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return passingDate//Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
    
    


}
