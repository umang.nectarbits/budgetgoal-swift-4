//
//  BillCategoryViewController.swift
//  Outlays
//
//  Created by harshesh on 26/10/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BillCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,GADBannerViewDelegate {
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var tblBillCategory: UITableView!
    @IBOutlet var HedarView: UIView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet weak var BottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var BannerHeightConstraint: NSLayoutConstraint!
    
    var category = [[String:AnyObject]]()
    var subcategory = [[String:AnyObject]]()
     var passingDate : Date!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        bannerView.adUnitID = "ca-app-pub-3258200689610307/2802395875"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
//        BottomHeightConstraint.constant = 0
        tblBillCategory.estimatedRowHeight = 44.0
        tblBillCategory.rowHeight = UITableViewAutomaticDimension
        tblBillCategory.tableFooterView = UIView.init(frame: CGRect.zero)
        lblHeaderTitle.font = headerTitleFont
        self.tblBillCategory.reloadData()
        getCategory()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaults.standard.value(forKey: "Purchase") != nil)
        {
            if(UserDefaults.standard.bool(forKey: "Purchase")){
                self.BottomHeightConstraint.constant = 0
                self.BannerHeightConstraint.constant = 0
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }else{
            self.BottomHeightConstraint.constant = 50
            self.BannerHeightConstraint.constant = 50
        }
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    func buttonPressed(sender:UIButton!)
    {
        let buttonRow = sender.tag
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillSubCategoryViewController") as! BillSubCategoryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCategory()
    {
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        let url = URL(string: "http://iappbits.in/outlays/ws_user/getCat/")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                
                print(error!.localizedDescription)
                
            } else {
                
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : AnyObject]
                    {
                        
                        //Implement your logic
                        print(json)
                        DispatchQueue.main.async {
                            self.category = json["categories"] as! [[String : AnyObject]]
                            self.subcategory  = json["categories_and_subcategories"] as! [[String : AnyObject]]
                            
                            print(self.category)
                            print(self.subcategory)
                            //                        print(json)
                            self.tblBillCategory.reloadData()
                        }
                       
                    }
                }
                 catch {
                    
                    print("error in JSONSerialization")
                    
                }
                
            }
            
        })
        self.tblBillCategory.reloadData()
        task.resume()
    }
    
//MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return category.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        let aCell = tableView.dequeueReusableCell(withIdentifier: "BillCell") as! BillCategoryTableCell
        aCell.lblTitle.text = ((category[indexPath.row] as NSDictionary).value(forKey: "vCatName") as? String)
        

        return aCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (subcategory.count) > indexPath.row
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillSubCategoryViewController") as! BillSubCategoryViewController
            vc.passingDate = passingDate
            vc.categoryData = category[indexPath.row]
            vc.subcategoryData = subcategory[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        vc.subcategorylist = (subcategory as! AnyObject) as! String
        
    }
    
    //MARK: - Admob Delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
            if(UserDefaults.standard.value(forKey: "Purchase") != nil)
            {
                if(UserDefaults.standard.bool(forKey: "Purchase")){
                    self.BottomHeightConstraint.constant = 0
                    self.BannerHeightConstraint.constant = 0
                }else{
                    self.BottomHeightConstraint.constant = 50
                    self.BannerHeightConstraint.constant = 50
                }
            }else{
                self.BottomHeightConstraint.constant = 50
                self.BannerHeightConstraint.constant = 50
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
        BottomHeightConstraint.constant = 0
    }

    
//MARK: - Action View
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
