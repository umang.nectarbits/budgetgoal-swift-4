//
//  EmiStatisticsViewController.swift
//  Outlays
//
//  Created by Nectarbits on 13/09/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import MessageUI

class EmiStatisticsViewController: UIViewController,SSSheetLayoutDataSource, UICollectionViewDataSource, MFMailComposeViewControllerDelegate {
    
    @IBOutlet var HeaderView : UIView!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnMail : UIButton!
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var lblHeaderTitle : UILabel!
    @IBOutlet var lblTotalIntrest : UILabel!
    @IBOutlet var lblTotalPayment : UILabel!
    @IBOutlet var lblEmi : UILabel!
    @IBOutlet var lblPayedPrincipal : UILabel!
    @IBOutlet var lblPayedIntrest : UILabel!
    @IBOutlet var lblPrincipal : UILabel!
    @IBOutlet var lblIntrest : UILabel!
    @IBOutlet var lblTotalRemaining : UILabel!
    @IBOutlet var collectionHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let cellName = "SLSheetCell"
    var detailValue = NSMutableDictionary()
    var yearWiseInstallment = [[String:AnyObject]]()
    var EmiArray = [[String:AnyObject]]()
    var startEMIDate : Date!
    var payedInterest : Double = 0
    var payedPrincipal : Double = 0
    var remainInterest : Double = 0
    var remainPrincial : Double = 0
    var sheetValue = [[String]]()
    var topBar = ["Month", "Principal", "Interest", "Balance"]
    var formatter = NumberFormatter()
    var PrincipalAmount : Double = 0.0
    var sectionForMonth : Int = 10000000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        let Image = UIImage(named: "ic_email")?.withRenderingMode(.alwaysTemplate)
        btnMail.setImage(Image, for: .normal)
        btnMail.tintColor = UIColor.white
        
//        lblHeaderTitle.font = headerTitleFont
        print(detailValue)
        PrincipalAmount = Double (detailValue.value(forKey: "PrincipalAmount") as! String)! - Double (detailValue.value(forKey: "DpAmount") as! String)!
        
        
        let r = Double (detailValue.value(forKey: "InterestAmount") as! String)! / 1200
        let r1 = pow(r+1, Double(detailValue.value(forKey: "LoanTime") as! String)! * 12)
        let monthlyPayment = (r + (r/(r1 - 1))) * PrincipalAmount
        let totalPayment = monthlyPayment * Double(detailValue.value(forKey: "LoanTime") as! String)! * 12
        let totalInterest = totalPayment - (PrincipalAmount)
        startEMIDate = Constant.convertEMIDate(detailValue.value(forKey: "EmiDate") as! String)
        
        
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        lblEmi.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(monthlyPayment)))!)"
        lblTotalPayment.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(totalPayment)))!)"
        lblTotalIntrest.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(totalInterest)))!)"
        
        getStatisticsByMonth(EmiDate: startEMIDate, p: PrincipalAmount, r: Double (detailValue.value(forKey: "InterestAmount") as! String)!, n: Int(Double(detailValue.value(forKey: "LoanTime") as! String)! * 12))
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    func getNoOfMonth() -> Int
    {
        let calendar = NSCalendar.current
        let now = Date()
        
        if now > startEMIDate
        {
            let components = calendar.dateComponents([.month], from: startEMIDate, to: now)
            return (components.month! + 1)
        }
        else
        {
            let components = calendar.dateComponents([.month], from: now, to: startEMIDate)
            return (components.month! + 1)
        }
    }
    
    func getStatisticsByMonth(EmiDate: Date, p: Double, r: Double, n: Int)
    {
        EmiArray = []
        let R = (r / 12) / 100;
        var P = p;
        let e = calcEmi(p: P, r: r, n: n);
        let totalInt = round((e * Double(n)) - p);
        let totalAmt = round((e * Double(n)));
        var tempDate : Date = EmiDate
        var intPerMonth = round(totalInt / Double(n));
        
        for i in 0 ..< n
        {
            intPerMonth = (P * R);
            P = ((P) - ((e) - (intPerMonth)));
            var dictEmi = [String:AnyObject]()
            dictEmi["MonthYear"] = Constant.getMonthYear(tempDate) as AnyObject
            dictEmi["Year"] = String(tempDate.year()) as AnyObject
            dictEmi["Month"] = String(i + 1) as AnyObject
            tempDate = Constant.getNextMonthDate(mydate: tempDate)
            dictEmi["Interest"] = String(round(intPerMonth)) as AnyObject
            dictEmi["Principal"] = String(round(e - intPerMonth)) as AnyObject
            dictEmi["Balance"] = String(round(P)) as AnyObject
            EmiArray.append(dictEmi)
        }
        print("Monthly : \(EmiArray)")
        
        sheetValue.append(topBar)
        
        for i in 0..<EmiArray.count
        {
            var drugObject = [String]()
            for index in 0..<topBar.count
            {
                if index == 0 {
                    drugObject.append(EmiArray[i]["MonthYear"] as! String)
                }
                else if index == 1
                {
                    let value = EmiArray[i]["Principal"] as! String
                    drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                }
                else if index == 2
                {
                    let value = EmiArray[i]["Interest"] as! String
                    drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                }
                else
                {
                    let value = EmiArray[i]["Balance"] as! String
                    drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                }
            }
            sheetValue.append(drugObject)
        }
        print(sheetValue)
        collectionView.dataSource = self
        register()
        collectionView.layoutIfNeeded()
        if collectionView.contentSize.height > self.view.frame.size.height - 64
        {
            collectionHeightConstraints.constant = self.view.frame.size.height - 64
        }
        else
        {
            collectionHeightConstraints.constant = collectionView.contentSize.height
        }
        
        getPayedAndRemainingAmount(MonthlyEmiArray: EmiArray)
        getStatisticsByYear(MonthlyEmiArray: EmiArray)
    }
    
    func getPayedAndRemainingAmount(MonthlyEmiArray: [[String:AnyObject]])
    {
        let noOfMonth = getNoOfMonth()
        
        for i in 0 ..< MonthlyEmiArray.count
        {
            if Int(MonthlyEmiArray[i]["Month"] as! String)! <= noOfMonth
            {
                payedInterest = payedInterest + Double(MonthlyEmiArray[i]["Interest"] as! String)!
                payedPrincipal = payedPrincipal + Double(MonthlyEmiArray[i]["Principal"] as! String)!
            }
            else
            {
                remainInterest = remainInterest + Double(MonthlyEmiArray[i]["Interest"] as! String)!
                remainPrincial = remainPrincial + Double(MonthlyEmiArray[i]["Principal"] as! String)!
            }
        }
        lblPayedPrincipal.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(payedPrincipal)))!)"
        lblPayedIntrest.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(payedInterest)))!)"
        lblPrincipal.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(remainPrincial)))!)"
        lblIntrest.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(remainInterest)))!)"
        lblTotalRemaining.text = "\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(formatter.string(from: NSNumber(value: Double(remainPrincial + remainInterest)))!)"
        
    }
    
    func getStatisticsByYear(MonthlyEmiArray: [[String:AnyObject]])
    {
        let yearArray = NSMutableArray()
        
        yearWiseInstallment = []
        for i in 0..<MonthlyEmiArray.count
        {
            let temp = MonthlyEmiArray[i]["Year"]
            
            if(!yearArray.contains(temp!))
            {
                yearArray.add(temp!)
            }
        }

        
        for j in 0 ..< yearArray.count
        {
            var interest : Double = 0.0
            var principal : Double = 0.0
            var balance : Double = 0.0
            var yearDict = [String:AnyObject]()
            for i in 0 ..< MonthlyEmiArray.count
            {
                if MonthlyEmiArray[i]["Year"] as! String  == yearArray[j] as! String
                {
                    interest = interest + Double(MonthlyEmiArray[i]["Interest"] as! String)!
                    principal = principal + Double(MonthlyEmiArray[i]["Principal"] as! String)!
                    balance = interest + principal//balance + Double(MonthlyEmiArray[i]["Balance"] as! String)!
                }
            }
            yearDict["Interest"] = String(interest) as AnyObject
            yearDict["Principal"] = String(principal) as AnyObject
            yearDict["Balance"] = String(balance) as AnyObject
            yearDict["Year"] = yearArray[j] as AnyObject
            yearWiseInstallment.append(yearDict)
        }
        print("Year : \(yearWiseInstallment)")
        
    }
    
    func register() {
        collectionView.register(UINib(nibName: cellName, bundle: nil), forCellWithReuseIdentifier: cellName)
    }
    
    func calcEmi(p: Double, r: Double, n: Int) -> Double
    {
        let R = (r / 12) / 100;
        return (p * R * (pow((1 + R), Double(n))) / ((pow((1 + R), Double(n))) - 1));
    }
    
    // MARK:- Action Method
    
    @IBAction func closeView(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mailAction(_ sender: UIButton) {
        
        if EmiArray.count > 0
        {
            let fileName = "Outlays_Emi_Report.csv"
            let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
            var csvText = "Month,Principal,Interest,Balance\n"
            
            for i in 0..<EmiArray.count {
                
                let newLine = "\(EmiArray[i]["MonthYear"] as! String),\(EmiArray[i]["Principal"] as! String),\(EmiArray[i]["Interest"] as! String),\(EmiArray[i]["Balance"] as! String)\n"
                csvText.append(newLine)
            }
            
            do {
                try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
                
                if MFMailComposeViewController.canSendMail() {
                    let emailController = MFMailComposeViewController()
                    emailController.mailComposeDelegate = self
                    emailController.setToRecipients([])
                    emailController.setSubject("data export")
                    //                    emailController.setMessageBody("Hi,\n\nThe .csv data export is attached\n\n\nSent from the MPG app: http://www.justindoan.com/mpg-fuel-tracker", isHTML: false)
                    let tempData = try NSData(contentsOf: path!) as Data
                    
                    emailController.addAttachmentData(tempData, mimeType: "text/csv", fileName: "\(fileName)")
                    
                    
                    present(emailController, animated: true, completion: nil)
                }
                
            } catch {
                
                print("Failed to create file")
                print("\(error)")
            }
        }
        else {
            alertView(alertMessage: AlertString.DataExport)
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeStatistics(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0
        {
            sheetValue = []
            
            topBar = ["Month", "Principal", "Interest", "Balance"]
            sheetValue.append(topBar)
            
            for i in 0..<EmiArray.count
            {
                var drugObject = [String]()
                for index in 0..<topBar.count
                {
                    if index == 0 {
                        drugObject.append(EmiArray[i]["MonthYear"] as! String)
                    }
                    else if index == 1
                    {
                        let value = EmiArray[i]["Principal"] as! String
                        drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                    }
                    else if index == 2
                    {
                        let value = EmiArray[i]["Interest"] as! String
                        drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                    }
                    else
                    {
                        let value = EmiArray[i]["Balance"] as! String
                        drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                    }
                }
                sheetValue.append(drugObject)
            }
            print(sheetValue)
            
            collectionView.reloadData()
            collectionView.layoutIfNeeded()
            if collectionView.contentSize.height > self.view.frame.size.height - 64
            {
                collectionHeightConstraints.constant = self.view.frame.size.height - 64
            }
            else
            {
                collectionHeightConstraints.constant = collectionView.contentSize.height
            }
        }
        else
        {
            sheetValue = []
            
            topBar = ["Year", "Principal", "Interest", "Balance"]
            sheetValue.append(topBar)
            
            for i in 0..<yearWiseInstallment.count
            {
                var drugObject = [String]()
                for index in 0..<topBar.count
                {
                    if index == 0 {
                        drugObject.append(yearWiseInstallment[i]["Year"] as! String)
                    }
                    else if index == 1
                    {
                        let value = yearWiseInstallment[i]["Principal"] as! String
                        drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                    }
                    else if index == 2
                    {
                        let value = yearWiseInstallment[i]["Interest"] as! String
                        drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                    }
                    else
                    {
                        let value = yearWiseInstallment[i]["Balance"] as! String
                        drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                    }
                }
                sheetValue.append(drugObject)
            }
            print(sheetValue)
            collectionView.reloadData()
            collectionView.layoutIfNeeded()
            if collectionView.contentSize.height > self.view.frame.size.height - 64
            {
                collectionHeightConstraints.constant = self.view.frame.size.height - 64
            }
            else
            {
                collectionHeightConstraints.constant = collectionView.contentSize.height
            }
        }
    }

    //MARK: - Collectionview Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sheetValue.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sheetValue[section].count
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! SLSheetCell
        cell.updateContent(value: sheetValue[indexPath.section][indexPath.row])
        print(sheetValue[indexPath.section][indexPath.row])
        
        
        cell.valueLabel.font = tabBarFont
        if indexPath.row == 0 {
            cell.updateLeftDockState()
        } else if indexPath.section == 0 {
            cell.updateTopDockState()
        } else {
            cell.updateCellState(indexPath: indexPath,currentMonth: false)
        }
        
        if sheetValue[indexPath.section][indexPath.row] == Constant.getMonthYear(Date())
        {
            sectionForMonth = indexPath.section
        }
        
        if sectionForMonth == indexPath.section
        {
            cell.updateCellState(indexPath: indexPath,currentMonth: true)
        }
        
//        cell.updateCellState(indexPath: indexPath,currentMonth: true)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, sizeForItem indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: 60, height: 30)
        }
        else
        {
            return CGSize(width: (collectionView.frame.size.width-60)/3, height: 30)
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        scrollView.keepDockOffset()
    }

}
