//
//  BarChartOverviewViewController.swift
//  Finance
//
//  Created by Cools on 7/6/17.
//  Copyright © 2017 Cools. All rights reserved.
//

import UIKit
import Charts

class BarChartOverviewViewController: UIViewController,ChartViewDelegate, IAxisValueFormatter, SSSheetLayoutDataSource, UICollectionViewDataSource,SambagDatePickerViewControllerDelegate {
    
    //MARK: - Define Outlet
    @IBOutlet var HeaderView : UIView!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var lblHeaderviewName : UILabel!
    @IBOutlet var barChartView : CombinedChartView!
    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var lblHeader : UILabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var viewDateFilter: UIView!
    @IBOutlet var btnLeftMonth: UIButton!
    @IBOutlet var btnRightMonth: UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var btnApply: UIButton!
    
    //MARK: - Define Variable
    private let cellName = "SLSheetCell"
    var listMonth1: NSMutableArray = []
    var filterListMonth: NSMutableArray = []
    var monthWiseData: NSMutableArray = []
    var months: NSMutableArray = []
    var sheetValue = [[String]]()
    let topBar = ["Months", "Income", "Expense", "Avaliable"]
//    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    var dateBtnSelection = ""
    var startDateofMonth = Date()
    var endDateofMonth = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")

        lblHeader.text = "Comparision \(Calendar.current.component(.year, from:Date()))"
        lblHeader.font = headerTitleFont//tableHeaderAmountFont
        startDateofMonth = Date().startOfMonth()
        endDateofMonth = Date().startOfMonth()
        btnLeftMonth.setTitle(Constant.getMonthYear(Date()), for: .normal)
        btnRightMonth.setTitle(Constant.getMonthYear(Date()), for: .normal)
        listMonth1 = Constant.getListOfMonth(Date())
        self.bgView.isHidden = true
        self.viewDateFilter.isHidden = true
        viewDateFilter.layer.cornerRadius = 8
        viewDateFilter.layer.masksToBounds = true
        btnApply.layer.cornerRadius = 5
        btnApply.layer.borderWidth = 2
        btnApply.layer.borderColor = SecondColor.cgColor
        btnApply.setTitleColor(SecondColor, for: .normal)
        print(listMonth1)
        
        setLayoutData(listMonth1)
        collectionView.dataSource = self
        register()
        
        HeaderView.setGradientColor(color1: FirstColor, color2: SecondColor)
        
        let Image = UIImage(named: "ic_NewBack")
        btnBack.setImage(Image, for: .normal)
        
        let Image1 = UIImage(named: "ic_filter_overview")?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(Image1, for: .normal)
        btnFilter.tintColor = UIColor.white
        
//        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: collectionView.frame.origin.y + collectionView.frame.size.height)
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Set orientation Set Portrait
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    func setLayoutData(_ listMonth : NSMutableArray)
    {
        monthWiseData = []
        sheetValue = []
        months = []
        
        for i in 0..<listMonth.count {
            
            let monthWiseDate = (listMonth[i] as! NSMutableDictionary).value(forKey: "Date") as! Date
            
            let sumOfIncome = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", monthWiseDate.startOfMonth(), monthWiseDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"1").value(forKey: "amount") as! NSArray
            
            let sumOfExpense = uiRealm.objects(Transaction.self).filter("date BETWEEN {%@,%@} AND accountId = %@ AND setType = %@ ", monthWiseDate.startOfMonth(), monthWiseDate.endOfMonth(),UserDefaults.standard.integer(forKey: "AccountID"),"0").value(forKey: "amount") as! NSArray
            
            let dict = NSMutableDictionary()
            
            dict.setValue(monthWiseDate, forKey: "previousMonthDate")
            if(sumOfIncome.count>0)
            {
                dict.setValue(Double(String.sum(array: sumOfIncome))!, forKey: "sumOfIncome")
            }
            else
            {
                dict.setValue("0.0", forKey: "sumOfIncome")
            }
            
            if(sumOfExpense.count>0)
            {
                dict.setValue(Double(String.sum(array: sumOfExpense))!, forKey: "sumOfExpense")
            }
            else
            {
                dict.setValue("0.0", forKey: "sumOfExpense")
            }
            dict.setValue(String(Double(Double(String.sum(array: sumOfIncome))! - Double(String.sum(array: sumOfExpense))!)), forKey: "avaliableBal")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM-yyyy"
            dict.setValue((listMonth[i] as! NSMutableDictionary).value(forKey: "Month"), forKey: "PreviousMonth")
            months.add((listMonth[i] as! NSMutableDictionary).value(forKey: "Month") as! String)
            monthWiseData.add(dict)
        }
        print(monthWiseData)
        sheetValue.append(topBar)
        
        for i in 0..<monthWiseData.count
        {
            var drugObject = [String]()
            for index in 0..<topBar.count
            {
                if index == 0 {
                    drugObject.append(months[i] as! String)
                }
                else if index == 1
                {
                    let value = String(describing: (monthWiseData[i] as! NSMutableDictionary).value(forKey: "sumOfIncome")!)
                    drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                }
                else if index == 2
                {
                    let value = String(describing: (monthWiseData[i] as! NSMutableDictionary).value(forKey: "sumOfExpense")!) //(monthWiseData[i] as! NSMutableDictionary).value(forKey: "sumOfExpense") as! String
                    drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                }
                else
                {
                    let value = String(describing: (monthWiseData[i] as! NSMutableDictionary).value(forKey: "avaliableBal")!) //(monthWiseData[i] as! NSMutableDictionary).value(forKey: "avaliableBal") as! String
                    drugObject.append("\(UserDefaults.standard.value(forKey: "currencySymbol") as! String) \(value)")
                }
            }
            sheetValue.append(drugObject)
        }
        
        
        
        barChartView.delegate = self;
        //        barChartView.frame = CGRect(x: 20, y: barChartView.frame.origin.y, width: barChartView.frame.size.width - 40, height: barChartView.frame.size.height)
        //        barChartView.setExtraOffsets(left: 20, top: 10, right: 20, bottom: 0)
        barChartView.chartDescription?.enabled = false
        
        barChartView.drawGridBackgroundEnabled = false
        barChartView.drawBarShadowEnabled = false
        barChartView.highlightFullBarEnabled = false
        
        barChartView.drawOrder = [0,2]
        
        let l = barChartView.legend
        l.wordWrapEnabled = true;
        l.horizontalAlignment = .center
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        
        let rightAxis = barChartView.rightAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.axisMinimum = 0.0
        
        let leftAxis = barChartView.leftAxis
        leftAxis.drawGridLinesEnabled = false
        leftAxis.axisMinimum = 0.0
        
        
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.axisMinimum = 0.0
        xAxis.granularity = 1.0
        xAxis.valueFormatter = self as? IAxisValueFormatter
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = false
        xAxis.setLabelCount(5, force: true)
        xAxis.centerAxisLabelsEnabled = false
        
        setChartData()
    }
    
    //MARK: - Helper Method
    
    
    func showAnimate()
    {
        bgView.isHidden = false
        viewDateFilter.isHidden = false
        bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        bgView.alpha = 0.0;
        viewDateFilter.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        viewDateFilter.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.bgView.alpha = 0.7
            self.bgView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.viewDateFilter.alpha = 1.0
            self.viewDateFilter.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.viewDateFilter.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.viewDateFilter.alpha = 0.0;
            self.bgView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.bgView.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.viewDateFilter.isHidden = true
                self.bgView.isHidden = true
            }
        });
    }
    
    func register() {
        collectionView.register(UINib(nibName: cellName, bundle: nil), forCellWithReuseIdentifier: cellName)
    }
    
    func setChartData()
    {
        let data = CombinedChartData()
        data.lineData = generateLineData()
        data.barData = generateBarData()
        
        barChartView.xAxis.axisMaximum = data.xMax + 0.25
        
        barChartView.data = data
    }
    
    func generateBarData() -> BarChartData {
        var entries1: [BarChartDataEntry] = [BarChartDataEntry]()
        let entries2: [BarChartDataEntry] = [BarChartDataEntry]()
        for index in 0..<monthWiseData.count {
            let index2 = Double(index) + 0.25
            entries1.append(BarChartDataEntry(x: index2, y: Double(String(describing: (monthWiseData[index] as! NSMutableDictionary).value(forKey: "sumOfIncome")!))!))
            //        // stacked
            //        [entries2 addObject:[[BarChartDataEntry alloc] initWithX:0.0 yValues:@[@(arc4random_uniform(13) + 12), @(arc4random_uniform(13) + 12)]]];
        }
        let set1 = BarChartDataSet(values: entries1, label: "Income")
//        set1.color = UIColor(red: CGFloat(60 / 255.0), green: CGFloat(220 / 255.0), blue: CGFloat(78 / 255.0), alpha: CGFloat(1.0))
        set1.setColor(UIColor(red: 60.0/255.0, green: 220.0/255.0, blue: 78.0/255.0, alpha: 1))
        set1.valueTextColor = UIColor(red: CGFloat(60 / 255.0), green: CGFloat(220 / 255.0), blue: CGFloat(78 / 255.0), alpha: CGFloat(1.0))
        set1.valueFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        set1.axisDependency = .left
        let set2 = BarChartDataSet(values: entries2, label: "")
        set2.stackLabels = ["Stack 1", "Stack 2"]
        set2.colors = [UIColor(red: CGFloat(61 / 255.0), green: CGFloat(165 / 255.0), blue: CGFloat(255 / 255.0), alpha: CGFloat(1.0)), UIColor(red: CGFloat(23 / 255.0), green: CGFloat(197 / 255.0), blue: CGFloat(255 / 255.0), alpha: CGFloat(1.0))]
        set2.valueTextColor = UIColor(red: CGFloat(61 / 255.0), green: CGFloat(165 / 255.0), blue: CGFloat(255 / 255.0), alpha: CGFloat(1.0))
        set2.valueFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        set2.axisDependency = .left
        var groupSpace: Float = 0.00
        var barSpace: Float = 0.00
        // x2 dataset
        var barWidth: Float = 0.5
        // x2 dataset
        var d = BarChartData(dataSet: set1)
        d.barWidth = Double(barWidth)
        // make this BarData object grouped
        
        d.groupBars(fromX: 0.0, groupSpace: Double(groupSpace), barSpace: Double(barSpace))
        // start at x = 0
        return d
    }
    
    func generateLineData() -> LineChartData {
        let d = LineChartData()
        var entries = [ChartDataEntry]()
        for index in 0..<monthWiseData.count {
            let index2 = Double(index) + 0.3
            
            entries.append(ChartDataEntry(x: index2, y: Double(String(describing: (monthWiseData[index] as! NSMutableDictionary).value(forKey: "sumOfExpense")!))!))
        }
        let set = LineChartDataSet(values: entries , label: "Expense")
//        set.setColor(UIColor(red: CGFloat(240 / 255.0), green: CGFloat(238 / 255.0), blue: CGFloat(70 / 255.0), alpha: CGFloat(1.0)))
        set.setColor(UIColor.red)
//        set.colors = [UIColor.red]
        set.lineWidth = 2.5
//        set.setCircleColor(UIColor(red: CGFloat(240 / 255.0), green: CGFloat(238 / 255.0), blue: CGFloat(70 / 255.0), alpha: CGFloat(1.0)))
        set.setCircleColor(UIColor.red)
//        set.circleColors = [UIColor.red]
        set.circleRadius = 5.0
        set.circleHoleRadius = 2.5
        
//        set.fillColor = UIColor(red: CGFloat(240 / 255.0), green: CGFloat(238 / 255.0), blue: CGFloat(70 / 255.0), alpha: CGFloat(1.0))
        set.fillColor = UIColor.red
        set.mode = .cubicBezier
        set.drawValuesEnabled = true
        set.valueFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        set.valueTextColor = UIColor(red: CGFloat(240 / 255.0), green: CGFloat(238 / 255.0), blue: CGFloat(70 / 255.0), alpha: CGFloat(1.0))
        set.valueTextColor = UIColor.red
        set.axisDependency = .left
        d.addDataSet(set)
        return d
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        
        if touch.view != viewDateFilter {
            removeAnimate()
        }
        
    }
    
    //MARK: - IAxisValueFormatter
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value) % months.count] as! String
    }
    
    //MARK: - Action Method
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        showAnimate()
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyDateSelection(_ sender: UIButton) {
        
        if startDateofMonth < endDateofMonth
        {
            removeAnimate()
            filterListMonth = Constant.getMonthListBetween2Date(startDateofMonth, endDate2: endDateofMonth)
            setLayoutData(filterListMonth)
            collectionView.dataSource = self
            register()
            
            collectionView.reloadData()

        }
        else
        {
            self.alertView(alertMessage: AlertString.ProperMont)
        }
    }
    
    
    @IBAction func btnLeftDateSelection(_ sender: UIButton) {
        dateBtnSelection = "left"
        let vc = SambagDatePickerViewController()
        vc.delegate = self
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnRightDateSelection(_ sender: UIButton) {
        dateBtnSelection = "right"
        let vc = SambagDatePickerViewController()
        vc.delegate = self
        vc.theme = .light
        present(vc, animated: true, completion: nil)
    }
    
    //MARK: - SambagDate Picker Method
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        
        let resultDate = Constant.convertDate(String(describing: result))
        if dateBtnSelection == "left"
        {
            startDateofMonth = resultDate.startOfMonth()
            btnLeftMonth.setTitle(Constant.getMonthYear(resultDate), for: .normal)
        }
        else if dateBtnSelection == "right"
        {
            endDateofMonth = resultDate.startOfMonth()
            btnRightMonth.setTitle(Constant.getMonthYear(resultDate), for: .normal)
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }

    
    //MARK: - Collectionview Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sheetValue.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sheetValue[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! SLSheetCell
        cell.updateContent(value: sheetValue[indexPath.section][indexPath.row])
        print(sheetValue[indexPath.section][indexPath.row])
        
        cell.valueLabel.font = tabBarFont
        if indexPath.row == 0 {
            cell.updateLeftDockState()
        } else if indexPath.section == 0 {
            cell.updateTopDockState()
        } else {
            cell.updateCellState(indexPath: indexPath,currentMonth: false)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, sizeForItem indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: 60, height: 30)
        }
        else
        {
            return CGSize(width: (collectionView.frame.size.width-60)/3, height: 30)
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.keepDockOffset()
    }

    

}
